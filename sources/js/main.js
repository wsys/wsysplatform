
export class Main {

    constructor() {
        this.observers = [];
    }

    main() {
        ///
        console.debug('run main');
        this.init();
    }

    init() {
        console.debug('setting history triggering start');
        // window.onhashchange = function(event) {
        //     console.debug('trigger onhashchange');
        //     // if (window.confirm("The page content will be lost. Do you really want to leave?")) {
        //     //     window.location.reload(true);
        //     // }
        // };

        // window.addEventListener('hashchange', function() {
        //     console.debug('The hash has changed!');
        // }, false);

        window.onpopstate = function(event) {
            console.debug('trigger onpopstate');
            // if (window.confirm("The page content will be lost. Do you really want to leave?")) {
            //     window.location.reload(true);
            // }
        };

        // window.addEventListener('popstate', function() {
        //     console.debug('trigger onpopstate');
        //     if (window.confirm("The page content will be lost. Do you really want to leave?")) {
        //         window.location.reload(true);
        //     }
        // }, false);

        console.debug('setting history triggering finish');

        // $App.navigate = new Navigate();
    }

    notify(subj) {
        let ev = this.accept(subj);
        console.debug('Main.notify: dispatch observers', ev);
        _.forEach(this.observers, (elem, key) => elem.notify(ev));
    }

    attach(subj) {
        this.observers.push(subj);
    }

    accept(subj) {
        let out = {type: null};
        if (_.isObject(subj)) {
            out = subj;
        } else if (_.isString(subj)) {
            out.type = subj;
        } else {
            throw new Error('unknown subj');
        }
        return out;
    }
}

// let m = new Main();
// m.init();
