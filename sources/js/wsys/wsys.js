import { Form } from './form';
import { PDFOut } from './pdfout';

let pool = {};

class Facade {
    static getForm(args) {
        // return new Form({selector: args[0]});
        return Facade.getFormCached(args[0]);
    }

    static getFormCached(id) {
        if (id in pool) {
            return pool[id];
        } else {
            return (pool[id] = Facade.createForm(id));
        }
    }

    static createForm(id) {
        return new Form({selector: id});
    }
}

let WsysMixin = {
    stub() {
        console.debug('WsysMixin called');
    }
};

function WsysFun() {
    return Facade.getForm(arguments);
}

class Backend {
    stub() {
        console.debug('Backend called');
        return 123;
    }

    importModule(...args) {
        if (args.length == 1) {
            const name = args[0];
            const modules = [
                {name: 'PDFOut'},
                {name: ':stub'}
            ];
            const found = _.find(modules, (m) => m.name === name);
            if (found) {
                if (found.name === 'PDFOut') {
                    return PDFOut;
                } else if (found.name === ':stub') {
                    ///
                }
            } else {
                throw new Error('unable to locate module ' + name);
            }
        } else {
            throw new Error('Invalid arguments');
        }
    }
}

var backendInstance = new Backend();

var handler = {
    // get: function(target, name) {
    //     // return name in target?
    //     //     target[name] :
    //     //     37;
    //     if (name in target) {
    //         ///
    //     }
    // }
    apply: function(target, thisValue, args) {
        // console.debug(target, thisValue, args, WsysMixin);
        if (args.length > 0) {
            return target(...args);
        } else {
            return backendInstance;
        }
    }
};

let Wsys = new Proxy(WsysFun, handler);

export {Wsys};
