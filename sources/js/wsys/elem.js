import $ from 'jquery';

let chartNvd3Mixin = {
    chart() {
        if (!this.chartd) {
            this.chartd = chartd({engine: 'nvd3'});
            this.chartd.invoker = this;
        }
        return this.chartd;
    }
};

export class Elem {

    constructor(p) {
        this.params = p;
        this.style = {};

        const which = this.which();
        // console.debug('Elem.which', which, p);
        if (which.which === 'chart.nvd3') {
            this.assignNvd3();
        }
    }

    which() {
        // console.debug(
        //     'which',
        //     this.elem(),
        //     typeof this.elem(),
        //     typeof Elem,
        //     elem === Elem,
        //     this.$(),
        //     this.$()[0],
        //     this.getSelector()
        // );
        let elem = this.$('svg');
        if (elem.length > 0 && elem[0].classList.contains('nvd3-svg')) {
        // if (elem.length > 0) {
            return {which: 'chart.nvd3', node: elem[0]};
        }
        return {which: null};
    }

    elem() {
        return this.$().get(0);
    }

    $() {
        const elem = $(this.getSelector());
        if (arguments.length == 0) {
            return elem;
        } else {
            return elem.find(arguments[0]);
        }
    }

    assignNvd3() {
        /// @todo
        // Object.assign(Elem.prototype, chartNvd3Mixin);
        Object.assign(this, chartNvd3Mixin);
        // window['chart.nvd3.ready'] = true;
    }

    forward() {
        const q = this.query();
        // console.debug('forward q', q);
        // console.debug('forward', arguments, q);
        // console.debug(
        //     'forward.ev',
        //     JSON.stringify(arguments[1])
        // );
        this.exchange(arguments[1]);
        q.trigger('forward', {exchange: true});
    }

    exchange(ev) {
        this.params.form.exchange(JSON.stringify(ev));
    }

    query() {
        return $(this.getSelector());
    }

    getSelector() {
        const s = this.params.form.getSelector() + ' ' + '.wfqual-' + this.params.id;
        // console.debug('elem.getselector', s);
        return s;
    }

    emptyNode() {
        var s;
        if (arguments.length == 0) {
            s = '> *';
        } else {
            s = arguments[0];
        }
        $(this.getSelector() + ' ' + s).empty();
    }

    reg() {
        /// it's cool
        const v = $App.Pooled.v;

        const k = this.getSelector() + '@' + arguments[0];
        if (arguments.length == 1) {
            // console.debug('reg read', k);
            return v[k];
        } else if (arguments.length == 2) {
            v[k] = arguments[1];
            // console.debug('reg put', k, arguments[1]);
            return this;
        } else {
            throw new Error('Invalid arguments count');
        }
    }

    getForm() {
        return this.params.form;
    }

    width() {
        return this.$().width();
    }

    height() {
        return this.$().height();
    }

    getWidth() {
        return this.width();
    }

    getHeight() {
        return this.height();
    }

    units(v) {
        return v * 0.2645833333;
    }

    dim(unit = 'px', k = 1) {
        let ascale;
        if (_.isObject(k)) {
            ascale = k;
        } else {
            ascale = {xscale: k, yscale: k};
        }
        let scale = ascale;
        const d = {
            width: this.width() * scale.xscale,
            height: this.height() * scale.yscale,
        };
        let r;
        if (unit == 'px') {
            r = {
                width: d.width,
                height: d.height,
            };
        } else if (unit == 'mm') {
            r = {
                width: this.units(d.width),
                height: this.units(d.height),
            };
        } else {
            throw new Error('Invalid arguments');
        }
        return r;
    }

    html() {
        return this.$().html();
    }

    text() {
        return this.$().text();
    }

    pushFont(spec) {
        this.style.fontFamily = this.elem().style.fontFamily;
        this.style.fontFamily = spec;
    }

    popFont() {
        this.elem().style.fontFamily = this.style.fontFamily;
    }
}
