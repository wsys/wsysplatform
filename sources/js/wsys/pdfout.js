
class Screenshot {

    constructor(p) {
        this.params = p;
    }

    async run() {
        let shoot = await this.shoot();
        // console.debug('screenshot run', shoot);
        return shoot;
    }

    async shoot() {
        // console.debug('shoot run stub start', p);
        let p = this.params.context;
        let owner = this.params.owner;
        let ret = false;
        try {
            // console.debug('shoot run stub');
            // console.debug(
            //     'shoot debug:',
            //     {
            //         node: owner.node(p.wid)
            //     }
            // );

            // let img = await domtoimage.toPng(this.node(p.wid))
            let that = this;
            let dim = owner.q(p.wid).dim('mm', {xscale: 0.9, yscale: 0.85});
            ret = await domtoimage.toJpeg(
                owner.node(p.wid),
                {
                    quality: 1,
                    bgcolor: 'white'
                })
                .then(function (dataUrl) {
                    var img = new Image();
                    img.src = dataUrl;
                    return {
                        img: img,
                        dataUrl: dataUrl,
                        dim: dim
                    };
                })
                .catch(function (error) {
                    console.error('oops, something went wrong!', error);
                });
        } catch (e) {
            console.error('shoot exception:', e);
        } finally {
            console.debug('shoot finally');
        }
        // console.debug('shoot run stub finish', ret);
        return ret;
    }
}

export class PDFOut {
    constructor (p) {
        this.params = p;
        console.debug('construct PDFOut', p);
    }

    select(p = {}) {
        ///
        const s = this.makeSelector(p);
    }

    // makeSelector(params) {
    //     _.defaults(params, {exclude: []});
    //     /// form selector but exclude some
    //     const exclude = _(params.exclude).map((elem) => `:not(.wfqual-${elem})`);
    //     return `{this.getFormSelector()}${exclude}`;
    // }

    getFormSelector() {
        return this.params.form;
    }

    form() {
        return Wsys(this.getFormSelector());
    }

    q(s) {
        return this.form().q(s);
    }

    elem(s) {
        return this.q(s).elem();
    }

    node(s) {
        return this.elem(s);
    }

    svg(s) {
        // return this.q(s).which().node;
        return this.q(s).chart().getSVGElement();
    }

    selectors(...s) {
        const that = this;
        function conv(t) {
            if (typeof t === 'object') {
                if (_.has(t, 'wid')) {
                    return '.wfqual-' + t.wid;
                } else {
                    throw new Error('Conv error with object');
                }
            } else if (typeof t == 'string') {
                return that.q(t).getSelector();
            } else {
                throw new Error('Conv error');
            }
        }
        // function normalize(t) {
        //     if (_.isArray(t)) {
        //         return t;
        //     } else if (typeof t == 'string') {
        //         return [t];
        //     } else {
        //         return conv(t);
        //     }
        // }
        function forceArray(t) {
            if (_.isArray(t)) {
                return t;
            } else {
                return [t];
            }
        }
        if (s.length == 1) {
            let a = forceArray(s[0]);
            // return _(a).map((t) => this.q(t).getSelector()).value();
            // return a;
            return _(a).map((t) => conv(t)).value();
        } else {
            throw new Error('Invalid arguments count');
        }
    }

    getContent() {
        return $(this.getFormSelector()).html();
    }

    renderProc(fn, opts = {}) {

        console.debug(
            'renderProc 0',
            html2canvas,
            jsPDF
        );

        _.defaults(opts, {exclude: []});

        const that = this;
        function selector(id) {
            return that.form().q(id).getSelector();
        }

        function every(ls, fn) {
            _(ls).each((elem) => $(selector(elem))[fn]());
        }

        try {

            console.debug('renderProc 2');

            // tie up a resource
            every(opts.exclude, 'hide');

            console.debug('renderProc 3');
            fn();

            console.debug('renderProc 4');

        } catch (e) {
            console.error('renderProc exception:', e);
            console.trace('renderProc trace');
        } finally {
            console.debug('renderProc 5');
            every(opts.exclude, 'show');
        }
    }

    units(v) {
        return v * 0.2645833333;
    }

    async addElement(p) {
        console.debug('addElement run stub start', p);
        try {
            console.debug('addElement run stub');
            // this.q(p.wid).pushFont('FreeSans');
            // await html2canvas(
            //     this.elem(p.wid))
            //     .then(
            //         canvas => {
            //             /// document.body.appendChild(canvas);
            //             console.debug();
            //         }
            //     );
            console.debug(
                'addElement debug:',
                {
                    node: this.node(p.wid)
                }
            );

            // let img = await domtoimage.toPng(this.node(p.wid))
            let that = this;
            let dim = this.q(p.wid).dim('mm', {xscale: 0.9, yscale: 0.85});
            await domtoimage.toJpeg(
            // await domtoimage.toPng(
                this.node(p.wid),
                {
                    quality: 1,
                    bgcolor: 'white'
                })
                .then(function (dataUrl) {
                    var img = new Image();
                    img.src = dataUrl;
                    // document.body.appendChild(img);
                    // console.debug('addElement img', img, dataUrl);
                    p.doc.addPage();
                    // p.doc.addImage(dataUrl, 'PNG', 20, 20);
                    // p.doc.addImage(img, 'PNG', 20, 20, that.units(1050), that.units(320));
                    // p.doc.addImage(img, 'JPEG', 20, 20, that.units(900), that.units(180));
                    // p.doc.addImage(img, 'JPEG', 20, 20);
                    // p.doc.addImage(img, 'PNG', 20, 20, null, null, null, 'SLOW');
                    // p.doc.addImage(img, 'JPEG', 20, 20, null, null, null, 'SLOW');
                    // p.doc.addImage(dataUrl, 'JPEG', 20, 20, null, null, null, 'SLOW');
                    p.doc.addImage(dataUrl, 'JPEG', 20, 20, dim.width, dim.height, null, 'NONE');
                    p.doc.setLanguage("ru");
                    p.doc.save();
                    // return img;
                })
                .catch(function (error) {
                    console.error('oops, something went wrong!', error);
                });

            // console.debug('addElement ret', img);

        } catch (e) {
            console.error('addElement exception:', e);
            console.trace('addElement trace');
        } finally {
            console.debug('addElement finally');
            // this.q(p.wid).popFont();
        }
        console.debug('addElement run stub finish');
    }

    screenshot(p) {
        let s = new Screenshot({owner: this, context: p});
        let r = s.run();
        // console.debug('screenshot return result:', r);
        return r;
    }

    async spoolScreenshot(s, doc, x = 20, y = 20, width = undefined, height = undefined, commit = true) {
        let r;
        // console.debug('addScreenshot', r);
        // doc.addImage(r.dataUrl, 'JPEG', 20, 20, dim.width, dim.height, null, 'NONE');
        // doc.addPage();
        let b, args;
        if (arguments.length == 1) {
            r = await s.screenshot;
            // let p = ;
            b = _.defaults(
                {
                    screenshot: r,
                    x: s.x,
                    y: s.y,
                    // y: 300,
                    width: s.width,
                    height: s.height,
                    commit: s.commit,
                    doc: s.doc,
                    saveFilename: s.saveFilename,
                },
                {
                    x: 20,
                    y: 20,
                    width: r.dim.width,
                    height: r.dim.height,
                    type: 'JPEG',
                    commit: undefined
                }
            );
        } else {
            r = await s;
            b = {
                screenshot: r,
                x: x,
                y: y,
                width: width,
                height: height,
                commit: commit,
                doc: doc,
                saveFilename: undefined,
            };
        }
        args = [
            b.screenshot.dataUrl,
            b.type,
            b.x,
            b.y,
            b.width,
            b.height,
            null,
            'NONE'
        ];
        // doc.addImage(r.dataUrl, 'JPEG', x, y);
        b.doc.addImage.apply(b.doc, args);
        console.debug('spoolScreenshot use params:', args);
        if (b.commit) {
            // b.doc.save();
            b.doc.save.apply(b.doc, [b.saveFilename]);
        }
    }
}
