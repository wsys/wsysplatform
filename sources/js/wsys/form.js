import { Elem } from './elem';
import { ModuleFactory } from './module/factory';

export class Form {
    constructor(p) {
        this.params = p;
        this.v = {};
        this.wpool = {};
    }

    q(id) {
        return this.getElem(id, false);
    }

    getElem(id, cached = true) {
        if (cached) {
            return this.getElemCached(id);
        } else {
            return this.createElem(id);
        }
    }

    getElemCached(id) {
        if (id in this.wpool) {
            return this.wpool[id];
        } else {
            return (this.wpool[id] = this.createElem(id));
        }
    }

    createElem(id) {
        /// @todo factory product
        /// @todo cache elem
        return new Elem({id: id, form: this});
    }

    getSelector() {
        return this.params.selector;
    }

    exchange(value) {
        const s = this.getSelector() + ' input[data-role=\'$exchange\']';
        console.debug('exchange: ', s, $(s), value);
        $(s).val(value);
    }

    module(...args) {
        return ModuleFactory.accept(this, args);
    }
}
