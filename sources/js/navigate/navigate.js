import i18next from 'i18next';
import i18nextHttpBackend from 'i18next-http-backend';
import i18nextBrowserLanguageDetector from 'i18next-browser-languagedetector';

// i18next.init(
//     {
//         lng: 'en',
//         debug: true,
//         resources: {
//             en: {
//                 translation: {
//                     "key": "hello world"
//                 }
//             }
//         }
//     },
//     function(err, t) {
//         // initialized and ready to go!
//         // document.getElementById('output').innerHTML = i18next.t('key');
//         console.debug('i18next', i18next.t('key'));
//     }
// );

// i18next.init({
//     lng: 'en',
//     debug: true,
//     resources: {
//         en: {
//             translation: {
//                 "key": "hello world"
//             }
//         },
//         de: {
//             translation: {
//                 "key": "hello welt"
//             }
//         }
//     }
// }).then(function(t) {
//     // initialized and ready to go!
//     // document.getElementById('output').innerHTML = i18next.t('key');
//     console.debug('i18next', i18next.t('key'));

// });

// i18next.changeLanguage('de');

///i18next
///    .use(i18nextHttpBackend)
///    .use(i18nextBrowserLanguageDetector)
///    .init({
///        fallbackLng: 'en',
///        debug: true,
///        ns: ['special', 'common'],
///        defaultNS: 'special',
///        backend: {
///            // load from i18next-gitbook repo
///            // loadPath: 'https://raw.githubusercontent.com/i18next/i18next-gitbook/master/locales/{{lng}}/{{ns}}.json',
///            loadPath: '/locales/{{lng}}/{{ns}}.json',
///            crossDomain: true
///        }
///    }, function(err, t) {
///        // init set content
///        // updateContent();
///        console.debug(
///            'i18next advanced example:',
///            i18next.t('title', { what: 'i18next' }),
///            i18next.t('common:button.save', { count: Math.floor(Math.random()*2+1)  }),
///            `detected user language: "${i18next.language}"  --> loaded languages: "${i18next.languages.join(', ')}"`
///        );
///    });

let debug = false;

i18next
    .use(i18nextHttpBackend)
    .use(i18nextBrowserLanguageDetector)
    .init({
        detection: {
            order: ['navigator', 'querystring', 'cookie', 'localStorage', 'sessionStorage', 'htmlTag']
        },
        fallbackLng: 'en',
        debug: debug,
        ns: ['special', 'common', 'navigate'],
        defaultNS: 'special',
        backend: {
            // load from i18next-gitbook repo
            // loadPath: 'https://raw.githubusercontent.com/i18next/i18next-gitbook/master/locales/{{lng}}/{{ns}}.json',
            loadPath: '/locales/{{lng}}/{{ns}}.json',
            crossDomain: true
        }
    }).then(function(t) {
        // init set content
        // updateContent();
        // console.debug(
        //     'i18next advanced example:',
        //     i18next.t('navigate:confirm.msg'),
        //     i18next.t('title', { what: 'i18next' }),
        //     i18next.t('common:button.save', { count: Math.floor(Math.random()*2+1)  }),
        //     `detected user language: "${i18next.language}"  --> loaded languages: "${i18next.languages.join(', ')}"`
        // );
        console.debug(
            'i18next init info:',
            `detected user language: "${i18next.language}"  --> loaded languages: "${i18next.languages.join(', ')}"`
        );
    });


export class Navigate {
    constructor(main) {
        console.debug('init  navigate');
        this.state = {
            level: -1
        };
        main.attach(this);

        // window.onpopstate = function(ev) {
        //     console.debug('trigger onpopstate', ev);
        //     // if (window.confirm("The page content will be lost. Do you really want to leave?")) {
        //     //     window.location.reload(true);
        //     // }
        // };
        // window.onhashchange = (ev) => console.debug('hashchange', ev);
    }

    notify(ev) {
        console.debug('Navigate.notify', ev);
        switch (ev.type) {
        case 'page.confirmable':
        case 'page.reload':
        case 'page.request':
            this.setConfirm(ev);
            this.entry(ev);
            break;
        default:
            console.warn('Navigate.notify unhandled event', ev);
        }
    }

    entry(ev) {
        console.debug('Navigate.entry', ev);
        this.state.level++;
        let p = _.defaults(ev, {title: '', url: '/'});
        window.history.pushState({ev: ev}, p.title, p.url);
        // window.history.replaceState({ev: ev}, p.title, p.url);
    }

    setConfirm(ev) {
        let that = this;
        window.onpopstate = function(event) {
            console.debug('trigger onpopstate', ev);
            let p = {url: '/'};
            let msg = i18next.t('navigate:confirm.msg');
            // if (window.confirm("The page content will be lost. Do you really want to leave?")) {
            if (window.confirm(msg)) {
                // window.location.reload(true);
                // $dc.util.location.cover.reload();
                // p.url = location.pathname;
                // if (location.pathname == '/')
                that.home();
            } else {
                p.url = location.pathname;
            }
            that.notify({type: 'page.confirmable', url: p.url});
        };
        console.debug('trigger onpopstate installed');
    }

    home() {
        $dc.util.location.cover.replace('/');
    }
}
