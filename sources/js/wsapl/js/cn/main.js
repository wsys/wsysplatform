/*globals $, alert, $dc, $con, jQuery */
/*jslint browser: true, white: true */
"use strict";

(function () {
   var wincomns = window.comns,
       lib,
       fnmeth,
       libFilter;
    fnmeth = function (p) {
        this.$params = p;
    };
    fnmeth.prototype = {
        $params: {},
        notifySelect: function(ev) {
            var p = {'selector': false}, sel = false;
            if (ev.info === 'single') {
                //$con.debug(ev);
                sel = '.wsapl--smask-single, .wsapl--smask-all';
            } else if (ev.info === 'multi') {
                //$con.debug(ev);
                sel = '.wsapl--smask-all';
            } else if (ev.info === 'none') {
                //$con.debug(ev);
                sel = '.wsapl--smask-none, .wsapl--smask-all';
            } else if (ev.info === 'ping') {
                $con.warn(ev);
            } else {
                //ignore
                return;
            }
            p.selector = sel;
            this.action(p);
            // $con.debug({sel: sel, evinfo: ev.info, gsel: this.$params.gsel});
        },
        action: function(p) {
            //var that = this;
            //$(this.$params.gsel).each(
            //    function (index) {
            //        $(this).addClass('disabled');
            //    }
            //);
            $(this.$params.gsel).addClass('disabled').filter(p.selector).removeClass('disabled');
        }
    };
    lib = {
        sysLib: false,
        sys: false,
        defineMeth: function (p) {
            var o = new wincomns.wsapl.cn.meth(p);
            this.sys.$reg.set(p.id, o);
            //$con.warn('defineMeth is done', p.id, o);
            //$(p.gsel).one(
            //    'remove',
            //    function () {
            //        $con.debug('remove');
            //    }
            //);
            return o;
        },
        selectMeth: function (id) {
            return this.sys.$reg.get(id);
        },
        undefineMeth: function (id) {
            this.sys.$reg.remove(id);
        }
    };
    //
    libFilter = {
        loadTrigger: function (p, name) {
            var trigger = this[name];
            jQuery.each(
                p,
                function (index, value) {
                    $(window.document).on(
                        'change', value.targets[0], value, trigger
                    );
                }
            );
        },
        loadRange: function (p) {
            this.loadTrigger(p, 'triggerRange');
        },
        loadSimple: function (p) {
            this.loadTrigger(p, 'triggerSimple');
        },
        triggerRange: function (event) {
            var flag = true, evdata = event.data, evtargets = evdata.targets,
                v = this.options[event.target.selectedIndex].value;
            switch (v) {
            case 'greater_than':
            case 'greater_or_equal_than':
                flag = false;
                $(evtargets[1]).val((v === 'greater_than') ? 'less_than' : 'less_or_equal_than');
                break;
            default:
                //noop
                $(evtargets[1]).val('none');
            }
            $(evtargets[1]).prop('disabled', flag);
            $(evtargets[2]).prop('disabled', flag);
        },
        triggerSimple: function (event) {
            var flag = false, evdata = event.data, evtargets = evdata.targets,
                v = this.options[event.target.selectedIndex].value;
            switch (v) {
            case 'is_null':
            case 'is_not_null':
                flag = true;
                break;
            default:
                //noop
                //$(evtargets[1]).val('none');
            }
            $(evtargets[1]).prop('disabled', flag);
        }
    };
    //
    wincomns.wsapl = {};
    wincomns.wsapl.cn = {};
    wincomns.wsapl.cn.meth = fnmeth;
    wincomns.wsapl.cn.lib = lib;
    wincomns.wsapl.cn.lib.sysLib = $dc.lib;
    wincomns.wsapl.cn.lib.sys = $dc;
    wincomns.wsapl.cn.filter = {};
    wincomns.wsapl.cn.filter.lib = libFilter;
    $con.info('cn/main.js loaded');
}());
