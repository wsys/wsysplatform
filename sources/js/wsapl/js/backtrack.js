/*globals $, alert, $dc, $con */
/*jslint browser: true, white: true */
"use strict";

(function () {
   var wincomns = window.comns,
       lib,
       fnpkg;
    fnpkg = function (p) {
        this.$params = p;
    };
    fnpkg.prototype = {
        $params: {}
    };
    lib = {
        sysLib: false,
        sys: false
    };
    //
    wincomns.wsapl.backtrack = {};
    wincomns.wsapl.backtrack.lib = lib;
    wincomns.wsapl.backtrack.lib.sysLib = $dc.lib;
    wincomns.wsapl.backtrack.lib.sys = $dc;
    //$con.info('backtrack.js loaded');
}());
