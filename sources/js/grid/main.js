/*globals $, alert, $dc, $con */
/*jslint browser: true, white: true */

(function () {
    "use strict";

    var utils = {
        defMask: function (cnt) {
            var ret = false;
            if (cnt === 1) {
                ret = 'single';
            } else if (cnt > 1) {
                ret = 'multi';
            } else if (cnt === 0) {
                ret = 'none';
            }
            // $con.debug('defMask:cnt', cnt);
            return ret;
        },
        openTriggerContextHead: function (event, m) {
            var p = m.acceptEvent(event),
                c = m.openContext(p);
            return {
                context: c,
                radio: {
                    state: m.openRadioState(p)
                },
                index: -1,
                key: 'row'
            };
        },
        locateRowIndex: function (p) {
            var index = -1, key = false, tr;
            if (p.elem) {
                tr = p.module.fetchTR(p.elem);
                index = tr[0].rowIndex;
                key = 'row' + index;
                // $con.debug(key, tr, index);
            }
            return {index: index, key: key, tr: tr};
        },
        openTriggerContext: function (event, m, elem) {
            var p = m.acceptEvent(event),
                c = m.openContext(p),
                row = this.locateRowIndex({module: m, elem: elem});
            return {
                context: c,
                check: {
                    state: m.openCheckState(p)
                },
                radio: {
                    state: m.openRadioState(p)
                },
                index: row.index,
                key: row.key
            };
        }
    },
        meth = {
            trigger: function (p) {
                var mc = p.pkg.selectMeth(p.context.meth.gid);
                if (mc) {
                    mc.notifySelect({'info': utils.defMask(p.cnt)});
                }
            },
            destroy: function(p) {
                window.comns.wsapl.cn.lib.undefineMeth(p.context.meth.gid);
            }
        },
        referenced = {
            isActive: function (context) {
                return (context.info.role === 'grid.reference');
            },
            setEnabled: function (context, enabled) {
                var meth_name = enabled ? 'removeClass' : 'addClass';
                $(context.refmode.cpanel_selector + ' .cmd-choice')[meth_name]('disabled');
                $(context.refmode.views.btn_choice.sel)[meth_name]('disabled');
                // $con.debug(context.refmode);
            },
            bind: function (context, selection, nrows) {
                // $con.debug('bind', context, selection, nrows);
                // $con.debug('bind', context, nrows);
                if (nrows > 0 && selection.length > 0) {
                    this.selectRow(selection, context.selection.pos);
                    //this.setEnabled(context, true);
                    this.trigger(context, 'on');
                } else {
                    this.unselectRow(context);
                    this.trigger(context, 'off');
                    //this.setEnabled(context, false);
                }
            },
            trigger: function (context, v) {
                var elem = $(context.refmode.selector.btn.choice),
                    choice = $(context.refmode.views.btn_choice.sel);
                if (v === 'on') {
                    elem.removeClass('disabled');
                    choice.removeClass('disabled');
                } else {
                    elem.addClass('disabled');
                    choice.addClass('disabled');
                }
            },
            selectRow: function (selection, aindex) {
                var index = (aindex >= 0) ? aindex : 0;
                // $con.debug('selectRow', selection);
                selection[index].click();
            },
            unselectRow: function (context) {
                $(context.radio.hd.selector).click();
            }
        },
        pkg =
        {
            sysLib: false,
            sys: false,
            wsapl: false,
            savedState: false,
            stub: function (p) {
                $con.debug(p);
            },
            fetchTR: function (elem) {
                return $(elem).closest('tr');
            },
            fetchRowIndex: function (elem) {
                return this.fetchTR(elem)[0].rowIndex;
            },
            fetchColIndex: function (elem) {
                return $(elem).closest('td')[0].cellIndex;
            },
            openCheckState: function (p) {
                var c = this.openContext(p);
                return this.openDict(c.check_state.reg_id);
            },
            openRadioState: function (p) {
                var c = this.openContext(p);
                return this.openDict(c.radio.state.reg_id);
            },
            checkTriggerChange: function (event) {
                /// @note triggered when rows checkbox changed
                var info = utils.openTriggerContext(event, pkg, this),
                    state = info.check.state,
                    infoctx = info.context,
                    hd_sel = infoctx.check_hd.selector,
                    hd_elem = $(hd_sel),
                    hdon = hd_elem.prop('checked'),
                    s = $(this), on = false, all = false, rows = $(infoctx.check_rows.selector),
                    someChecked = null, hdChecked = hdon,
                    selectOff = rows.not(':checked').length,
                    rowsAll = null,
                    selectOn = rows.length - selectOff;

                // $con.debug(
                //     'checkTriggerChange:debug',
                //     info.context.element.target,
                //     rows.length,
                //     $(infoctx.check_rows),
                //     infoctx.check_rows
                // );

                if (s.prop('checked')) {
                    on = true;
                    someChecked = true;
                    state.set(info.key, info.index);
                    /// if hd not on - set on it
                    if (!hdon) {
                        if (selectOff === 0) {
                            hd_elem.prop('checked', true);
                            hdChecked = true;
                            all = true;
                        }
                    }
                } else {
                    state.remove(info.key);
                    someChecked = selectOn > 0;
                    /// if hd is on - set off it
                    if (hdon) {
                        hd_elem.prop('checked', false);
                        hdChecked = false;
                        all = (selectOff === rows.length);
                    }
                }
                state.set('row', info.index);
                // $con.debug('checkTriggerChange', info, state, hd_sel, hd_elem, hdon, s, on);
                // state.set('checked', on);
                pkg.toggleChecked({tr: s.closest('tr'), on: on});
                // pkg.updateCheckState(infoctx, state);
                pkg.updateCheckState0(
                    {
                        checked: on,
                        hdon: hdChecked,
                        selectOn: selectOn,
                        selectOff: selectOff,
                        rowsCount: rows.length,
                        c: infoctx,
                        state: state,
                        rowsAll: all,
                        from: 'checkTriggerChange'
                    }
                );

                if ($(this).data('event') != 'row:click') {
                    pkg.trigger('row:select', info.context.element.target);
                }
            },
            toggleChecked: function (p) {
                p.tr.toggleClass('nwfc-g-tr-checked', p.on);
            },
            radioTriggerChange: function (event) {
                var info = utils.openTriggerContext(event, pkg, this),
                    state = info.radio.state, c = info.context;
                state.set('row', info.index);
                $(c.radio.state.selector).val(state.toJSON());
                if (c.meth.gid) { /**< when gid valued */
                    pkg.triggerMeth(c, false);
                    if (referenced.isActive(c)) {
                        referenced.setEnabled(c, true);
                    }
                }
                if ($(this).data('event') != 'row:click') {
                    pkg.trigger('row:select', c.element.target);
                }
            },
            triggerMeth: function (c, unselected) {
                var checked = pkg.countCheckedFromRadioTrigger(c),
                    nselected;
                if (unselected) {
                    nselected = checked;
                } else {
                    if (checked > 1) {
                        nselected = checked;
                    } else if (checked === 0) {
                        nselected = 0;
                    } else if (checked === 1) {
                        nselected = 1;
                    } else {
                        nselected = 0;
                    }
                }
                $con.debug(
                    'triggerMeth',
                    {
                        unselected: unselected,
                        checked: checked,
                        nselected: nselected
                    }
                );
                // $con.debug('context:', c, 'count checked:', checked, unselected, nselected);
                meth.trigger(
                    {
                        context: c,
                        pkg: pkg,
                        cnt: nselected
                    }
                );
            },
            acceptEvent: function (event) {
                return {'id': event.data.id};
            },
            openContext: function (p) {
                return this.openDict(p.id);
            },
            openDict: function (id) {
                return this.sys.$reg.get(id);
            },
            selectMeth: function (id) {
                //return this.wsapl.cn.lib.selectMeth(id);
                return window.comns.wsapl.cn.lib.selectMeth(id);
            },
            checkAll: function (p) {
                var c = this.openContext(p), state = pkg.openDict(c.check_state.reg_id),
                    hdon = p.checked, that = this, cnt = 0, selectOn = 0, selectOff = 0;
                state.clear();
                state.set('row', -1);
                state.set('checked', false);
                $(c.check_rows.selector).each(
                    function() {
                        var elem = $(this), info = utils.locateRowIndex({module: pkg, elem: elem});
                        if (hdon) {
                            state.set(info.key, info.index);
                            selectOn++;
                        } else {
                            selectOff++;
                        }
                        elem.prop('checked', hdon);
                        that.toggleChecked({tr: info.tr, on: hdon});
                        cnt++;
                    }
                );
                // $con.debug('checkAll', c.check_rows.selector, $(c.check_rows.selector).length, cnt);
                // /// @warning incomplete
                // /// @warning untested
                // /// @warning check: cnt > 0 seems incorrect without hdon
                // state.set('checked', nchecked > 0);
                // state.set('all', hdon && (nchecked == cnt));
                // this.updateCheckState(c, state);
                // $con.debug(cnt, nchecked, hdon, hdon && (nchecked == cnt));
                // $(() => pkg.trigger('rows:all', c.element.target));
                this.updateCheckState0(
                    {
                        checked: cnt > 0,
                        selectOn: selectOn,
                        selectOff: selectOff,
                        rowsCount: cnt,
                        hdon: hdon,
                        c: c,
                        state: state,
                        rowsAll: true,
                        from: 'checkAll'
                    }
                );
            },
            updateCheckState0: (p) => {
                /// @warning incomplete
                /// @warning untested
                /// @warning check: cnt > 0 seems incorrect without hdon
                /// @note mean just selection is checked type
                p.state.set('checked', p.checked);
                p.state.set('selectOn', p.selectOn);
                p.state.set('selectOff', p.selectOff);
                p.state.set('rowsCount', p.rowsCount);
                pkg.updateCheckState(p.c, p.state);
                // $con.debug(cnt, nchecked, hdon, hdon && (nchecked == cnt));
                $con.debug('updateCheckState0', p.c.element.target, p);
                if (p.rowsAll && ((p.selectOn === p.rowsCount) || (p.selectOff === p.rowsCount))) {
                    $(() => pkg.trigger('rows:all', p.c.element.target));
                }
            },
            updateCheckState: function(c, state) {
                // $con.debug('updateCheckState', state, state.toJSON());
                $(c.check_state.selector).val(state.toJSON());
                // $con.debug('updateCheckState', c.check_state.selector, state.toJSON(), $(c.check_state.selector), c.meth.gid);
                if (c.meth.gid) {
                    meth.trigger({context: c, pkg: pkg, cnt: pkg.countChecked(state)});
                }
            },
            countChecked: function(state) {
                /// @internal assumed - 2 is for exclude aux fields
                /// @todo verify that aux fields is present or change state format
                // var cnt = state.getCount() - 2;
                /// @internal WARNING!
                var cnt = state.getCount() - 2 - 3;
                $con.debug('countChecked', state.selectOn, state.get('selectOn'), cnt);
                return cnt;
                // return state.selectOn;
            },
            countCheckedFromRadioTrigger: function (c) {
                return pkg.countChecked(pkg.openDict(c.check_state.reg_id));
            },
            checkHeadTrigger: function (event) {
                //$con.debug('checkHead start');
                $dc.syscover.on();
                var elem = $(this),
                    p = {
                        'id': event.data.id,
                        'checked': elem.is(':checked')
                    };
                elem.prop('disabled', true);
                pkg.checkAll(p);
                elem.prop('disabled', false);
                $dc.syscover.off();
                //$con.debug('checkHead finish');
            },
            radioHeadTrigger: function (event) {
                var info = utils.openTriggerContextHead(event, pkg),
                    state = info.radio.state, c = info.context;
                state.set('row', info.index);
                $(info.context.radio.state.selector).val(state.toJSON());
                if (info.context.meth.gid) {
                    // meth.trigger({context: info.context, pkg: pkg, cnt: 0});
                    pkg.triggerMeth(c, true);
                    if (referenced.isActive(c)) {
                        // $con.debug('radioHeadTrigger setEnabled FALSE:start');
                        referenced.setEnabled(c, false);
                        // $con.debug('radioHeadTrigger setEnabled FALSE:finish');
                    }
                }
                //$con.debug('radioHeadTrigger done', state.get('row'));
                //$con.debug('radioHeadTrigger info', info.context);
                // $con.debug('radioHeadTrigger finish');
            },
            handleRef: function (event) {
                // $con.debug('handleRef', event);
                $dc.syscover.on();
                var refs = pkg.openRefs(event),
                    v = {state: {row: false, col: false}},
                    rindex = pkg.fetchRowIndex(this),
                    cindex = pkg.fetchColIndex(this);
                v.state.col = cindex;
                v.state.row = rindex;
                if (refs) {
                    // $con.debug('handleRef: refs', event, v);
                    $(refs.state.selector).val(JSON.stringify(v));
                    $(refs.broker.selector).click();
                } else {
                    $con.warn('unable translate ref click', event);
                }
            },
            openRefs: function (event) {
                var p, c;
                p = pkg.acceptEvent(event);
                c = pkg.openContext(p);
                if (c) {
                    // $con.debug('openRefs', p, c);
                    return c.body.refs;
                }
                $con.warn('unable openRefs', p, c);
                return false;
            },
            saveState: function (p) {
                var checks = [], that = this;
                /// keep selector check state
                $(p.gcheck.sel + ':checked').each(
                    function () {
                        var elem = $(this);
                        var row = that.fetchTR(elem);
                        // $con.debug('saveState.0.1:', elem, elem.prop(':checked'), row, row.data('rowid'));
                        // checks.push(elem.attr('data-'));
                        checks.push(row.data('rowid'));
                    }
                );
                // return {'checks': checks};
                this.savedState = {'checks': checks};
            },
            restoreState: function () {
                $con.debug('restoreState', this.savedState.checks);
                $.each(
                    this.savedState.checks,
                    function (index, value) {
                        var tr = $(`tr[data-rowid="${value}"]`);
                        var elem = tr.find('input');
                        // $con.debug('restoreState each', index, value, tr, elem);
                        // elem.prop('checked', true);
                        tr.trigger('click');
                        elem.trigger('click');
                    }
                );
            },
            bindRows: function (p) {
                var c = this.openContext(p), that = this;
                var rows = $(p.body.dock.selector);
                $(p.gcheck.sel).on('change', {id: p.id}, pkg.checkTriggerChange);
                this.bindSelection(p);
                $(p.refs.sel).on('click', {id: p.id}, pkg.handleRef);
                this.bindRadio(p, rows.length);
                /// @warning $('.selector').ready(... seems is deprecated since jQuery 3.0
                /// @warning $.isReady can be true but event triggered by ready-interface
                $(function () {
                    that.trigger('rows:loaded', c.element.target);
                }
                 );
            },
            bindRadio: function (p, nrows) {
                var grs = $(p.gradio.sel);
                // $con.debug(p, nrows, grs);
                // $con.debug('bindRadio', p, nrows);
                grs.on('change', {id: p.id}, pkg.radioTriggerChange);
                this.bindReferenced(p, grs, nrows);
            },
            bindReferenced: function (p, grs, nrows) {
                var c = this.openContext(p);
                if (referenced.isActive(c)) {
                    referenced.bind(c, grs, nrows);
                }
            },
            focusActiveRow: function (p) {
                var s = p.tr, state = {fresh: false};
                // $con.debug('focus', s.is('.nwfc-g-tr-active'), p.row);
                if (!s.is('.nwfc-g-tr-active')) {
                    s.addClass('nwfc-g-tr-active');
                    state.fresh = true;
                    if (p.clear) {
                        this.clearingOther(p);
                    }
                }
                this.trigger(
                    'row:focused',
                    {
                        row: p.row,
                        state: state
                    }
                );
                return state;
            },
            clearingOther: function (p) {
                var that = this;
                p.tr.siblings('.nwfc-g-tr-active').each(
                    function () {
                        // $(this).removeClass('nwfc-g-tr-active');
                        that.clearActive({tr: $(this)});
                    }
                );
            },
            clearActive: function (p) {
                p.tr.removeClass('nwfc-g-tr-active');
            },

            /**
             * Bind selection
             *
             * @param p
             *
             * @todo filter selecting tr by t.prop('tagName') and type
             *
             * @return void
             */
            bindSelection: function (p) {
                var that = this;
                $(p.body.dock.selector + ' tr').on(
                    'click',
                    {id: p.id},
                    function (event) {
                        var t = $(event.target), s = $(this), q, c,
                            mkey = event.ctrlKey && event.shiftKey, elem;

                        // focus active tr
                        that.focusActiveRow(
                            {
                                tr: s,
                                clear: true,
                                row: t.filter('div.nwfc-g-td-edge').length > 0 || t.filter('td').length > 0
                            }
                        );

                        if (!mkey && event.ctrlKey) {
                            that.clearActive({tr: s});
                        } else {
                            // force select tr - if event on tr
                            if (t.filter('div.nwfc-g-td-edge').length > 0 || t.filter('td').length > 0) {
                                /// @note force checkbox click
                                if (!mkey) {
                                    // s.find('.' + event.data.id).trigger('click', {bar: 1});
                                    elem = s.find('.' + event.data.id);
                                    elem.data('event', 'row:click').trigger('click');
                                    elem.data('event', null);
                                    // s.find('.' + event.data.id).trigger({type: 'click', user: 'qqq'});
                                }

                                /// @note for unselectRow, row:click
                                c = pkg.openContext(p);

                                /// @note force radio clicked
                                q = s.find(p.gradio.sel);
                                if (q.length) {
                                    if (q.prop('checked') && mkey) {
                                        referenced.unselectRow(c);
                                    } else {
                                        // q.trigger('click', {foo: 1});
                                        q.data('event', 'row:click').trigger('click');
                                        q.data('event', null);
                                    }
                                }
                                that.trigger('row:click', c.element.target);
                                that.trigger('row:select', c.element.target);
                            }
                        }
                        // $con.debug('finish tr click');
                    }
                );
            },
            trigger: function (ev, c) {
                /// @note c is class of element to selection
                $con.debug('trigger: starting', c, ev);
                switch (ev) {
                case 'row:select':
                    if ($(c).attr('data-opt-row-select-sync') == 'off') {
                        break;
                    }
                case 'rows:loaded':
                case 'row:click':
                case 'rows:all':
                    $(c).trigger(ev);
                    break;
                case 'row:focused':
                    break;
                default:
                    $con.warn('unknown event', ev, c);
                }
            },
            setContext: function (p) {
                var reg = this.sys.$reg;
                reg.set(p.id, p.data);
                reg.set(p.data.check_state.reg_id, new this.sysLib.dict());
                reg.set(p.data.radio.state.reg_id, new this.sysLib.dict());
            },
            setCellBorderVertical: function (p) {
                var c = pkg.openContext(p);
                // $con.debug('context:', c, p);
            },
            initCheck: function (p) {
                $(p.data.check_hd.selector).on('click', {id: p.id}, pkg.checkHeadTrigger);
            },
            initRadio: function (p) {
                $(p.data.radio.hd.selector).on('click', {id: p.id}, pkg.radioHeadTrigger);
            },
            initHead: function(p) {
                // $con.debug('initHead', p);
                this.initCheck(p);
                this.initRadio(p);
            },
            handleScroll: function(event) {
                var p = pkg.acceptEvent(event),
                    c = pkg.openContext(p),
                    elem = $(this),
                    v = elem.scrollLeft(),
                    elem_limit = elem[0].scrollWidth - elem[0].clientWidth,
                    slave = $(c.header.dock.selector), factor = 15;
                //$con.debug(v, elem_limit, slave_left);
                if (v + factor < elem_limit ){
                    slave.scrollLeft(v);
                } else {
                    elem.scrollLeft(elem_limit - factor);
                    slave.scrollLeft(elem_limit - factor);
                }
            },
            bindScroll: function(p) {
                $(p.data.body.dock.selector).on('scroll', {id: p.id}, pkg.handleScroll);
            },
            destroy: function(p) {
                var c = pkg.openContext(p);
                $(c.body.dock.selector).off('scroll', pkg.handleScroll);
                $(c.check_hd.selector).off('click', pkg.checkHeadTrigger);
                $(c.radio.hd.selector).off('click', pkg.radioHeadTrigger);
                $(c.check_rows.selector).off('change', pkg.checkTriggerChange);
                $(c.radio.rows.selector).off('change', pkg.radioTriggerChange);
                $(c.body.refs.selector).off('click', pkg.handleRef);
                if (c.meth.gid) {
                    meth.destroy({context:c});
                }
            },
            init: function (p) {
                // $con.debug('init', p);
                //$con.debug(this.test);
                $con.debug('init', p.data.element.target, p);
                this.setContext(p);
                this.initHead(p);
                this.bindRows(
                    {
                        'id': p.id,
                        'gcheck': {'sel': p.data.check_rows.selector},
                        'gradio': {'sel': p.data.radio.rows.selector},
                        'refs': {'sel': p.data.body.refs.selector},
                        'body': p.data.body
                    }
                );
                this.bindScroll(p);
                $(p.data.element.target).one(
                    'remove',
                    function () {
                        $con.info('trigger remove handler', p.data.element.target);
                    }
                );
            }
        };
    pkg.selector = {
        reset: function (p) {
            /// @note called to clear internal state of check selector after refresh|reload data
            var info = utils.openTriggerContext({data: {id: p.id}}, pkg, false),
                state = info.check.state;
            $con.debug('reset', 'pkg.selector', info, state);
            state.clear();
            $(info.context.check_state.selector).val(state.toJSON());
            if (info.context.meth.gid) {
                meth.trigger(
                    {
                        context: info.context,
                        pkg: pkg,
                        /// @note assume state cleared instead calling pkg.countChecked()
                        /// @todo assert cnt === 0
                        cnt: 0
                    }
                );
            }
            pkg.checkAll({id: p.id, checked: false});
            pkg.restoreState();
        }
    };
    pkg.sysLib = $dc.lib;
    pkg.sys = $dc;
    pkg.wsapl = window.comns.wsapl;
    $dc.grid = pkg;
    //$con.info('grid/main.js loaded');
    //$dc.grid.stub({'foo':'bar'});
}());
