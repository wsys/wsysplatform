<?php
namespace w\fgup_u\test_fgup\form_10;

use WSPLRTL\Widget\Label;
use WSPLRTL\Widget\Button;
use WSPLRTL\Widget\ButtonOK;
use WSPLRTL\Widget\ButtonCancel;
use WSPLRTL\Widget\Panel;
use WSPLRTL\Widget\Edit;
use WSPLRTL\Widget\Memo;
use WSPLRTL\Widget\DateEdit;
use WSPLRTL\Widget\Table;
use WSPLRTL\Widget\TableRow;
use WSPLRTL\Widget\TableCell;
use WSPLRTL\Widget\AbstractForm;
use WSPLRTL\Widget\DbGrid;
use WSPLRTL\Widget\Radio;
use WSPLRTL\Widget\Checkbox;
use WSPLRTL\Widget\Combobox;
use WSPLRTL\Widget\Password;
use WSPLRTL\Widget\Upload;
use WSPLRTL\Widget\Img;
use WSPLRTL\Widget\Map;
use WSPLRTL\Db\Filter\Access\Operator as OperatorAccess;
use com\danscode\wf\html\element\Panel as ElementPanel;
use WSPLRTL\Widget\Markup\MarkupHTML;
use WSPLRTL\Widget\Markup\MarkupMarkdown;
use WSPLRTL\Widget\Timer;

/// after resolveDeps
class Main extends AbstractForm
{
    protected function initialize()
    {
        $this->wsys_form_module = __FILE__;
        parent::initialize();
    }

    protected function initParameters()
    {
        parent::initParameters();
        //
        $body = [];
        $this->getCreatingParams()
            ->attachProps(['id' => 'win', 'control_id' => '1000035418', 'form' => $this, 'body' => $body, ]);
        $this->getCreatingParams()
            ->attachAttrs(['class' => '', 'style' => [], ]);
    }

    protected function startup()
    {
        parent::startup();
        $this->getSpec()
            ->setProp('control_id', '1000035417');
        $this->sys()
            ->injectCSS(['href' => '/sys/w/unpkg.hashed/bxo06ly/assets/css/wsys.css@0.0.1/wsys.css', 'id' => 'aZehEHj', 'data-priority' => '99', ]);
        $this->sys()
            ->injectCSS(['href' => 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css', 'id' => 'aDrFoty', 'data-priority' => '99', ]);
    }

    protected function handleEventClick_id_btncancel1($ev)
    {
        //

    }

    protected function renderAuto_id_btncancel1()
    {
        $widget = ButtonCancel::create(['id' => 'id_btncancel1', 'control_id' => '1000035423', 'form' => $this, 'caption' => <<<'_wsys60408afe10dac'
<< Назад
_wsys60408afe10dac
                                        , ],
        ['class' => 'w_button btn-primary ', 'value' => null, 'style' => ['width' => 'em', 'height' => 'em', ], ]);
        $this->addEventListener('click', $widget, 'handleEventClick_id_btncancel1');
        return $widget;

   protected function renderAuto_id_blkdiv1($body)
   {
       $widget = Panel::create(['id' => 'id_blkdiv1', 'control_id' => '1000035420', 'form' => $this, 'body' => $body, ],
                               ['class' => '', 'style' => ['padding-top' => ' 1rem', 'text-align' => 'center', ], ]);
       return $widget;
   }

       protected function renderAuto_id_edit_name()
       {
           $widget = Edit::create(['id' => 'id_edit_name', 'control_id' => '1000035425', 'form' => $this, ], ['title' => <<<'_wsys60408afe113f1'
Сотрите Имя и Фамилию и нажмите Кнопку, чтобы увидеть полный список
_wsys60408afe113f1
           , 'class' => '', 'value' => null, 'style' => ['width' => 'em', 'height' => 'em', ], ]);
           return $widget;
       }

       protected function renderAuto_id_blkdiv4($body)
       {
           $widget = Panel::create(['id' => 'id_blkdiv4', 'control_id' => '1000035429', 'form' => $this, 'body' => $body, ],
                                   ['class' => '', 'style' => ['padding-top' => ' 1rem', 'text-align' => 'center', ], ]);
           return $widget;
       }

       protected function renderAuto_id_edit_famil()
       {
           $widget = Edit::create(['id' => 'id_edit_famil', 'control_id' => '1000035426', 'form' => $this, ], ['title' => <<<'_wsys60408afe11986'
Сотрите Имя и Фамилию и нажмите Кнопку, чтобы увидеть полный список
_wsys60408afe11986
            , 'class' => '', 'value' => null, 'style' => ['width' => 'em', 'height' => 'em', ], ]);
           return $widget;
       }

       protected function renderAuto_id_blkdiv2($body)
       {
           $widget = Panel::create(
               ['id' => 'id_blkdiv2', 'control_id' => '1000035421', 'form' => $this, 'body' => $body, ],
               ['class' => '', 'style' => ['padding-top' => ' 1rem', 'text-align' => 'center', ], ]);
           return $widget;
       }

       protected function handleEventClick_id_btn1($ev)
       {

           $this->cursor('FGUP_U_U_FULL')
                ->param('P_NAME')
                ->setValue($ev->q('id_edit_name')
                              ->getValue());
           $this->cursor('FGUP_U_U_FULL')
                ->param('P_FAMIL')
                ->setValue($ev->q('id_edit_famil')
                              ->getValue());
           $q = $this->q('id_dbgrid1')
                     ->openQuery($this->cursor('FGUP_U_U_FULL'));

           $gr = $this->q('id_dbgrid1');
           $gr->column('#radio')
              ->Hide();
           $gr->setBodyCellBorderVertical(true);
           $gr->setHeadCellBorderVertical(true);
           $gr->setHeadCellBorderHorizontal(true);
           $gr->setBodyCellBorderHorizontal(true);
           $gr->setHeadVisible(true);

           $gr->column('ID')
              ->Hide();

           $gr->column('NAME')
              ->setTitle('Имя');
           $gr->column('NAME')
              ->setAlign('left');
           $gr->column('NAME')
              ->setSize(20);

           $gr->column('FAMIL')
              ->setTitle('Фамилия');
           $gr->column('FAMIL')
              ->setAlign('left');
           $gr->column('FAMIL')
              ->setSize(30);

           $gr->column('AGE')
              ->setTitle('Возраст');
           $gr->column('AGE')
              ->setAlign('right');
           $gr->column('AGE')
              ->setSize(10);

           $gr->column('PLACE')
              ->setTitle('Место жительства');
           $gr->column('PLACE')
              ->setAlign('left');
           $gr->column('PLACE')
              ->setSize(25);

           $gr->column('INFORMATION')
              ->setTitle('Информация');
           $gr->column('INFORMATION')
              ->setAlign('left');
           $gr->column('INFORMATION')
              ->setSize(30);

           $q->refresh();
           if ($this->cursor('FGUP_U_U_FULL')
                    ->count() == 0)
           {
               $this->wireAlert('Данных не найдено');
           }
           $this->cursor('FGUP_U_U_FULL')
                ->close();
       }

       protected function renderAuto_id_btn1()
       {
           $widget = Button::create(['id' => 'id_btn1', 'control_id' => '1000035427', 'form' => $this, 'caption' => <<<'_wsys60408afe11ea8'
Кнопка
_wsys60408afe11ea8
                                     , ],
            ['class' => 'btn-primary w_button ', 'value' => null, 'style' => ['width' => 'em', 'height' => 'em', ], ]);
           $this->addEventListener('click', $widget, 'handleEventClick_id_btn1');
           return $widget;
       }

       protected function renderAuto_id_blkdiv3($body)
       {
           $widget = Panel::create(
               ['id' => 'id_blkdiv3', 'control_id' => '1000035422', 'form' => $this, 'body' => $body, ],
               ['class' => 'animated delay-2s bounce ', 'style' => ['padding-top' => ' 1rem', 'text-align' => 'center', ], ]);
           return $widget;
       }

       protected function renderAuto_id_dbgrid1()
       {
           $widget = DbGrid::create(
               ['id' => 'id_dbgrid1', 'control_id' => '1000035441', 'form' => $this, ],
               ['class' => '', 'style' => ['display' => 'inline-block', ], ]);
           return $widget;
       }

       protected function renderAuto_id_blkdiv5($body)
       {
           $widget = Panel::create(
               ['id' => 'id_blkdiv5', 'control_id' => '1000035440', 'form' => $this, 'body' => $body, ],
               ['class' => '', 'style' => ['padding-top' => ' 1rem', ], ]);
           return $widget;
       }

       protected function onShow()
       {
           $this->q('id_edit_name')
                ->setValue('Иван');
           $this->q('id_edit_name')
                ->wireAttr('placeholder', 'Имя');
           $this->q('id_edit_famil')
                ->setValue('Иванов');
           $this->q('id_edit_famil')
                ->wireAttr('placeholder', 'Фамилия');
       }

       protected function renderAuto_win($body)
       {
           $this->onShow();
           return $body;
       }

       public function dispatchFromRS(callable$action)
       {

       }

       public function getRSMap()
       {
           return [];
       }

       public function renderForm()
       {
           return [
               $this->renderAuto_win(
                   [
                       $this->renderAuto_id_blkdiv1([$this->renderAuto_id_btncancel1()]),
                       $this->renderAuto_id_blkdiv4([$this->renderAuto_id_edit_name()]),
                       $this->renderAuto_id_blkdiv2([$this->renderAuto_id_edit_famil()]),
                       $this->renderAuto_id_blkdiv3([$this->renderAuto_id_btn1() ]),
                       $this->renderAuto_id_blkdiv5([$this->renderAuto_id_dbgrid1()])
                   ]
               )
           ];
       }
}
