// import {app as myapp, EntryPoint2} from './js/module';
import {Loader as CSSLoader} from './js/css/loader';
import {Loader as ScriptLoader} from './js/script/loader';
import {Sched} from './js/sched/sched';
import {Pooled} from './js/pooled/pooled';
import {Wsys} from './js/wsys/wsys';
import {chartd} from './js/chartd/chartd';
import {Util} from './js/util';
import {Main} from './js/main';
import {Navigate} from './js/navigate/navigate';

// export class EntryPoint1 {
//   static run() {
//     console.log('Run EntryPoint');
//   }
// }

// export class EntryPoint3 {
//   static run() {
//     console.log('Run EntryPoint3');
//   }
// }

// console.log(
//   `I'm a silly entry point`,
//   myapp,
//   typeof EntryPoint
// );

let myapp = 'myapp provided';
// window.myappRef = myapp;

window.Wsys = Wsys;
window.chartd = chartd;
// window.MyModule = {};
// export default {myapp};


let main = new Main();

/// Navigate
let navigate = new Navigate(main);


export {CSSLoader, ScriptLoader, Sched, Pooled, Wsys, Util, main, navigate};
// export {EntryPoint2, CSSLoader, ScriptLoader, Sched};
// export {EntryPoint2, CSSLoader};

console.debug(typeof $App);
