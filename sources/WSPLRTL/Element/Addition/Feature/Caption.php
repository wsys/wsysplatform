<?php
/**
 * Caption.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Addition\Feature;

/**
 * Caption.php
 *
 * @trait WSPLRTL\Element\Addition\Feature\Caption
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
trait Caption
{
    /**
     * @implements Nodelimit\Access\Widget\Feature\Caption
     *
     * @param content
     *
     * @return void
     */
    public function setCaption($content)
    {
        $this->getSpec()->wired()->setCaption($content);
    }

    /**
     * @implements Nodelimit\Access\Widget\Feature\Caption
     *
     * @return mixed caption content
     */
    public function getCaption()
    {
        return $this->getSpec()->getProp('caption');
    }
}
