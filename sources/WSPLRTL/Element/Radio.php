<?php
/**
 * Radio.php
 *
 * @page WSPLRTL\Element\Radio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Radio
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element package
 */
namespace WSPLRTL\Element;

use com\danscode\wf\html\element\input\Radio as GenericRadio;

/**
 * Radio.php
 *
 * Description
 *
 * @class WSPLRTL\Element\Radio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element package
 */
class Radio extends AbstractRadio
{
    protected function createButton($props, $attrs)
    {
        return new GenericRadio($props, $attrs);
    }
}
