<?php
/**
 * @file
 *
 * @brief Password.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Password
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\Edit;
use Nodelimit\WF\Element\Input\Password as InputPassword;

/**
 * The Password class
 *
 * Description
 *
 * @class WSPLRTL\Element\Password
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Password extends Edit
{
    public function render()
    {
        return new InputPassword(
            $this->makeBaseProps(),
            $this->getAttrs()
        );
    }
}
