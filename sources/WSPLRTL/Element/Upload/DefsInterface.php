<?php
/**
 * @file
 *
 * @brief DefsInterface.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Upload\DefsInterface
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Upload;

/**
 * The DefsInterface interface
 *
 * Description
 *
 * @interface WSPLRTL\Element\Upload\DefsInterface
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
interface DefsInterface
{
    const EVSOCK_TRAP = 'upload.trap';
    const EVSOCK_HANDLER = '_dispatchEventSocket';
}
