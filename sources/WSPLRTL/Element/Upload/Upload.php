<?php
/**
 * @file
 *
 * @brief Upload.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Upload\Upload
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Upload;

use WSPLRTL\Element\Edit;
use Nodelimit\WF\Component\Control\Upload\Simple\Element;
use Nodelimit\WF\Systype\Postback\Event\Socket as EventSocket;
use com\danscode\wf;
use WSPLRTL\Element\Upload\DefsInterface;

/**
 * The Upload class
 *
 * Description
 *
 * @class WSPLRTL\Element\Upload\Upload
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Upload extends Edit implements DefsInterface
{
    public function render()
    {
        list($props, $attrs) = $this->_makeElementArgs();
        return new Element(
            $props,
            $attrs
        );
    }

    private function _makeElementArgs()
    {
        $props = $this->makeBaseProps();
        $props['evsock'] = [
            'value' => $this->_makeEventSocket(self::EVSOCK_TRAP),
            'target' => WF::target()
        ];
        $props['submit.target'] = WF::target();
        $props['reset.target'] = WF::target();
        $props['upload.value.key'] = $this->_getComponent()->getValueKey();
        $attrs = $this->getAttrs();
        return [$props, $attrs];
    }

    private function _makeEventSocket($what)
    {
        return EventSocket::create($this->_makeEventSpec($what));
    }

    private function _makeEventSpec($what)
    {
        return $this->_getForm()->createEventSpec(
            $what, $this->_getComponent(), $this->_makeMethodName($what)
        );
    }

    /**
     * Create method name
     *
     * Make method name for handling EventSocket events
     *
     * @param string $what type or subject of event (Event::what)
     *
     * @note assume that name used for EventSocket events here
     * @note assume that self::EVSOCK_HANDLER is present in form
     *
     * @return string method name that handling EventSocket events
     */
    private function _makeMethodName($what = self::EVSOCK_TRAP)
    {
        return self::EVSOCK_HANDLER;
    }

    private function _getForm()
    {
        return $this->getForm();
    }

    private function _getComponent()
    {
        return $this->getComponent();
    }
}
