<?php
/**
 * @file
 *
 * @brief AbstractMap.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Map\AbstractMap
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Map;

use WSPLRTL\Element\AbstractElement;
use com\danscode\wf\html\element\Panel as GenericPanel;
use com\danscode\wf as WF;
use Nodelimit\WF\Element\Input\Hidden as ElementInputHidden;

/**
 * The AbstractMap class
 *
 * Description
 *
 * @class WSPLRTL\Element\Map\AbstractMap
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
abstract class AbstractMap extends AbstractElement
{
    private static $_my_props = [];
    private static $_my_attrs = [
        'class' => 'wfsys-ymaps-normal fitvp'
    ];

    private static $_store_props = [
        'map.id', 'ev.info.target'
    ];
    private static $_store_attrs = [];

    public function __construct($props = [], $attrs = [], $context = null)
    {
        //$props['target'] = WF::idtarget($props['id']);
        $this->registerStoreProps(self::$_store_props);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $p = $this->getProps();
        $s = $this->getStyle();
        //$s->setProp('overflow', 'hidden');
        $s->setProp('white-space', 'nowrap');
        // $s->setProp('min-height', '70vmin');
        // $s->setProp('height', '85vh');
        // $s->setProp('height', '100%');
        // $s->setProp('margin-bottom', '2vh');
        $this->renderPrepare();
        return [
            $this->_makeHiddenElement($this->_getEventInfoTarget()),
            new GenericPanel(
                [
                    'body' => $this->createBody(),
                    'target' => $p['target']
                ],
                $this->getAttrs()
            )
        ];
    }

    public function makeBase($subj)
    {
        switch ($subj) {
            case 'api.js.params':
                return $this->_makeBase();
            default:
                throw new \Exception('unknown subj');
        }
    }

    public function getEventInfoTarget()
    {
        return $this->_getEventInfoTarget();
    }

    private function _getEventInfoTarget()
    {
        return $this->getProp('ev.info.target');
    }

    private function _makeBase()
    {
        $p = $this->getProps();
        $t = $p['target'];
        $sel = $t->toSelector();
        $out = [
            'id' => $this->getMapId(),
            'sel' => $sel,
            'evsel' => $this->_getEventInfoTarget()->toSelector()
        ];
        return $out;
    }

    private function _makeHiddenElement($target, $value = '')
    {
        $attrs = ['value' => $value];
        $p = [
            'target' => $target
        ];
        return new ElementInputHidden($p, $attrs);
    }

    protected function initiate()
    {
        $s = $this->getSpec();
        $id = WF::tempID();
        $s->setProp('map.id', $id);
        $s->setProp('ev.info.target', WF::target());
        $s->setAttr('id', $id);
    }

    protected function getMapId()
    {
        return $this->getProp('map.id');
        // return $this->getForm()->getSystemGID(
        //     $this->getSpec()->getProp('name.base')
        // );
    }


    abstract protected function renderPrepare();
    abstract protected function createBody();
}
