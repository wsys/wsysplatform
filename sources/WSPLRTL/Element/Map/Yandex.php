<?php
/**
 * @file
 *
 * @brief Yandex.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Map\Yandex
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Map;

use WSPLRTL\Element\Map\AbstractMap;
use com\danscode\wf;
use Nodelimit\Code\Part\Former;
use Nodelimit\Code\Code;

/**
 * The Yandex class
 *
 * Description
 *
 * @class WSPLRTL\Element\Map\Yandex
 *
 * @todo instead switch for domain try to use direct part path or keys
 *
 * @internals
 * + $dc.ymaps.q({id:"a5db33f59323a9"})['om'].options.set('clusterize', false);
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Yandex extends AbstractMap
{
    protected function createBody()
    {
        return [];
    }

    protected function initiate()
    {
        parent::initiate();
        $this->initFrontend();
    }

    protected function renderPrepare()
    {
        ///
    }

    public function addPlacemarkLegacy($geometry, $props = [], $opts = [], $index = null)
    {
        $props['foo'] = 'bar';
        // $props['id'] = 123;
        // $props['id'] = uniqid('pm');
        $out = [
            'id' => $this->getMapId(),
            'geometry' => $geometry,
            'props' => $props,
            'opts' => $opts,
            'index' => $index
        ];
        $this->_api('addPlacemark', $out);
    }

    /**
     * Add map object
     *
     * Adds a map object to object manager
     *
     * @see https://tech.yandex.com/maps/jsapi/doc/2.1/ref/reference/ObjectManager-docpage/#method_detail__add
     * @see https://tech.yandex.ru/maps/jsapi/doc/2.1/ref/reference/ObjectManager-docpage/#method_detail__add
     *
     * @param array $p
     *
     * @return void
     */
    public function addMapObject($p)
    {
        $this->_api(
            'add',
            [
                'id' => $this->getMapId(),
                'data' => $p
            ]
        );
    }

    public function setMapObjectManagerOption($name, $value)
    {
        $this->mapAPI('set', [$name, $value], 'om.options');
    }

    public function setMapOption($name, $value)
    {
        switch ($name) {
            case 'clusterize':
                return $this->setMapObjectManagerOption($name, $value);
            default:
                //body...
                throw new \DomainException('unknown map option');
        }
    }

    public function mapAPI($method, array $args = [], $domain = 'map')
    {
        //  this works:
        // $dc.ymaps.api({id: 'a5d77f3832c03f', api:2, domain:'map', method: 'setZoom', args:[1]});
        // $dc.ymaps.api({id: 'a5d7e5c061c5c6', api:2, domain:'map.container', method: 'getSize', args:[]});
        // $dc.ymaps.api({id: 'a5d7e5c061c5c6', api:2, domain:'map.container', method: 'isFullscreen', args:[]});
        // $dc.ymaps.api({id: 'a5d7e5c061c5c6', api:2, domain:'map.container', method: 'fitToViewport', args:[]});
        //
        $b = $this->baseParam();
        $p = array_merge(
            $b,
            [
                'domain' => $domain,
                'method' => $method,
                'api' => 2,
                'args' => $args
            ]
        );
        $this->_api(
            'api',
            $p
        );
    }

    public function addPlacemark($geometry, $props = [], $opts = [], $index = null)
    {
        $props['foo'] = 'bar';
        // $props['id'] = 123;
        // $props['id'] = uniqid('pm');
        $out = [
            'id' => $this->getMapId(),
            'geometry' => $geometry,
            'props' => $props,
            'opts' => $opts,
            'index' => $index
        ];
        $this->addMapObject($this->convertPlacemarkToPoint($out));
    }

    private function convertPlacemarkToPoint($p)
    {
        $props = $p['props'];
        if (!isset($props['index']) && isset($p['index'])) {
            $props['index'] = $p['index'];
        } elseif (isset($props['index']) && isset($p['index'])) {
            throw new \Exception('index properties conflict');
        }
        $id = null;
        if (isset($props['id'])) {
            $id = $props['id'];
        } else {
            self::console(__METHOD__, 'WARNING: undefined id');
        }
        return [
            'type' => 'Feature',
            'id' => $id,
            'geometry' => [
                'type' => 'Point',
                'coordinates' => $p['geometry']
            ],
            'properties' => $p['props'],
            'options' => $p['opts']
        ];
    }

    private function initFrontend()
    {
        $p = $this->getProps();
        $out = $this->makeBase('api.js.params');
        if (isset($p['map.center'])) {
            $out['center'] = $p['map.center'];
        } else {
            $out['center'] = [55.76, 37.64];
        }
        if (isset($p['map.zoom'])) {
            $out['zoom'] = $p['map.zoom'];
        } else {
            $out['zoom'] = 10;
        }
        WF::wire(
            Code::vsprintf(
                '$dc.ymaps.load(%s);',
                [
                    json_encode($out)
                ]
            )
        );
    }

    private function baseParam()
    {
        return [
            'id' => $this->getMapId()
        ];
    }

    private function _api($meth, array $p)
    {
        WF::wire(
            Code::vsprintf(
                '$dc.ymaps.%s(%s);',
                [
                    $meth,
                    json_encode($p)
                ]
            )
        );
    }
}
