<?php
/**
 * Edit.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;
use com\danscode\wf\html\element\input\Text as GenericEdit;
use com\danscode\wf as WF;
use WSPLRTL\Access\ValueHost as ValueHostAccess;
use WSPLRTL\Element\ValueHostAddition;
use WSPLRTL\Element\AbstractControl;

/**
 * Edit.php
 *
 * @todo move ValueHost functionality onto AbstractValueHost
 *
 * @class WSPLRTL\Element\Edit
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Edit extends AbstractControl implements ValueHostAccess
{
    private static $_my_props = [];
    private static $_my_attrs = [
        'class' => 'form-control input-sm form-control-inline'
    ];
    private static $_store_props = [];
    private static $_store_attrs = [];

    use ValueHostAddition;

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $pout = $this->makeBaseProps();
        return new GenericEdit(
            $pout,
            $this->getAttrs()
        );
    }
}
