<?php
/**
 * ValueHostAddition.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Value\Lib\Utils as ValueUtils;

/**
 * ValueHostAddition.php
 *
 * @trait WSPLRTL\Element\ValueHostAddition
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
trait ValueHostAddition
{

    use ValueUtils;

    public function setValue($value)
    {
        $this->getSpec()->wired()->setValAttr(
            'value',
            self::normalizeValue($value)
        );
    }

    public function getValue()
    {
        return $this->readAttr('value');
    }
}
