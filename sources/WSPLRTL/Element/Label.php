<?php
/**
 * @file Label.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractControl;
use com\danscode\wf\html\element\Span as WFSpan;
use com\danscode\wf\html\element\Panel as WFPanel;
use com\danscode\wf as WF;
use Nodelimit\Access\Widget\Feature\Caption;
use WSPLRTL\Element\Addition\Feature\Caption as CaptionFeatureAddition;

/**
 * Label.php
 *
 * @class WSPLRTL\Element\Label
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Label extends AbstractControl implements Caption
{

    private static $_my_props = ['caption' => ''];
    private static $_my_attrs = [];
    private static $_store_props = ['caption'];
    private static $_store_attrs = [];

    use CaptionFeatureAddition;

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->registerStoreProps(self::$_store_props);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $p = $this->getProps();
        //$attrs = $this->getAttrs();
        //$attrs['style']['display'] = 'inline-block';
        $this->getStyle()->setProp('display', 'inline-block');
        return new WFPanel(
            [
                'body' => isset($p['caption']) ? $p['caption'] : '',
                'target' => $p['target']
            ],
            $this->getAttrs()
        );
    }
}
