<?php
/**
 * Table.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;
use Nodelimit\WF\Element\Table\Generic as GenericTable;
use com\danscode\wf as WF;


/**
 * Table.php
 *
 * description
 *
 * @class WSPLRTL\Element\Table
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Table extends AbstractElement
{
    private static $_my_props = [];
    private static $_my_attrs = [];
    private static $_store_props = [];
    private static $_store_attrs = [];

    public function __construct($props = [], $attrs = [], $context = null)
    {
        //$props['target'] = WF::idtarget($props['id']);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $p = $this->getProps();
        return new GenericTable(
            [
                'body' => $p['body'],
                'target' => $p['target']
            ],
            $this->getAttrs()
        );
    }
}
