<?php
/**
 * @file
 *
 * @brief Img.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Img
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;
use Nodelimit\WF\Element\Img\Img as Elem;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;

/**
 * The Img class
 *
 * Description
 *
 * @class WSPLRTL\Element\Img
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Img extends AbstractElement
{
    use LibClientServiceLocal;

    private static $_my_props = [];
    private static $_my_attrs = [];
    private static $_store_props = [];
    private static $_store_attrs = [];

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        return $this->_getElement();
    }

    public function src()
    {
        return $this->_getElement()->src();
    }

    public function reload()
    {
        $this->wireAttr('src', $this->src()->render());
    }

    private function _getLocalServiceBindings()
    {
        return [
            'img.elem' => function () {
                return $this->_createElement();
            }
        ];
    }

    private function _createElement()
    {
        $p = $this->getProps();
        return new Elem(
            [
                'target' => $p['target'],
                'path.prefix' => 'app/file'
            ],
            $this->getAttrs()
        );
    }

    private function _getElement()
    {
        return $this->_getLocalServiceItem('img.elem');
    }
}
