<?php
/**
 * AbstractGrid.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;

use WSPLRTL\Element\AbstractControl;

/**
 * AbstractGrid.php
 *
 * description
 *
 * @class WSPLRTL\Element\AbstractGrid
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
abstract class AbstractGrid extends AbstractControl
{
}
