<?php
/**
 * @file
 *
 * @brief InternalTestsTrait.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\DbGrid\InternalTestsTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\DbGrid;

/**
 * The InternalTestsTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Element\DbGrid\InternalTestsTrait
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
trait InternalTestsTrait
{
    private function _test()
    {
        assert_options(ASSERT_ACTIVE, 1);
        $dsn_string = 'sqlite:/opt/databases/mydb.sq3';
        $dsn = DataSourceSpecFactory::create($dsn_string);
        assert('gettype($dsn) === \'object\'');
        assert('$dsn->getValue() == $dsn_string');
        assert('(string) $dsn == $dsn_string');
        $m = serialize($dsn);
        $dsn = unserialize($m);
        assert('gettype($dsn) === \'object\'');
        $p = DataSourceParamsFactory::create($dsn);
        assert('$p->spec == $dsn');
        $ds = DataSourceFactory::create($p);
        //assert('(bool) $ds->getConnection() == true;');
        $get_conn_result = $ds->getConnection();
        $sz = serialize($ds);
        $code = new Code('SELECT * FROM DUAL');
        $sz_code = serialize($code);
        $unsz_code = unserialize($sz_code);
        $code1 = new Code();
        $sz_code1 = serialize($code1);
        $unsz_code1 = unserialize($sz_code1);
        $model_params_conf = [
            'data_source' => $ds, 'pager_data' => null, 'sql' => $code
        ];
        $model_params = ModelParams::create($model_params_conf);
        $sz_model_params = serialize($model_params);
        $unsz_model_params = unserialize($sz_model_params);
        $model = new GenericModel($model_params);
        $sz_model = serialize($model);
        $unsz_model = unserialize($sz_model);

        $te = DataSourceSpecFactory::create(
            (object) [
                'tag' => 'tag',
                'value' => ['foo' => 'bar']
            ]
        );

        $dump = [
            'ds' => $ds,
            'sz' => $sz,
            'unsz' => unserialize($sz),
            'code' => $code,
            'sz_code' => $sz_code,
            'unsz_code' => $unsz_code,
            'code1' => $code1,
            'sz_code1' => $sz_code1,
            'unsz_code1' => $unsz_code1,
            '$sz_model_params' => $sz_model_params,
            '$unsz_model_params' => $unsz_model_params,
            'model' => [$model, $sz_model, $unsz_model],
            'te' => $te,
            'sz_te' => serialize($te),
            'unsz_te' => unserialize(serialize($te)),
            'string_te' => (string) $te,
            'get_conn_result' => $get_conn_result
        ];
        self::logger($dump);
    }

    private function _testProcd()
    {
        $m = $this->getModel();
        //$sql = 'SELECT 1 FROM DUAL';
        //$sql = 'SELECT id FROM W.V_#MD#CLASSES WHERE rownum < 20';
        $sql = 'SELECT * FROM W.V_#MD#CLASSES WHERE rownum <= 10';
        $q = $m->openQuery(
            $sql
        );
        $rs = $q->execute()->fetchAll();
        $rs->refresh();
        $dump = ['whoami' => [__METHOD__, __LINE__]];
        $dump['rs.it.count'] = $rs->getIterator()->count();
        //$dump['rs.it.array'] = iterator_to_array($rs->getIterator());
        self::logger($dump);
    }

    private function _testOpenQuery()
    {
        //specify provider
        //getmodel
        $m = $this->getModel();
        //$q = $m->openQuery('SELECT 1');
        //$q = $m->openQuery('SELECT 1 as q, * FROM sqlite_master');
        //$q = $m->openQuery(
        //    'SELECT 1 as q, type, name, tbl_name, rootpage FROM sqlite_master'
        //);

        //$sql = 'SELECT 1 as q, * FROM sqlite_master';
        //$sql = 'SELECT 1 as q, type, name, tbl_name, rootpage FROM sqlite_master';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 100';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 50';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 50';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 10';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 5';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 3';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 20';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 25';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 15';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 10';
        //3m42s
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 1000';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 1500';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\' limit 1300';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CLASSES\'';
        //$sql = 'SELECT 1 as q, type, name, tbl_name, rootpage FROM sqlite_master LIMIT 27';
        //$sql = 'SELECT 1 as q, * FROM sqlite_master';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CONTROLS\'';
        $sql = 'SELECT 1 as q, * FROM \'V_#MD#CRITERIA_COLUMNS\' LIMIT 263';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CRITERIA_COLUMNS\' WHERE ID >= 77690';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CRITERIA_COLUMNS\'';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CRITERIA_COLUMNS\' LIMIT 7';
        //ID 	CRITERIA_ID 	POSITION 	NAME 	WIDTH 	UNVISIBLE 	ALIGN 	DATA_PRECISION
        //REF 	TARGET_CLASS_ID 	BASE_CLASS_ID 	ALIAS 	STRIP 	VALUE_IF_TRUE
        //VALUE_IF_FALSE 	VALUE_IF_NULL 	FORMAT 	HIDE_URL
        //$cols = [
        //    'ALIGN', 'DATA_PRECISION', 'REF', 'TARGET_CLASS_ID',
        //    'ALIAS'
        //];
        //$sql = 'SELECT 1 as q, CRITERIA_ID, POSITION, NAME, WIDTH, UNVISIBLE ' .
        //    (implode(',', $cols)) .
        //    ' FROM \'V_#MD#CRITERIA_COLUMNS\'';
        //$sql = 'SELECT 1 as q, * FROM \'V_#MD#CRITERIA_COLUMNS\' ';

        $q = $m->openQuery(
            //'SELECT 1 as q, * FROM \'V_#MD#FORMS\''
            //'SELECT 1 as q, id, name, num FROM \'V_#MD#FORMS\''
            //'SELECT 1 as q, * FROM \'V_#MD#FORMS\''
            $sql
        );

        $rs = $q->execute()->fetchAll();
        //$out = [];
        foreach ($rs as $r) {
            $r['q'] = $rs->getPosition();
            //or rownum?
            //$r[1] = '\'' . $r->q(1) . '\'';
            //$r['sql'] = substr($r->q('sql'), 0, 10);
            //$out[] = [$r[0], $r[1], $r['q'], $r['name']];
        }
        $tmp = [];
        $tmp['refresh out'] = get_class($rs->refresh());
        $rows = $m->getRows();
        $it = $rs->getIterator();
        $sm = serialize($m);
        $mr = unserialize($sm);
        $rows2 = $mr->getRows();
        //show result
        $dump = [
            //'model' => $m,
            //'q' => $q,
            //'fetch' => $it->getArrayCopy(),
            'count' => $it->count(),
            'rows' => get_class($rows->getIterator()),
            //'out' => $out,
            'tmp' => $tmp,
            //'head' => iterator_to_array($m->getHead()->getIterator()),
            //'head.iterator' => $m->getHead()->getIterator(),
            'head.iterator.class' => get_class($m->getHead()->getIterator()),
            'head.reader.class' => get_class($m->getHead()->getReader()),
            'head.reader.debug.info' => $m->getHead()->getReader()->getDebugInfo()
        ];
        self::logger($dump);

        //@todo grid dump
        //@todo update tux

        //@todo bind/prepare
        //@todo procd oci, bind

        //@todo basic postback
        //@todo html,css, layout for grid

        //@todo bind code onto compiler

        //@todo openQuery('dbgrid', 'select 1');

        //@todo client for refresh
        //@todo fetchnext
        //@todo active spec mdata

        self::_testCollection();
    }

    static private function _testCollection()
    {
        $c = Collection::createFromArray([1, 2, 3]);
        $t = BoxedTupleStorage::createFromArray([11, 22, 33]);
        $cc = FACollection::createFromArray([110, 222, 330]);
        $s = GenSets::createFromArray(['foo', 'bar', 'baz', 'foo']);
        $s[] = 'bah';
        $s[] = 'foo';
        $s[1] = null;
        $s->mergeArray(['a', 'b', 'bar', 'foo']);
        self::logger(
            $c,
            $c[0],
            $c[2],
            $t,
            $cc[0],
            $cc,
            $s->has('baz'),
            $s[0],
            $s->has('bar'),
            $s->toArray()
        );
    }
}
