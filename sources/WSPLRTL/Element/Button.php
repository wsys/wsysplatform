<?php
/**
 * Button.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractButton;
use Nodelimit\WF\Element\Button as GenericButton;
use com\danscode\wf as WF;
use Namedio\Core\Edge\Lang\Arrays\Arrays;

/**
 * Button.php
 *
 * description
 *
 * @class WSPLRTL\Element\Button
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Button extends AbstractButton
{
    private static $_my_props = [];
    private static $_my_attrs = [];
    private static $_store_props = [];
    private static $_store_attrs = [];

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $p = $this->getProps();
        $caption = isset($p['caption']) ? $p['caption'] : '';
        return new GenericButton(
            [
                'body' => $caption,
                'target' => $p['target']
            ],
            $this->getAttrs()
        );
    }
}
