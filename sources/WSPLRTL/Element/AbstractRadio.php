<?php
/**
 * AbstractRadio.php
 *
 * @page WSPLRTL\Element\AbstractRadio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\AbstractRadio
 *
 * @warning use Panel importing with caution, because can be names conflicts
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element package
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;
// use com\danscode\wf\html\element\input\Radio as GenericRadio;

use com\danscode\WF;
use WSPLRTL\Access\ValueHost as ValueHostAccess;
use WSPLRTL\Element\ValueHostAddition;

use Nodelimit\WF\Element\Form\Label as LabelElement;
use com\danscode\wf\html\element\Panel as PanelElement;

use com\danscode\wf\html\attrs\ClassObject;

use WSPLRTL\Element\AbstractControl;

/**
 * AbstractRadio.php
 *
 * Description
 *
 * @class WSPLRTL\Element\AbstractRadio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element package
 */
abstract class AbstractRadio extends AbstractControl implements ValueHostAccess
{
    const CAPTION_CLASSES = 'wsplrtl-radio caption';
    const NAME_LABEL_TARGET = '__label_target';

    private static $_my_props = [];
    private static $_my_attrs = [
        'class' => 'wsplrtl-radio-btn'
    ];
    private static $_store_props = [self::NAME_LABEL_TARGET];
    private static $_store_attrs = [];

    use ValueHostAddition;

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->registerStoreProps(self::$_store_props);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function changeChecked($checked = true)
    {
        $this->getSpec()->wired()->setChecked($checked);
    }

    public function setCaption($content)
    {
        $this->_setCaption($content);
        WF::update($this->_getCaptionSelector(), $content);
    }

    public function render()
    {
        return $this->_makeBodyLabeled();
    }

    abstract protected function createButton($props, $attrs);

    protected function makeButton()
    {
        return $this->createButton(
            $this->makeBaseProps(['checked']), $this->getAttrs()
        );
    }

    protected function initiate()
    {
        $t = WF::target();
        $s = $this->getSpec();
        $name = $this->getForm()->getSystemGID(
            $s->getProp('name.base')
        );
        $this->getSpec()->setAttr('name', $name);
        $this->setProp(self::NAME_LABEL_TARGET, $t);
    }

    protected function makeLabel($control)
    {
        return $this->_genLabel([$control, $this->_makeCaption()]);
    }

    private static function _dumpClasses()
    {
        $db = new \ArrayIterator(get_declared_classes());
        $it = new \RegexIterator($db, '/.*panel.*/i');
        self::console(iterator_to_array($it));
    }

    private function _makeBodyLabeled()
    {
        return $this->makeLabel($this->makeButton());
    }

    private function _genLabel($body)
    {
        return new LabelElement(
            [
                'body' => $body,
                'target' => $this->_getLabelTarget()
            ],
            ['class' => 'wsplrtl clabel']
        );
    }

    private function _makeCaption()
    {
        return $this->_makeCaptionBlock($this->_getCaption());
    }

    private function _getCaption()
    {
        return $this->getProp('caption', '');
    }

    /**
     * Makes caption in block
     *
     * @param mixed $caption
     *
     * @internal possible declarations of Panel:
     * @code
     *    Array
     *     (
     *         [0] => Array
     *             (
     *                 [214] => com\danscode\wf\html\element\Panel
     *                 [589] => WSPLRTL\Widget\Panel
     *                 [590] => WSPLRTL\Element\Panel
     *             )
     *
     *     )
     * @endcode
     *
     * @return object PanelElement
     */
    private function _makeCaptionBlock($caption)
    {
        $body = [$caption];
        // self::_dumpClasses();
        return new PanelElement(
            ['body' => $body],
            ['class' => self::CAPTION_CLASSES]
        );
    }

    private function _setCaption($content)
    {
        $this->setProp('caption', $content);
    }

    private function _getCaptionSelector()
    {
        return $this->_getLabelSelector() . ' ' . $this->_getCaptionSelectorBase();
    }

    private function _getLabelSelector()
    {
        return $this->_getLabelTarget()->toSelector();
    }

    private function _getLabelTarget()
    {
        return $this->getProp(self::NAME_LABEL_TARGET);
    }

    private function _getCaptionSelectorBase()
    {
        return ClassObject::create(self::CAPTION_CLASSES)->toSelector();
    }
}
