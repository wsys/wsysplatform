<?php
/**
 * Memo.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractControl;
use Nodelimit\WF\Element\Textarea;
use com\danscode\wf as WF;
use WSPLRTL\Value\Lib\Utils as ValueUtils;
use Namedio\Core\Edge\Lang\Strings\Strings;

/**
 * Memo.php
 *
 * @class WSPLRTL\Element\Memo
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Memo extends AbstractControl
{
    private static $_my_props = [];
    private static $_my_attrs = [
        'class' => 'form-control input-sm form-control-inline'
    ];
    private static $_store_props = ['text'];
    private static $_store_attrs = [];

    use ValueUtils;

    public function __construct($props = [], $attrs = [], $context = null)
    {
        //$props['target'] = WF::idtarget($props['id']);
        $this->registerStoreProps(self::$_store_props);

        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        return new Textarea($this->getProps(), $this->getAttrs());
    }

    public function setValue($value)
    {
        // self::logTraceDump(
        //     __METHOD__,
        //     $value,
        //     ValueUtils::normalizeValueText($value)
        //     // json_encode($value),
        //     // Strings::unquote(json_encode($value, JSON_UNESCAPED_UNICODE))
        // );
        $this->getSpec()->wired()->setVal(
            'text',
            ValueUtils::normalizeValueText($value)
        );
    }

    public function getValue()
    {
        $v = $this->readProp('text');
        return $v;
    }
}
