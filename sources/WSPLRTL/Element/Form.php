<?php
/**
 * Form.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractControl;
use com\danscode\wf\html\element\Panel as WFPanel;
use com\danscode\wf as WF;
use WSPLRTL\Systype\WidgetPool;

use Nodelimit\Access\Widget\Feature\Visualize\Visibling as VisiblingAccess;
use Nodelimit\Access\Widget\Feature\Visualize\Exposing as ExposingAccess;

use Nodelimit\WF\Element\Lib\Visibling as LibVisibling;
use Nodelimit\WF\Element\Lib\Exposing as LibExposing;
use com\danscode\init\Main;
use Nodelimit\WF\Element\Input\Hidden as ElementInputHidden;
use Namedio\Ext\System\System;
use Namedio\Ext\SimpleResult\Result;
use Namedio\Ext\SimpleResult\Error;
use Namedio\Ext\SimpleResult\ExceptionResult;
use Namedio\Ext\SimpleResult\AbstractResult;
use Namedio\Core\Edge\Util\Registry;

/**
 * Form.php
 *
 * @class WSPLRTL\Element\Form
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Form extends AbstractControl implements VisiblingAccess, ExposingAccess
{
    use LibVisibling;
    use LibExposing;

    private static $_my_props = [];
    private static $_my_attrs = [];
    private static $_store_props = ['wpool', 'target', 'exchangeTarget'];
    //private static $_store_props = [];
    //private static $_store_props = [];
    private static $_store_attrs = [];

    public function __construct($props = [], $attrs = [], $context = null)
    {
        //$props['target'] = WF::idtarget('form');
        $this->registerStoreProps(self::$_store_props);

        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
        $wp = null;
        if ($this->isRecycle()) {
            // self::console(
            //     $this->isPropSet('wpool'),
            //     $this->getPersistentDebugInfo()
            // );
            $wp = $this->_getWpool();
            //self::logger([__METHOD__, 'recycle', gettype($wp)]);
        } else {
            $wp = $this->_initial();
            // self::logger(
            //     __METHOD__,
            //     __LINE__,
            //     'use context:',
            //     (string) $this->getPersistentSpec()
            // );
        }
        // self::logger(__METHOD__, __LINE__, gettype($wp));
        $wp->setForm($this->getComponent());
    }

    /**
     * @warning parent constructor is undefined
     *
     *
     * @return
     */
    public function __destruct()
    {
        // self::console([__METHOD__, 'wpool', 'start', $this->isPropSet('wpool')]);
        // //parent::__destruct();
        // self::console($this->isPropSet('wpool'));
        if ($this->isPropSet('wpool')) {
            // $this->readProp('wpool')->free();
        } else {
            self::console('WARNING: wpool is not set');
        }
        // self::logger(__METHOD__, __LINE__, 'destructed', Main::getState());
    }

    /**
     * @todo fixme: when class not defined - target is not defined
     * @warning class attribute define is required
     * @note example of usage spec
     *
     * @return
     */
    public function render()
    {
        $p = $this->getProps();
        $c = $this->getComponent();
        // $spec = $this->getSpec();
        // $spec->setAttr('class', '');
        // self::console(__METHOD__, self::getBacktraceString());
        // self::vdump(__METHOD__, $p['target']);
        // self::logTraceDump(__METHOD__, $c);

        $embedUp = [
            new WFPanel(
                [
                    'body' => [],
                    'target' => ''
                ],
                [
                    'class' => 'dial slot up'
                ]
            )
        ];

        $embedDown = [
            new WFPanel(
                [
                    'body' => [],
                    'target' => ''
                ],
                [
                    'class' => 'dial slot down'
                ]
            )
        ];


        // self::logger(__METHOD__, 'before renderFormCall');
        $r = System::attempt(
            function () use ($c) {
                $content = $c->renderFormCall();
                return $content;
            }
        );
        // self::logger(__METHOD__, 'finish renderFormCall');

        $reg = Registry::getInstance();
        if ($reg->has('$cnode#triggerHidding')) {
            self::logger('$cnode#triggerHidding', 'available');
            $trig = $reg->get('$cnode#triggerHidding');
            $reg->unregister('$cnode#triggerHidding');
            call_user_func($trig);
        }

        $content = [];
        // self::vdump($r);
        if ($r->isOK()) {
            $content = $r->getValue();
        } elseif ($r instanceof ExceptionResult) {
            /// @todo Throwable
            $e = $r->getValue();
            throw $e;
        } else {
            throw new \Exception('internal error');
        }

        return new WFPanel(
            [
                'body' => [
                    $this->makeHidden($this->getTargetHidden()),
                    $embedUp,
                    /// @note direct call AbstractForm::renderFrom
                    // $c->renderForm(),
                    $content,
                    $embedDown,
                    $this->renderActivate()
                ],
                'target' => $p['target'],
                'use_context' => $p['use_context']
            ],
            $this->getAttrs()
        );
    }

    private function renderActivate()
    {
        $this->getComponent()->runActivate();
        $out = [];
        return $out;
    }

    private function makeHidden($target, $value = '')
    {
        $attrs = [
            'value' => $value,
            'data-role' => '$exchange'
        ];
        $p = [
            'target' => $target
        ];
        return new ElementInputHidden($p, $attrs);
    }

    private function getTargetHidden()
    {
        return $this->getProp('exchangeTarget');
    }

    private function _getWpool()
    {
        // self::console($this->isRecycle(), $this->isPropSet('wpool'));
        $wp = $this->readProp('wpool');
        // self::console('wpool:readProp:ok');
        return $wp;
    }

    private function _setWpool($wp)
    {
        $this->putProp('wpool', $wp);
    }

    private function _initial()
    {
        $wp = new WidgetPool();
        $this->_setWpool($wp);
        //self::logger([__METHOD__, 'init', gettype($wp)]);
        /// @see com\danscode\systype\wf\Target
        $this->setProp('exchangeTarget', WF::target());
        $this->setProp('target', WF::idtarget('form'));
        // self::logger(__METHOD__, __LINE__, 'create widget pool');
        return $wp;
    }
}
