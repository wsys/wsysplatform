<?php
/**
 * @file AbstractElement.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use com\danscode\wf\html\element\Base as BaseElement;
use WSPLRTL\Access\General as GeneralAccess;
use Nodelimit\WF\Element\Addition\Wired;
use Nodelimit\WF\Element\Addition\CoreStyleModel;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use WSPLRTL\SpoolActions;
use Namedio\Core\Util\Options\OptionsTrait;
use Namedio\Core\Util\Options\OptionsClientTrait;
use Hashids\Hashids;

/**
 * AbstractElement.php
 *
 * @class WSPLRTL\Element\AbstractElement
 *
 * @warning used bootstrap3 classes directly
 *
 * @todo new abstract layer for bootstrap3 controls or separate this
 * presentation level
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractElement extends BaseElement implements GeneralAccess
{
    use LibClientServiceLocal;
    use Wired;
    use CoreStyleModel;
    use OptionsTrait;
    use OptionsClientTrait;

    private static $_my_props = [];
    private static $_my_attrs = [];
    private static $_store_props = [];
    private static $_store_attrs = [];

    private $_component;

    private $refreshMethod;

    /**
     * @warning property __component required in props
     *
     * @param props
     * @param attrs
     * @param context
     *
     * @return
     */
    public function __construct($props = [], $attrs = [], $context = null)
    {
        $comp = $this->_component = $props['__component'];
        $this->registerStoreProps($comp->getStoreProps());
        $this->registerStoreProps(self::$_store_props);
        $this->registerStoreAttrs($comp->getStoreAttrs());
        $this->registerStoreAttrs(self::$_store_attrs);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }


    public function getRegStorage($spec)
    {
        // $this->getScope();
        // $this->setSharedArrayOnce();
        // $this->setSharedValueOnce($spec->makeName([$prefix, 'reg']), $v);
        $h = new Hashids((string) $this->getPersistentSpec());
        $name = $spec->makeName($h->encode(1));
        $this->setSharedValueOnce(
            $name,
            function () {
                return new \ArrayObject();
            }
        );
        return $this->getSharedValue($name);
    }

    public function setRefresh($method)
    {
        $this->refreshMethod = $method;
    }

    public function getRefresh()
    {
        return $this->refreshMethod;
    }

    public function send($code)
    {
        $this->getSpec()->wired()->wire($code);
    }

    public function wired()
    {
        return $this->getSpec()->wired();
    }

    public function wireElementStyle($name, $value)
    {
        $this->wired()->style()->setProp($name, $value);
    }

    public function wireAttr($name, $value)
    {
        $this->wired()->updateAttr($name, $value);
    }

    public function wireAttrRemove($name)
    {
        $this->wired()->removeAttr($name);
    }

    public function wireClassAdd($class)
    {
        $this->wired()->addClass($class);
    }

    public function wireClassRemove($class)
    {
        $this->wired()->removeClass($class);
    }

    public function changeAttr($name, $value)
    {
        $this->wireAttr($name, $value);
    }

    /**
     * Creates actions spooler
     *
     * @return SpoolActions
     */
    public function createSpoolActions()
    {
        return SpoolActions::create(['owner' => $this]);
    }

    public function getForm()
    {
        return $this->getComponent()->getForm();
    }

    public function setFrontOption($key, $value)
    {
        /// data-option-row-select-sync
        $this->wireAttr('data-opt-' . $key, $value);
    }

    public function getComponent()
    {
        return $this->_component;
    }

    protected function makeBaseProps($clone = [])
    {
        $p = $this->getProps();
        $pout = [
            'target' => $p['target']
        ];
        foreach ($clone as $k) {
            $pout[$k] = $p[$k];
        }
        return $pout;
    }

    protected function spoolAction($action, $args = [], $refresh = true)
    {
        $this->getSpoolActions()->spool($action, $args, $refresh);
    }

    /**
     * Gets spooled actions
     *
     * @return WSPLRTL\SpoolActions
     */
    private function getSpoolActions()
    {
        return $this->_getLocalServiceItem('spool.actions');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'spool.actions' => [$this, 'createSpoolActions']
        ];
    }
}
