<?php
/**
 * DbGrid.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractGrid;
use com\danscode\wf\html\element\Panel as GenericPanel;
use com\danscode\wf as WF;
use Nodelimit\WF\Component\Grid\Element as ElementGrid;

use com\danscode\systype\LazyInit;

/**
 * Grid and Model interactions requirements, behaviour for plugging ModelHostOpener
 */
use Nodelimit\WF\Component\Grid\Access\ModelAware;
use Nodelimit\WF\Component\Grid\Addition\ModelHostOpener as ModelHostOpenerAddition;

/**
 * Model originator stuff
 */
use Nodelimit\WF\Component\Grid\Access\ModelHostOriginator;
use Nodelimit\WF\Component\Grid\Model\CreateHelper as ModelCreateHelper;


use Nodelimit\Db\DataSource\Spec\Factory as DataSourceSpecFactory;
use Nodelimit\Db\DataSource\Params\Factory as DataSourceParamsFactory;
use Nodelimit\Db\DataSource\Factory as DataSourceFactory;
use Nodelimit\Db\SQL\Code;
use Nodelimit\WF\Component\Grid\Model\Params as ModelParams;
use Nodelimit\WF\Component\Grid\Model\Generic as GenericModel;

use Nodelimit\Systype\Collection\Bridge\Generic as Collection;
use Nodelimit\Systype\Storage\Bridge\BoxedTuple as BoxedTupleStorage;
use Nodelimit\Systype\Collection\Bridge\FixedArray as FACollection;

use Nodelimit\Systype\Sets\Ext\Generic as GenSets;
use WSPLRTL\Element\DbGrid\InternalTestsTrait;
use Namedio\UI\Grid\Reuse\ReuseGridTrait;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use com\danscode\lib\Props;
use Hashids\Hashids;

/**
 * DbGrid.php
 *
 * @class WSPLRTL\Element\DbGrid
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class DbGrid extends AbstractGrid implements ModelAware, ModelHostOriginator
{
    private static $_my_props = [];
    private static $_my_attrs = [];
    private static $_store_props = [
        'shared_context', 'element_grid_context'
    ];
    private static $_store_attrs = [];

    use ModelHostOpenerAddition;
    use InternalTestsTrait;
    use ReuseGridTrait;
    use LibClientServiceLocal;

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->registerStoreProps(self::$_store_props);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
        $this->initModelHostOpenerAddition();
    }

    protected function initiate()
    {
        $s = $this->getSpec();
        $s->setProp('element_grid_context', $this->makeSlotSpec());
        /// @warning shared_context is not defined before it
        /// @warning but defined in getPersistentSharedSpec
        /// @todo verify why is temporary defined getPersistentSharedSpec at init
        /// @todo verify where stored and specified getPersistentSharedSpec
        /// @todo check getPersistentSharedSpec on reuse stage
        /// @todo specify shared_context at top level
        $this->_setSharedContext($this->getPersistentSharedSpec());
    }

    //public function __destruct()
    //{
    //    self::logger(__METHOD__);
    //    //unset($this->_model_host);
    //    //self::logger(__METHOD__);
    //}

    public function render()
    {
        return $this->getGridElement();
    }

    /**
     * Create Model
     *
     * @see WSAPL\ClassesNode\Grid\Model\Facade
     * @see Nodelimit\WF\Component\Grid\Model\Generic
     *
     * @return Nodelimit\WF\Component\Grid\Model\AbstractModel an instance
     */
    public function createModel()
    {
        return (new ModelCreateHelper($this))->run();
    }

    /**
     * Notify observer
     *
     * @return mixed
     */
    public function notify($info = null)
    {
        if ($info === 'node:rowset:update') {
            return $this->_notifyRowsetUpdate();
        }
    }


    public function selectRow($id = null)
    {
        $code = '';
        $cid = $this->getProp('id');
        $hashids = new Hashids('secWsysret');
        $hash = $hashids->encode($id);
        self::logger(__FILE__, __METHOD__, __LINE__, $id, $hash);
        if ($id) {
            $code = "$(\".wfqual-${cid} [data-rowid='${hash}'] input\").click();";
        } else {
            $code = "$(\".wfqual-${cid} *[data-rowid] input\").first().click();";
        }
        $this->send($code);
    }

    public function selectRowByIndex($index = null)
    {
        $code = '';
        $cid = $this->getProp('id');
        if (isset($index)) {
            $index0 = $index + 1;
            $code = "$(\".wfqual-${cid} *[data-rowid]:nth-child(${$index0}) input\").click();";
        } else {
            $code = "$(\".wfqual-${cid} *[data-rowid] input\").first().click();";
        }
        $this->send($code);
    }

    public function getGridElement()
    {
        return $this->_getLocalServiceItem('grid.element');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'grid.element' => function () {
                return $this->_createGridElement();
            }
        ];
    }

    private function _createGridElement()
    {
        $p = $this->getProps();
        // self::logTraceDump($p);
        // self::console(
        //     __METHOD__,
        //     (string) $p['target'],
        //     array_keys($p),
        //     Props::getSafe($p, 'width', 'N/A'),
        //     $this->getAttrs()
        // );
        $shared_spec = $this->getPersistentSharedSpec();
        $g = new ElementGrid(
            [
                //'body' => $p['body'],
                'body' => __CLASS__,
                'target' => $p['target'],
                'width' => Props::getSafe($p, 'width', null),
                // linkage, bonding, anchor, bind
                //'opener' => $this,
                'shared_context' => $shared_spec,
                'use_context' => $this->getReuseContext()
            ],
            $this->getAttrs()
        );
        $g->setBodyScrollVertical(false);
        return $g;
    }

    /**
     * Trigger when rowset is updated
     *
     * @return void
     */
    private function _notifyRowsetUpdate()
    {
        if ($this->isRecycle()) {
            $this->_notifyRowsetUpdateRecycle();
        }
    }

    private function _notifyRowsetUpdateRecycle()
    {
        // self::logTraceDump($this->reuse());
        $g = $this->reuse();
        // $g->eventRefresh();
        $g->eventRemap();
    }

    private function _testCols()
    {
        // $m = $this->getModel();
        // self::logTraceDump(
        //     $m->getHead(),
        //     $m->getHead()->getGuideline(),
        //     $m->getHead()->getCols(),
        //     $m->getLayout(),
        //     $m->getLayout()->getCols(),
        //     // $m->getLayout()->getColsKeys(),
        //     $m->getColumnsInfoDOC()->read()
        // );
    }

    private function _setSharedContext($spec)
    {
        $this->getSpec()->setProp('shared_context', $spec);
    }

    protected function createReused($context = null, $props = null)
    {
        return ElementGrid::createWithContext($context, $props);
    }

    protected function getReuseProps()
    {
        return $this->createReusePropsArray(['body' => []]);
    }

    protected function getReuseContext()
    {
        return $this->readProp('element_grid_context');
    }

    protected function getReusePropTarget()
    {
        // self::console(
        //     (string) $this->getPersistentSpec(),
        //     array_keys($this->getSpec()->getAllProps())
        // );
        return ['target', $this->readProp('target')];
    }

    protected function getReusePropContext()
    {
        return ['use_context', $this->getReuseContext()];
    }

    protected function getReusePropSharedContext()
    {
        return ['shared_context', $this->readProp('shared_context')];
    }

    protected function getReusePropNodeContext()
    {
        return [
            '::master_node_context',
            null
        ];
    }
}
