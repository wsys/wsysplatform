<?php
/**
 * Checkbox.php
 *
 * @page WSPLRTL\Element\Checkbox
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Checkbox
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element package
 */
namespace WSPLRTL\Element;

use com\danscode\wf\html\element\input\Checkbox as GenericCheckbox;

/**
 * Checkbox.php
 *
 * Description
 *
 * @class WSPLRTL\Element\Checkbox
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element package
 */
class Checkbox extends AbstractRadio
{
    protected function createButton($props, $attrs)
    {
        return new GenericCheckbox($props, $attrs);
    }
}
