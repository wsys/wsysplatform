<?php
/**
 * Panel.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;
use com\danscode\wf\html\element\Panel as GenericPanel;
use com\danscode\wf as WF;
use Nodelimit\Access\Widget\Feature\Caption;
use WSPLRTL\Element\Addition\Feature\Caption as CaptionFeatureAddition;

/**
 * Panel.php
 *
 * @class WSPLRTL\Element\Panel
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Panel extends AbstractElement implements Caption
{
    private static $_my_props = ['caption' => ''];
    private static $_my_attrs = [];
    private static $_store_props = ['caption'];
    private static $_store_attrs = [];

    use CaptionFeatureAddition;

    public function __construct($props = [], $attrs = [], $context = null)
    {
        //$props['target'] = WF::idtarget($props['id']);
        $this->registerStoreProps(self::$_store_props);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $p = $this->getProps();
        // $s = $this->getStyle();
        //$s->setProp('overflow', 'hidden');
        // $s->setProp('white-space', 'nowrap');
        return new GenericPanel(
            [
                'body' => $this->getBodyContent(),
                'target' => $p['target']
            ],
            $this->getAttrs()
        );
    }

    public function setBody($body, $refresh = true)
    {
        $this->setProp('body', $body);
        $this->spoolAction('wired.body', [$body], $refresh);
    }

    private function getBodyContent()
    {
        //
        $p = $this->getProps();
        $out = [];
        if (isset($p['caption'])) {
            $out[] = [$p['caption']];
        }
        $out[] = $p['body'];
        return $out;
    }
}
