<?php
/**
 * @file
 *
 * @brief Timer.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Timer\Timer
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Timer;

use WSPLRTL\Element\AbstractElement;
use com\danscode\wf\html\element\Panel;
use Namedio\Core\Edge\Lang\Arrays\Arrays;

/**
 * The Timer class
 *
 * @class WSPLRTL\Element\Timer\Timer
 *
 * @todo gc in destructor ? optional or automatic destroy on form close
 * @todo use separated task id genefation
 * @todo allow to configure rt-parameters
 * @todo remove timer
 * @todo allow callback with eval
 * @todo allow callback with call something
 * @todo documenting specs
 * @todo controls covers
 * @todo controls blocking
 * @todo control and test on show
 * @todo hook on form close
 * @todo use dates (start, end)
 * @todo use timers pool on front to configure init interval
 * @todo options of execution: blocking, async, without postback
 * @todo reset
 * @todo dynamic change task parameters
 * @todo use initiate method
 * @todo move to system level
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
class Timer extends AbstractElement
{
    private static $_my_props = [];
    private static $_my_attrs = [
        'class' => ''
    ];
    private static $_store_props = ['target'];
    private static $_store_attrs = [];

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $this->sendInitial();
        $spec = $this->getSpec();
        $p = $spec->getAllProps();
        $attrs = $spec->getAllAttrs();
        $content = [
            new Panel($p, $attrs)
        ];
        // $this->hookDebug();
        return $content;
    }

    public function setEnabled($enabled = false)
    {
        $this->send(
            $this->callJS('$App.Sched.enableTask', $this->getInternalID(), $enabled)
        );
    }

    public function setInterval($interval)
    {
        $props = ['tickInterval' => $interval];
        $this->send(
            // $this->callJS('$App.Sched.setTaskProps', $this->getInternalID(), $props)
            $this->callJS('$App.Sched.setTaskProps', $this->parametrize(['props' => $props]))
        );
    }

    private function parametrize($p = [])
    {
        $base = ['taskId' => $this->getInternalID(), 'selector' => $this->getSelector()];
        return array_merge($base, $p);
    }

    private function hookDebug()
    {
        $widget = $this;
        if ($widget->getControlID() === '1321366358') {
            // $timer = $this->qsafe('id_timer');
            $timer = $widget;
            if ($timer) {
                $timer->setInterval(120);
            }
        }
    }

    private function sendInitial()
    {
        if ($this->isInitial()) {
            $r = $this->sendInitialScripts();
            $this->getSpec()->setAttr('data-timer-type', $r['info']['type']);
        }
    }

    private function getInternalID()
    {
        return (string) $this->getContext()->toGuide()->getAnchor();
    }

    private function getSelector()
    {
        return $this->getContext()->toSelector();
    }

    private function getControlId()
    {
        return $this->getProp('control_id');
    }

    private function sendInitialScripts()
    {
        $spec = $this->getSpec();
        $s = $this->getSelector();
        $id = $this->getInternalID();
        $allProps = $spec->getAllProps();
        // $attrs = $spec->getAllAttrs();
        $type = Arrays::getSafe($allProps, 'type', null);
        $tp = [
            'id' => $id,
            // 'selector' => (string) $this,
            // 'tickInterval' => Arrays::getSafe($allProps, 'interval', 0)/1000,
            // 'totalRuns' => 1
            // 'taskInfo' => ['type' => $type]
        ];
        if ($spec->getAttrSafe('disabled') === 'disabled') {
            $tp['enabled'] = false;
        }
        $tickDelay = $spec->getPropSafe('tickDelay');
        if ($tickDelay !== null) {
            $tp['tickDelay'] = $tickDelay;
        }

        /// overlaying
        $map = function ($v) {
            // return $v + 240;
            // return ($v >= 1000) ? $v/1000 : $v;
            // if ($this->getControlId() === '1321366358') {
            //     return 300;
            // }
            return $v;
        };

        $interval = Arrays::getSafe($allProps, 'interval', 0);
        $tp['tickInterval'] = call_user_func($map, $interval);
        if ($type === 'alarm') {
            $tp['totalRuns'] = 1;
        } elseif ($type === 'pulse') {
            $tp['totalRuns'] = 0;
        } else {
            // throw new \DomainException();
            $tp['totalRuns'] = Arrays::getSafe($allProps, 'totalRuns', 1);
        }
        $p = [
            'taskParams' => $tp,
            'info' => [
                'selector' => (string) $s,
                'type' => $type,
                'formSelector' => $this->getFormSelector()
            ]
        ];
        $this->send(
            $this->callJS('$App.Sched.spool', $p)
        );
        return $p;
    }

    private function callJS()
    {
        $args = func_get_args();
        $v = array_slice($args, 1);
        $outp = [];
        foreach ($v as $m) {
            $outp[] = json_encode($m);
        }
        return sprintf('%s(%s);', $args[0], implode(', ', $outp));
    }

    private function getFormSelector()
    {
        return (string) $this->getForm()->getProp('target');
    }
}
