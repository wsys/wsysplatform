<?php
/**
 * AbstractButton.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;
use WSPLRTL\Access\ValueHost as ValueHostAccess;
use WSPLRTL\Element\ValueHostAddition;
use Nodelimit\Access\Widget\Feature\Caption;
use WSPLRTL\Element\Addition\Feature\Caption as CaptionFeatureAddition;
use WSPLRTL\Element\AbstractControl;

/**
 * AbstractButton.php
 *
 * @class WSPLRTL\Element\AbstractButton
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractButton extends AbstractControl implements ValueHostAccess, Caption
{
    use ValueHostAddition;
    use CaptionFeatureAddition;
}
