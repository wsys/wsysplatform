<?php
/**
 * @file
 *
 * @brief MarkupMarkdown.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Markup\MarkupMarkdown
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Markup;

// use Namedio\Core\Edge\Util\Registry;
use WSPLRTL\Element\Markup\AbstractMarkup;
use Parsedown;
use Namedio\Ext\Arrayed\FluentArray;
use Namedio\Ext\Markdown\Preprocess\Preprocess;
use Namedio\Ext\Markdown\MarkdownUtils;
use Namedio\Ext\Markdown\Markdown;


/**
 * The MarkupMarkdown class
 *
 * Description
 *
 * @class WSPLRTL\Element\Markup\MarkupMarkdown
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
class MarkupMarkdown extends AbstractMarkup
{
    private $renderer;

    public function renderMarkup()
    {
        return $this->markup($this->getProp('markup', ''));
    }

    private function markup($markup)
    {
        // return $this->getRenderer()->text($markup);
        return $this->callRenderer($this->getRenderer(), [$this->preprocess($markup)]);
    }

    private function preprocess($markup)
    {
        $conf = [
            /// 'wsys.doc.alias'
            /// @note can be shown only document full path or document home
            'path' => $this->getInternal('path')
        ];
        // self::console($conf);
        return Preprocess::run($markup, $conf);
    }

    private function getInternal($k, $context = null)
    {
        if ($k === 'path') {
            $alias = null;
            if ($this->options()->has('wsys.doc.alias')) {
                // return Registry::getInstance()->get('wsys.doc.map')[$this->getParams()->get('wsys.doc.alias')]['real.path'];
                $alias = $this->options()->get('wsys.doc.alias');
            } else {
                $alias = MarkdownUtils::getMeta($context, 'wsys.doc.alias');
            }
            if ($alias) {
                // $map = Registry::getInstance()->get('wsys.doc.map');
                // $map[$alias]['path']
                /// @todo empty alias: fallback
                $section = 'handbook';
                $key = "wsys.doc.{$section}.{$alias}.root";
                return $this->getEnv($key);
            } else {
                /// @todo auto detect
                /// @todo auto detect when no file: default path (/app/file? ext/res /app/db/query (example))
                return null;
            }
        }
    }

    private function getEnv($key)
    {
        /// how to get relative path from full
        return $this->getForm()->lib('env')->rget($key);
    }

    private function callRenderer(callable $cb, array $args)
    {
        /// var_dump(preg_match('/\[(?P<name>:\w+)\]: # [\'](?P<value>.*)[\']/', $s, $out));
        return call_user_func_array($cb, $args);
    }

    private function getRenderer()
    {
        return (isset($this->renderer)) ? $this->renderer : ($this->renderer = $this->createRenderer());
    }

    private function createRenderer()
    {
        return [
            Markdown::CLASS_NAME,
            'render'
        ];
    }

    private function createRendererOld()
    {
        return [
            new Parsedown(),
            'text'
        ];
    }
}
