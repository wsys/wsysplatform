<?php
/**
 * @file
 *
 * @brief MarkupHTML.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Markup\MarkupHTML
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Markup;

use WSPLRTL\Element\Markup\AbstractMarkup;
use Namedio\Ext\Arrayed\FluentArray;

/**
 * The MarkupHTML class
 *
 * Description
 *
 * @class WSPLRTL\Element\Markup\MarkupHTML
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
class MarkupHTML extends AbstractMarkup
{
    public function renderMarkup()
    {
        return $this->getMarkup();
    }

    public function emptyNode($selector = '> *')
    {
        $s = $this->getSelector() . ' ' . $selector;
        $this->send(
            <<<EOD
$('$s').empty();
EOD
        );
    }

    private function getSelector()
    {
        return $this->getContext()->toSelector();
    }
}
