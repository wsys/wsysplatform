<?php
/**
 * @file
 *
 * @brief AbstractMarkup.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Markup\AbstractMarkup
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element\Markup;

use WSPLRTL\Element\AbstractControl;
use com\danscode\wf\html\element\Panel as GenericPanel;
use Namedio\Core\Edge\Lang\Arrays\Arrays;

// use Namedio\Ext\Arrayed\FluentArray;

/**
 * The AbstractMarkup class
 *
 * Description
 *
 * @class WSPLRTL\Element\Markup\AbstractMarkup
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
abstract class AbstractMarkup extends AbstractControl
{

    private static $_my_props = [];
    private static $_my_attrs = [
        'class' => '', 'markup' => ''
    ];
    private static $_store_props = ['markup'];
    private static $_store_attrs = [];

    public function __construct($props = [], $attrs = [], $context = null)
    {
        $this->registerStoreProps(self::$_store_props);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        return $this->panelize($this->renderMarkup());
    }

    public function setMarkup($markup)
    {
        $this->setProp('markup', $markup);
    }

    public function refresh()
    {
        $this->wired()->updateContent($this->renderMarkup());
    }

    public function getRenderMarkup()
    {
        return $this->renderMarkup();
    }

    abstract public function renderMarkup();

    protected function getMarkup()
    {
        $m = Arrays::getSafe($this->getProps(), 'markup', []);
        return $m;
    }

    private function panelize($input)
    {
        $p = $this->getProps();
        $p['body'] = $input;
        $attrs = $this->getAttrs();
        // self::logTraceDump(
        //     $p,
        //     $attrs
        // );
        // self::console(__METHOD__, get_class($this), $this->getMarkup(), $input);
        return new GenericPanel($p, $attrs);
    }
}
