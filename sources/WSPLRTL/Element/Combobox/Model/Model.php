<?php
/**
 * Model.php
 *
 * @page WSPLRTL\Element\Combobox\Model\Model
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Combobox\Model\Model
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element\Combobox\Model package
 */
namespace WSPLRTL\Element\Combobox\Model;

use WSPLRTL\Element\Combobox\Access\Defs as ComboboxDefsAccess;
use com\danscode\traits\Debug;

/**
 * Model.php
 *
 * Description
 *
 * @class WSPLRTL\Element\Combobox\Model\Model
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element\Combobox\Model\Model package
 */
class Model
{
    use Debug;

    private $_director;
    private $_data;

    public function __construct($director)
    {
        $this->_director = $director;
    }

    public static function createStorage($originator)
    {
        static $_data = [
            ['id' => 0, 'text' => 'элемент 0 item'],
            ['id' => 1, 'text' => 'элемент 1 item'],
            ['id' => 2, 'text' => 'элемент 2 item'],
            ['id' => 3, 'text' => 'элемент 3 item'],
            ['id' => 4, 'text' => 'элемент 4 item'],
            ['id' => 5, 'text' => 'элемент 5 item'],
            ['id' => 6, 'text' => 'элемент 6 item'],
            ['id' => 7, 'text' => 'элемент 7 item']
        ];
        return new \ArrayObject([]);
    }

    public function getData()
    {
        return $this->_getData();
    }

    public function ls()
    {
        return $this->_getArrayCopy();
    }

    /**
     * Returns result of query by search string
     *
     * Performs filtering elements from data with search string from $q
     *
     * @param string $q search text
     *
     * @return array of found (filtered) all elements. The element is
     * associative array with keys: 'text', 'id'
     */
    public function query($q)
    {
        return $this->_filter($this->_getArrayCopy(), $q, 'text');
    }

    public function findBy($key, $v)
    {
        return $this->_accept($this->_queryBy($v, $key));
    }

    public function getCount()
    {
        return $this->_getData()->count();
    }

    public function clear()
    {
        // self::logger(__METHOD__, __LINE__, $this->getValueKey(), $this->getCount(), $this->getFirstText());
        $this->_clear();
    }

    public function addItem($item, $key = false)
    {
        // self::logger(__METHOD__, __LINE__, $this->getValueKey(), $key, $item);
        $this->_getData()[] = $this->_createElement($item, $key);
    }

    public function addItemWithTag($item, $tag = null)
    {
        $this->_getData()[] = $this->_createElement($item, false, $tag);
    }

    public function removeItem($index)
    {
        $this->removeItemAt($index);
    }

    public function removeItemAt($index)
    {
        $this->_getData()->offsetUnset($index);
        $this->_reindex();
    }

    public function getItem($index)
    {
        return $this->getItemAt($index);
    }

    public function getItemAt($index)
    {
        return $this->_getText($this->_getElement($index));
    }

    public function getItemField($index, $name)
    {
        return $this->_getElement($index)[$name];
    }

    public function hasItemAt($index)
    {
        return $this->hasItemIndex($index);
    }

    public function hasItemIndex($index)
    {
        return $this->_getData()->offsetExists($index);
    }

    /**
     * Finds first item from data by search text
     *
     * @details
     *
     * @~English Performs detailed search among the entries of the list
     * and returns the first found index of matching element.  @~
     *
     * @~Russian Выполняет точный поиск среди элементов данных списка и
     * возвращает индекс первого найденного элемента.  @~
     *
     * @param string $item
     *
     * @return array with two elements:
     * - bool whether found or not
     * - mixed int|bool found index by text from first item or FALSE if not found
     */
    public function findItemIndex($item)
    {
        return $this->_findItem($item);
    }

    public function getFirstText()
    {
        if ($this->hasItemIndex(0)) {
            return $this->getItem(0);
        }
        return '';
    }

    private function getValueKey()
    {
        return $this->_director->getComponent()->getValueKey();
    }


    private function _getElement($index)
    {
        // self::console(__METHOD__, $this->_getData(), $index);
        return $this->_getData()->offsetGet($index);
    }

    private function _queryBy($v, $key)
    {
        return $this->_query($this->_getArrayCopy(), $v, $key);
    }

    /**
     * Finds first item from data by search text
     *
     * @param string $item
     *
     * @return array with two elements:
     * - bool whether found or not
     * - mixed int|bool found index by text from first item or FALSE if not found
     */
    private function _findItem($item)
    {
        return $this->_find([$this, '_findItemAll'], [$item]);
    }

    private function _find(callable $cb, $args = [])
    {
        return $this->_accept(call_user_func_array($cb, $args));
    }

    private function _accept($r)
    {
        if (empty($r)) {
            return [false, false];
        } else {
            return [true, $r[0]['id']];
        }
    }

    private function _findItemAll($item)
    {
        $q = $this->query($item);
        $out = [];
        foreach ($q as $elem) {
            $out[] = $elem;
        }
        return $out;
    }

    private function _reindex()
    {
        $save = $this->_getArrayCopy();
        $this->_clear();
        foreach ($save as $elem) {
            $this->addItem($this->_getText($elem));
        }
    }

    private function _getText($elem)
    {
        return $elem['text'];
    }

    private function _clear()
    {
        $this->_getData()->exchangeArray([]);
        // self::console(
        //     __METHOD__,
        //     $out
        // );
        // self::logger(__METHOD__, $this->_director->getComponent()->getWID());
    }

    private function _getArrayCopy()
    {
        return $this->_getData()->getArrayCopy();
    }

    private function _createElement($item, $key = false, $tag = null)
    {
        return [
            'id' => $this->_getLastIndex(),
            'key' => $key,
            'tag' => $tag,
            'text' => $item
        ];
    }

    private function _getLastIndex()
    {
        return $this->getCount();
    }

    private function _query(array $rows, $q, $k)
    {
        return $this->_queryCall($rows, $q, $k, [$this, '_cmpBy']);
    }

    private function _filter(array $rows, $q, $k)
    {
        return $this->_queryCall($rows, $q, $k, [$this, '_contains']);
    }

    private function _queryCall(array $rows, $q, $k, callable $cb)
    {
        $out = [];
        foreach ($rows as $elem) {
            if (call_user_func($cb, $elem, $q, $k)) {
                $out[] = $elem;
            }
        }
        return $out;
    }

    /**
     * Perform compare by key
     *
     * @param array $elem
     * @param mixed $q int|string search criteria
     * @param mixed $k int|string field name
     *
     * @warning mb_stripos is important for filetring queries when input
     * for filtering at client side
     *
     * @warning this search position of a substring $q in field by $k
     *
     * @todo make separated method for exact search
     *
     * @return boolean TRUE if found
     */
    private function _cmpBy(array $elem, $q, $k)
    {
        return ($elem[$k] === $q);
    }

    private function _contains(array $elem, $q, $k)
    {
        return !(mb_stripos($elem[$k], $q) === false);
    }

    private function _getData()
    {
        if ($this->_data) {
            return $this->_data;
        } else {
            return ($this->_data = $this->_openData());
        }
    }

    private function _openData()
    {
        return $this->_director->getSpec()->getProp(ComboboxDefsAccess::K_MSTORAGE);
    }
}
