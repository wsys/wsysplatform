<?php
/**
 * Bootstrap.php
 *
 * @page WSPLRTL\Element\Combobox\Action\Model\Bootstrap
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Combobox\Action\Model\Bootstrap
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element\Combobox\Action\Model package
 */
namespace WSPLRTL\Element\Combobox\Action\Model;

use Nodelimit\App\Action\AbstractAction;

use WSPLRTL\Element\Combobox\Access\Defs as ComboboxDefsAccess;

use WSPLRTL\Element\Combobox\Model\Model;

/**
 * Bootstrap.php
 *
 * Description
 *
 * @class WSPLRTL\Element\Combobox\Action\Model\Bootstrap
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element\Combobox\Action\Model package
 */
class Bootstrap extends AbstractAction
{
    public function run()
    {
        return $this->_createModel();
    }

    /**
     * Prepare model data
     *
     * Perform initialize model storage
     *
     * @param object $originator
     *
     * @todo specify SharedStorage provider access in originator
     * 
     * @return void
     */
    public static function prepare($originator)
    {
        $originator->getSpec()->setProp(
            ComboboxDefsAccess::K_MSTORAGE, self::_makeStorage($originator)
        );
    }

    private static function _makeStorage($originator)
    {
        return Model::createStorage($originator);
    }

    private function _createModel()
    {
        return new Model($this->getSubject());
    }
}