<?php
/**
 * Defs.php
 *
 * @page WSPLRTL\Element\Combobox\Access\Defs
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Combobox\Access\Defs
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element\Combobox\Access package
 */
namespace WSPLRTL\Element\Combobox\Access;

/**
 * Defs.php
 *
 * Description
 *
 * @interface WSPLRTL\Element\Combobox\Access\Defs
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element\Combobox\Access package
 *
 * @section EVSOCK_RPC
 * Indicate onto AJAX-query with the specified Event::type.
 * Query used to perform operations such as: Remote data list, translating
 * selected item
 */
interface Defs
{
    const EVSOCK_RPC = 'combobox:rpc'; /**< @see @link EVSOCK_RPC @endlink */
    const EVCUST_MEDIATOR = 'ev.cust.mediator.cb';

    /// set of elements props
    const K_MSTORAGE = 'mstorage'; /**< storable models data */
}
