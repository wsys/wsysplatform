<?php
/**
 * Select2.php
 *
 * @page WSPLRTL\Element\Combobox\View\Select2
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Combobox\View\Select2
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element\Combobox\View package
 */
namespace WSPLRTL\Element\Combobox\View;

use Nodelimit\Forge\View\AbstractView;
use Nodelimit\WF\Component\Control\Vendor\Select2\Element as Select2Element;

use Nodelimit\Exception\Raise;
use Nodelimit\WF\Systype\Postback\Event\Socket as EventSocket;

use WSPLRTL\Element\Combobox\Access\Defs;
use Nodelimit\WF\Element\Input\Hidden as ElementInputHidden;
use Nodelimit\App\Attend\AttendTrait;
// use Namedio\Ext\Reg\RegTrait;

/**
 * Select2.php
 *
 * Description
 *
 * @class WSPLRTL\Element\Combobox\View\Select2
 *
 * @todo proposal: header-access-interface to keys such as evsock
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element\Combobox\View package
 */
class Select2 extends AbstractView implements Defs
{
    // use RegTrait;
    use AttendTrait;

    const EVSOCK_HANDLER = '_dispatchEventSocket';

    // public function getRegStorage($spec)
    // {
    //     return $this->_getComponent()->getRegStorage($spec);
    // }

    public function getOriginReg()
    {
        return $this->_getComponent()->getForm()->reg(':sys');
    }

    public function getOriginSpec()
    {
        return $this->_getComponent()->getOriginSpec();
    }

    public function render()
    {
        return $this->_makeBody();
    }

    public function reload()
    {
        self::logger(
            __METHOD__,
            __LINE__,
            $this->_getComponent()->getValueKey()
        );

        $delay = 0;

        $code =<<<EOT
_.delay(
  function () {
    $('%1\$s').select2('search', '');
    $('%1\$s').select2('close');
  },
  $delay
);
EOT;

        $this->wired()->vprintf(
            $code,
            [$this->_getImpTarget()]
        );
    }

    public function refresh()
    {
        $c = $this->_getComponent();
        // self::logger(
        //     __METHOD__,
        //     $c->isEventing() ? 'eventing' : 'involved',
        //     $c->getRefresh(),
        //     $c->getValueKey()
        // );
        $ignore = ($c->getRefresh() === 'ignore');
        $eventing = $c->isEventing();
        $reload = $eventing || $ignore;
        $reload = false;
        if ($reload) {
            $this->reload();
        } else {
            $this->refreshChange();
        }
    }

    public function refreshChange()
    {
        $c = $this->_getComponent();
        // self::logger(
        //     __METHOD__,
        //     $c->isEventing() ? 'eventing' : 'involved',
        //     $c->getRefresh(),
        //     $c->getValueKey()
        // );
        $this->change();
    }

    public function change()
    {
        // $attend = $this->spoolAttend($this->_getComponent(), 'change');
        $attend = true;
        // self::logger(
        //     __METHOD__,
        //     __LINE__,
        //     $this->_getComponent()->getValueKey(),
        //     var_export($attend, true)
        // );

        if (!$attend) {
            return false;
        }

        // return;
//         $code =<<<'EOT'
// window.setTimeout(
//   function () {
//     $('%s').trigger('change', ['refresh']);
//   },
//   500
// );
// EOT;

        $delay = 0;

        $code =<<<EOT
_.delay(
  function () {
    $('%s').trigger('change', ['refresh']);
  },
  $delay
);
EOT;

        $this->wired()->vprintf(
            $code,
            [$this->_getImpTarget()]
        );
    }

    public function refreshLegacy()
    {
        $this->wired()->vprintf(
            '$(\'%s\').select2(\'%s\');',
            [$this->_getImpTarget(), 'updateResults']
        );
    }

    /**
     * Get target
     *
     * @todo proposal: rename from _getImpTarget to _getTarget
     *
     * @return  mixed target
     */
    private function _getImpTarget()
    {
        // self::logTrace($this->getContext()['target']);
        return $this->getContext()['target'];
    }

    private function _makeBody()
    {
        return [
            // $this->_createEventServer(),
            $this->_makeElement()
        ];
    }

    private function _createEventServer()
    {
        $t = WF::target();
        $elem = $this->_makeHiddenElement($t);
        $this->getElement()->getForm()->addEventListener();
        return $elem;
    }

    private function _makeElement()
    {
        list($props, $attrs) = $this->_makeElementArgs();
        return new Select2Element($props, $attrs);
    }

    private function _makeHiddenElement($target, $value = '')
    {
        $attrs = ['value' => $value];
        $p = [
            'target' => $target
        ];
        return new ElementInputHidden($p, $attrs);
    }

    private function _makeElementArgs()
    {
        /// @note attendInfo created primary here
        $attendInfo = [
            'formTarget' => $this->_getForm()->getTargetEx(false)
        ];
        $props = [
            'evsock' => $this->_makeEventSocket(self::EVSOCK_RPC),
            'target' => $this->_getImpTarget(),
            'evinfo_target' => $this->getContext()['evinfo_target'],
            'change_trigger' => $this->getContext()['change_trigger'],
            'attendInfo' => $attendInfo
        ];
        $attrs = $this->getElement()->getAttrs();
        return [$props, $attrs];
    }

    private function _makeEventSocket($what)
    {
        return EventSocket::create($this->_makeEventSpec($what));
    }

    private function _makeEventSpec($what)
    {
        return $this->_getForm()->createEventSpec(
            $what, $this->_getComponent(), $this->_makeMethodName($what)
        );
    }

    /**
     * Create method name
     *
     * Make method name for handling EventSocket events
     *
     * @param string $what type or subject of event (Event::what)
     *
     * @note assume that name used for EventSocket events here
     * @note assume that self::EVSOCK_HANDLER is present in form
     *
     * @return string method name that handling EventSocket events
     */
    private function _makeMethodName($what = self::EVSOCK_RPC)
    {
        return self::EVSOCK_HANDLER;
    }

    private function _getForm()
    {
        return $this->getElement()->getForm();
    }

    private function _getComponent()
    {
        return $this->getContext()['component'];
    }
}
