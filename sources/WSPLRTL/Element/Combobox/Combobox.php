<?php
/**
 * Combobox.php
 *
 * @page WSPLRTL\Element\Combobox\Combobox
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Element\Combobox\Combobox
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Element\Combobox package
 */
namespace WSPLRTL\Element\Combobox;

use WSPLRTL\Element\AbstractElement;

use com\danscode\wf\component\sys\view\CouplingViewAccess;

use com\danscode\lib\Props;

use WSPLRTL\Element\Combobox\Action\Model\Bootstrap as ActionModelBootstrap;

use WSPLRTL\Element\Combobox\Access\Defs as ComboboxDefsAccess;

use com\danscode\wf;

use WSPLRTL\Element\AbstractControl;

/**
 * Combobox.php
 *
 * Description
 *
 * @class WSPLRTL\Element\Combobox\Combobox
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Element\Combobox package
 */
class Combobox extends AbstractControl
    implements CouplingViewAccess
{

    const CNAME_SELECT2 = 'WSPLRTL\Element\Combobox\View\Select2';

    private static $_my_props = [];

    /// no needed store target - its done by attachWidget
    private static $_store_props = [
        ComboboxDefsAccess::K_MSTORAGE,
        'evinfo_target', /**< @warning duplicated as in select2 */
        'change_trigger'
    ];

    private $_mbootstrap;       /**< helper to serving model */

    public function __construct($props = [], $attrs = [], $context = null)
    {
        /// @todo options setSelectedItemIndex refresh and no refresh when unselect
        /// @todo options setSelectedItemIndex refresh and refresh when unselect
        /// @todo options setSelectedItemIndex no refresh and refresh when unselect
        /// @todo options setSelectedItemIndex no refresh and no refresh when unselect
        $this->options()->set('sync', 'auto');
        $this->registerStoreProps(self::$_store_props);
        $this->mergeMyVars(self::$_my_props);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        $out = $this->makeView()->render();
        /// @todo move at system common level
        $this->getComponent()->emit('widget.render.after');
        return $out;
    }

    /**
     * Set selected index to element
     *
     * @param mixed $index
     *
     * @todo for never versions of select2 this code not valid. Hint:
     * check implementor and use another code
     *
     * @warning this action not perform refresh
     *
     * @return void
     */
    public function setSelectedIndex($index)
    {
        // $this->getSpec()->wired()->vprintf(
        //     '$(\'%s\').select2(\'val\', %s);',
        //     [$this->getProp('target'), $index]
        // );
        $this->getSpec()->wired()->vprintf(
            '$(\'%s\').val(%s);',
            [$this->getProp('target'), $index]
        );
    }


    public function refresh()
    {
        $this->makeView()->refresh();
    }

    public function unselect($refresh = true)
    {
        $aux = '';
        // $change = $this->options()->getSafe('autosync', true);
        $change = false;
        if ($refresh) {
            $change = true;
        }
        if ($change) {
            $aux = ".trigger('change', ['unselect'])";
        }

        // $this->getSpec()->wired()->vprintf(
        //     "\$('%s').select2('val', null)${aux};",
        //     [$this->getProp('target')]
        // );

        $this->getSpec()->wired()->vprintf(
            "\$('%s').val(null)${aux};",
            [$this->getProp('target')]
        );
    }

    public function getViewContext($sender)
    {
        // self::console($this->getAttrs());
        $base = Props::filterKeys(
            $this->getProps(),
            [
                'target',
                'evinfo_target',
                'change_trigger'
            ]
        );
        // self::logTrace($this->getProp('target'));
        $c = $this->getComponent();
        // $dump = [
        //     'key' => $c->getValueKey(),
        //     'target' => (string) $this->getProp('target'),
        //     'evinfo_target' => (string) $this->getProp('evinfo_target')
        // ];
        // self::console(__METHOD__, $dump);
        return $base + [
            'component' => $c
        ];
    }

    public function getModel()
    {
        return $this->_bootstrapModel();
    }

    public function isTriggered()
    {
        $ret = false;

        if (isset($_POST['eventInfo']['context'])) {
            // self::vdump($_POST['eventInfo']['context']);
            $v = $_POST['eventInfo']['context'];
            $ret = !empty($v);
        }

        // self::console(
        //     __METHOD__,
        //     (string) $this->getChangeTriggerTarget(),
        //     $this->getComponent()->getValueKey(),
        //     $ret,
        //     $_POST
        // );

        return $ret;
    }

    public function isTriggeredLegacy()
    {
        $ret = null;
        $k = $this->getChangeTriggerTargetValueKey();
        $v = 'n/a';
        $ts = 'n/a';
        $c = 'n/a';
        if (isset($_POST[$k])) {
            $c = json_decode($_POST[$k], true);
            if (isset($c['changeInfo'])) {
                $ret = true;
                $v = $c['changeInfo'];
            } else {
                /// special case: assume its triggered from postback
                $ret = true;
            }
            if (isset($c['timeStamp'])) {
                $ts = $c['timeStamp'];
            }
        }
        self::console(
            __METHOD__,
            $ret,
            $k,
            $v,
            (string) $this->getChangeTriggerTarget(),
            $this->getComponent()->getValueKey(),
            $ts,
            $c,
            $_POST
        );
        return $ret;
    }

    /**
     * Perform initiate of this element
     *
     * @warning no needed initialize target here - its done by attachWidget
     *
     * @see WSPLRTL\Widget\AbstractForm::attachWidget
     *
     * @return void
     */
    protected function initiate()
    {
        ActionModelBootstrap::prepare($this);
        $this->setProp('evinfo_target', WF::target());

        $changeTriggerTarget = WF::target();
        $this->setProp(
            'change_trigger',
            [
                'selector' => $changeTriggerTarget
            ]
        );
    }

    protected function makeView()
    {
        $cname = self::CNAME_SELECT2;
        return new $cname($this);
    }

    private function _bootstrapModel()
    {
        if ($this->_mbootstrap) {
            return $this->_mbootstrap;
        } else {
            return ($this->_mbootstrap = $this->_bootstrapModelCreate());
        }
    }

    private function _bootstrapModelCreate()
    {
        return ActionModelBootstrap::bootstrap($this);
    }

    private function getChangeTriggerTarget()
    {
        return $this->getProp('change_trigger')['selector'];
    }

    private function getChangeTriggerTargetValueKey()
    {
        // $elem = $this;
        // $c = $elem->getContext();
        // list($elem_props, $elem_attrs) = $elem->reflect();
        // $anchor = $c->toBasenameAnchor();
        // $id = $c->toBasenameID();
        // return $anchor . '%' . $id;

        $t = WF::target(
            (string) $this->getChangeTriggerTarget()->toClassName()
        );
        $k = $t->toGuide()->toBasenameAnchor();
        return $k;
    }
}
