<?php
/**
 * DateEdit.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;
use Nodelimit\Polyfill\Webshim\Date\Element as DateElement;
use com\danscode\wf as WF;
use WSPLRTL\Access\ValueHost as ValueHostAccess;
// use WSPLRTL\Element\ValueHostAddition;
use WSPLRTL\Element\AbstractControl;
use WSAPL\Page\Instant\Stand\Date\Date;
use com\danscode\wf\html\element\input\Text as GenericEdit;
use WSPLRTL\Element\Edit;

/**
 * DateEdit.php
 *
 * @todo move ValueHost functionality onto AbstractValueHost
 *
 * @class WSPLRTL\Element\DateEdit
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
// class DateEdit extends AbstractControl implements ValueHostAccess
class DateEdit extends Edit
{
    private static $_my_props = [];
    private static $_my_attrs = [];
    private static $_store_props = [];
    private static $_store_attrs = [];

    // use ValueHostAddition;

    public function __construct($props = [], $attrs = [], $context = null)
    {
        //$props['target'] = WF::idtarget($props['id']);
        $this->mergeMyVars(self::$_my_props, self::$_my_attrs);
        parent::__construct($props, $attrs, $context);
    }

    public function render()
    {
        // return $this->_render();
        $p = $this->getProps();
        $attrs = $this->getAttrs();
        $attrs['data-format'] = 'dd-MM-yyyy';
        // $attrs['data-format'] = '{"month":"short","year":"numeric","day":"numeric"}';
        // $attrs['data-polyfill'] = 'all';
        /// @warning issues with firefox 52 for windows xp - polyfill disabled completely
        $attrs['data-polyfill'] = 'none';
        // $attrs['lang'] = 'ru';
        return new DateElement(
            [
                'target' => $p['target']
            ],
            $attrs
        );
    }

    private function _render()
    {
        $pout = $this->makeBaseProps();
        return new GenericEdit(
            $pout,
            $this->getAttrs()
        );
    }

    // public function setValue($value)
    // {
    //     self::console(__METHOD__, 'noop');
    // }
}
