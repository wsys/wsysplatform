<?php
/**
 * AbstractControl.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Element;

use WSPLRTL\Element\AbstractElement;

/**
 * AbstractControl.php
 *
 * description
 *
 * @class WSPLRTL\Element\AbstractControl
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
abstract class AbstractControl extends AbstractElement
{
    /**
     * Creates basic props for element
     *
     * @param array $clone tells which props to copy
     *
     * @todo move readonly to relevent level
     *
     * @return array of basic props
     */
    protected function makeBaseProps($clone = [])
    {
        $p = $this->getProps();
        $pout = parent::makeBaseProps($clone);
        if (isset($p['disabled'])) {
            $pout['disabled'] = $p['disabled'];
        }
        if (isset($p['readonly'])) {
            $pout['readonly'] = $p['readonly'];
        }
        return $pout;
    }
}
