<?php
/**
 * @file
 *
 * @brief Dblclick.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Dblclick
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;

/**
 * The Dblclick class
 *
 * Description
 *
 * @class WSPLRTL\Event\Dblclick
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Dblclick extends AbstractEvent
{
}
