<?php
/**
 * @file
 *
 * @see WSPLRTL\Event\AbstractEvent
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use com\danscode\systype\Arguments;
use Nodelimit\Systype\Dict;

use com\danscode\traits\Debug;

/**
 * The AbstractEvent class
 *
 * @details
 *
 * @par member: $this->sender | $ev->sender
 *
 * @parblock
 * see @link WSPLRTL\Event\AbstractEvent::__get() @endlink<br>
 * see @link WSPLRTL\Event\AbstractEvent::getSender() @endlink
 * @endparblock
 *
 * @par member: $this->data | $ev->data
 *
 * @parblock
 * see @link WSPLRTL\Event\AbstractEvent::__get() @endlink
 * @endparblock
 *
 * @class WSPLRTL\Event\AbstractEvent
 *
 * @see use WSPLRTL\Systype\SentVars;
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractEvent
{
    use Debug;

    private $_sentvars;         /**< object WSPLRTL\Access\SentVars */
    private $_sender;           /**< object com\danscode\systype\LazyInit
                                   * @see getSender */
    private $_data;             /**< mixed NULL in most cases */
    private $type;             /**< string type of event */

    final private function __construct()
    {
        //
    }

    public static function create()
    {
        $args = new Arguments(func_get_args(), 1, 1);
        switch ($args->check()) {
        case 1:
            $arg1 = $args[0];
            if (is_array($arg1)) {
                return static::createFromArray($arg1);
            } else {
                throw new \UnexpectedValueException('unknown type');
            }
        default:
            throw new \LogicException();
        }
    }

    public static function createFromArray(array $a)
    {
        $o = new static();
        $d = new Dict($a);
        $o->_data = $d->getSafe('data');
        $o->_sentvars = $d['sentvars']; /**< WSPLRTL\Systype\SentVars */
        $o->_sender = $d->getSafe('sender');
        $o->type = $d->getSafe('type');
        $o->init();
        return $o;
    }

    /**
     * Magic method property overloading
     *
     * @param string $name can be: 'data', 'sender'
     *
     * @note $ev->sender WSPLRTL-API-User
     * @note $ev->data WSPLRTL-API
     * @note in the paragraph about description of $this->sender (or
     * field data) $this meaning that is a keyword to refer to
     * AbstractEvent the object. Not any other object. <br> Also used
     * $ev variable as typical sample of code. It describes the same
     * thing. <br> Hereinafter will be used this agreement.
     *
     * @par member: $this->sender | $ev->sender
     *
     * @parblock
     * **type:** mixed $this->sender<br> see also @link
     * WSPLRTL\Event\AbstractEvent::getSender() @endlink
     * <br>read-only
     * @endparblock
     *
     * @parblock
     * @~English $this->sender is a short form for calling
     * getSender. Handling the form $this->sender or eg $ev->sender is
     * equivalent to calling $this->getSender() or $ev->getSender() @~
     *
     * @~Russian $this->sender представляет собой сокращённую форму для
     * вызова метода getSender. Обращение вида $this->sender или
     * например $ev->sender равносильно вызову $this->getSender() или
     * $ev->getSender() @~
     * @endparblock
     *
     * @par member: $this->data | $ev->data
     *
     * @parblock
     * **type:** mixed $this->data
     *   - in most cases this field is NULL
     *   - in some cases this field can contains event-related data
     * <br>read-only
     * @endparblock
     *
     * @see http://php.net/manual/en/language.oop5.overloading.php#object.get
     * @see WSPLRTL\Event\AbstractEvent::getSender()
     *
     * @throws \UnexpectedValueException when unknown name
     *
     * @return mixed depending on the $name it is string or object
     */
    public function __get($name)
    {
        switch ($name) {
        case 'type':
            return $this->type;
        case 'data':
            return $this->_data;
        case 'sender':
            return $this->_getSender();
        default:
            throw new \UnexpectedValueException('unknown property ' . $name);
        }
    }

    /**
     * This is preset magic method for isset(). Is triggered by calling isset() or
     * empty() on inaccessible properties.
     *
     * @details Determine if a variable is set and is not NULL.
     *
     * @param string $name can be 'data'|'sender'
     *
     * @todo proposal: implement isset checking when value is wrapped in
     * lazy loader (as sender)
     *
     * @see http://php.net/manual/en/function.isset.php
     *
     * @return bool
     */
    public function __isset($name)
    {
        switch ($name) {
            case 'type':
                return isset($this->type);
            case 'data':
                return isset($this->_data);
            case 'sender':
                $sender = $this->_getSender();
                return isset($sender);
            default:
                throw new \UnexpectedValueException('unknown property ' . $name);
        }
    }

    /**
     * Magic method stub to blocking by exception when performing
     * changing properties on inaccessible properties
     *
     * This method just throws exception to prevent changing any public
     * properties. For example: if try $ev->sender = null or another
     * operand exception will throwed.
     *
     * @param string $name
     * @param mixed $value
     *
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        throw new \Exception('read-only property');
    }

    /**
     * Magic method stub to blocking by exception when performing
     * unsetting on inaccessible properties
     *
     * This method just throws exception to prevent changing on
     * inaccessible properties. For example: if try unset($ev->sender)
     * or another operand exception will throwed.
     *
     * @param string $name
     *
     * @throws \Exception
     */
    public function __unset($name)
    {
        throw new \Exception('read-only property');
    }


    public function __invoke()
    {
        if (func_num_args()) {
            return $this->q(func_get_arg(0));
        } else {
            throw new \BadMethodCallException();
        }
    }

    /**
     * Returns object with access to transit variables of controls
     * associated with request
     *
     * @note WSPLRTL-API-Internal
     *
     * @deprecated return type. It will changed to WSPLRTL\Access\SentVars
     *
     * @see WSPLRTL\Access\Value\Sent
     * @see WSPLRTL\Access\SentVars
     *
     * @return object WSPLRTL\Systype\SentVars
     */
    public function getSentVars()
    {
        return $this->_sentvars;
    }

    /**
     * Returns a sender associated with the event
     *
     * @deatils
     *
     * @~English Usually the sender points to an object associated with
     * this event. <br>In the vast majority, sender is a reference
     * to the widget which is associated with the event. <br>For
     * example, when the Click event for the button, as the value of the
     * sender is a reference to the widget Button. In the
     * case of processing clicking on the widget Label, Label value
     * sender will, etc. @~
     *
     * @~Russian Обычно sender указывает на объект связанный с данным
     * событием. <br>В подавляющем большинстве, sender представляет
     * собой ссылку на виджет с которым связано событие. <br>Например
     * при обработке события Click на кнопке, в качестве значения sender
     * будет ссылка на виджет Button. В случае обработки клика на
     * виджете Label, значением sender будет Label и т.д. @~
     *
     * @note the sender is almost always available for the associated event
     * @note the first call made lazy loading of sender
     * @note WSPLRTL-API-User
     *
     * @return mixed can be:
     *   - an object as instance of WSPLRTL\Widget\AbstractWidget (in most cases)
     *   - extremely rare and theoretically: object|NULL or any other types
     */
    public function getSender()
    {
        return $this->_getSender();
    }

    /**
     * Query sent value object by id of widget
     *
     * @param string $id this is ID_WCONTROL of any control on the form
     *
     * @note WSPLRTL-API-User
     * @note In most cases after calling this method will called
     * getValue (in current implementation of interface
     * WSPLRTL\Access\Value\Sent is
     * WSPLRTL\Systype\Post\Boxed\Generic::getValue()) for example:<br>
     * \code{.php} $ev->q('id_edit1')->getValue(); \endcode
     *
     * @see WSPLRTL\Access\Value\Sent
     * @see WSPLRTL\Systype\SentVars::q()
     * @see WSPLRTL\Systype\Post\Boxed\Generic
     * @see WSPLRTL\Systype\Post\Boxed\Generic::getValue()
     * @see Nodelimit\Access\Value\Provider\StringType
     * @see Nodelimit\Access\Value\Provider\StringType::getValue()
     * @see Nodelimit\Access\Value\Provider
     * @see Nodelimit\Access\Value\Provider::getValue()
     *
     * @internal access by $id from WSPLRTL\Systype\SentVars object
     * @internal currently returns WSPLRTL\Systype\Post\Boxed\Generic,
     * but it can be changed in next versions
     *
     * @return object with implemented WSPLRTL\Access\Value\Sent interface
     */
    public function q($id)
    {
        return $this->_sentvars->q($id);
    }

    /**
     * Perform validation by calling senders validate method
     *
     * @details
     *
     * @~English Involves a sender to start the server validation logic
     * @~
     *
     * @~Russian Вовлекает отправителя в запуск механизма логики
     * серверной валидации @~
     *
     * @throws \Exception if sender is not available
     *
     * @todo proposal: perform checking availability of sender
     * @todo proposal: add method hasSender for checking availability of sender
     *
     * @note WSPLRTL-API-User
     *
     * @return boolean (TRUE in most cases)
     */
    public function validate()
    {
        return $this->_getSender()->validate();
    }

    /**
     * Get associated object with the event
     *
     * This is just shortcut to $ev->q('id')
     *
     * @return WSPLRTL\Systype\Post\Boxed\AbstractBoxed an instance
     */
    public function own()
    {
        return $this->q($this->_id());
    }

    protected function init()
    {
        // noop
    }

    /**
     * Getting a sender associated with the event
     *
     * Retrieve sender by calling lazy load hook
     *
     * @return mixed can be:
     *   - an object as instance of WSPLRTL\Widget\AbstractWidget
     *   - object|NULL or any other types
     */
    private function _getSender()
    {
        $s = $this->_sender;
        return ((bool) $s) ? $s->subject : null;
    }

    private function _id()
    {
        return $this->_getSender()->getProp('id');
    }
}
