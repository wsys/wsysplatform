<?php
/**
 * @file
 *
 * @brief Click.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Map\Obj\Click
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Map\Obj;

use WSPLRTL\Event\Map\AbstractEvent;

/**
 * The Click class
 *
 * Description
 *
 * @class WSPLRTL\Event\Map\Obj\Click
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Click extends AbstractEvent
{
}
