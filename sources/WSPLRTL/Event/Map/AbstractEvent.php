<?php
/**
 * @file
 *
 * @brief AbstractEvent.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Map\AbstractEvent
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Map;

use WSPLRTL\Event\AbstractEvent as AbstractEventBase;

/**
 * The AbstractEvent class
 *
 * Description
 *
 * @class WSPLRTL\Event\Map\AbstractEvent
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
abstract class AbstractEvent extends AbstractEventBase
{
}
