<?php
/**
 * Change.php
 *
 * @page WSPLRTL\Event\Change
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Change
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Event package
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;

/**
 * Change.php
 *
 * Description
 *
 * @class WSPLRTL\Event\Change
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Event package
 */
class Change extends AbstractEvent
{
}
