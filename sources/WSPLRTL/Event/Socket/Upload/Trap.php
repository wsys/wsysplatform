<?php
/**
 * @file
 *
 * @brief Trap.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Socket\Upload\Trap
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Socket\Upload;

use WSPLRTL\Event\AbstractEvent;

/**
 * The Trap class
 *
 * Description
 *
 * @class WSPLRTL\Event\Socket\Upload\Trap
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Trap extends AbstractEvent
{
}
