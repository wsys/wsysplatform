<?php
/**
 * RPC.php
 *
 * @page WSPLRTL\Event\Socket\Combobox\RPC
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Socket\Combobox\RPC
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Event\Socket\Combobox package
 */
namespace WSPLRTL\Event\Socket\Combobox;

use WSPLRTL\Event\AbstractEvent;

/**
 * RPC.php
 *
 * Description
 *
 * @class WSPLRTL\Event\Socket\Combobox\RPC
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Event\Socket\Combobox package
 */
class RPC extends AbstractEvent
{
}
