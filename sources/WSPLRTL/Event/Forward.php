<?php
/**
 * @file
 *
 * @brief Forward.php
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Forward
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;
use WSPLRTL\Event\Dig;
use com\danscode\wf\selector\Utils as SelectorUtils;

/**
 * The Forward class
 *
 * Description
 *
 * @class WSPLRTL\Event\Forward
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 */
class Forward extends AbstractEvent
{
    private $dig;

    public function dig()
    {
        return ($this->dig ? $this->dig : ($this->dig = new Dig($this->makeDigKey())));
    }

    public function JSON()
    {
        return $this->dig()->JSON();
    }

    private function makeDigKey()
    {
        /// get target from form
        $t = $this->getExchangeTarget();

        /// get base
        // $base = $t->toBasename();

        $split = SelectorUtils::splitSysTempID((string) $t->toClassName());

        self::console(__METHOD__, $split);

        /// compose key
        return $split;
    }

    private function getExchangeTarget()
    {
        return $this->getForm()->getProp('exchangeTarget');
    }

    private function getForm()
    {
        return $this->sender->getForm();
    }
}
