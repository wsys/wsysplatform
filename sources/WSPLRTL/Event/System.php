<?php
/**
 * System.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;

/**
 * System.php
 *
 * description
 *
 * @class WSPLRTL\Event\System
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class System extends AbstractEvent
{
}
