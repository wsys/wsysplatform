<?php
/**
 * @file
 *
 * @brief Mediator.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Custom\Mediator
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Custom;

use WSPLRTL\Event\AbstractEvent;

/**
 * The Mediator class
 *
 * Description
 *
 * @class WSPLRTL\Event\Custom\Mediator
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Mediator extends AbstractEvent
{
}
