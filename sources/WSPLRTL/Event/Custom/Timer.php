<?php
/**
 * @file
 *
 * @brief Timer.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Custom\Timer
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Custom;

use WSPLRTL\Event\AbstractEvent;

/**
 * The Timer class
 *
 * Description
 *
 * @class WSPLRTL\Event\Custom\Timer
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
class Timer extends AbstractEvent
{
}
