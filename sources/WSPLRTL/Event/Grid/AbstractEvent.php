<?php
/**
 * @file
 *
 * @brief AbstractEvent.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Grid\AbstractEvent
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Grid;

use WSPLRTL\Event\AbstractEvent as Base;

/**
 * The AbstractEvent class
 *
 * Description
 *
 * @class WSPLRTL\Event\Grid\AbstractEvent
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
abstract class AbstractEvent extends Base
{
    /**
     * Get selected row
     *
     * @return object of row
     */
    public function debugDraft()
    {
        $origin = $this->_getOrigin();
        $origin->debugDraft();
        return false;
    }

    private function _getOrigin()
    {
        return $this->sender;
    }
}
