<?php
/**
 * @file
 *
 * @brief Click.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Grid\Row\Click
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Grid\Row;

use WSPLRTL\Event\Grid\AbstractEvent;

/**
 * The Click class
 *
 * Description
 *
 * @class WSPLRTL\Event\Grid\Row\Click
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
class Click extends AbstractEvent
{
}
