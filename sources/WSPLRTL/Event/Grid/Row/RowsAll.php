<?php
/**
 * @file
 *
 * @brief RowsAll.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Grid\Row\RowsAll
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event\Grid\Row;

use WSPLRTL\Event\Grid\AbstractEvent;

/**
 * The RowsAll class
 *
 * Description
 *
 * @class WSPLRTL\Event\Grid\Row\RowsAll
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
class RowsAll extends AbstractEvent
{
}
