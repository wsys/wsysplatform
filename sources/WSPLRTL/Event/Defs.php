<?php
/**
 * @file
 *
 * @brief Defs.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Defs
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

/**
 * The Defs interface
 *
 * Description
 *
 * @interface WSPLRTL\Event\Defs
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
interface Defs
{
}
