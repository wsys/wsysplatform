<?php
/**
 * @file
 *
 * @brief Blur.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Blur
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;

/**
 * The Blur class
 *
 * Description
 *
 * @class WSPLRTL\Event\Blur
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Blur extends AbstractEvent
{
}
