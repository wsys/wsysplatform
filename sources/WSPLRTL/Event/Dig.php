<?php
/**
 * @file
 *
 * @brief Dig.php
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Dig
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use Namedio\Core\Edge\Lang\Arrays\Arrays;

/**
 * The Dig class
 *
 * Description
 *
 * @class WSPLRTL\Event\Dig
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 */
class Dig
{
    private $digKey;

    public function __construct($digKey)
    {
        $this->digKey = $digKey;
    }

    public function JSON($assoc = true)
    {
        return new \ArrayObject(json_decode($this->getVal(), $assoc));
    }

    private function getVal()
    {
        return Arrays::getSafe($_POST, $this->getPostKey());
    }

    private function getPostKey()
    {
        return $this->digKey;
    }
}
