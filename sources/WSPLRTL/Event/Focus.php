<?php
/**
 * @file
 *
 * @brief Focus.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Focus
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;

/**
 * The Focus class
 *
 * Description
 *
 * @class WSPLRTL\Event\Focus
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Focus extends AbstractEvent
{
}
