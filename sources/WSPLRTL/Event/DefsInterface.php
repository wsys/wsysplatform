<?php
/**
 * @file
 *
 * @brief DefsInterface.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\DefsInterface
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use Namedio\UI\Grid\Event\DefsInterface as GridDefsInterface;

/**
 * The DefsInterface interface
 *
 * Description
 *
 * @interface WSPLRTL\Event\DefsInterface
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
interface DefsInterface extends GridDefsInterface
{
    const EVDEBUG = 'w.debug';

    /// @note map related events
    const EVMAP_CLICK = 'map:click';
    const EVMAP_OBJ_CLICK = 'map:obj:click';
    const EVMAP_INIT = 'map:init';

    /// clicks on exposed search results
    const EVMAP_SEARCH_RESULTS_CLICK = 'map:search:results:click';

    const EV_FORWARD = 'forward';
}
