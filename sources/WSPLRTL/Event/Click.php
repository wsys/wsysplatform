<?php
/**
 * Click.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;

/**
 * Click.php
 *
 * description
 *
 * @class WSPLRTL\Event\Click
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Click extends AbstractEvent
{
}
