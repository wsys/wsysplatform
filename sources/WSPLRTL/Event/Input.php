<?php
/**
 * @file
 *
 * @brief Input.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\Input
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;

/**
 * The Input class
 *
 * Description
 *
 * @class WSPLRTL\Event\Input
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Input extends AbstractEvent
{
}
