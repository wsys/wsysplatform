<?php
/**
 * @file
 *
 * @brief EventInterface.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Event\EventInterface
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Event;

/**
 * The EventInterface interface
 *
 * Description
 *
 * @interface WSPLRTL\Event\EventInterface
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
interface EventInterface
{
}
