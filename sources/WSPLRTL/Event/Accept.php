<?php
/**
 * Accept.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Event;

use WSPLRTL\Event\AbstractEvent;


/**
 * Accept.php
 *
 * description
 *
 * @class WSPLRTL\Event\Accept
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Accept extends AbstractEvent
{
}
