<?php
/**
 * @file
 *
 * @brief IM.php
 *
 * @copyright Copyright (C) 2021  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Ext\IM\IM
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Ext\IM;

use Nodelimit\App\App;
use Namedio\Ext\Arrayed\FluentArray;
use com\danscode\traits\Debug;
use Namedio\Ext\XMPP\Utils;
use GetClientIp;
use Namedio\Ext\XMPP\WhoItem;
use Namedio\Ext\XMPP\WhoCollection;

/**
 * The IM class
 *
 * Description
 *
 * @class WSPLRTL\Ext\IM\IM
 *
 * @copyright Copyright (C) 2021  The Wsysplatform Development Team
 */
class IM
{
    use Debug;

    public function __construct()
    {
        ///
    }

    public function getUserSessionsInfo($user)
    {
        ///
    }

    /**
     * Gets on-line information
     *
     * Gets on-line information about user sessions based on xmpp
     *
     * @see https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#user-sessions-info
     *
     * @return array
     */
    public function who($all = false)
    {

        if (!App::getInstance()->getSession()->isAuthorized()) {
            return WhoCollection::fromArray([]);
        }

        // $getClientIp = new GetClientIp;
        // $ip = $getClientIp->getClientIp();
        // $longIp = $getClientIp->getLongClientIp();
        // self::console(
        //     $ip,
        //     $longIp,
        //     gethostbyaddr($ip),
        //     $_SERVER['REMOTE_ADDR'],
        //     gethostbyaddr($_SERVER['REMOTE_ADDR'])
        // );

        $app = App::getInstance();

        $defs = [
            'domain' => '192.168.0.90',
        ];

        $conf = [
            'domain' => $app->digConfig(['xmpp', 'domain'], $defs['domain'], 'node')
        ];

        $p = [
            'room' => 'usr',
            'service' => "conference.${conf['domain']}"
        ];

        $c = [];
        $c['xmpp'] = $xmpp = App::getInstance()->xmpp();
        $ls = $xmpp->who($p['room'], $p['service']);
        $c['res'] = $res = App::getInstance()->getWallet('im')->get('im.res');

        // $out = $ls;
        $c['usr'] = App::getInstance()->getUsers();
        $out = FluentArray::from($ls)->map(
            function ($elem) use ($c) {
                $elem['foo'] = 'bar';
                // $out = array_intersect_key($elem, array_flip(['']));
                $nick = $this->parseNick($elem['nick']);
                $jid = $this->parseJID($nick['jid'], $elem);
                $userInfo = $c['usr']->findByLogin($jid['user']);
                $out = null;
                if ($userInfo) {
                    // self::console(
                    //     $p,
                    //     $usr->findByLogin($p['user'])
                    // );
                    // self::console(
                    //     $c['xmpp']->getUserSessionsInfo($jid['user'], $jid['res.real'])
                    // );
                    $out = [
                        'uid' => $userInfo['user_id'],
                        'jid' => $jid,
                        'login' => $userInfo['login'],
                        'user.agent' => $nick['user.agent'],
                        'ami' => $jid['res'] === $c['res'],
                        'session.info' => $c['xmpp']->getUserSessionsInfo($jid['user'], $jid['res.real'])
                        // 'remote.ip.real' => null,
                        // 'remote.ip' => null,
                        // 'remote.host' => null,
                        // 'remote.host.real' => null
                    ];
                } else {
                    throw new \Exception('unable to locate user\'s account');
                }
                return WhoItem::fromArray($out);
            }
        );

        if ($all) {
            $a = $out->getArrayCopy();
        } else {
            $a = $out->filter(
                function ($elem) {
                    return $elem->isOther();
                }
            )->getArrayCopy();
        }
        return WhoCollection::fromArray($a);
    }

    private function parseNick($nick)
    {
        $p = explode(' ', $nick);
        $jid = $p[0];
        $ua = substr($nick, strlen($jid) + 1);
        return array_combine(['jid', 'user.agent'], [$jid, $ua]);
    }

    private function parseJID($jid, $elem)
    {
        // self::console($elem);
        $p0 = Utils::parseJID($jid);
        $p1 = Utils::parseJID($elem['jid']);
        $p0['res.real'] = $p1['res'];
        $p0['jid'] = $jid;
        $p0['jid.real'] = $p0['bare'] . '/' . $p0['res.real'];
        return $p0;
    }
}
