<?php
/**
 * @page WSPLRTL\Value\Lib\Utils
 *
 * Utils.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Value\Lib\Utils
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Value\Lib;

use Namedio\Core\Edge\Lang\Strings\Strings;

/**
 * Utils.php
 *
 * @trait WSPLRTL\Value\Lib\Utils
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
trait Utils
{
    /**
     * Perform normailzation of value to "web-safe" form
     *
     * @param mixed $value
     *
     * @return string
     */
    public static function normalizeValue($value)
    {
        //if ((bool) $value) {
        //    return quotemeta((string) $value);
        //} elseif (!is_string($value)) {
        //    return (string) $value;
        //}
        // return htmlspecialchars((string) $value, ENT_QUOTES);
        return self::_toStr($value);
    }

    public static function normalizeValueTextNew($value)
    {
        if (is_string($value)) {
            $v0 = json_encode($value, JSON_UNESCAPED_UNICODE);
            return addslashes(Strings::unquote($v0));
        } else {
            return (string) $value;
        }
    }

    public static function normalizeValueText($value)
    {
        return self::_toStr($value, true);
    }

    private static function _toStr($value, $mode = false)
    {
        if (is_string($value)) {
            // return self::_unquote(self::_json(addslashes(self::_unbr($value))));
            return self::_unbr(addslashes($value), $mode);
            // return self::_unbr($value, $mode);
        } else {
            return (string) $value;
        }
    }

    private static function _unbr($s, $mode = false)
    {
        // return str_replace(['\''], '<wbr>', $s);
        // return $s;
        if ($mode) {
            $m0 = str_replace(["\n", "\r"], ['\n', ''], $s);
            // $m0 = str_replace(["\n"], ["\\n", ''], $m0);
            return $m0;
            // return str_replace(["\n", "\r"], ['\n', ''], $s);
        } else {
            // return str_replace(["\r", "\n"], [], $s);
            return $s;
        }
    }

    private static function _unbrNew($s, $mode = false)
    {
        // return str_replace(['\''], '<wbr>', $s);
        // return $s;
        if ($mode) {
            // return json_enocde($s)
            // return str_replace(["\n", "\r"], ['&#10;', ''], $s);
            // $m0 = str_replace(["\n", "\r"], ["\n", ''], $s);
            // $m0 = str_replace(["\n"], ["\\n", ''], $m0);
            $t = uniqid('____$lf');
            // $m0 = str_replace(["\n", "\r"], [$t, ''], $s);
            $m0 = str_replace(["\n\r"], [$t], $s);
            $m0 = addslashes($m0);
            $m0 = str_replace([$t], ["\\n"], $m0);
            // $m0 = str_replace(["\n", "\r"], ['\n', ''], $s);
            return $m0;
            // return str_replace(["\n", "\r"], ['\n', ''], $s);
        } else {
            // return str_replace(["\r", "\n"], [], $s);
            return $s;
        }
    }

    private static function _json($v)
    {
        return json_encode($v, JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT);
    }

    private static function _unquote($v)
    {
        $n = strlen($v);
        if ($v) {
            return substr($v, 1, $n - 2);
        } else {
            return $v;
        }
    }
}
