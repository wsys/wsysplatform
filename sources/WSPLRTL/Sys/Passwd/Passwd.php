<?php
/**
 * @file
 *
 * @brief Passwd.php
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Sys\Passwd\Passwd
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Sys\Passwd;

use Nodelimit\DI\Service\AbstractManaged;
use Namedio\Ext\SimpleResult\Result;
use Namedio\Ext\SimpleResult\Error;
use Namedio\Ext\SimpleResult\ExceptionResult;
use Nodelimit\App\Action\Sandbox;
use WSAPL\User\Model\Model as Users;
use Nodelimit\App\App;
use WSPLShared\Login\Utils;
use Namedio\Ext\SimpleResult\AbstractResult;
use Namedio\Core\Edge\Lang\Strings\Strings;

/**
 * The Passwd class
 *
 * Description
 *
 * @class WSPLRTL\Sys\Passwd\Passwd
 *
 * @note language detection based on https://habr.com/ru/post/159129/
 * @see https://gist.github.com/la2ha/8f177b2cc4a75e1f1577be6381d3e07f
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 */
class Passwd extends AbstractManaged
{
    const PASSWD_CRIT_EMPTY = 1;
    const PASSWD_EMAIL_EXPECTED = 2;
    const PASSWD_EAPI_FIELD = 3;
    const PASSWD_EAPI_RESULT = 4;
    const PASSWD_DENY = 5;
    const PASSWD_WRONG = 6;

    static public $L10n = [
        'en' => [
            self::PASSWD_CRIT_EMPTY => 'unspecified recipient',
            self::PASSWD_EMAIL_EXPECTED => 'no email specified',
            self::PASSWD_EAPI_FIELD => 'API error: missing required field',
            self::PASSWD_EAPI_RESULT => 'API error: incompatible type of the query result',
            self::PASSWD_DENY => 'request rejected',
            self::PASSWD_WRONG => 'wrong recipient',
        ],
        'ru' => [
            self::PASSWD_CRIT_EMPTY => 'не задан адресат',
            self::PASSWD_EMAIL_EXPECTED => 'не указан e-mail',
            self::PASSWD_EAPI_FIELD => 'ошибка API: отсутствует требуемое поле',
            self::PASSWD_EAPI_RESULT => 'ошибка API: несовместимый тип результата запроса',
            self::PASSWD_DENY => 'запрос отклонен',
            self::PASSWD_WRONG => 'недопустимый адресат',
        ]
    ];

    /// @todo ensure keys
    private static $_keys = [
        'procd_conn'
    ];

    private $result;
    private $language;

    protected function init()
    {
        ///
    }

    public function getPassword()
    {
        if (func_num_args() > 0) {
            return $this->retrivePassword(func_get_args());
        } else {
            throw new \BadMethodCallException();
        }
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getErrorMessage()
    {
        $r = $this->getResult();
        if ($r && $r->isError()) {
            // $code = $r->getCode();
            // if ($code )
            if ($r->isException()) {
                return $r->extractMessage();
            } else {
                return $this->translateCode($r->getCode());
            }
        }
        return null;
    }

    private function translateCode($code)
    {
        $lang = $this->getLang();
        $defs = $this->getL10n();
        $sysLang = 'en';
        // self::console(__METHOD__, $lang, $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        if (isset($defs[$lang][$code])) {
            return $defs[$lang][$code];
        } elseif (isset($defs[$sysLang][$code])) {
            return $defs[$sysLang][$code];
        } else {
            return 'unknown error code ' . (string) $code;
        }
    }

    private function getL10n()
    {
        return self::$L10n;
    }

    /*
    private function getErrorMessageResult()
    {
        $r = $this->getResult();
        if ($r && $r->isError()) {
            if ($r->isException()) {
                $
                if (Strings::isFilled($msg)) {
                    return $msg;
                }
            }
        }
        return null;
    }
    */

    private function getLang()
    {
        return App::getInstance()->getLang();
    }

    private function retrivePassword(array $args)
    {
        $q = $this->storeResult($this->queryPassword($args[0]));
        // self::vdump(__METHOD__, $q);
        if ($q->isSuccess()) {
            return $q->getValue();
        } else {
            return null;
        }
    }

    private function storeResult($r)
    {
        return ($this->result = $r);
    }

    private function prevalidate($login)
    {
        $v = Utils::isEmail($login);
        if ($v) {
            return $this->makeResult(true);
        } else {
            return $this->makeError(self::PASSWD_EMAIL_EXPECTED);
        }
    }

    private function makeResult($v)
    {
        return new Result($v);
    }

    private function makeError($v)
    {
        return new Error($v);
    }

    private function queryPassword($criteria)
    {
        $v = $this->prevalidate($criteria);
        if ($v->isSuccess()) {
            return $this->factory($this->fetch($criteria));
        }
        return $v;
    }

    private function factory($v)
    {
        if (is_integer($v)) {
            return $this->makeError($v);
        } elseif ($v instanceof \Exception) {
            /// @todo Throwable
            return new ExceptionResult($v);
        } elseif ($v instanceof AbstractResult) {
            return $v;
        } else {
            return $this->makeResult($v);
        }
    }

    private function fetch($criteria)
    {
        // function () use ($criteria) {
        //     ///
        //     $conn = $this->getProcdConn();
        //     $channel = $conn->selectChannel();
        //     $channel->connect();
        //     $payload = $criteria;
        //     $cmd = 'ping';
        //     $channel->send($cmd, $payload);
        //     $packet = $channel->receive();
        //     return $packet->payload;
        // }

        list($ok, $ret) = Sandbox::bootstrap(
            function () use ($criteria) {
                ///
                $r = $this->getUsers()->findByLogin($criteria);
                // self::vdump($r, is_array($r), isset($r['password']));
                // self::console($r);
                // throw new \Exception('test exception');
                if (is_array($r) && isset($r['password'])) {
                    return $r['password'];
                } elseif (is_array($r) && array_key_exists('password', $r) && is_null($r['password'])) {
                    return $this->makeError(self::PASSWD_DENY);
                } elseif (is_array($r)) {
                    return $this->makeError(self::PASSWD_EAPI_FIELD);
                } elseif ($r === false || is_null($r)) {
                    return $this->makeError(self::PASSWD_WRONG);
                } elseif (isset($r)) {
                    // self::vdump(__METHOD__, __LINE__, $r);
                    // self::console(__METHOD__, __LINE__, $r);
                    return $this->makeError(self::PASSWD_EAPI_RESULT);
                } else {
                    return $r;
                }
            }
        );
        if ($ok) {
            return $ret;
        } else {
            /// @todo return exception result or error directly
            return $ret;
        }
    }

    private function getProcdConn()
    {
        return $this->getServiceItem('procd_conn');
    }

    private function getUsers()
    {
        return App::getInstance()->getUsers();
    }

    // private function getUsers()
    // {
    //     return ($this->users)
    //         ? ($this->users) :
    //         (
    //             $this->users = new Users(
    //                 [
    //                     'procd_conn' => function () {
    //                         return $this->getProcdConn();
    //                     }
    //                 ]
    //             )
    //         );
    // }
}
