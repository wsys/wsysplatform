<?php
/**
 * @file
 *
 * @brief Sys.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Sys\Sys
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Sys;

use Nodelimit\DI\Service\AbstractManaged;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use WSPLRTL\Sys\Deps\Deps;
use Namedio\Access\Context\Prop as ContextPropAccess;
use Namedio\Context\Lib\Prop as LibContextProp;
use Nodelimit\Systype\Dict;
use Namedio\API\Composer\Composer;
use Nodelimit\WF\Wired\WiredUtils;
use WSPLRTL\Widget\Form\API\Connection\Connection;
use WSPLRTL\Sys\Passwd\Passwd;

/**
 * The Sys class
 *
 * Description
 *
 * @class WSPLRTL\Sys\Sys
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
class Sys extends AbstractManaged implements ContextPropAccess
{
    use LibClientServiceLocal;
    use LibContextProp;

    private static $_keys = [
        'md.node',

        /// @note required for Connection interface
        'procd_conn'
    ];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    public function composer()
    {
        return $this->_getLocalServiceItem('composer');
    }

    public function injectCSS($p)
    {
        WiredUtils::injectCSS($p);
    }

    public function injectScript($p)
    {
        WiredUtils::injectScript($p);
    }

    public function injectES6($p)
    {
        WiredUtils::injectES6($p);
    }

    /**
     * Get Deps backend
     *
     * @return WSPLRTL\Sys\Deps\Deps
     */
    public function deps()
    {
        return call_user_func_array(
            [$this->_getLocalServiceItem('deps'), '__invoke'],
            $this->makeDepsArgs(func_get_args())
        );
    }

    public function getContextProps()
    {
        return $this->_getLocalServiceItem('.cprops');
    }

    public function passwd()
    {
        return $this->_getLocalServiceItem('passwd');
    }

    public function connection()
    {
        return $this->_getLocalServiceItem('connection');
    }

    private function makeDepsArgs(array $args)
    {
        $n = count($args);
        switch ($n) {
            case 0:
                return $this->makeDefaultFormArgs();
            case 1:
                /// @todo proposal: check string type?
                return [['form', $args[0]]];
            default:
                throw new \DomainException();
        }
    }

    private function makeDefaultFormArgs()
    {
        return $this->_getLocalServiceItem('current.form.args');
    }

    /**
     * Gets local service bindings
     *
     * @return array with keys:
     *   + deps WSPLRTL\Sys\Deps\Deps
     *   + current.form.args array
     */
    private function _getLocalServiceBindings()
    {
        return [
            'passwd' => function () {
                return Passwd::create(
                    [
                        'procd_conn' => $this->getServiceItem('procd_conn')
                    ]
                );
            },
            'deps' => function () {
                return Deps::create(
                    [
                        'md.node' => $this->getServiceItem('md.node')
                    ]
                );
            },
            'composer' => function () {
                return new Composer();
            },
            'connection' => function () {
                return Connection::create(
                    [
                        'procd_conn' => $this->getServiceItem('procd_conn')
                    ]
                );
            },
            'current.form.args' => function () {
                return [
                    ['form', $this->getContextProp('current.form.id')]
                ];
            },
            '.cprops' => new Dict()
        ];
    }
}
