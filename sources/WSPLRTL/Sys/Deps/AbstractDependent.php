<?php
/**
 * @file
 *
 * @brief AbstractDependent.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Sys\Deps\AbstractDependent
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Sys\Deps;

use Nodelimit\DI\Service\AbstractManaged;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\Core\Struct\Result\ResultClientTrait;

/**
 * The AbstractDependent class
 *
 * Description
 *
 * @class WSPLRTL\Sys\Deps\AbstractDependent
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
abstract class AbstractDependent extends AbstractManaged
{
    use LibClientServiceLocal;
    use ResultClientTrait;

    private static $_keys = [
        'deps', 'md.node'
    ];

    public function ensure()
    {
        return $this->getBackend()->ensure();
    }

    public function ls()
    {
        return $this->getBackend()->ls();
    }

    public function expand()
    {
        return $this->getBackend()->expand();
        // return $this->resultNull();
    }

    public function result()
    {
        return $this->getResult();
    }

    public function dispose()
    {
        $this->getDeps()->dispose();
    }

    public function getMetaDataNode()
    {
        return $this->getServiceItem('md.node');
    }

    abstract protected function createBackend();

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
        $this->resetResult();
    }

    protected function getBackend()
    {
        return $this->_getLocalServiceItem('backend');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'backend' => function () {
                return $this->createBackend();
            }
        ];
    }

    private function getDeps()
    {
        return $this->getServiceItem('deps');
    }
}
