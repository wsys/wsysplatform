<?php
/**
 * @file
 *
 * @brief FormDepsUtilsTrait.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Sys\Deps\FormDepsUtilsTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Sys\Deps;

/**
 * The FormDepsUtilsTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Sys\Deps\FormDepsUtilsTrait
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
trait FormDepsUtilsTrait
{
    abstract public function getMetaDataNode();

    public function queryFormDeps($id)
    {
        $mdn = $this->getMetaDataNode();
        $deps = $mdn->getById('form.deps');
        return $deps->lsByFormId($id, ['type' => 'composer']);
    }
}
