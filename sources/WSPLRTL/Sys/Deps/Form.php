<?php
/**
 * @file
 *
 * @brief Form.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Sys\Deps\Form
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Sys\Deps;

use WSPLRTL\Sys\Deps\AbstractDependent;
use WSPLShared\Deps\DepsTrait;
use WSPLRTL\Sys\Deps\FormDepsUtilsTrait;

/**
 * The Form class
 *
 * Description
 *
 * @class WSPLRTL\Sys\Deps\Form
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
class Form extends AbstractDependent
{
    use DepsTrait;
    use FormDepsUtilsTrait;

    protected function getFormDepsCallback()
    {
        return [$this, 'queryFormDeps'];
    }

    /**
     * Create form deps backend
     *
     * @return WSPLShared\Deps\Form
     */
    protected function createBackend()
    {
        return $this->openDeps()->form($this->getFormId());
    }

    private function getFormId()
    {
        return $this->getServiceItem('id');
    }
}
