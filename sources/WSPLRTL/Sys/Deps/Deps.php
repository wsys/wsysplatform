<?php
/**
 * @file
 *
 * @brief Deps.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Sys\Deps\Deps
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Sys\Deps;

use Nodelimit\DI\Service\AbstractManaged;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\Core\Edge\DI\Service\Entity\Pool;
use WSPLRTL\Sys\Deps\Form;

/**
 * The Deps class
 *
 * Description
 *
 * @class WSPLRTL\Sys\Deps\Deps
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
class Deps extends AbstractManaged
{
    use LibClientServiceLocal;

    /// @todo ensure keys
    private static $_keys = [
        'md.node'
    ];

    public function __invoke()
    {
        $n = func_num_args();
        switch ($n) {
            case 0:
                return $this;
            case 1:
                return call_user_func_array([$this, 'invoke1'], func_get_args());
            default:
                throw new \DomainException();
        }
    }

    public function createEntryPool($pool, $key)
    {
        list($what, $id) = $this->split($key);
        switch ($what) {
            case 'form':
                return Form::create($this->base(['id' => $id]));
            default:
                throw new \DomainException();
        }
    }

    public function dispose()
    {
        self::logger(__METHOD__);
    }

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    private function base($initial = [])
    {
        $base = [
            'deps' => $this,
            'md.node' => $this->getMetaDataNode()
        ];
        return array_merge(
            $base,
            $initial
        );
    }

    private function getMetaDataNode()
    {
        return $this->getServiceItem('md.node');
    }

    private function invoke1($args)
    {
        if (is_array($args)) {
            return $this->pooled($this->unsplit($args));
        } else {
            throw new \InvalidArgumentException();
        }
    }

    private function unsplit(array $args)
    {
        list($what, $id) = $args;
        return $what . ':' . $id;
    }

    private function split($key)
    {
        return explode(':', $key);
    }

    private function pooled($key)
    {
        return $this->_getLocalServiceItem('pool')->get($key);
    }

    private function _getLocalServiceBindings()
    {
        return [
            'pool' => function () {
                return new Pool([$this, 'createEntryPool']);
            },
        ];
    }
}
