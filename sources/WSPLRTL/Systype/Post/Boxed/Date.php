<?php
/**
 * @file
 *
 * @brief Date.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Date
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed;

use WSPLRTL\Systype\Post\Boxed\AbstractBoxed;
use WSPLRTL\Form\Model\Node\RS\Value\DateType;

/**
 * The Date class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Date
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Date extends AbstractBoxed
{
    public static function create($value)
    {
        $c = DateType::convertPostValue($value);
        // self::console(__METHOD__, $c, $value);
        return parent::create($c);
    }
}
