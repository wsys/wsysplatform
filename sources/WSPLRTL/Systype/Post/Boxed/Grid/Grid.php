<?php
/**
 * @file
 *
 * @brief Grid.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Grid\Grid
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Grid;

use WSPLRTL\Systype\Post\Boxed\Grid\AbstractGrid;

/**
 * The Grid class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Grid\Grid
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
class Grid extends AbstractGrid
{
}
