<?php
/**
 * @file
 *
 * @brief AbstractGrid.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Grid\AbstractGrid
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Grid;

use WSPLRTL\Systype\Post\Boxed\AbstractBoxed;
use Namedio\UI\Grid\Event\EventTrait;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\UI\Grid\Event\GridEventInterface;

/**
 * The AbstractGrid class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Grid\AbstractGrid
 *
 * @todo update selectors in rowset
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
abstract class AbstractGrid extends AbstractBoxed implements GridEventInterface
{
    use LibClientServiceLocal;
    use EventTrait;

    public function stub()
    {
        return __METHOD__;
    }

    public function getGridElement()
    {
        return $this->_getLocalServiceItem('grid.element');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'grid.element' => function () {
                return $this->_getWidget()->getGridElement();
            }
        ];
    }

    private function _getWidget()
    {
        return $this->getContextProp('widget');
    }
}
