<?php
/**
 * @file
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed;

use WSPLRTL\Systype\Post\Boxed\AbstractBoxed;

/**
 * The class WSPLRTL\Systype\Post\Boxed\Generic
 *
 * @~English Is a simple container with access to a string value with
 * getValue method @~
 *
 * @~Russian Представляет собой простой контейнер с доступом к
 * строковому значению @~
 *
 * @class WSPLRTL\Systype\Post\Boxed\Generic
 *
 * @see Nodelimit\Access\Value\Provider\StringType
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Generic extends AbstractBoxed
{
}
