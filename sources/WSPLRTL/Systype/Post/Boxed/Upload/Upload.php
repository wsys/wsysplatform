<?php
/**
 * @file
 *
 * @brief Upload.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Upload\Upload
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Upload;

use WSPLRTL\Systype\Post\Boxed\AbstractBoxed;
use Nodelimit\Upload\File\Temp as File;

/**
 * The Upload class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Upload\Upload
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Upload extends AbstractBoxed
{
    private $_collection;

    protected function init()
    {
        parent::init();
        self::console(
            $_SERVER,
            $_GET,
            $_POST,
            $_FILES,
            $this->getFilesCount()
        );
        $this->_collection = $this->_loadFiles();
    }

    private function _loadFiles()
    {
        $ao = new \ArrayObject();
        foreach ($_FILES as $val) {
            $ao->append($this->_makeFile($val));
        }
        return $ao;
    }

    private function _makeFile(array $f)
    {
        return File::createFromArray($f);
    }

    public function getFilesCount()
    {
        return count($_FILES);
    }

    public function file($index = 0)
    {
        return $this->_collection->offsetGet($index);
    }
}
