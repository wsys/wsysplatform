<?php
/**
 * @file
 *
 * @brief AbstractRadio.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Radio\AbstractRadio
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Radio;

use WSPLRTL\Systype\Post\Boxed\AbstractBoxed;

/**
 * The AbstractRadio class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Radio\AbstractRadio
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
abstract class AbstractRadio extends AbstractBoxed
{
    public function isChecked()
    {
        return $this->getContextProp('checked');
    }
}
