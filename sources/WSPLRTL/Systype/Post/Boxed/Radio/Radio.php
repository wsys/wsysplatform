<?php
/**
 * @file
 *
 * @brief Radio.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Radio\Radio
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Radio;

use WSPLRTL\Systype\Post\Boxed\Radio\AbstractRadio;

/**
 * The Radio class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Radio\Radio
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Radio extends AbstractRadio
{
}
