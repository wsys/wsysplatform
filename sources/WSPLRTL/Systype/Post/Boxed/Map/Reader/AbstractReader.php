<?php
/**
 * @file
 *
 * @brief AbstractReader.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Map\Reader\AbstractReader
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Map\Reader;

use Nodelimit\Entity\AbstractBase;
use Nodelimit\DI\Service\Lib\Client\Outer as LibClientOuterService;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\Core\Edge\DI\Service\Entity\Pool;
use Nodelimit\Systype\Dict;

/**
 * The AbstractReader class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Map\Reader\AbstractReader
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
abstract class AbstractReader extends AbstractBase
{
    use LibClientOuterService;
    use LibClientServiceLocal;

    private static $_keys = [
        'client'
    ];

    public function __construct($service = [])
    {
        $this->_acceptOuterService($service);
        $this->_ensureServiceKeys(self::$_keys);
    }

    public function q($name)
    {
        return $this->_getPool()->get($name);
    }

    abstract public function createItem($pool, $key);

    protected function readSafe($key, $dflt = false)
    {
        // self::console(
        //     // $this->_getRaw(),
        //     // new Dict($this->_getServiceItem('client')->getRawProps()),
        //     __CLASS__,
        //     get_class(),
        //     get_class($this),
        //     $this->_getRaw()->getSafe($key, $dflt),
        //     $key,
        //     $dflt
        // );
        return $this->_getRaw()->getSafe($key, $dflt);
    }

    private function _getPool()
    {
        return $this->_getLocalServiceItem('pool');
    }

    private function _getRaw()
    {
        return $this->_getLocalServiceItem('raw');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'pool' => new Pool([$this, 'createItem']),
            'raw' => new Dict($this->_getServiceItem('client')->getRawProps())
        ];
    }
}
