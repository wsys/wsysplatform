<?php
/**
 * @file
 *
 * @brief Stub.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Map\Reader\Stub
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Map\Reader;

use WSPLRTL\Systype\Post\Boxed\Map\Reader\AbstractReader;

/**
 * The Stub class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Map\Reader\Stub
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
class Stub extends AbstractReader
{
    public function createItem($pool, $key)
    {
        return false;
    }
}
