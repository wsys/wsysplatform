<?php
/**
 * @file
 *
 * @brief Map.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Map\Map
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Map;

use WSPLRTL\Systype\Post\Boxed\Map\AbstractMap;
use WSPLRTL\Systype\Post\Boxed\Map\Reader\Stub;

/**
 * The Map class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Map\Map
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Map extends AbstractMap
{
    protected function createReader()
    {
        return new Stub($this->createReaderServiceParams());
    }
}
