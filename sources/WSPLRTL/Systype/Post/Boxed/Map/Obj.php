<?php
/**
 * @file
 *
 * @brief Obj.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Map\Obj
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Map;

use WSPLRTL\Systype\Post\Boxed\Map\AbstractMap;
use WSPLRTL\Systype\Post\Boxed\Map\Reader\Simple;

/**
 * The Obj class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Map\Obj
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Obj extends AbstractMap
{
    public function get($k)
    {
        switch ($k) {
            case 'obj.id':
            case 'objectId':
                return $this->read('obj.id');
            default:
                return parent::get($k);
        }
    }

    protected function createReader()
    {
        return new Simple($this->createReaderServiceParams());
    }
}
