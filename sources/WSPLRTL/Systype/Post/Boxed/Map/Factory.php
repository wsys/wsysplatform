<?php
/**
 * @file
 *
 * @brief Factory.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Map\Factory
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Map;

use WSPLRTL\Systype\Post\Boxed\Map\Map;
use WSPLRTL\Systype\Post\Boxed\Map\Obj;
use com\danscode\traits\Debug;

/**
 * The Factory class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Map\Factory
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Factory
{
    use Debug;

    public static function create($v)
    {
        $p = self::_accept($v);
        if (isset($p['obj.id'])) {
            /// @note this is just map object
            /// @note it can be any objects, that has obj.id (clusters and so on)
            return Obj::create([$v, $p]);
        } else {
            /// @note assume that this is an click on objects without id
            return Map::create([$v, $p]);
        }
    }

    private static function _accept($v)
    {
        return json_decode($v, true);
    }
}
