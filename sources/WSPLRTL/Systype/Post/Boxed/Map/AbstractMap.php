<?php
/**
 * @file
 *
 * @brief AbstractMap.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Map\AbstractMap
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed\Map;

use WSPLRTL\Systype\Post\Boxed\AbstractBoxed;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use com\danscode\lib\Props;
use Namedio\Core\Edge\Lang\Arrays\Arrays;
use Namedio\Core\Edge\Lang\Value\Value;

/**
 * The AbstractMap class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Map\AbstractMap
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
abstract class AbstractMap extends AbstractBoxed
{
    use LibClientServiceLocal;

    private $_overlay;

    public static function create($v)
    {
        list($s, $p) = $v;
        $o = parent::create($s);
        $o->_overlay = $p;
        return $o;
    }

    public function getCoords()
    {
        return $this->_read('coords');
    }

    public function getBounds()
    {
        return $this->_read('bounds');
    }

    public function isClustered()
    {
        $key = 'isClustered';
        $exists = $this->hasKey($key);
        if ($exists) {
            $v = $this->readKey($key);
            if (is_null($v)) {
                /// @warning assume that clustered
                return true;
            } else {
                return (bool) $v;
            }
        } else {
            /// no such key - assume undefined - returns null
            return null;
        }
    }

    public function get($k)
    {
        switch ($k) {
            case 'isClustered':
                return $this->isClustered();
            case 'bounds':
            case 'coords':
                return $this->_read($k);
            default:
                throw new \Exception('unknown key: ' . Value::dump($k));
        }
    }

    public function read($k)
    {
        return $this->readKey($k);
    }

    public function getRawProps()
    {
        return Props::getSafe($this->_get(), 'propsAll', []);
    }

    public function getName()
    {
        return $this->_q('name');
    }

    public function getAddress()
    {
        // self::console(__CLASS__, get_class($this), $this->_q('address'));
        return $this->_q('address');
    }

    public function getDescription()
    {
        return $this->_q('description');
    }

    public function getKind()
    {
        return $this->_q('kind');
    }

    public function getType()
    {
        return $this->_q('type');
    }

    public function checkProp($name, $v)
    {
        return ($this->_q($name) === $v);
    }

    protected function createReaderServiceParams()
    {
        return [
            'client' => $this
        ];
    }

    abstract protected function createReader();

    protected function readSafe($name, $default = null)
    {
        $gt = $this->getOverlay();
        return Arrays::getSafe($gt, $name, $default);
    }

    private function _q($name)
    {
        return $this->_getLocalServiceItem('reader')->q($name);
    }

    private function _getLocalServiceBindings()
    {
        return [
            'reader' => $this->createReader()
        ];
    }

    private function _read($k)
    {
        return $this->readKey($k);
    }

    private function readKey($k)
    {
        return $this->getOverlay()[$k];
    }

    private function hasKey($name)
    {
        $a = $this->getOverlay();
        return array_key_exists($name, $a);
    }

    private function _get()
    {
        return $this->_overlay;
    }

    private function getOverlay()
    {
        return $this->_overlay;
    }
}
