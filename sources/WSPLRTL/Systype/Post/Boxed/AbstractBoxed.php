<?php
/**
 * @file
 *
 * @brief AbstractBoxed.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\AbstractBoxed
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed;

use Nodelimit\Exception\Raise;
use WSPLRTL\Access\Value\Sent as SentValueAccess;
use Namedio\Core\Edge\Lang\Strings\Strings;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\Access\Context\Prop as ContextPropAccess;
use Namedio\Context\Lib\Prop as LibContextProp;
use Nodelimit\Systype\Dict;
use Namedio\Core\Edge\Lang\Strings\ValuedTrait;
use Namedio\Core\Edge\Lang\Value\UnifiedTrait;
use com\danscode\traits\Debug;

/**
 * The AbstractBoxed class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\AbstractBoxed
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
abstract class AbstractBoxed implements SentValueAccess, ContextPropAccess
{
    use Debug;
    use LibClientServiceLocal;
    use LibContextProp;
    use ValuedTrait;
    use UnifiedTrait;

    private $_value;            /**< stored value with string type */

    /**
     * The constructor of class
     *
     * Constructs a new WSPLRTL\Systype\Post\Boxed\Generic class
     *
     * @return void
     */
    private function __construct()
    {
        //
    }

    /**
     * @brief Create new instance of the class by $value
     *
     * @details @~English Constructs a new instance of the class, checking for a
     * string type parameter $value. If the type does not match the
     * string, then an exception is thrown. ~@
     *
     * @~Russian Конструирует новый экземпляр класса, выполняя проверку
     * на строковый тип параметра $value. Если тип не соответствует
     * строковому, то возникает исключение. @~
     *
     * @param mixed $value that holds as value in this object
     *
     * @throws \UnexpectedValueException thrown if a value does not
     * match with a string type.
     *
     * @return object a new instance of WSPLRTL\Systype\Post\Boxed\Generic class
     */
    public static function create($value)
    {
        $o = new static();
        $o->_value = self::_ensureString($value);
        $o->init();
        return $o;
    }

    /**
     * @brief Gets the value
     *
     * @see WSPLRTL\Systype\Post\Boxed\Generic::create()
     * @see WSPLRTL\Access\Value\Sent
     * @see WSPLRTL\Access\Value\Sent::getValue()
     *
     * @implements WSPLRTL\Access\Value\Sent::getValue()
     *
     * @note WSPLRTL-API-User
     *
     * @return string The value that stored in object container
     */
    public function getValue()
    {
        return $this->_value;
    }

    public function isNull()
    {
        return is_null($this->getValue());
    }

    public function isEmpty()
    {
        $v = $this->getValue();
        return empty($v);
    }

    public function isBlank()
    {
        return Strings::isBlank($this->getValue());
    }

    public function isFilled()
    {
        return Strings::isFilled($this->getValue());
    }

    public function isValued()
    {
        return Strings::isPayload($this->getValue());
    }

    public function getContextProps()
    {
        return $this->_getContextProps();
    }

    public function getLength()
    {
        return $this->_getValueLen();
    }

    public function mbgetLength()
    {
        return $this->_mbgetValueLen();
    }

    protected function init()
    {
        // noop
    }

    private function _getValueLen()
    {
        return strlen($this->getValue());
    }

    private function _mbgetValueLen()
    {
        return mb_strlen($this->getValue());
    }

    private function _getLocalServiceBindings()
    {
        return [
            '.cprops' => new Dict()
        ];
    }

    private function _getContextProps()
    {
        return $this->_getLocalServiceItem('.cprops');
    }

    /**
     * Check $value for string type and throws \Exception if not string type
     *
     * @param mixed $value checking value
     *
     * @throws \UnexpectedValueException thrown if a value does not
     * match with a string type.
     *
     * @return string $value checked value
     */
    private static function _ensureString($value)
    {
        if (is_string($value)) {
            return $value;
        }
        Raise::badMatch($value);
    }
}
