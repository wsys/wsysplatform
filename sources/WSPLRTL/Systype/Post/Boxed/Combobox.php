<?php
/**
 * @file
 *
 * @brief Combobox.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Systype\Post\Boxed\Combobox
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype\Post\Boxed;

use WSPLRTL\Systype\Post\Boxed\AbstractBoxed;
use WSPLRTL\Widget\Access\API\User\Combobox\ReadBaseInterface;

/**
 * The Combobox class
 *
 * Description
 *
 * @class WSPLRTL\Systype\Post\Boxed\Combobox
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Combobox extends AbstractBoxed implements ReadBaseInterface
{
    public function getSelectedIndex()
    {
        return $this->_getWidget()->getSelectedIndex();
    }

    public function hasIndex()
    {
        return $this->_getWidget()->hasIndex();
    }

    public function getText()
    {
        return $this->_getWidget()->getText();
    }

    public function getSelectedKey()
    {
        return $this->_getWidget()->getSelectedKey();
    }

    public function getSelectedItem()
    {
        return $this->_getWidget()->getSelectedItem();
    }

    public function getSelectedTag()
    {
        return $this->_getWidget()->getSelectedTag();
    }

    private function _getWidget()
    {
        return $this->getContextProp('widget');
    }
}
