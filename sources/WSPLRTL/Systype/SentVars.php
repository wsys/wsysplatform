<?php
/**
 * @file
 *
 * @see WSPLRTL\Systype\SentVars
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Systype;

use WSPLRTL\Access\SentVars as SentVarsAccess;
use WSPLRTL\Widget\AbstractForm as Form;

use WSPLRTL\Systype\Post\Boxed\Generic as PostValue;

use Nodelimit\Systype\Lazy\AdaptivePool;
use Namedio\Core\Edge\Lang\Value\Value;
use com\danscode\traits\Debug;
use WSPLRTL\Widget\Radio;
use WSPLRTL\Widget\Checkbox;
use WSPLRTL\Systype\Post\Boxed\Radio\Radio as PostValueRadio;
use WSPLRTL\Systype\Post\Boxed\Upload\Upload as PostValueUpload;
use WSPLRTL\Systype\Post\Boxed\Date as PostValueDate;
use WSPLRTL\Widget\Upload;
use WSPLRTL\Systype\Post\Boxed\Combobox as PostValueCombobox;
use WSPLRTL\Widget\Combobox;
use WSPLRTL\Widget\DateEdit;
use WSPLRTL\Widget\Map;
use WSPLRTL\Systype\Post\Boxed\Map\Factory as MapFactory;
use WSPLRTL\Widget\AbstractGrid;
use WSPLRTL\Systype\Post\Boxed\Grid\Grid;

/**
 * The SentVars class
 *
 * @details
 *
 * @~English
 *
 * Class SentVars, provides access to transit variables controls
 * associated with a form that is transmitted together with the browser
 * requests to the server. In the simplest case, such a transit variable
 * is accepted from the actual data associated HTML-control on the form.
 *
 * For example, it can be typed text to the appropriate control and
 * passed to the handler browser as a value of the variable associated
 * with the event on a form submit.
 *
 * @~
 *
 * @~Russian
 *
 * Класс SentVars, обеспечивает доступ к транзитным переменным элементов
 * управления, связанных с формой, переданных вместе с запросом браузера
 * к серверу. В простейшем случае такая транзитная переменная
 * представляет собой принятые актуальные данные от связанного
 * HTML-элемента управления на форме.
 *
 * Например, это может быть введённый текст для соответствующего
 * элемента управления и переданный обработчиками браузера в виде
 * значения переменной связанного с событием на форме submit.
 *
 * @~
 *
 * @class WSPLRTL\Systype\SentVars
 *
 * @implements WSPLRTL\Access\SentVars
 *
 * @todo more documenting and details: reason, why, special aspects,
 * shades, purposes, formals
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class SentVars implements SentVarsAccess
{
    use Debug;

    private $_form;             /**< reference to WSPLRTL\Widget\AbstractForm */
    private $_pool;             /**< adaptive pool of PostValue classes */

    /**
     * @brief The contructor of SentVars class
     *
     * @details
     *
     * @~English Creates an object of class SentVars. @~
     *
     * @~Russian Создает объект класса SentVars @~
     *
     * @param object $f reference to WSPLRTL\Widget\AbstractForm class
     *
     * @see Nodelimit\Systype\Lazy\AdaptivePool
     * @see WSPLRTL\Access\Value\Sent
     * @see WSPLRTL\Widget\AbstractForm
     *
     * @return void
     */
    public function __construct(Form $f)
    {
        $this->_form = $f;
        $this->_initPool();
    }

    /**
     * @brief Query the object wrapper of transitional variable
     * associated with control by control id ($key)
     *
     * @details
     *
     * @~English Request by $key the object wrapper transit variable
     * associated directly with the control on the client side at the
     * time of event processing on the server @~
     *
     * @~Russian Запросить по $key транзитную переменную,
     * непосредственно связанную с элементом управления на клиентской
     * стороне и переданную на сервер при наступлении соответствующиего
     * события @~
     *
     * @param string $key identifier of any control defined on form (ID_WCONTROL)
     *
     * @note @~English $key corresponds to the concept ID_WCONTROL in
     * the metadata model. @~
     * @note @~Russian $key соответствует понятию ID_WCONTROL в модели
     * метаданных. @~
     * @note assume that widget is value-aware
     *
     * @see WSPLRTL\Access\Value\Sent
     * @see WSPLRTL\Systype\Post\Boxed\Generic
     *
     * @return object with WSPLRTL\Access\Value\Sent interface
     */
    public function q($key)
    {
        return $this->_pool[$key];
    }

    /**
     * The hook-method for creating wrapper to POST variable for pool
     *
     * Utility method to create required in pool object on
     * demand. Called from constructor.
     *
     * @param string $key this is identifier that was queried by self::q() method
     * @param mixed $initial_data any bindings that can be used to create the object
     * @param bool $bound its flag to determinate whether found initial data in pool
     *
     * @note this is internal public method to implementation lazy loading to pool
     *
     * @see Nodelimit\Systype\Lazy\AdaptivePool
     *
     * @throws \Exception if key not exists in $_POST
     *
     * @warning a call to a non-valid key for the widget will lead to the exception
     * @warning a call to a non-value-aware widgets will cause fatal error
     *
     * @throw \DomainException when $key has no association with the post var
     *
     * @return object with WSPLRTL\Access\Value\Sent interface
     */
    public function createPostValue($key, $initial_data, $bound)
    {
        $w = $this->_form->q($key);
        $vkey = $w->getValueKey();
        // self::console($_POST, $key, $vkey);
        $v = false;
        if (($w instanceof Radio) || ($w instanceof Checkbox)) {
            $v = false;
            $checked = false;
            if (array_key_exists($vkey, $_POST)) {
                $v = $_POST[$vkey];
                $checked = true;
            } else {
                $v = '';
            }
            $r = PostValueRadio::create($v);
            $r->setContextProp('checked', $checked);
            // self::logTraceDump($checked);
            return $r;
        } elseif ($w instanceof Upload) {
            // self::console(
            //     $this->_form->api('file.storage')->file(
            //         ['tmp', 'zero.gif'],
            //         [
            //             'dry-run' => true
            //         ]
            //     )->remove()
            // );
            return PostValueUpload::create($_POST[$vkey]);
        } elseif ($w instanceof Combobox) {
            $r = PostValueCombobox::create($_POST[$vkey]);
            $r->setContextProp('widget', $w);
            return $r;
        } elseif ($w instanceof AbstractGrid) {
            $r = Grid::create('');
            $r->setContextProp('widget', $w);
            return $r;
        } elseif ($w instanceof Map) { /**< just create relevant boxed object */
            return MapFactory::create($_POST[$vkey]);
        } elseif (array_key_exists($vkey, $_POST)) {
            if ($w instanceof DateEdit) {
                return PostValueDate::create($_POST[$vkey]);
            } else {
                return PostValue::create($_POST[$vkey]);
            }
        } else {
            self::console($_POST, $key, $vkey);
            throw new \DomainException(
                Value::sprintf(
                    'could not locate value by key: %s '.
                    '(there is no association for the post '.
                    'variable to the specified key)',
                    $key
                )
            );
        }
    }

    /**
     * Performs creating and initialize pool for SentVars
     *
     * @return void
     */
    private function _initPool()
    {
        $this->_pool = AdaptivePool::create([$this, 'createPostValue']);
    }
}
