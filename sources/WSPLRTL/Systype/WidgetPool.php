<?php
/**
 * WidgetPool.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Systype;

use com\danscode\lib\props\Lazy;
use Nodelimit\WF\Systype\RenewInfo;
use com\danscode\traits\Debug;
use Namedio\Core\Edge\Lang\Strings\Strings;
use Namedio\Core\Edge\Lang\Value\Value;

/**
 * WidgetPool.php
 *
 * description
 *
 * @class WSPLRTL\Systype\WidgetPool
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class WidgetPool extends Lazy implements \Serializable
{
    private $_form;

    use Debug;

    public function __construct($bindings = [])
    {
        parent::__construct($bindings);
    }

    public function free()
    {
        $this->clearCache();
        $this->unsetForm();
    }

    public function serializeNew()
    {
        return serialize($this->b);
    }

    public function serialize()
    {
        $b = $this->b;
        // self::logger(__METHOD__, 'buffer', $b, gettype($b));
        $out = new \ArrayObject();
        array_walk(
            $b,
            function ($v, $k) use ($out) {
                $out[] = self::conv($k, $v);
            }
        );
        // self::logger(__METHOD__, $out, implode(';', $out));
        // return implode(';', $out->getArrayCopy());
        return implode(';', $out->getArrayCopy());
    }

    public function unserialize($s)
    {
        //$this->b = [];
        // $a = array_filter(explode(';', $s));
        $a = array_filter(explode(';', $s));
        $out = new \ArrayObject();
        //self::logger(__METHOD__, $s, $a);
        array_walk(
            $a,
            function ($v, $k) use ($out) {
                list($out_k, $out_v) = self::unconv($v);
                $out[$out_k] = $out_v;
            }
        );
        $this->b = $out->getArrayCopy();
    }


    public function serializeBase()
    {
        $b = $this->b;
        // self::logger(__METHOD__, 'buffer', $b, gettype($b));
        $out = new \ArrayObject();
        array_walk(
            $b,
            function ($v, $k) use ($out) {
                $out[] = self::conv($k, $v);
            }
        );
        // self::logger(__METHOD__, $out, implode(';', $out));
        // return implode(';', $out->getArrayCopy());
        return base64_encode(implode(';', $out->getArrayCopy()));
    }

    public function __toString()
    {
        return $this->serialize();
    }

    public function unserializeNew($s)
    {
        $this->b = unserialize($s);
    }

    public function unserializeBase($s)
    {
        //$this->b = [];
        // $a = array_filter(explode(';', $s));
        $a = array_filter(explode(';', base64_decode($s)));
        $out = new \ArrayObject();
        //self::logger(__METHOD__, $s, $a);
        array_walk(
            $a,
            function ($v, $k) use ($out) {
                list($out_k, $out_v) = self::unconv($v);
                $out[$out_k] = $out_v;
            }
        );
        $this->b = $out->getArrayCopy();
    }

    public function setForm($form)
    {
        $this->_form = $form;
    }

    public function unsetForm()
    {
        $this->setForm(null);
    }

    /**
     * @warning this function working with valid serial and unique keys
     *
     * @param c
     *
     * @return
     */
    public function findKeyByContext($c)
    {
        $ret = $this->findBindingByContext($c);
        if ($ret) {
            list($key, $elem) = $ret;
            return $key;
        } else {
            return false;
        }
    }

    public function findBindingByContext($c)
    {
        $b = $this->b;
        foreach ($b as $key => $elem) {
            if ($elem->context === $c) {
                return [$key, $elem];
            }
        }
        return null;
    }

    /**
     * Filter elements by name from pool
     *
     * @param string $name
     *
     * @return array of assoicated by keys elements, where element is RenewInfo
     */
    public function findByName($name)
    {
        $b = $this->b;
        $ret = [];
        foreach ($b as $key => $elem) {
            if ($elem->name === $name) {
                $ret[$key] = $elem;
            }
        }
        return $ret;
    }

    public function addCached($key, $def, $value)
    {
        $this->add($key, $def);
        $this->put($key, $value);
    }

    protected function load($key)
    {
        if (isset($this->b[$key])) {
            $t = $this->b[$key];
            if ($t instanceof RenewInfo) {
                return $this->loadRenew($key, $t);
            } else {
                return parent::load($key);
            }
        } else {
            $msg =<<<'EOT'
Primary: unrecognized key: %s
Detail: Access key is not having any association with known objects
Hint: use a valid key is known to the system
EOT;

            throw new \DomainException(Value::sprintf($msg, $key));
        }
    }

    protected function loadLegacy($key)
    {
        $t = $this->b[$key];
        if ($t instanceof RenewInfo) {
            return $this->loadRenew($key, $t);
        } else {
            return parent::load($key);
        }
    }

    protected function loadRenew($k, $r)
    {
        $out = call_user_func(
            [$r->class_name, 'resumeFromForm'], $this->_form, $r->context
        );
        return $this->put($k, $out);
    }

    protected static function conv($k, $v)
    {
        return $k . '%' . ( (string) $v );
    }

    protected static function unconv($s)
    {
        list($k, $v) = explode('%', $s);
        return [$k, RenewInfo::createFromString($v)];
        //return [$k, new \stdClass()];
    }
}
