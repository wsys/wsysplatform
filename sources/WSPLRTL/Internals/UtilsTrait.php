<?php
/**
 * @file
 *
 * @brief UtilsTrait.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Internals\UtilsTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Internals;

use Namedio\Core\Edge\Lang\Strings\Strings;

/**
 * The UtilsTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Internals\UtilsTrait
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
trait UtilsTrait
{
    private static function _undots($s)
    {
        return Strings::undots($s);
    }
}
