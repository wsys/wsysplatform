<?php
/**
 * Facade.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model;

use com\danscode\systype\exception\Generic as SysException;
use com\danscode\traits\Debug;
use com\danscode\systype\LazyInit;
use WSPLRTL\Form\Model\Node\RS\Facade as RS;
use WSPLRTL\Form\Model\Node\RefPool\Facade as RefPool;
use WSAPL\Procd\Access\Peer as ProcdPeerAccess;
use WSPLL\MD\Access\Peer as MDPeerAccess;
use WSPLL\MD\Lib\Peer as LibMDPeer;
use WSPLRTL\Widget\AbstractForm;
use Namedio\Lab\Stored\Packet as StoredPacket;
use Nodelimit\App\Action\Sandbox;
use Namedio\Core\Edge\Lang\Value\Value;

/**
 * Facade.php
 *
 * @class WSPLRTL\Form\Model\Facade
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Facade
    implements
    MDPeerAccess,                /**< @see WSPLL\MD\Lib\Peer */
    ProcdPeerAccess             /**< @see self::getProcdConn() */
{
    const KEY_RUN_INFO = 'run_info';

    private $_director;         /**< @note WSPLRTL\Widget\AbstractForm an instance */
    private $_lazy_rs;
    private $_lazy_refpool;

    use Debug;
    use LibMDPeer;

    private function __construct()
    {
        //
    }

    /**
     * Creates Model
     *
     * @param WSPLRTL\Widget\AbstractForm $director
     *
     * @note $director deps:
     *   + RUN_INFO
     *   + RS/Facade -> RTTI
     *
     * @return object
     */
    public static function create($director)
    {
        $o = new static();
        $o->_director = $director;
        $o->_init();
        return $o;
    }

    /**
     * @brief checking if need to call package initialization associated with form
     *
     *
     * @return void
     */
    public function greeting()
    {
        //
    }

    /**
     * Opens RS-object
     *
     * @param string $key something like: C656781|P_DISCOUNT
     *
     * @see WSPLRTL\Form\Model\Node\RS\Value\Generic
     *
     * @return object instance of WSPLRTL\Form\Model\Node\RS\Value\AbstractValue
     */
    public function RS($key)
    {
        $this->validateRSKey($key);
        $rs = $this->_getRS();
        $v = $rs->getValueNode($key);
        // self::logTraceDump($key, $v, $rs);
        return $v;
    }

    /**
     * Gets RS-facade
     *
     * @return WSPLRTL\Form\Model\Node\RS\Facade
     */
    public function getRSFacade()
    {
        return $this->_getRS();;
    }

    /**
     * Create RS-facade
     *
     * @return WSPLRTL\Form\Model\Node\RS\Facade
     */
    public function createRS()
    {
        return RS::create($this);
    }

    public function createRefPool()
    {
        return RefPool::create($this);
    }

    public function getProcdConn()
    {
        return $this->_getProcdConn();
    }

    public function getRunInfo()
    {
        return $this->_getSysProp(self::KEY_RUN_INFO);
    }

    public function submit($ev)
    {
        $payload = $this->_makeRequestPayloadBase();
        $reply = $this->_request('rs.submit', $payload);
        return true;
    }

    public function submitFinally($ev)
    {
        $payload = $this->_makeRequestPayloadBase();
        $reply = $this->_request('form.submit.finally', $payload);
        return true;
    }

    public function rollback()
    {
        $payload = $this->_makeRequestPayloadBase();
        $this->_request('form.rollback', $payload);
        return true;
    }

    public function commit()
    {
        $payload = $this->_makeRequestPayloadBase();
        $reply = $this->_request('form.commit', $payload);
        return true;
    }

    public function callValidate($data_id, $form_id, $control_id)
    {
        /// @see Nodelimit\WF\Component\Grid\Forge\Row\Assoc
        $payload = $this->_makeRequestPayloadBase();
        // self::console(
        //     __METHOD__,
        //     0,
        //     $payload,
        //     self::getBacktraceString()
        // );
        $payload->data_id = $data_id;
        $payload->form_id = $form_id;
        $payload->control_id = $control_id;
        // self::console(__METHOD__, $payload);
        $this->_request('form.validate', $payload);
        $this->_resetRS();
        return true;
    }

    public function getDirector()
    {
        return $this->_director;
    }

    public function getRefPool()
    {
        return $this->_lazy_refpool->read();
    }

    /**
     * Gets a Form
     *
     * @return WSPLRTL\Widget\AbstractForm an instance
     */
    public function getForm()
    {
        return $this->_director;
    }

    /**
     * Perform key validation
     *
     * @param mixed $key
     *
     * @todo check key existence
     * @todo allow numeric
     * @todo check naming rules
     * @todo check common token
     * @todo check allowed chars
     * @todo check length?
     *
     * @return bool;
     */
    private function validateRSKey($key)
    {
        $fn = function () use ($key) {
            if (is_string($key)) {
                return true;
            } elseif (is_null($key)) {
                throw new \DomainException(
                    'expected a non-null type for the key'
                );
            } elseif (!is_string($key)) {
                throw new \DomainException(
                    'expected string type for key: ' . Value::dump($key)
                );
            } else {
                throw new \LogicException('key validation error');
            }
        };

        list($ok, $ret) = Sandbox::bootstrap(
            $fn
        );

        if ($ok) {
            return true;
        } else {
            $dump = [
                'whoami' => __METHOD__,
                'trace' => self::getBacktraceString(),
                'key' => Value::dump($key, 2),
                'pid' => getmypid(),
            ];
            self::console($dump);
            throw $ret;
        }
    }

    private function _resetRS()
    {
        $rs = $this->_getRS();
        $rs->reset();
    }

    private function _makeRequestPayloadBase()
    {
        return $this->getRunInfo();
    }

    private function _init()
    {
        $this->_lazy_rs = LazyInit::create([$this, 'createRS']);
        $this->_lazy_refpool = LazyInit::create([$this, 'createRefPool']);
        $this->_initLibMDPeer();
    }

    /**
     * Opens RS-facade
     *
     * @return WSPLRTL\Form\Model\Node\RS\Facade
     */
    private function _getRS()
    {
        return $this->_lazy_rs->read();
    }

    /**
     * @brief wrapper calling Package$.Init()
     *
     * @param object $info with structure (example):
     * stdClass Object
     * (
     *     [meth_rec] => Nodelimit\WF\Component\Grid\Forge\Row\Assoc Object
     *         (
     *           ...
     *         )
     *     [new_id] => 516066
     *     [initial_next] => Array
     *         (
     *                     [ID] => 516066
     *                     [CURRENT_FORM] =>
     *                     [NEXT_FORM] => 510243
     *                     [NEXT_FORM_NEED_INIT] => 0
     *         )
     * )
     *
     * @note unused
     *
     * @return void
     */
    private function _initPackage($info)
    {
        $this->_request('form.launch.controller.init', $info);
    }

    /**
     * Perofrm request
     *
     * @param string $cmd can be:
     *   + form.validate
     * @param mixed $payload can be:
     *   + stdObject
     *
     * @return com\wsplatform\syslib\procinit\packet\request\Reply
     */
    private function _request($cmd, $payload)
    {
        $conn = $this->_getProcdConn();
        $channel = $conn->selectChannel();
        $channel->connect();
        $info = $payload;
        $preparedPayload = $this->preparePayload($info);
        $channel->send($cmd, $preparedPayload);
        $packet = $channel->receive();
        $preparedPayload->free();
        return $packet;
    }

    private function preparePayload($payload)
    {
        $packet = new StoredPacket($payload);
        $packet->commit();
        return $packet;
        // return $payload;
    }


    private function _getSysProp($key)
    {
        return $this->_director->getSysProp($key);
    }

    private function _getProcdConn()
    {
        return $this->_getSysProp('procd_conn');
    }

    /**
     * @todo move onto debug
     *
     * @param x
     *
     * @return
     */
    private function _dump($x)
    {
        ob_start();
        var_dump($x);
        return ob_get_clean();
    }
}
