<?php
/**
 * Facade.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Form\Model\Node\RefPool;

use Nodelimit\Systype\Dict;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Facade as Ref;

use com\danscode\traits\Debug;
use WSPLL\MD\Access\Peer as MDPeerAccess;
use WSAPL\Procd\Access\Peer as ProcdPeerAccess;

/**
 * Facade.php
 *
 * description
 *
 * @class WSPLRTL\Form\Model\Node\RefPool\Facade
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Facade implements MDPeerAccess, ProcdPeerAccess
{
    const KEY_POOL = '___refpool';

    private $_director;
    use Debug;

    private function __construct()
    {
        //
    }

    public static function create($director)
    {
        $o = new static();
        $o->_director = $director;
        $o->_init();
        return $o;
    }

    public function open($elem)
    {
        $p = $this->_getPool();
        $k = $elem->getKey();
        if ($p->has($elem->getKey())) {
            return $p[$k];
        } else {
            $prepared = $this->_prepareElement($elem);
            $p[$k] = $prepared;
            return $prepared;
        }
    }

    /**
     * @copydoc WSPLL\MD\Access\Peer::getMetaDataNode()
     */
    public function getMetaDataNode()
    {
        return $this->_director->getMetaDataNode();
    }

    public function getProcdConn()
    {
        return $this->_director->getProcdConn();
    }

    /**
     *
     *
     *
     * @return object Nodelimit\Systype\Dict
     */
    private function _getPool()
    {
        return $this->_getSharedStorage()->getValue($this->_getKey());
    }

    private function _getKey()
    {
        return self::KEY_POOL;
    }

    private function _prepareElement($elem)
    {
        $c = [
            'method_id' => $this->_getMethodID(),
            'procd_conn' => $this->getProcdConn()
        ];
        return Ref::create($elem, $c);
    }

    /**
     * Retrieve ID from method record from MD
     *
     *
     * @return mixed ID
     */
    private function _getMethodID()
    {
        return $this->_director->getRunInfo()->meth_rec['ID'];
    }

    private function _init()
    {
        if ($this->_isInitialPool()) {
            $this->_initPool();
        }
        //self::logTrace($this->_director->getRunInfo());
    }

    private function _initPool()
    {
        $p = $this->_createPool();
        $s = $this->_getSharedStorage();
        $s->setValue($this->_getKey(), $p);
    }

    private function _createPool()
    {
        return new Dict();
    }

    private function _getSharedStorage()
    {
        return $this->_getTopDirecor()->getSharedStorage();
    }

    private function _getTopDirecor()
    {
        return $this->_director->getDirector();
    }

    private function _isInitialPool()
    {
        //return $this->_getTopDirecor()->isInitial();
        $s = $this->_getSharedStorage();
        return !$s->hasValue(self::KEY_POOL);
    }
}
