<?php
/**
 * @file
 *
 * @brief NumberType.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\NumberType
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value;

use WSPLRTL\Form\Model\Node\RS\Value\AbstractValue;

/**
 * The NumberType class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\NumberType
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class NumberType extends AbstractValue
{

    protected function putAsNumber($v)
    {
        // self::logTraceDump(
        //     __METHOD__,
        //     $v
        // );
        // $this->put($v, -1, SQLT_INT);
        // $this->put($v, -1, SQLT_NUM);
        $this->put($v, -1, SQLT_CHR);
        // $this->put($v, -1, SQLT_CHR);
        // $this->setValue($v);
    }

    public function getAsNumber()
    {
        // return $this->typed('number')->getValue();
        $x = $this->typed('number')->getValue();
        // self::logTraceDump(
        //     __METHOD__,
        //     $x
        // );
        return $x;
    }

    public function setAsNumber($v)
    {
        $this->putAsNumber($this->_acceptAsNumber($v));
    }

    private function _acceptAsNumber($v)
    {
        $x = $this->toNumber($v);
        // self::logTraceDump(
        //     __METHOD__,
        //     $x,
        //     $v
        // );
        return $x;
    }
}
