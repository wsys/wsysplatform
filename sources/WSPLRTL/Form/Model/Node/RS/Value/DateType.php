<?php
/**
 * @file
 *
 * @brief DateType.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\DateType
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value;

use WSPLRTL\Form\Model\Node\RS\Value\AbstractValue;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;

/**
 * The DateType class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\DateType
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class DateType extends AbstractValue
{
    use LibClientServiceLocal;

    public static function convertPostValue($v)
    {
        // assume date came in yyyy-mm-dd format
        $d = new \DateTime($v);
        // self::logTraceDump($v);
        // self::console(
        //     gettype($v),
        //     $v,
        //     $d,
        //     $d->format('y-M-d')
        // );
        // return strtoupper($d->format('y-M-d'));
        return strtoupper($d->format('d-M-y'));
    }


    public function setValueWithDefaultFormat($avalue)
    {
        /// assume Y-m-d '2019-07-22'
        $v = strtoupper(date('d-M-y', strtotime($avalue)));
        // $d = new \DateTime($v);
        $this->setValue($v);
    }

    protected function peek()
    {
        return $this->_wrap();
    }

    protected function resetReader()
    {
        parent::resetReader();
        $this->_resetLocalServiceItem('val');
    }

    private function _wrap()
    {
        return $this->_getLocalServiceItem('val');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'val' => function () {
                return $this->_conv();
            }
        ];
    }

    private function _conv()
    {
        return $this->_convert($this->peekValue());
    }

    private function _convert($v)
    {
        // $x = new \DateTime($v);
        // self::console($v, $x, $x->format('Y-m-d'));
        if ($v === null) {
            return null;
        } else {
            $d = \DateTime::createFromFormat('d-M-y', $v);
            $f = $d->format('Y-m-d');
            // self::console(__METHOD__, $f);
            return $f;
            // return $v;
        }
    }
}
