<?php
/**
 * @file
 *
 * @brief BooleanType.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\BooleanType
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value;

use WSPLRTL\Form\Model\Node\RS\Value\AbstractValue;

/**
 * The BooleanType class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\BooleanType
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class BooleanType extends AbstractValue
{

    /**
     * Puts value as bool
     *
     * Default implementation to put value as emulated bool
     *
     * @param bool $v
     *
     * @return void
     */
    protected function putAsBool($v)
    {
        $this->put($this->castBool($v), -1, SQLT_CHR);
    }

    /**
     * Sets value from bool
     *
     * @param bool $v
     *
     * @note this is common method to set value as bool
     *
     * @return void
     */
    public function setAsBool($v)
    {
        $this->putAsBool($this->_acceptAsBool($v));
    }

    /**
     * Accepts a value to field with DB boolean type
     *
     * @param mixed $v
     *
     * @return mixed BOOL|NULL
     */
    private function _acceptAsBool($v)
    {
        if ($v === null) {
            return $v;
        } elseif (is_string($v)) {
            return $this->stringToBool($v);
        } else {
            return (bool) $v;
        }
    }
}
