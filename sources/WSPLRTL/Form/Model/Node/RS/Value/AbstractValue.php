<?php
/**
 * AbstractValue.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value;

use com\danscode\systype\LazyInit;


use com\danscode\traits\Debug;
use WSPLL\MD\Access\Peer as MDPeerAccess;
use Namedio\Core\Edge\Lang\Value\UnifiedTrait;
use WSPLRTL\Form\Model\Node\RS\Value\Typed\TypedTrait;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\Core\Edge\DI\Service\Entity\Pool;
use Nodelimit\Systype\Boxed\Typed\Natural\GenBool;
use Nodelimit\Systype\Boxed\Custom as BoxedCustom;
/**
 * AbstractValue.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\AbstractValue
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractValue implements MDPeerAccess
{
    use Debug;
    use UnifiedTrait;
    use TypedTrait;
    use LibClientServiceLocal;

    private $_director;
    private $_key;

    private $_lazy_ref;

    private function __construct()
    {
        //
    }

    /**
     * Creates AbstractValue object
     *
     * @param WSPLRTL\Form\Model\Node\RS\Facade $director
     * @param string $key is key like "C657111" | "P_*"
     * @param mixed $extra
     *
     * @return object instance of WSPLRTL\Form\Model\Node\RS\Value\AbstractValue
     */
    public static function create($director, $key, $extra = false)
    {
        // self::logTraceDump(
        //     $director, $key, $extra
        // );
        $o = new static();
        $o->_director = $director;
        $o->_key = $key;
        $o->_init();
        return $o;
    }

    public function getValue()
    {
        return $this->peek();
    }

    public function isNull()
    {
        return is_null($this->getValue());
    }

    public function isEmpty()
    {
        $v = $this->getValue();
        return empty($v);
    }

    public function isBlank()
    {
        return $this->isEmpty();
    }

    public function clear()
    {
        $this->setValue(null);
    }

    public function setValue($value)
    {
        $this->_setValue($value);
        if ($value == 'GET_PRICE') {
            // $this->$getValue();
            // $m = $this->getValue;
            // self::$getValue;
        }
    }

    /**
     * Open referenced object
     *
     * Retrieve from RefPool by key associated with object.
     * If it not exists then will created or extracted from storage provided by form.
     *
     * @return object
     */
    public function ref()
    {
        return $this->_lazy_ref->read();
    }

    public function getKey()
    {
        return $this->_key;
    }

    public function openRef()
    {
        $rp = $this->_director->getRefPool();
        return $rp->open($this);
    }

    public function getMetaDataNode()
    {
        return $this->_director->getMetaDataNode();
    }

    public function getLength()
    {
        return $this->_getValueLen();
    }

    public function mbgetLength()
    {
        return $this->_mbgetValueLen();
    }

    /**
     * Create typed container
     *
     * @param Pool $pool
     * @param string $type identifier of type
     *
     * @todo support int type as sub-type of NUMBER (separated)
     *
     * @return BoxedCustom
     */
    public function createTyped($pool, $type)
    {
        switch ($type) {
            case 'bool':
                return $this->_makeBool();
            case 'number':
                return $this->_makeNumber();
            default:
                return $this->_makeGeneric();
        }
    }

    protected function typed($type)
    {
        return $this->_getPool()->get($type);
    }

    protected function put($value, $maxlength = -1, $type = SQLT_CHR)
    {
        $this->_write(
            [$this, '_putValue'],
            ['value' => $value, 'maxlength' => $maxlength, 'type' => $type]
        );
    }

    protected function castBoolToString($v)
    {
        if ($v) {
            return '1';
        } else {
            return '';
        }
    }

    protected function castBool($v)
    {
        return $this->castBoolToString($v);
    }

    protected function stringToBool($v)
    {
        return $this->_stringToBool($v);
    }

    protected function toNumber($v)
    {
        return $this->_toNumber($v);
    }

    /**
     * Peeks value from internal buffer
     *
     * Peeks value by key from internal buffer. If buffer not exists
     * then force to loading and create it by reader.
     *
     * @return mixed => string|NULL
     */
    protected function peekValue()
    {
        return $this->_getReader()->getByKey($this->_key);
    }

    /**
     * Wrapper for peeks value from internal buffer
     *
     * This method similar with peekValue: just call peekValue. Created
     * for possibility to access the buffer by peekValue
     *
     * @see AbstractValue::peekValue
     *
     * @return mixed => string|NULL
     */
    protected function peek()
    {
        return $this->peekValue();
    }

    protected function resetReader()
    {
        $this->_resetReader();
    }

    private function _write(callable $cb, $value)
    {
        call_user_func($cb, $value);
        $this->resetReader();
    }

    private function _writeValue($value)
    {
        $this->_getWriter()->setByKey($this->_key, $value);
    }

    private function _putValue(array $value)
    {
        $this->_getWriter()->put($this->_key, $value);
    }

    private function _setValue($value)
    {
        $this->_write(
            [$this, '_writeValue'],
            $value
        );
    }

    private function _makeGeneric()
    {
        return BoxedCustom::create($this->peek());
    }

    private function _makeNumber()
    {
        return BoxedCustom::create($this->_toNumber($this->peek()));
    }

    private function _makeBool()
    {
        $v = $this->_toBool($this->peek());
        return GenBool::create($v);
    }

    private function _toBool($v)
    {
        if (is_string($v)) {
            return $this->_stringToBool($v);
        } elseif (is_bool($v)) {
            return $v;
        } elseif ($v === null) {
            return $v;
        } else {
            return (bool) $v;
        }
    }

    /**
     * Converts given value to NUMBER type
     *
     * Performs conversion of given value to db-NUMBER type.
     *
     * @param mixed $v expected types:
     * + BOOL
     * + INT|FLOAT
     * + NULL
     * + STRING
     *
     * @throw \InvalidArgumentException when $v did not mattch relevent
     * type: BOOL|INT|FLOAT|NULL|STRING
     *
     * @return mixed NULL|FLOAT
     */
    private function _toNumber($v)
    {
        self::console(__METHOD__, $v, floatval($v));
        if (is_bool($v)) {
            return intval($v);
        } elseif (is_numeric($v)) {
            $f = floatval($v);
            // $n = (int) $f;
            // if (($f - $n) > 0) {
            //     return $f;
            // } else {
            //     return $n;
            // }
            return $f;
        } elseif ($v === null) {
            return null;
        } else {
            throw new \InvalidArgumentException();
        }
    }

    private function _stringToBool($v)
    {
        switch ($v) {
        case '':
        case '0':
            return false;
        case '1':
            return true;
        default:
            return (bool) $v;
        }
    }

    private function _getPool()
    {
        return $this->_getLocalServiceItem('pool');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'pool' => function () {
                return new Pool([$this, 'createTyped']);
            }
        ];
    }

    private function _init()
    {
        $this->_lazy_ref = LazyInit::create([$this, 'openRef']);
    }

    private function _getValueLen()
    {
        return strlen($this->getValue());
    }

    private function _mbgetValueLen()
    {
        return mb_strlen($this->getValue());
    }

    private function _resetReader()
    {
        $this->_getReader()->reset();
    }

    private function _getReader()
    {
        return $this->_director->getReader();
    }

    private function _getWriter()
    {
        return $this->_director->getWriter();
    }
}
