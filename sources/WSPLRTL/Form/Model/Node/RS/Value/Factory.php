<?php
/**
 * Factory.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value;
use com\danscode\traits\Debug;
use WSPLRTL\Form\Model\Node\RS\Facade as RSFacade;
use WSPLRTL\Form\Model\Node\RS\Value\BooleanType;

/**
 * Factory.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Factory
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Factory
{
    use Debug;

    const FS = '\\';

    /**
     * Create an value node object
     *
     * @param WSPLRTL\Form\Model\Node\RS\Facade $director
     * @param string $key
     * @param mixed $extra
     *
     * @note $director needed because IO operations required
     *
     * @return WSPLRTL\Form\Model\Node\RS\Value\Generic
     */
    public static function create(RSFacade $director, $key, $extra = false)
    {
        list($ok, $info) = $director->getRTTI($key);
        $product = false;
        if ($ok) {
            $product = self::_makeClassBase($info['type']);
        } else {
            $product = self::_makeGeneric();
        }
        // self::console($product, $key, $info);
        return $product::create($director, $key, $extra);
    }

    private static function _makeClassBase($type)
    {
        switch ($type) {
        case 'BOOLEAN':
            return self::_makeName('BooleanType');
        case 'NUMBER':
            return self::_makeName('NumberType');
        case 'DATE':
            return self::_makeName('DateType');
        default:
            return self::_makeGeneric();
        }
    }

    private static function _makeGeneric()
    {
        return self::_makeName('Generic');
    }

    private static function _makeName($name)
    {
        return __NAMESPACE__ . self::FS . $name;
    }
}
