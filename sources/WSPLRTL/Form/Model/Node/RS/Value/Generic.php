<?php
/**
 * Generic.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value;

use WSPLRTL\Form\Model\Node\RS\Value\AbstractValue;

/**
 * Generic.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Generic
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Generic extends AbstractValue
{
}
