<?php
/**
 * Facade.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter;

use com\danscode\systype\Arguments;

use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Factory as ClauseFactory;

use WSAPL\ClassesNode\Filter\Clause\Collection\Code;
use com\danscode\traits\Debug;

/**
 * Facade.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Facade implements \IteratorAggregate
{
    use Debug;

    private $_ao;

    private function __construct()
    {
        //
    }

    public static function create()
    {
        $o = new static();
        $o->_init();
        return $o;
    }

    public function add(Arguments $args)
    {
        $cnt = $args->count();
        $elem = false;
        if ($cnt === 5) {
            $elem = ClauseFactory::createRange($args);
        } elseif ($cnt === 3) {
            $elem = ClauseFactory::createSimple($args);
        } else {
            throw new \BadMethodCallException();
        }
        $this->_put($elem);
    }

    public function addWhereSQL(Arguments $args)
    {
        $cnt = $args->count();
        $elem = false;
        if ($cnt === 1) {
            $elem = ClauseFactory::createRaw($args);
        } else {
            throw new \BadMethodCallException();
        }
        $this->_put($elem);
    }

    public function bindWhereSQL(Arguments $args)
    {
        $cnt = $args->count();
        $elem = false;
        if ($cnt === 1) {
            $this->addWhereSQL($args);
        } elseif ($cnt > 1) {
            $elem = ClauseFactory::createPositionalSimple($args);
        } else {
            throw new \BadMethodCallException();
        }
        $this->_put($elem);
    }

    public function bindWhereSQLFormat(Arguments $args)
    {
        $cnt = $args->count();
        $elem = false;
        if ($cnt === 1) {
            $this->addWhereSQL($args);
        } elseif ($cnt > 1) {
            $elem = ClauseFactory::createPositionalFormat($args);
        } else {
            throw new \BadMethodCallException();
        }
        $this->_put($elem);
    }

    public function getIterator()
    {
        return $this->_ao->getIterator();
    }

    /**
     * Render code
     *
     * @see WSAPL\ClassesNode\Filter\Clause\Range
     * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Range
     * @see WSAPL\ClassesNode\Filter\Clause\Unary
     * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Simple
     *
     * @return WSAPL\ClassesNode\Filter\Clause\Collection\Code
     */
    public function render()
    {
        $out = [];
        foreach ($this as $elem) {
            $r = $elem->render();
            $out[] = $r;
            // self::logTraceDump($r, $elem);
        }
        $code = Code::createFromArray($out);
        $code->setSeparator(' AND ');
        // self::logTraceDump(
        //     count($out),
        //     $code->getSeparator(),
        //     (string) $code
        // );
        return $code;
    }

    public function reset()
    {
        $this->clear();
    }

    public function clear()
    {
        $this->_getCollection()->exchangeArray([]);
    }

    private function _init()
    {
        $this->_ao = $this->_createCollection();
    }

    private function _createCollection()
    {
        return new \ArrayObject();
    }

    private function _put($elem)
    {
        $c = $this->_getCollection();
        $c[] = $elem;
    }

    private function _getCollection()
    {
        return $this->_ao;
    }
}
