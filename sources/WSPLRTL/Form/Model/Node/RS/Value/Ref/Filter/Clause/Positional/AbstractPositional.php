<?php
/**
 * @file
 *
 * @brief AbstractPositional.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\AbstractPositional
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional;

use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\AbstractClause;
use com\danscode\systype\Arguments;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\BaseTrait;

/**
 * The AbstractPositional class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\AbstractPositional
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
abstract class AbstractPositional extends AbstractClause
{
    use BaseTrait;

    private $_params;

    public static function createFromArguments(Arguments $args)
    {
        $o = new static(false);
        $o->setDomain($o->_init($args));
        return $o;
    }

    // public function render()
    // {
    //     return (string) $this->getDomain();
    // }

    protected function getField()
    {
        return $this->getDomain();
    }

    protected function getArgsTail()
    {
        return $this->_getParams();
    }

    private function _init(Arguments $args)
    {
        $this->_setParams($args->tail());
        return $args[0];
    }

    private function _setParams(array $p)
    {
        $this->_params = $p;
    }

    private function _getParams()
    {
        return $this->_params;
    }
}
