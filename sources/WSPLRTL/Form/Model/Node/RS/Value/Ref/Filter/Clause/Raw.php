<?php
/**
 * @file
 *
 * @brief Raw.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Raw
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause;

use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\AbstractClause;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\BaseTrait;
use com\danscode\systype\Arguments;
use WSAPL\ClassesNode\Filter\Clause\Factory as FilterClauseFactory;

/**
 * The Raw class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Raw
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Raw extends AbstractClause
{
    private $_clause;

    use BaseTrait;

    public static function createFromArguments(Arguments $args)
    {
        return new static($args[0]);
    }

    protected function createClause()
    {
        if (!isset($this->_clause)) {
            $this->_clause = $this->_createClause();
        }
        return $this->_clause;
    }

    private function _createClause()
    {
        return FilterClauseFactory::createRaw($this->getClauseDomain());
    }
}
