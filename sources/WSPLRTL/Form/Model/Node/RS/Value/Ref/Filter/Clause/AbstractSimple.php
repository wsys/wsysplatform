<?php
/**
 * AbstractSimple.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause;

use Nodelimit\Db\Filter\Access\Operator\Defs;
use Nodelimit\Systype\Message as SystypeMessage;
use Nodelimit\Systype\Boxed\Factory as BoxedFactory;
use WSAPL\ClassesNode\Filter\Clause\Predicate\Factory as PredicateFactory;
use WSAPL\ClassesNode\Filter\Clause\Factory as ClassesNodeClauseFactory;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\BaseTrait;

/**
 * AbstractSimple.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\AbstractSimple
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractSimple extends AbstractClause
{
    use BaseTrait;

    const K_CONDITION = 'condition';
    const K_VALUE = 'value';

    private $_ao;

    protected function __construct($domain)
    {
        parent::__construct($domain);
        $this->_ao = new \ArrayObject();
    }

    protected function embed($element)
    {
        $this->_ao[] = $element;
    }

    protected function makeElement($condition, $value)
    {
        return (object) [
            self::K_CONDITION => $this->_validateConition($condition),
            self::K_VALUE => $value
        ];
    }

    protected function getElementByOffset($offset = 0)
    {
        return $this->getElements()[$offset];
    }

    protected function getElements()
    {
        return $this->_ao;
    }

    protected function getElementCondition($offset = 0)
    {
        return $this->_getElementKey($offset, self::K_CONDITION);
    }

    protected function getElementValue($offset = 0)
    {
        return $this->_getElementKey($offset, self::K_VALUE);
    }

    protected function createPredicate($condition, $value)
    {
        return PredicateFactory::createUnary(
            $this->_normalizeCondition($condition),
            $this->_normalizeValue($value)
        );
    }

    protected function createPredicateByOffset($offset)
    {
        return $this->createPredicate(
            $this->getElementCondition($offset),
            $this->getElementValue($offset)
        );
    }

    protected function createPredicate1()
    {
        return $this->createPredicateByOffset(0);
    }

    private function _getElementKey($offset, $key)
    {
        return $this->getElementByOffset($offset)->$key;
    }

    private function _validateConition($condition)
    {
        if (!$this->_isValidCondition($condition)) {
            self::_raiseInvalidCondition($condition);
        }
        return $condition;
    }

    private function _isValidCondition($condition)
    {
        static $values = [
            Defs::LIKE, Defs::NOT_LIKE, Defs::EQ, Defs::NE,
            Defs::LT, Defs::LE, Defs::GT, Defs::GE, Defs::IS_NULL,
            Defs::IS_NOT_NULL
        ];
        return in_array($condition, $values, true);
    }

    private function _normalizeCondition($condition)
    {
        return $this->_boxed($condition);
    }

    private function _normalizeValue($value)
    {
        return $this->_boxed($value);
    }

    private function _boxed($term)
    {
        return BoxedFactory::createMatch($term);
    }

    static private function _raiseInvalidCondition($condition)
    {
        throw new \DomainException(
            sprintf(
                'operator: %s is not valid',
                (string) SystypeMessage::create($condition)
            )
        );
    }
}
