<?php
/**
 * Factory.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause;

use com\danscode\systype\Arguments;

use com\danscode\systype\exception\Generic as SysException;
// use WSAPL\ClassesNode\Filter\Clause\Predicate\Raw;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Raw;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\Format;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\Simple
    as PositionalSimple;

/**
 * Factory.php
 *
 * description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Factory
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Factory
{
    public static function createRange(Arguments $args)
    {
        return Range::createFromArguments($args);
    }

    public static function createSimple(Arguments $args)
    {
        return Simple::createFromArguments($args);
    }

    public static function createRaw(Arguments $args)
    {
        return Raw::createFromArguments($args);
    }

    public static function createPositionalFormat(Arguments $args)
    {
        return Format::createFromArguments($args);
    }

    public static function createPositionalSimple(Arguments $args)
    {
        return PositionalSimple::createFromArguments($args);
    }
}
