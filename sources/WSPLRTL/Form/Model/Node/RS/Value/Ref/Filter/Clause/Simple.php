<?php
/**
 * Simple.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause;

use com\danscode\systype\Arguments;
use com\danscode\systype\exception\Generic as SysException;
use WSAPL\ClassesNode\Filter\Clause\Factory as ClassesNodeClauseFactory;

/**
 * Simple.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Simple
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Simple extends AbstractSimple
{
    private $_clause;

    public function __construct($domain, $condition, $value)
    {
        parent::__construct($domain);
        $this->embed($this->_createElement($condition, $value));
    }

    public static function createFromArguments(Arguments $args)
    {
        return new static($args[0], $args[1], $args[2]);
    }

    public static function create(
        $domain, $condition, $value
    ) {
        return new static($domain, $condition, $value);
    }

    /**
     * Create clause
     *
     * @return WSAPL\ClassesNode\Filter\Clause\Unary
     */
    protected function createClause()
    {
        if (!isset($this->_clause)) {
            $this->_clause = $this->_createClause();
        }
        return $this->_clause;
    }

    /**
     * Create clause
     *
     * @return WSAPL\ClassesNode\Filter\Clause\Unary
     */
    private function _createClause()
    {
        // @todo there are is no information about bindings here but it created early
        // @todo who create ref filter clause

        // self::console(
        //     __METHOD__, $this->getClauseDomain(), $this->createPredicate1()
        // );
        $u = ClassesNodeClauseFactory::createUnary(
            $this->getClauseDomain(),
            $this->createPredicate1()
        );
        // self::console(
        //     __METHOD__,
        //     $u,
        //     (string) $u,
        //     $this->getBindings()
        // );
        return $u;
    }

    private function _createElement($condition, $value)
    {
        return $this->makeElement($condition, $value);
    }
}
