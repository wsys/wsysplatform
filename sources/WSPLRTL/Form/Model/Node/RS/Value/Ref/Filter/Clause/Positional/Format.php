<?php
/**
 * @file
 *
 * @brief Format.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\Format
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional;

use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\AbstractPositional;
use WSAPL\ClassesNode\Filter\Clause\Factory as FilterClauseFactory;

/**
 * The Format class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\Format
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Format extends AbstractPositional
{
    protected function createClause()
    {
        return FilterClauseFactory::createPositionalFormat(
            $this->getClauseDomain(), $this->getArgsTail()
        );
    }
}
