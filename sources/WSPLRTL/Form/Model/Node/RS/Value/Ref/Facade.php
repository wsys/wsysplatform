<?php
/**
 * Facade.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref;

use Nodelimit\Systype\Dict;
use com\danscode\systype\Arguments;


use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade as Filter;
use com\danscode\traits\Debug;

use WSAPL\Procd\Access\Peer as ProcdPeerAccess;
use WSPLL\MD\Access\Peer as MDPeerAccess;
use WSPLL\MD\Lib\Peer as LibMDPeer;

use Nodelimit\WF\Component\Grid\Forge\Row\AbstractRow;
use Nodelimit\WF\Component\Grid\Access\Row as AccessRow;
use WSPLRTL\Widget\Form\API\Filter\Internals\AliasTrait;
use WSPLRTL\Widget\Form\API\Filter\Internals\InternalsTrait;
use WSPLRTL\Widget\Form\API\Filter\Internals\StdTrait;
use Namedio\Core\Edge\Lang\Value\Value;

/**
 * Facade.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Facade
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Facade
    implements
    \Serializable,
    MDPeerAccess,                /**< @see WSPLL\MD\Lib\Peer */
    ProcdPeerAccess             /**< @see self::getProcdConn() */
{
    private $_dict;

    use Debug;
    use LibMDPeer;
    use AliasTrait;
    use InternalsTrait;
    use StdTrait;

    private function __construct()
    {
        //
    }

    public static function create($originator, $context)
    {
        $o = new static();
        $o->_initialize($originator, $context);
        return $o;
    }

    public function serialize()
    {
        return serialize($this->_dict);
    }

    /**
     * Unserialize
     *
     * @param string $s
     *
     * @warning after unserialization calling _init required to attach LibMDPeer
     *
     * @return
     */
    public function unserialize($s)
    {
        $this->_dict = unserialize($s);
        $this->_init();
    }

    public function setCriteriaHint($hint)
    {
        $this->_openCriteriaHints()[] = $hint;
    }

    public function getProcdConn()
    {
        return $this->_dict['context']['procd_conn'];
    }

    /**
     * Sets Criteria
     *
     * @param short_name
     *
     * @return void
     */
    public function setCriteria($short_name)
    {
        $mdn = $this->getMetaDataNode();
        $v = $mdn->getById('views');
        $c = $this->_dict['context'];
        $r = $v->locate($c['target_class_id'], $c['scheme_id'], $short_name);
        //self::logTrace($r, $c, $short_name);
        //$this->_dict['criteria_id'] = $r['ID'];
        $this->_addCriteria($r);
    }

    public function setMode($mode)
    {
        $this->_dict->offsetSet('mode', $this->_validateMode($mode));
    }

    public function getMode()
    {
        return $this->_dict->getSafe('mode', 'choice');
    }

    public function isAnyCriteriaDefined()
    {
        $d = $this->_dict;
        return $d->has('criterias') && $d->offsetGet('criterias')->count() > 0;
    }

    /**
     *
     *
     * @param criteria_id
     *
     * @return bool
     */
    public function isCriteriaDefined($criteria_id)
    {
        return (bool) $this->_findCriteriaById($criteria_id);
    }

    public function getCriteriaInfo($offset = 0)
    {
        $c = $this->_getCriterias()[$offset];
        $ctx = $this->_getContext();
        return (object) [
            'id' => $c,
            'hints' => $this->_openCriteriaHints(),
            'filter' => $this->_getFilterByCriteraID($c),
            'target_class_id' => $ctx['target_class_id']
        ];
    }

    public function getCriteriasCount()
    {
        return $this->_getCriterias()->count();
    }

    /**
     * @warning removing filter not linked with deps
     * @note changes not written immediate in storage
     *
     * @todo proposal observers, when reset
     *
     *
     * @return void
     */
    public function reset()
    {
        $this->_removeEntity('criterias');
        $this->_removeEntity('filter');
        $this->_removeEntity('hints');
        $this->_removeEntity('mode');
    }

    protected function getFilterHost()
    {
        return $this->_openFilter();
    }

    protected function hasCriteria()
    {
        return $this->_hasCriteria();
    }

    protected function getCriteriaId()
    {
        return $this->_getCriteriaId();
    }

    private function _validateMode($mode)
    {
        if ($mode && in_array($mode, ['choice', 'show'], true)) {
            return $mode;
        } else {
            throw new \DomainException(
                Value::sprintf(
                    'could not accept mode: %s '.
                    '(there is no association for such mode'.
                    ')',
                    $mode
                )
            );
        }
    }

    private function _getFilterByCriteraID($criteria_id)
    {
        return $this->getFilterHost();
    }

    private function _getContext()
    {
        return $this->_dict['context'];
    }

    private function _getCriterias()
    {
        // return $this->_dict['criterias'];
        return $this->_dict->getSafeExt(
            'criterias',
            function () {
                return new \ArrayObject();
            }
        );
    }

    /**
     *
     *
     * @param offset
     *
     * @return bool
     */
    private function _hasCriteria($offset = 0)
    {
        return isset($this->_dict['criterias'][$offset]);
    }

    /**
     *
     *
     * @param offset
     *
     * @return string
     */
    private function _getCriteria($offset = 0)
    {
        return $this->_dict['criterias'][$offset];
    }

    private function _getCriteriaId()
    {
        return $this->_getCriteria();
    }

    /**
     * @warning used position-based index not key-based
     *
     * @param AccessRow $r
     *
     * @return void
     */
    private function _addCriteria(AccessRow $r)
    {
        $d = $this->_dict;
        $c = false;
        if (!$d->has('criterias')) {
            $c = new \ArrayObject();
            $d['criterias'] = $c;
        } else {
            $c = $d['criterias'];
        }
        $id = $r['ID'];
        //$k = $id;
        //$c[$k] = $id;
        $c[] = $id;
    }

    /**
     * @note this method is safe for undefined criterias
     *
     * @param id
     *
     * @return mixed FALSE if not found
     */
    private function _findCriteriaById($id)
    {
        if (!$this->isAnyCriteriaDefined()) {
            return false;
        }
        $c = $this->_getCriterias();
        $a = $c->getArrayCopy();
        return array_search($id, $a, true);
    }

    /**
     * Opens filter
     *
     * @return WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade
     */
    private function _openFilter()
    {
        return $this->_openEntity('filter');
    }

    private function _openCriteriaHints()
    {
        return $this->_openEntity('hints');
    }

    private function _createHints()
    {
        return new \ArrayObject();
    }

    private function _openEntity($k)
    {
        $d = $this->_dict;
        if (!$d->has($k)) {
            $d[$k] = call_user_func([$this, '_create' . ucfirst($k)]);
        }
        return $d[$k];
    }

    private function _removeEntity($k)
    {
        $d = $this->_dict;
        if ($d->has($k)) {
            unset($d[$k]);
        }
    }

    /**
     * Create filter
     *
     * @return WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade
     */
    private function _createFilter()
    {
        return Filter::create();
    }

    private function _init()
    {
        $this->_initLibMDPeer();
    }

    private function _initialize($originator, $context)
    {
        $this->_dict = new Dict();
        $this->_dict['context'] = self::_acceptContext($originator, $context);
        $this->_init();
    }

    private static function _acceptContext($originator, $context)
    {
        $mdn = $originator->getMetaDataNode();
        $mp = $mdn->getById('method_param')->findReference(
            $context['method_id'], $originator->getKey()
        );
        $target_class_id = $mp['TARGET_CLASS_ID'];
        $out = [
            'target_class_id' => $target_class_id,
            'scheme_id' => $mp['TARGET_SCHEME_ID'],
            'procd_conn' => $context['procd_conn'],
            /// @warning this valid: class_id is not similar with TARGET_CLASS_ID
            'class_id' => $mp['TARGET_CLASS']
        ];
        return $out;
    }

    private static function _acceptContextLegacy($originator, $context)
    {
        $mdn = $originator->getMetaDataNode();
        // self::console($context['method_id'], $originator->getKey());
        $mp = $mdn->getById('method_param')->findReference(
            $context['method_id'], $originator->getKey()
        );
        $target_class_id = $mp['TARGET_CLASS_ID'];
        $classes = $mdn->getById('classes')->getById($target_class_id);
        $out = [
            'target_class_id' => $target_class_id,
            'scheme_id' => $classes['SCHEME_ID'],
            'procd_conn' => $context['procd_conn'],
            'class_id' => $classes['CLASS_ID']
        ];
        //self::logTrace($out);
        return $out;
    }
}
