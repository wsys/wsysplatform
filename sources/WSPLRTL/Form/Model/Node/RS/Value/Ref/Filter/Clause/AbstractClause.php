<?php
/**
 * AbstractClause.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause;

use Nodelimit\Exception\UnexpectedValue;
use com\danscode\systype\Arguments;
use com\danscode\traits\Debug;

/**
 * AbstractClause.php
 *
 * @todo merge with ClassesNode AbstractClause
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\AbstractClause
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractClause
{
    use Debug;

    private $_domain;

    protected function __construct($domain)
    {
        $this->setDomain($domain);
    }

    protected function setDomain($domain)
    {
        $this->_domain = $domain;
    }

    public static function createFromArguments(Arguments $args)
    {
        throw new \RuntimeException('Unimplemented');
    }

    /**
     * Retrieve domain
     *
     * Extract private field, that hold domain information about clause
     *
     * @return object
     */
    public function getDomain()
    {
        return $this->_domain;
    }

    public function getBindings()
    {
        // return $this->_getClause()->getBindings();
        $c = $this->_getClause();
        $b = $c->getBindings();
        self::logTrace($c, $b);
        return $b;
    }

    public function __toString()
    {
        return $this->_stringify();
    }

    public function render()
    {
        return $this->_getClause();
    }

    private function _stringify()
    {
        return (string) $this->render();
    }

    private function _getClause()
    {
        return $this->renderCode();
    }

    abstract public function renderCode();
}
