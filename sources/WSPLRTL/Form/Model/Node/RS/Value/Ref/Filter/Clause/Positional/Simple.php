<?php
/**
 * @file
 *
 * @brief Simple.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\Simple
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional;

use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\Format;
use WSAPL\ClassesNode\Filter\Clause\Factory as FilterClauseFactory;

/**
 * The Simple class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Positional\Simple
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Simple extends Format
{
    protected function createClause()
    {
        return FilterClauseFactory::createPositionalSimple(
            $this->getClauseDomain(), $this->getArgsTail()
        );
    }
}
