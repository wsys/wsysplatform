<?php
/**
 * @file
 *
 * @brief BaseTrait.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\BaseTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause;

/**
 * The BaseTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\BaseTrait
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
trait BaseTrait
{
    abstract protected function createClause();

    protected function getClauseDomain()
    {
        return $this->getDomain();
    }

    public function renderCode()
    {
        return $this->createClause();
    }
}
