<?php
/**
 * Range.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause;

use com\danscode\systype\Arguments;
use com\danscode\systype\exception\Generic as SysException;

use WSAPL\ClassesNode\Filter\Clause\Factory as ClassesNodeClauseFactory;

use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\AbstractSimple;
use Nodelimit\Systype\Boxed\Factory as BoxedFactory;
use WSAPL\ClassesNode\Filter\Clause\Predicate\Factory as PredicateFactory;

/**
 * Range.php
 *
 * description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Clause\Range
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Range extends AbstractSimple
{
    private $_clause;

    protected function __construct(
        $domain, $condition1, $value1, $condition2, $value2
    ) {
        parent::__construct($domain);
        $this->embed($this->_createElement($condition1, $value1));
        $this->embed($this->_createElement($condition2, $value2));
    }

    private function _createElement($condition, $value)
    {
        return $this->makeElement($condition, $value);
    }

    public static function createFromArguments(Arguments $args)
    {
        return self::create($args[0], $args[1], $args[2], $args[3], $args[4]);
    }

    /**
     * @todo typehinting for parameters
     * @todo documenting
     *
     * @param domain
     * @param condition1
     * @param value1
     * @param condition2
     * @param value2
     *
     * @return
     */
    public static function create(
        $domain, $condition1, $value1, $condition2, $value2
    ) {
        return new static($domain, $condition1, $value1, $condition2, $value2);
    }

    protected function createClause()
    {
        if (!isset($this->_clause)) {
            $this->_clause = $this->_createClause();
        }
        return $this->_clause;
    }

    private function _createClause()
    {
        return ClassesNodeClauseFactory::createRange(
            $this->getClauseDomain(),
            $this->createPredicate1(),
            $this->_createPredicate2()
        );
    }

    private function _createPredicate2()
    {
        return $this->createPredicateByOffset(1);
    }
}
