<?php
/**
 * @file
 *
 * @brief TypedTrait.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\Value\Typed\TypedTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Value\Typed;

/**
 * The TypedTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Form\Model\Node\RS\Value\Typed\TypedTrait
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
trait TypedTrait
{
    abstract public function setValue($v);

    abstract protected function typed($type);

    /**
     * Perform casting given bool value
     *
     * Performs casting given value to value supported by low-level
     * driver. In most cases it's convert bool value to string value
     *
     * @param bool $v
     *
     * @see WSPLRTL\Form\Model\Node\RS\Value\AbstractValue::castBoolToString
     * @see WSPLRTL\Form\Model\Node\RS\Value\AbstractValue::castBool
     * @see WSPLRTL\Form\Model\Node\RS\Value\AbstractValue
     *
     * @note for bool type of a value object this method is not used
     * @note this method required for non-boolean value objects
     * @note default implementation are defined in AbstractValue
     *
     * @return mixed in most cases is string as default implementation
     */
    abstract protected function castBool($v);

    abstract protected function stringToBool($v);

    protected static function ensureBool($v)
    {
        if (is_bool($v)) {
            return true;
        } else {
            throw new \InvalidArgumentException();
        }
    }

    // protected function putAsBool($v)
    // {
    //     $this->setValue($this->castBool($v));
    // }

    // protected function acceptAsBool($v)
    // {
    //     $this->_acceptAsBool($v);
    // }

    public function getAsBool()
    {
        return $this->typed('bool')->getValue();
    }
}
