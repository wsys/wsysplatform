<?php
/**
 * @file
 *
 * @brief RTTI.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Model\Node\RS\RTTI
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS;

use Nodelimit\DI\Service\AbstractManaged;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\Core\Edge\DI\Service\Entity\Pool;

/**
 * The RTTI class
 *
 * Description
 *
 * @class WSPLRTL\Form\Model\Node\RS\RTTI
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class RTTI extends AbstractManaged
{
    use LibClientServiceLocal;

    private static $_keys = [
        'form'
    ];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    public function get($key)
    {
        // self::console($this->_getLocalServiceItem('pool')->get($key));
        return $this->_getLocalServiceItem('pool')->get($key);
    }

    public function createInfo($pool, $key)
    {
        $m = $this->_getLocalServiceItem('rs.map');
        if (isset($m[$key])) {
            return $this->_makeElement($m[$key]);
        } else {
            return $this->_makeElementUnknown($key);
        }
    }

    private function _makeElement(array $info)
    {
        return $this->_make(true, ['type' => $info['type']]);
    }

    private function _makeElementUnknown($key)
    {
        return $this->_make(false, false);
    }

    private function _make($status, $v)
    {
        return [$status, $v];
    }

    private function _getLocalServiceBindings()
    {
        return [
            'rs.map' => function () {
                return $this->getServiceItem('form')->getRSMap();
            },
            'pool' => function () {
                return new Pool([$this, 'createInfo']);
            }
        ];
    }
}
