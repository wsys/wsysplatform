<?php
/**
 * Writer.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Flow;

use Namedio\Lab\Stored\Packet;
use WSPLRTL\Form\Model\Node\RS\Flow\AbstractFlow;
use Namedio\Ext\System\System;

/**
 * Writer.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Flow\Writer
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Writer extends AbstractFlow
{
    public function setByKey($key, $value)
    {
        // $dump = [
        //     'method' => __METHOD__,
        //     'key' => $key,
        //     'value' => $value,
        //     'value.hex.dump' => bin2hex($value),
        //     'value.unsigned-char.dump' => unpack('C*', $value),
        //     'value.strlen' => strlen($value),
        //     'value.mb_strlen' => mb_strlen($value)
        // ];
        // self::console($dump);
        // self::logger(__METHOD__, 'rs.set');
        // usleep(1000);
        $ret = $this->spool($key, $value, 'rs.set');
        // $memory = System::getMemory();
        // self::logger(__METHOD__, $key, $value, $memory['swap']);
        // $this->gc();
        return $ret;
    }

    /**
     * Puts data to DB
     *
     * Puts data by command rs.put provided by $value structure onto rs
     * record into DB
     *
     * @param string $key filed name
     * @param array $value assoc array:
     * + mixed value
     * + integer maxlength
     * + string type
     *
     * @return mixed result of the action
     */
    public function put($key, array $value)
    {
        // self::logger(__METHOD__, 'rs.put');
        return $this->spool($key, $value, 'rs.put');
    }

    public function flush()
    {
        static $n = 0;
        // self::logger(__METHOD__, 'flush start', memory_get_usage() / 1048576);
        $b = $this->getBuffer();
        $this->sendCmd($b, 'rs.buffered');
        // self::logger(__METHOD__, 'flush sent', memory_get_usage() / 1048576);
        $this->clearBuffer();
        // self::logger(__METHOD__, 'flush clear', memory_get_usage() / 1048576);
        // gc_collect_cycles();
        self::logger(__METHOD__, 'flushing', ++$n, memory_get_usage() / 1048576);
    }

    private function gc()
    {
        static $n = 0;
        if (++$n % 5 == 0) {
            $r = gc_collect_cycles();
            self::logger(__METHOD__, $n, $r, gc_enabled());
        }
    }

    private function spool($key, $value, $cmd)
    {
        // return $this->sendCmd([$key, $value], $cmd);
        if ($this->isBuffering()) {
            return $this->spoolKV($key, $value, $cmd);
        } else {
            return $this->sendCmd([$key, $value], $cmd);
        }
    }

    private function spoolKV($key, $value, $cmd)
    {
        $b = $this->getBuffer();
        $b[] = array_combine(['key', 'value', 'cmd'], [$key, $value, $cmd]);
    }

    /**
     * Send command
     *
     * @param string $key
     * @param mixed $value
     * @param string $cmd
     *  + rs.set
     *
     * @return mixed
     */
    private function sendCmd($data, $cmd)
    {
        $payload = $this->_makeRequestPayload();
        $packet = $this->setPayloadInfo($payload, $data);
        $packet->commit();
        $ret = $this->sendCommand($cmd, $packet);
        $packet->free();
        return $ret->payload;
    }

    /**
     * Sets payload info
     *
     * @param object $payload can be:
     *   + \stdClass
     * @param string $key
     * @param mixed $value can be:
     *   + string
     *   + numeric
     *   + boolean
     *   + NULL
     *
     * @return Packet
     */
    private function setPayloadInfo($payload, $data)
    {
        $payload->info = $data;
        $packet = new Packet($payload);
        return $packet;
    }

    /**
     * Clearing payload
     *
     * @param mixed $payload can be:
     *  + stdClass
     *
     * @warning this important because self::_makeRequestPayload() returns reference
     *
     * @todo make safe field for this or else
     *
     * @return void
     */
    private function clearPayload($payload)
    {
        $payload->info = null;
        unset($payload->info);
    }

    private function _makeRequestPayload()
    {
        return $this->getRunInfo();
    }
}
