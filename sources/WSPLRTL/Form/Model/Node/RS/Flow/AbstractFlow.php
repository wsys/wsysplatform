<?php
/**
 * AbstractFlow.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS\Flow;

use com\danscode\systype\exception\Generic as SysException;
use com\danscode\traits\Debug;

/**
 * AbstractFlow.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Flow\AbstractFlow
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractFlow
{
    private $_director;
    private $buffer;

    use Debug;

    private function __construct()
    {
        $this->buffer = new \ArrayObject();
    }

    public static function create($director)
    {
        $o = new static();
        $o->_director = $director;
        $o->_init();
        return $o;
    }

    private function _init()
    {
        $this->initialize();
    }


    protected function initialize()
    {
        //noop
    }

    protected function isBuffering()
    {
        return $this->getDirector()->isBuffering();
    }

    protected function getBuffer()
    {
        return $this->buffer;
    }

    protected function clearBuffer()
    {
       $this->buffer->exchangeArray([]);
    }

    final protected function sendCommand($cmd, $payload)
    {
        return $this->_request($cmd, $payload);
    }

    final protected function getRunInfo()
    {
        return $this->_director->getRunInfo();
    }

    /**
     * @todo this code similar as in Model
     * @see WSPLRTL\Form\Model\Facade
     *
     * @param cmd
     * @param payload
     *
     * @return
     */
    private function _request($cmd, $payload)
    {
        $conn = $this->_getProcdConn();
        $channel = $conn->selectChannel();
        $channel->connect();
        $info = $payload;
        $channel->send($cmd, $info);
        $packet = $channel->receive();
        return $packet;
    }

    private function _getProcdConn()
    {
        return $this->_director->getProcdConn();
    }

    private function getDirector()
    {
        return $this->_director;
    }

}
