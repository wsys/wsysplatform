<?php
/**
 * Reader.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Form\Model\Node\RS\Flow;

use com\danscode\systype\exception\Generic as SysException;
use com\danscode\systype\LazyInit;
use WSPLRTL\Form\Model\Node\RS\Flow\AbstractFlow;

/**
 * Reader.php
 *
 * description
 *
 * @class WSPLRTL\Form\Model\Node\RS\Flow\Reader
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Reader extends AbstractFlow
{
    private $_lazy_data;

    protected function initialize()
    {
        parent::initialize();
        $this->_lazy_data = LazyInit::create([$this, 'createData']);
    }

    public function createData()
    {
        $payload = $this->_makeRequestPayload();
        $ret = $this->sendCommand('rs.get', $payload);
        // self::console($ret);
        return $ret->payload;
    }

    public function getKeys()
    {
        return $this->_getDataKeys();
    }

    public function getByKey($key)
    {
        return $this->_getData()[$key];
    }

    public function reset()
    {
        $this->_lazy_data->reset();
    }

    public function getBuffer()
    {
        return $this->_getData();
    }

    /**
     * @todo almost similar as in model

     **
     *
     * @return
     */
    private function _makeRequestPayload()
    {
        return $this->getRunInfo();
    }

    private function _getDataKeys()
    {
        return array_keys($this->_getData());
    }

    private function _getData()
    {
        return $this->_lazy_data->read();
    }
}
