<?php
/**
 * Facade.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Form\Model\Node\RS;

use com\danscode\systype\LazyInitPool;
use com\danscode\systype\LazyInit;

use com\danscode\systype\exception\Generic as SysException;

use WSPLRTL\Form\Model\Node\RS\Flow\Reader;
use WSPLRTL\Form\Model\Node\RS\Flow\Writer;

use WSPLRTL\Form\Model\Node\RS\Value\Factory as ValueNodeFactory;


use com\danscode\traits\Debug;
use WSPLL\MD\Access\Peer as MDPeerAccess;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use WSPLRTL\Form\Model\Facade as Model;

/**
 * Facade.php
 *
 * @class WSPLRTL\Form\Model\Node\RS\Facade
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Facade implements MDPeerAccess
{
    use Debug;
    use LibClientServiceLocal;

    private $_director;
    private $_pool;
    private $_lazy_reader;
    private $_lazy_writer;
    private $buffering;

    private function __construct()
    {
        //
    }

    /**
     * Creates Facade
     *
     * @param WSPLRTL\Form\Model\Facade $director
     *
     * @return object
     */
    public static function create($director)
    {
        $o = new static();
        $o->_director = $director;
        $o->_init();
        return $o;
    }

    private function _init()
    {
        $this->_initReader();
        $this->_initWriter();
        $this->_initPool();
    }

    private function _initReader()
    {
        $this->_lazy_reader = LazyInit::create([$this, 'createReader']);
    }

    private function _initWriter()
    {
        $this->_lazy_writer = LazyInit::create([$this, 'createWriter']);
    }

    private function _initPool()
    {
        $this->_pool = LazyInitPool::create(
            [$this, 'createValueNode'],
            $this->_getPoolBindings()
        );
    }

    /**
     * Make bindings for pool
     *
     * @return array
     */
    private function _getPoolBindings()
    {
        $keys = $this->_getReader()->getKeys();
        $out = [];
        foreach ($keys as $k) {
            $out[$k] = true;    /**< @note by default initial is true */
        }
        // self::console($keys);
        return $out;
    }

    private function _getReader()
    {
        return $this->_lazy_reader->read();
    }

    private function _getWriter()
    {
        return $this->_lazy_writer->read();
    }

    /**
     * Makes value node
     *
     * @param string $key
     * @param mixed $initial
     *
     * @return object instance of WSPLRTL\Form\Model\Node\RS\Value\AbstractValue
     */
    private function _makeValueNode($key, $initial)
    {
        return ValueNodeFactory::create($this, $key, $initial);
    }

    private function _getPool()
    {
        return $this->_pool;
    }

    private function _getLocalServiceBindings()
    {
        return [
            'form' => function () {
                return $this->_director->getForm();
            },
            'rtti' => function () {
                return new RTTI(['form' => $this->_getLocalServiceItem('form')]);
            }
        ];
    }

    public function createReader()
    {
        return Reader::create($this);
    }

    public function createWriter()
    {
        return Writer::create($this);
    }

    /**
     * Creates value node (rs-object)
     *
     * @param string $key
     * @param mixed $initial
     *
     * @return object instance of WSPLRTL\Form\Model\Node\RS\Value\AbstractValue
     */
    public function createValueNode($key, $initial)
    {
        return $this->_makeValueNode($key, $initial);
    }

    public function getValueNode($key)
    {
        return $this->_pool[$key];
    }

    public function getProcdConn()
    {
        return $this->_director->getProcdConn();
    }

    public function getRunInfo()
    {
        return $this->_director->getRunInfo();
    }

    public function getReader()
    {
        return $this->_getReader();
    }

    public function getWriter()
    {
        return $this->_getWriter();
    }

    public function getRefPool()
    {
        return $this->_director->getRefPool();
    }

    public function getMetaDataNode()
    {
        return $this->_director->getMetaDataNode();
    }

    public function reset()
    {
        $this->getReader()->reset();
        $this->_getPool()->clear();
    }

    public function getRTTI($key)
    {
        return $this->_getLocalServiceItem('rtti')->get($key);
    }

    public function startBuffering()
    {
        $this->setBuffering(true);
    }

    public function stopBuffering()
    {
        // $this->ensurePass();
        $this->setBuffering(false);
    }

    public function isBuffering()
    {
        return $this->getBuffering();
    }

    private function setBuffering($enable)
    {
        $this->buffering = $enable;
        if (!$enable) {
            $this->getWriter()->flush();
        }
    }

    private function getBuffering()
    {
        return $this->buffering;
    }

    private function ensurePass()
    {
        static $i = 0;
        if (++$i > 1000) {
            throw new \Exception('ensurePass exhausted');
        }
    }
}
