<?php
/**
 * Dispatch.php
 *
 * @page WSPLRTL\Form\Action\Combobox\Dispatch
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Action\Combobox\Dispatch
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Form\Action\Combobox\Dispatch package
 */
namespace WSPLRTL\Form\Action\Combobox;

use WSPLRTL\Form\Action\AbstractAction;

use com\danscode\wf as WF;

use Nodelimit\Exception\Raise;

/**
 * Dispatch.php
 *
 * Description
 *
 * @class WSPLRTL\Form\Action\Combobox\Dispatch
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Form\Action\Combobox package
 */
class Dispatch extends AbstractAction
{

    public function run()
    {
        $this->getSender()->dispatch($this->getEvent());
    }
}
