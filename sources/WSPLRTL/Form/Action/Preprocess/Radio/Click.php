<?php
/**
 * Click.php
 *
 * @page WSPLRTL\Form\Action\Preprocess\Radio\Click
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Action\Preprocess\Radio\Click
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Form\Action\Preprocess\Radio package
 */
namespace WSPLRTL\Form\Action\Preprocess\Radio;

use WSPLRTL\Form\Action\AbstractAction;

/**
 * Click.php
 *
 * Description
 *
 * @class WSPLRTL\Form\Action\Preprocess\Radio\Click
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Form\Action\Preprocess\Radio package
 */
class Click extends AbstractAction
{
    public function run()
    {
        self::logTraceDump(
            __METHOD__,
            $this->getForm(),
            $this->getEvent()
        );
    }
}
