<?php
/**
 * AbstractAction.php
 *
 * @page WSPLRTL\Form\Action\AbstractAction
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Form\Action\AbstractAction
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Form\Action package
 */
namespace WSPLRTL\Form\Action;

use WSPLRTL\Widget\Action\Event\AbstractEvent;
use Nodelimit\Exception\Raise;

/**
 * AbstractAction.php
 *
 * Description
 *
 * @class WSPLRTL\Form\Action\AbstractAction
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Form\Action\AbstractAction package
 */
abstract class AbstractAction extends AbstractEvent
{
    protected function getForm()
    {
        return $this->getSubject();
    }
}
