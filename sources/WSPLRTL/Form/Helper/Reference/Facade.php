<?php
/**
 * Facade.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Form\Helper\Reference;

use com\danscode\traits\Debug;

use com\danscode\systype\exception\Generic as SysException;

use WSAPL\Reference\Facade as ReferenceFacade;

use WSAPL\Procd\Access\Peer as ProcdPeerAccess;
use WSPLL\MD\Access\Peer as MDPeerAccess;
use WSPLL\MD\Lib\Peer as LibMDPeer;

use Nodelimit\WF\Component\Grid\Forge\Row\Assoc as RowAssoc;

use WSAPL\Reference\Data\Factory as ReferenceDataFactory;
use WSAPL\Reference\Data\Info;
use WSAPL\ClassesNode\Explore\Spec\Factory as ExploreSpecFactory;


/**
 * Facade.php
 *
 * description
 *
 * @class WSPLRTL\Form\Helper\Reference\Facade
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Facade
    implements
    MDPeerAccess,                /**< @see WSPLL\MD\Lib\Peer */
    ProcdPeerAccess             /**< @see self::getProcdConn() */
{

    use Debug;
    use LibMDPeer;

    private $_director;
    private $_context;

    protected function __construct()
    {
        //
    }

    /** 
     * Create helper to refernce task when pressed referenced button
     * 
     * @param WSPLRTL\Widget\AbstractForm $director 
     * @param array $context with fields:
     *   + event
     *   + launcher-hinting
     *   + launcher-opener-pbi
     * 
     * @return object WSPLRTL\Form\Helper\Reference\Facade
     */
    public static function create($director, $context = false)
    {
        $o = new static();
        $o->_director = $director;
        $o->_context = $context;
        $o->_init();
        return $o;
    }

    /** 
     * Create facade WSAPL\Reference\Facade and run it
     * @see WSAPL\Reference\Facade
     * 
     * @return null
     */
    public function run()
    {
        return $this->_makeFacade()->run();
    }

    public function getSender()
    {
        return $this->getContext()['event']->sender;
    }

    public function getLauncherHinting()
    {
        return $this->getContext()['launcher-hinting'];
    }

    public function getLauncherOpenerPBI()
    {
        return $this->getContext()['launcher-opener-pbi'];
    }

    /**
     *
     *
     *
     * @return WSAPL\ClassesNode\Explore\Spec\AbstractSpec
     */
    public function getExploreSpec()
    {
        return $this->_createSpec($this->_makeRefData());
    }

    public function getProcdConn()
    {
        return $this->_getForm()->getProcdConn();
    }

    /**
     * @internal this method renamed from old getExploreInfo
     * @see WSAPL\ClassesNode\Grid\Helper\Query::getContext()
     *
     * @return WSAPL\Reference\Data\Info
     */
    private function _makeRefData()
    {
        $info = ReferenceDataFactory::createFromArray(
            [
                'mode' => $this->_getRefMode(),
                'ref_value' => $this->_getRefValue()
            ]
        );
        list($info->target_class_id, $info->criteria_id) = $this->_resolveCriteria();
        return $info;
    }

    /**
     *
     *
     * @param WSAPL\Reference\Data\Info $info
     *
     * @return WSAPL\ClassesNode\Explore\Spec\Custom
     */
    private function _createSpec(Info $info)
    {
        //self::logTrace($info, (array) $info);
        return ExploreSpecFactory::createFromReferenceHelper(
            $info, $this->_getRefObject()
        );
    }

    private function _getRefValue()
    {
        $form = $this->_getForm();
        $sender = $this->getSender();
        $cvalue_key = 'C' . $sender->readProp('control_id');
        return $form->RS($cvalue_key)->getValue();
    }

    private function _getRefMode()
    {
        return $this->_getRefObject()->getMode();
    }

    /**
     *
     * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Facade\getCriteriaInfo()
     *
     * @return array [
     *   'target_class_id' => (string) $target_class_id,
     *   'criteria_id' => (string) $criteria_id
     * ]
     */
    private function _resolveCriteria()
    {
        $r = $this->_getRefObject();
        if ($r->isAnyCriteriaDefined()) {
            /* info: array(:id, :hints, :filter, :target_class_id) */
            $info = $r->getCriteriaInfo();
            return [$info->target_class_id, $info->id];
        } else {
            return $this->_getCriteriaAuto();
        }
    }

    private function _getCriteriaAuto()
    {
        list($mdn, $mp) = $this->_openMethodParam();
        if (isset($mp['CRIT_ID'])) {
            return [$mp['TARGET_CLASS_ID'], $mp['CRIT_ID']];
        } else {
            return $this->_selectCriteria($mdn, $mp);
        }
    }

    private function _openMethodParam()
    {
        $mdn = $this->getMetaDataNode();
        $sender = $this->getSender();
        $mp = $mdn->getById('method_param')->getById(
            $sender->readProp('meth_param_id')
        );
        return [$mdn, $mp];
    }


    private function _selectCriteria($mdn, RowAssoc $mp)
    {
        $target_classs_id = $mp['TARGET_CLASS_ID'];
        $v = $mdn->getById('views');
        list($ok, $found) = $v->getByClassesIdInfo($target_classs_id);
        if ($ok) {
            return [$target_classs_id, $found['ID']];
        } else {
            throw new \UnexpectedValueException('cant locate view by target class');
        }
    }

    private function _getRefObject()
    {
        return $this->_getForm()->RS($this->_getMethodParam()['SHORT_NAME'])->ref();
    }

    private function _getMethodParam()
    {
        return $this->getMetaDataNode()->getById('method_param')->getById(
            $this->_getMethodParamID()
        );
    }

    private function _getMethodParamID()
    {
        return $this->getSender()->readProp('meth_param_id');
    }

    private function _init()
    {
        $this->_initLibMDPeer();
    }

    private function _getForm()
    {
        return $this->getDirector();
    }

    private function _makeFacade()
    {
        return ReferenceFacade::create($this, $this->_makeFacadeContext());
    }

    private function _makeFacadeContext()
    {
        return false;
    }

    protected function getDirector()
    {
        return $this->_director;
    }

    protected function getContext()
    {
        return $this->_context;
    }
}
