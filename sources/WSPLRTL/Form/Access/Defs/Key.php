<?php
/**
 * Key.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Form\Access\Defs;

/**
 * Key.php
 *
 * description
 *
 * @interface WSPLRTL\Form\Access\Defs\Key
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
interface Key
{
    const MODAL_RESULT = '___modal_result';
}
