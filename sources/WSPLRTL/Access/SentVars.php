<?php
/**
 * @file
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Access;

/**
 * Interface WSPLRTL\Access\SentVars
 *
 * @interface WSPLRTL\Access\SentVars
 *
 * @todo
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
interface SentVars
{
}
