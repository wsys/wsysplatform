<?php
/**
 * ValueHost.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Access;

/**
 * ValueHost.php
 *
 * description
 *
 * @interface WSPLRTL\Access\ValueHost
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
interface ValueHost
{
    public function setValue($value);
    public function getValue();
}
