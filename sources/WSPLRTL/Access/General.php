<?php
/**
 * General.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Access;

/**
 * General.php
 *
 * description
 *
 * @interface WSPLRTL\Access\General
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
interface General
{
    public function setColor($value);
    public function getColor();
    public function setBgColor($value);
    public function getBgColor();
    public function setWidth($value);
    public function getWidth();
    public function setHeight($value);
    public function getHeight();
}
