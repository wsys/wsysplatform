<?php
/**
 * @file
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Access\Value\Sent
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Access\Value;

use Nodelimit\Access\Request\Value\Sent as RequestValueSentAccess;

/**
 * @interface WSPLRTL\Access\Value\Sent
 *
 * @brief @~English Interface to access the value of the variable sent
 * in the request @~
 *
 * @~Russian Интерфейс доступа к значению переменной, отправленной в
 * запросе @~
 *
 * @details @~English Is an interface to access the original value of
 * the transit variable passed in the request from the client (browser)
 * to the server. Implementing this interface is typically a wrapper
 * object of the container that holds the value and provides access to
 * value through a method getValue. @~
 *
 * @~Russian Представляет собой интерфейс доступа к оригинальному
 * значению транзитной переменной, переданной в запросе от
 * клиента (браузера) к серверу. Реализация данного интерфейса обычно
 * представляет собой объектную обертку над контейнером, хранящим
 * значение и предоставляющая доступ к значению через метод getValue.
 * @~
 *
 * @see Nodelimit\Access\Request\Value\Sent
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 */
interface Sent extends RequestValueSentAccess
{
}
