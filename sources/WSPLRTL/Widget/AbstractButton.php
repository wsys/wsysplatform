<?php
/**
 * AbstractButton.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use Nodelimit\Access\Widget\Feature\Caption;
use WSPLRTL\Widget\Addition\Feature\Caption as CaptionFeatureAddition;
use WSPLRTL\Access\ValueHost as ValueHostAccess;
use WSPLRTL\Widget\Addition\Feature\ValueHost as ValueHostFeature;

use WSPLRTL\Widget\AbstractGeneric;
use WSPLRTL\Widget\AbstractControl;

/**
 * AbstractButton.php
 *
 * description
 *
 * @class WSPLRTL\Widget\AbstractButton
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
abstract class AbstractButton extends AbstractControl implements
    Caption,
    ValueHostAccess
{
    use CaptionFeatureAddition;
    use ValueHostFeature;

    /**
     * Indicate that button is invoke reference
     *
     * @warning don't check QUAL_BASE_TYPE_ID
     * @todo use QUAL_BASE_TYPE_ID
     *
     * @return bool
     */
    final public function isReferencer()
    {
        return $this->isReferenced();
    }
}
