<?php
/**
 * AbstractAction.php
 *
 * @page WSPLRTL\Widget\Action\AbstractAction
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Action\AbstractAction
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget\Action package
 */
namespace WSPLRTL\Widget\Action;

use Nodelimit\App\Action\AbstractAction as AbstractActionBase;

/**
 * AbstractAction.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Action\AbstractAction
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\Action package
 */
abstract class AbstractAction extends AbstractActionBase
{
    /** 
     * Determines whether an element-formed in reactivation mode
     * 
     * @return bool
     */
    protected function isReactive()
    {
        return $this->getWidget()->isRecycle();
    }

    protected function isPostKey()
    {
        $k = $this->getKey();
        $post = $this->getPostVars();
        return array_key_exists($k, $post);
    }

    protected function getKey()
    {
        return $this->getWidget()->getValueKey();
    }

    protected function getWidget()
    {
        return $this->getSubject();
    }

    protected function getPostVars()
    {
        return $_POST;
    }

    /** 
     * Read tha name attribute
     *
     * @warning only some widgets can share this attr. (like input, radio, checkbxs)
     * @warning this method used only in WSPLRTL\Widget\Action\Radio\Update
     *
     * @todo if widget can operate with this attribute then must use own method
     * @todo check interface other error
     * @todo move to WSPLRTL\Widget\Action\Radio level
     * 
     * @return string value of attribute "name"
     */
    protected function getName()
    {
        return $this->getWidget()->getAttr('name');
    }

    protected function getForm()
    {
        return $this->getWidget()->getForm();
    }
}
