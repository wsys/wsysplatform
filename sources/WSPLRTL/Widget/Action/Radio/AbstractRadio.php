<?php
/**
 * AbstractRadio.php
 *
 * @page WSPLRTL\Widget\Action\Radio\AbstractRadio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Action\Radio\AbstractRadio
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget\Action\Radio package
 */
namespace WSPLRTL\Widget\Action\Radio;

use WSPLRTL\Widget\Action\AbstractAction;

/**
 * AbstractRadio.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Action\Radio\AbstractRadio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\Action\Radio package
 */
abstract class AbstractRadio extends AbstractAction
{
}
