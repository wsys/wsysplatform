<?php
/**
 * Check.php
 *
 * @page WSPLRTL\Widget\Action\Radio\Check
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Action\Radio\Check
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget\Action\Radio package
 */
namespace WSPLRTL\Widget\Action\Radio;

use WSPLRTL\Widget\AbstractRadio;
use WSPLRTL\Widget\Action\Radio\AbstractRadio as AbstractAction;

/**
 * Check.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Action\Radio\Check
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\Action\Radio package
 */
class Check extends AbstractAction
{
    public function run()
    {
        if ($this->isReactive()) {
            return $this->_runReactive();
        } else {
            return $this->_runCreate();
        }
    }

    private function _runReactive()
    {
        return $this->isPostKey();
    }

    private function _runCreate()
    {
        return $this->getWidget()->getProp('checked');
    }
}
