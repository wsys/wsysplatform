<?php
/**
 * Update.php
 *
 * @page WSPLRTL\Widget\Action\Radio\Update
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Action\Radio\Update
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget\Action\Radio package
 */
namespace WSPLRTL\Widget\Action\Radio;

use WSPLRTL\Widget\Action\Radio\AbstractRadio as AbstractAction;

use WSPLRTL\Widget\Radio;
use Nodelimit\Exception\Raise;

/**
 * Update.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Action\Radio\Update
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\Action\Radio package
 */
class Update extends AbstractAction
{
    public function run()
    {
        if ($this->isReactive()) {
            $this->_runReactive();
        } else {
            $this->_runCreate();
        }
    }

    /**
     * Run updating when button is first created
     *
     * @return void
     */
    private function _runCreate()
    {
        // Raise::todo(__METHOD__);
        $this->_resetRelated();
        $this->_update();
    }

    /**
     * Run updating when button reactivated by resume process
     *
     * @return void
     */
    private function _runReactive()
    {
        $this->_resetRelated();
        $this->_update();
    }

    private function _getParam()
    {
        return $this->getContext();
    }


    /**
     * Resets related elements in the group
     *
     * Performs a forced reset state "checked" (to off - unchecked)
     * elements affecting $_POST variable and HTML-element attribute
     *
     * @warning resetting performed only for a radio buttons - checkboxes not altered
     *
     * @return void
     */
    private function _resetRelated()
    {
        $related = $this->_getRelated();
        foreach ($related as $elem) {
            if ($this->_matchMutex($elem) && $this->_isNeedReset($elem)) {
                $this->_resetRelatedElement($elem);
            }
        }
    }

    private function _resetRelatedElement($elem)
    {
        self::_removePostKey($elem);
        $elem->clearChecked();
    }

    /**
     * Check whether required performing real reseting
     *
     * @param elem
     *
     * @todo not implemented yet
     *
     * @return boolean
     */
    private function _isNeedReset($elem)
    {
        return true;
    }

    private function _matchMutex($elem)
    {
        return $elem instanceof Radio && !$this->_isMe($elem);
    }

    private function _isMe($elem)
    {
        return $this->_getID() === self::_getElementID($elem);
    }

    private function _getID()
    {
        return self::_getElementID($this->getWidget());
    }

    private function _getRelated()
    {
        $name = $this->getName();
        return $this->getForm()->findByName($name);
    }

    private function _update()
    {
        $elem = $this->getWidget();
        $on = $this->_getParam();
        if ($on) {
            self::_insertPostKey($elem);
        } else {
            self::_removePostKey($elem);
        }

        $this->getWidget()->changeChecked($on);
    }

    private static function _getElementID($elem)
    {
        return $elem->getProp('id');
    }

    private static function _removePostKey($elem)
    {
        $key = $elem->getValueKey();
        if (self::_hasPostKey($key)) {
            unset($_POST[$key]);
        }
    }

    private static function _hasPostKey($key)
    {
        return array_key_exists($key, $_POST);
    }

    /**
     * Inserts value to $_POST by key from element
     *
     * @param string $elem
     *
     * @warning if key exists nothing changed - only new keys will added
     * @warning this method may change relevant values in $_POST
     * @warning this method change value if it not exists in $_POST
     * @warning changing the value does not occur synchronously with CS
     * @warning it is not recommended to use these values after calling this method
     * @warning not guaranteed equivalence modified values and the
     * corresponding values in the DOM
     *
     * @note changing value is formal and needed (only?) for holding key in $_POST
     *
     * @todo check getValue and conversion to string
     * @todo proposal: implement sync method with CS when altering "post-value"
     * @todo investigate the problem of incompatibility with the client part
     * @todo proposal: to prevent lost values from changing implement changing cache
     *
     * @return void
     */
    private static function _insertPostKey($elem)
    {
        $key = $elem->getValueKey();
        if (!self::_hasPostKey($key)) {
            $_POST[$key] = (string) $elem->getValue();
        }
    }
}
