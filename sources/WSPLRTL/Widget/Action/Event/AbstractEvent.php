<?php
/**
 * AbstractEvent.php
 *
 * @page WSPLRTL\Widget\Action\Event\AbstractEvent
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Action\Event\AbstractEvent
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget\Action\Event package
 */
namespace WSPLRTL\Widget\Action\Event;

use Nodelimit\App\Action\AbstractAction as AbstractActionBase;

/**
 * AbstractEvent.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Action\Event\AbstractEvent
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\Action\Event package
 */
abstract class AbstractEvent extends AbstractActionBase
{
    /** 
     * Create Action object by validation parameters
     *
     * This method similar as in at parent level but for validate parameters
     * 
     * @param director 
     * @param context 
     * 
     * @todo add interface or type hinting for parameter $director
     *
     * @internal check method name createCustom at parent level
     *
     * @deprecated use createWithArrayContext
     *
     * @return object instance of Nodelimit\App\Action\AbstractAction
     */
    public static function createCustom($director, array $context = [])
    {
        return self::createWithArrayContext($director, $context);
    }

    public static function createWithArrayContext($director, array $context = [])
    {
        return self::create($director, $context);
    }

    protected function getEvent()
    {
        return $this->getContext()['event'];
    }

    /** 
     * Get Sender
     *
     * @warning this method override parent method getSender that
     * returns value by getSubject
     * 
     * @return object an associated sender in event
     */
    protected function getSender()
    {
        return $this->getEvent()->getSender();
    }

    protected function getForm()
    {
        return $this->getSender()->getForm();
    }
}
