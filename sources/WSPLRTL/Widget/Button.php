<?php
/**
 * Button.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractButton;

/**
 * Button.php
 *
 * description
 *
 * @class WSPLRTL\Widget\Button
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Button extends AbstractButton
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Button';

    public static function create($props = [], $attrs = [])
    {
        $f = $props['form'];
        $o = parent::create($props, $attrs);
        // $f->addEventListener(self::EV_FORWARD, $o, 'debugEvent');
        return $o;
    }

    protected function initialize()
    {
        parent::initialize();
    }
}
