<?php
/**
 * Memo.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;


use WSPLRTL\Widget\AbstractGeneric;
use WSPLRTL\Widget\AbstractControl;

/**
 * Memo.php
 *
 * @class WSPLRTL\Widget\Memo
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Memo extends AbstractControl
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Memo';

    protected function initialize()
    {
        parent::initialize();
    }

    public function __toString()
    {
        return (string) $this->getValue();
    }

    /**
     * @implements MemoValueAccess
     *
     *
     * @return
     */
    public function getValue()
    {
        $v = $this->getProp('text', '');
        // self::logTraceDump(__METHOD__, $v);
        return $v;
    }

    /**
     * @implements MemoValueAccess
     *
     *
     * @return
     */
    public function setValue($value)
    {
        $elem = $this->getElement();
        $elem->setValue((string) $value);
    }

    /**
     *
     * @implements @todo IdentifyAccess
     * @see WSPLRTL\Widget\Addition\Feature\ValueHost
     * @note same as WSPLRTL\Widget\Addition\Feature\ValueHost::getValueKey
     *
     * @return
     */
    // public function getValueKey()
    // {
    //     $elem = $this->getElement();
    //     $c = $elem->getContext();
    //     list($elem_props, $elem_attrs) = $elem->reflect();
    //     $anchor = $c->toBasenameAnchor();
    //     $id = $c->toBasenameID();
    //     return $anchor . '.' . $id;
    // }
}
