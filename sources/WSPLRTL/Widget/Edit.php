<?php
/**
 * Edit.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractEdit;

/**
 * Edit.php
 *
 * description
 *
 * @class WSPLRTL\Widget\Edit
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Edit extends AbstractEdit
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Edit';

    protected function initialize()
    {
        parent::initialize();
    }
}
