<?php
/**
 * AbstractWidget.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use Nodelimit\WF\ElementAccess;
use com\danscode\systype\wf\Guide as GuideContext;
use WSPLRTL\Widget\Params;
use com\danscode\traits\Debug;
use Nodelimit\WF\Systype\Postback\Delegate;
use com\danscode\wf\postback\Info as PostbackInfo;
use WSPLRTL\Widget\Addition\General as AdditionGeneral;
use WSPLRTL\Access\General as GeneralAccess;
use Nodelimit\WF\Element\Access\Style as ElementStyleAccess;
use Nodelimit\Access\Feature\Capable\Driven as CapableDrivenAccess;
use Nodelimit\WF\Element\Lib\AppearanceTrait;
use Namedio\Core\Lang\Lang;
use Namedio\Core\Edge\Lang\Arrays\Arrays;
use Namedio\Ext\Sed\Sed;
use WSPLRTL\Event\DefsInterface;
use Namedio\Core\Edge\Lang\Strings\Strings;
use Namedio\Core\Edge\Lang\Utils;
use Namedio\Core\Util\Options\OptionsClientTrait;
use Evenement\EventEmitterTrait;
use Namedio\Ext\Reg\RegTrait;
use Nodelimit\App\Attend\AttendTrait;

/**
 * AbstractWidget.php
 *
 * @class WSPLRTL\Widget\AbstractWidget
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractWidget implements
    ElementAccess,
    GeneralAccess,
    ElementStyleAccess,
    CapableDrivenAccess,
    DefsInterface
    // \Serializable
{

    use EventEmitterTrait;

    const REG_SYSFLAGS = ':sysflags';

    /// @var WSPLRTL\Element\AbstractElement
    private $_element;

    /// @var WSPLRTL\Widget\Params
    private $_params;

    private $_form;

    protected $eventing;

    use Debug;
    use AdditionGeneral;
    use AppearanceTrait;
    use OptionsClientTrait;
    use RegTrait;
    use AttendTrait;

    private function __construct()
    {
        //
    }

    public static function create($props = [], $attrs = [])
    {
        $o = new static();
        $o->_params = Params::create($props, $attrs);
        $o->initialize();

        // self::console(
        //     __METHOD__,
        //     get_class($o),
        //     Arrays::getSafe($props, 'caption', 'N/A'),
        //     Arrays::getSafe($props, 'id', 'N/A'),
        //     Arrays::getSafe($props, 'control_id', 'N/A'),
        //     $o->getProp('caption', 'n/a')
        // );

        return $o;
    }

    public static function resume($context)
    {
        $o = new static();
        $o->_params = ParamsResuming::create($context);
        $o->initialize();
        return $o;
    }

    public static function resumeFromForm($form, $context)
    {
        $o = new static();
        $o->_params = ParamsResuming::createFromForm($form, $context);
        $o->initialize();
        return $o;
    }

    public static function createLauncher($hinting)
    {
        $class_name = get_class($this->_element);
        return $class_name::cretateLauncher($hinting);
    }

    ////
    // public function serialize()
    // {
    //     self::console(
    //         get_class($this),
    //         __METHOD__,
    //         self::getBacktraceString()
    //     );
    //     $s = get_class($this);
    //     return $s;
    // }

    // public function unserialize($s)
    // {
    //     /// noop
    //     self::console(
    //         get_class($this),
    //         __METHOD__,
    //         $s
    //     );
    // }

    public function getOriginReg()
    {
        return $this->getForm()->reg(':sys');
    }

    public function getRegStorage($spec)
    {
        return $this->_element->getRegStorage($spec);
    }

    /**
     * Get context info about object creating in postback context.
     * Use this method only after parent::__construct
     *
     * @return bool true if object called by context generally in postback context
     */
    public function isRecycle()
    {
        return $this->_element->isRecycle();
    }

    /**
     * Get context info about object creating not in postback context.
     * Use this method only after parent::__construct
     *
     * @return bool true if object called in first and not in postback context
     */
    public function isInitial()
    {
        return $this->_element->isInitial();
    }

    public function getDelegate()
    {
        return $this->_element->getDelegate();
    }

    public function getProp()
    {
        return call_user_func_array([$this->_element, 'getProp'], func_get_args());
    }

    public function readProp($name)
    {
        return $this->_element->readProp($name);
    }

    public function getAttr()
    {
        return call_user_func_array([$this->_element, 'getAttr'], func_get_args());
    }

    public function updateAttr($name, $value)
    {
        return $this->_element->updateAttr($name, $value);
    }

    public function getAttrs()
    {
        return $this->_element->getAttrs();
    }

    public function getProps()
    {
        return $this->_element->getProps();
    }

    public function setDefaultAttrs($a)
    {
        return $this->_element->setDefaultAttrs($a);
    }

    public function setDefaultProps($a)
    {
        return $this->_element->setDefaultProps($a);
    }

    /**
     * setDefaults
     *
     * @param props
     * @param attrs
     *
     * @return
     */
    public function setDefaults($props, $attrs)
    {
        return $this->_element->setDefaults($props, $attrs);
    }

    public function reflect()
    {
        return $this->_element->reflect();
    }

    public function setContext(GuideContext $c)
    {
        return $this->_element->setContext($c);
    }

    public function getContext()
    {
        return $this->_element->getContext();
    }

    public function premergeVars($props, $attrs)
    {
        return $this->_element->premergeVars($props, $attrs);
    }

    /**
     *
     * @todo implement sender: need convert this to postback\Info
     *
     * @param what
     * @param data
     * @param tag
     *
     * @return
     */
    public function makeEvent($what, $data = [], $tag = null)
    {
        return $this->_element->makeEvent($what, $data, $tag);
    }

    /**
     * Implements PBIProducerAccess interface
     *
     * @brief Create postback info object of this class
     * @see Nodelimit\WF\Systype\Postback\Info\ProducerAccess
     * @todo check reference to get_class($this)
     *
     * @return object com\danscode\wf\postback\Info
     */
    public function makePostbackInfo()
    {
        return $this->_element->makePostbackInfo();
    }

    public function makeSlotSpec()
    {
        return $this->_element->makeSlotSpec();
    }

    public function render()
    {
        // $this->emit('widget.render.after');
        return $this->_element;
    }

    public function getForm()
    {
        return $this->_form;
    }

    public function getOriginSpec()
    {
        return $this->getForm()->getSpec();
    }

    public function validate()
    {
        return $this->getForm()->invokeValidate($this->getControlID());
    }

    public function getControlID()
    {
        return $this->readProp('control_id');
    }

    public function getWID()
    {
        return $this->readProp('id');
    }

    /**
     * Extract creating parameters.
     *
     * Available only at initial creating time
     *
     * @implements AttachWidgetAccess
     *
     * @return WSPLRTL\Widget\Params instance
     */
    public function getCreatingParams()
    {
        return $this->_params;
    }

    /**
     * @implements AttachWidgetAccess
     *
     *
     * @return
     */
    public function makeElement()
    {
        $element_class = static::ELEMENT_CLASS;
        $p = $this->_params;
        return new $element_class($p->props, $p->attrs, $p->context);
    }

    /**
     * @implements @todo StorePropsAccess
     * @todo refactor with using an storage
     *
     * @return
     */
    public function getStoreProps()
    {
        return ['target', 'control_id', 'meth_param_id', 'id'];
    }

    /**
     * @implements @todo StoreAttrsAccess
     *
     * @return
     */
    public function getStoreAttrs()
    {
        return [];
    }

    /**
     * @implements ElementStyleAccess
     *
     *
     * @return
     */
    public function getStyle()
    {
        return $this->_element->getStyle();
    }

    /**
     * Get callback info about sender
     *
     * For API and interfaces.
     * This method needed for implementing referenced behaviour.
     * Target object will rectivate by this info original sender when callback.
     *
     * @param mixed $method method name of handling events:
     *        string, defaults: null or ommit
     *
     * @internal high experimental
     *
     * @todo proposal: implement same method as for elements
     * @todo proposal: remove default parameter?
     * @todo refactor
     *
     * @return object Nodelimit\WF\Systype\Postback\Event\Spec
     */
    public function makeSenderInfo(
        $method = Delegate::DEFAULT_EVENT_METHOD
    ) {
        $spec = $this->getForm()->createEventSpec(
            'accept',
            $this,
            ($method) ? $method : 'handleEventAccept_' . $this->readProp('id')
        );
        return $spec;
    }

    public function getSharedStorage()
    {
        return $this->_element->getSharedStorage();
    }

    /**
     *
     * @implements @todo ValueKeyPeerAccess
     *
     * @warning key can be defined but name is not, because no attribute
     * in element
     *
     * @note experimental
     *
     * @return
     */
    public function getValueKey()
    {
        $elem = $this->getElement();
        $c = $elem->getContext();
        list($elem_props, $elem_attrs) = $elem->reflect();
        $anchor = $c->toBasenameAnchor();
        $id = $c->toBasenameID();
        return $anchor . '%' . $id;
    }

    /**
     * @copydoc %com\danscode\wf\html\element\Base::getSpec()
     *
     */
    final public function getSpec()
    {
        return $this->_element->getSpec();
    }

    /**
     * @note user API
     *
     *
     * @return bool
     */
    final public function isReferenced()
    {
        return $this->getSpec()->isPropSet('meth_param_id');
    }

    public function isReferencer()
    {
        return false;
    }

    public function send($code)
    {
        $data1 = '[]';
        $info = [
            'control_id' => $this->getProp('control_id'),
            // 'form_id' => $this->getForm()->getProp('control_id')
        ];
        self::logTraceDump(
            __METHOD__,
            $this->getSpec()->getPropSafe('form', 'N/A')
        );
        self::console(__METHOD__, $info);
        // if ($info['control_id'] === '1000058831') {
        $o = [];

        /// vnt: details
        // $o[] = '1000058831';

        /// vnt: custom filter type
        // $o[] = '1000062829';

        /// vnt: catalog
        $o[] = '1000070217';

        $debug = false;
        // $debug = true;
        if ($debug && in_array($info['control_id'], $o)) {
            // $code .= "\nconsole.log('send modified');\n";
            // $this->q('id_markupchart')->emptyNode();
            $code = $this->injectLines($code);
//             $code = Sed::run(
//                 $code,
//                 <<<EOD
// s/.legendRightAxisHint(' \[Using Right Axis\]')//g
// EOD
//             );

            ///
            ///{{{
            ///


//             $code = Sed::run(
//                 $code,
//                 <<<EOD
// s/<Javascript>/<JavascripT>/
// EOD
//             );

            $stub = ' /* some code */ ';
            /// Wsys(${this}).q('id_markupchart').chart().refreshMarkers();
            $updateLines = <<<EOD
nv.utils.windowResize(() => { console.debug('update lines'); Wsys(${this}).q('id_markupchart').chart().refreshMarkers(); return chart.update(); });
EOD;

//             $updateLines = <<<EOD
// console.debug('update lines');
//             // Wsys({$this}).q('id_markupchart').chart().refreshMarkers();
// EOD;


            ///
            ///}}}
            /// <<<EOD ends
            ///

            // s/nv.utils.windowResize(chart.update);/nv.utils.windowResize(() => { ${updateLines} return chart.update();});/
            /// s/nv.utils.windowResize(chart.update);/nv.utils.windowResize(chart['update']);/
            /// s/nv.utils.windowResize(chart.update);/nv.utils.windowResize(() => \{ ${updateLines} return chart.update();\});/
            $code = Sed::run(
                $code,
                <<<EOD
s/nv.utils.windowResize(chart.update);/$updateLines/
EOD
            );

//             $code = Sed::run(
//                 $code,
//                 <<<EOD
// /useInteractiveGuideline: true/i \
// focusEnable: true,
// EOD
//             );



//             $code = Sed::run(
//                 $code,
//                 <<<EOD
// /nv.addGraph/i \
// Wsys({$this}).q('id_markupchart').emptyNode();
// EOD
//             );

//             $code = Sed::run(
//                 $code,
//                 <<<EOD
// /useInteractiveGuideline: true/i \
// tooltips: false,
// EOD
//             );

//             $code = Sed::run(
//                 $code,
//                 <<<EOD
// /.options({duration: 300,/i \
// .tooltip({enabled: false})
// EOD
//             );

//             $code = Sed::run(
//                 $code,
//                 <<<EOD
//                 s/useInteractiveGuideline: true/useInteractiveGuideline: false/
// EOD
//             );


//             $code = Sed::run(
//                 $code,
//                 <<<EOD
// /console.dir(chart);/i \
// Wsys({$this}).q('id_markupchart').reg('chart', chart);
// Wsys({$this}).q('id_markupchart').reg('data', {$data1});
// EOD
//             );


            /// <<<EOD ends
            self::console(__METHOD__, $code);
        }

        $this->getElement()->send($code);
    }

    public function setFrontOption($key, $value)
    {
        $this->getElement()->setFrontOption($key, $value);
    }

    public function wireElementStyle($name, $value)
    {
        $this->forwardElem(__METHOD__, func_get_args());
    }

    public function wireAttr($name, $value)
    {
        $this->forwardElem(__METHOD__, func_get_args());
    }

    public function wireAttrRemove($name)
    {
        $this->forwardElem(__METHOD__, func_get_args());
    }

    public function wireClassAdd($class)
    {
        $this->forwardElem(__METHOD__, func_get_args());
    }

    public function wireClassRemove($class)
    {
        $this->forwardElem(__METHOD__, func_get_args());
    }

    public function wired()
    {
        return $this->forwardElem(__METHOD__);
    }

    public function changeAttr($name, $value)
    {
        $this->forwardElem(__METHOD__, func_get_args());
    }

    public function isResuming()
    {
        return $this->_isResuming();
    }

    public function getOptions()
    {
        return $this->getElement()->getOptions();
    }

    public function js()
    {
        /// this is prototype only
        $argc = func_num_args();
        if ($argc >= 3) {
            $args = array_slice(func_get_args(), 0, 3);
            $context = ($argc >= 4) ? func_get_arg(3) : null;
            if ($args[2] === 'wsys.chart.nvd3') {
                $context = (is_null($context)) ? 'svg' : $context;
                $args[] = $context;
                $this->enterJS(array_combine(['method', 'args', 'domain', 'context'], $args));
            } else {
                throw \InvalidArgumentException();
            }
        } elseif ($argc >= 1) {
            $p = [
                'domain' => null,
                'context' => null
            ];
            $argv = func_get_args();
            $p['method'] = $argv[0];
            $p['args'] = ($argc == 1) ? [] : $argv[1];
            $this->enterJS($p);
        } else {
            throw new \BadMethodCallException();
        }
    }

    public function matchForm($t)
    {
        $id = $this->getForm()->getProp('control_id');
        return (string) $t === (string) $id;
    }

    public function isTriggered()
    {
        return null;
    }

    public function isEventing()
    {
        return $this->eventing;
    }

    public function setRefresh($method)
    {
        $this->getElement()->setRefresh($method);
    }

    public function getRefresh()
    {
        return $this->getElement()->getRefresh();
    }

    public function enableDeadlockProtectOnce()
    {
        $this->setSysFlagOnce('deadlock.protect', true);
    }

    public function enableDeadlockProtect()
    {
        $this->setSysFlag('deadlock.protect', true);
    }

    public function disableDeadlockProtect()
    {
        $this->setSysFlag('deadlock.protect', false);
    }

    public function disableDeadlockProtectOnce()
    {
        $this->setSysFlagOnce('deadlock.protect', false);
    }

    public function isDeadlockProtect()
    {
        return $this->isSysFlag('deadlock.protect');
    }

    protected function setSysFlag($key, $value)
    {
        $this->reg(self::REG_SYSFLAGS)->set($key, $value);
    }

    protected function setSysFlagOnce($key, $value)
    {
        $this->reg(self::REG_SYSFLAGS)->setOnce($key, $value);
    }

    protected function isSysFlag($key)
    {
        return $this->reg(self::REG_SYSFLAGS)->is($key);
    }

    private function enterJS($info)
    {
        $d = $info['domain'];
        if ($d === 'wsys.chart.nvd3') {
            $info['prefix'] = $this->prefixJS('chart()');
            $this->callJS($info);
        } elseif (is_null($d)) {
            $info['prefix'] = null;
            $this->callJS($info);
        } else {
            throw \InvalidArgumentException();
        }
    }

    private function prefixJS($prefix)
    {
        $form = $this->getForm();
        $id = $this->getProp('id');
        /// Wsys({$this}).q('id_markupchart').chart()
        $code = <<<EOT
Wsys({$form}).q('{$id}').{$prefix}
EOT;
        return $code;
    }

    private function normalizeArgs($args)
    {
        return $args;
        if (is_array($args)) {
            if (Arrays::isAssoc($args)) {
                return [$args];
            } else {
                return $args;
            }
        // } elseif (is_object($args)) {
        //     return
        } else {
            return [$args];
        }
    }

    private function callJS($info)
    {
        $prefix = null;
        if (isset($info['prefix'])) {
            $prefix = $info['prefix'];
        }
        $code = $this->renderCall($info['method'], $prefix, $info['args']);
        $this->sendJS($info, $code);
    }

    private function analyzeArgs($args)
    {
        if (is_array($args)) {
            if (Arrays::isAssoc($args)) {
                return 'assoc';
            } else {
                return 'list';
            }
        } else {
            return 'value';
        }
    }

    private function renderCall($method, $callPrefix, $params)
    {
        $prefix = '';
        if ($callPrefix) {
            $prefix = "{$callPrefix}.";
        }
        $what = $this->analyzeArgs($params);
        $apply = '';
        $thisArg = '';
        $args = $params;
        if ($what === 'list') {
            /// @warning apply can used only for calls without thisArg - todo
            /// call, like: $this->js('prefix', [123]); -> prefix.apply(null, [123])
            $apply = '.apply';
            // $fs = Utils::iif();
            $fs = ', ';
            $thisArg = "null{$fs}";
            // $args = Utils::iif($what === 'assoc', [$params], $params);
        } else {
            /// @todo call with arguments list without apply (apply optional as flag)
            /// call, like: $this->js('prefix', ['foo' => 123]); -> prefix(['foo' => 123])
            /// call, like: $this->js('prefix', 123); -> prefix(123)
        }
        $json = json_encode($args);
        $code = "{$prefix}{$method}{$apply}({$thisArg}{$json});";
        return $code;
    }

    private function sendJS(array $info, $code)
    {
        if ($info['domain'] === 'wsys.chart.nvd3') {
            /// @warning !!! checks 'chart.nvd3.ready'
            $form = $this->getForm();
            // $formSelector = Strings::unquote((string) $form);
            // self::console(
            //     __METHOD__,
            //     (string) $form,
            //     $formSelector,
            //     $formSelector === $this->getContext()->toSelector()
            // );
            $formSelector = (string) $form->getContext()->toSelector();
            $id = $this->getProp('id');
            $code = $this->wrapWait(
                $code,
                [
                    'd3',
                    'nv',
                    [
                        'waitClass' => 'nvd3-svg',
                        'selector' => "{$formSelector}.wfqual-form .wfqual-{$id} svg"
                    ]
                ]
            );
        }
        $this->getElement()->send($code);
    }

    private function wrapWait($code, $opts)
    {
            $g = json_encode($opts);
            $s = <<<EOT
\$App.Util.waitGlobals(
  $g,
  () => {
  $code
  },
  {delay: 0}
);
EOT;
            $out = $s;
            return $out;
    }


    private function injectLines($acode)
    {
        /// Wsys({$this}).q('id_markupchart').jq().on();
        /// Wsys({$this}).q('id_markupchart').jquery().on();
        /// Wsys({$this}).q('id_markupchart').jQuery().on();
        /// Wsys({$this}).q('id_markupchart').$().on();
        /// Wsys({$this}).q('id_markupchart').elem().on();
        /// Wsys({$this}).q('id_markupchart').$call().on();
        /// $App.chart.on('elementClick', f, chart);
        /// $App.chart(chart).on('elementClick', f);
        /// $App.Chart.stroke({ev: e}).;
        /// $App.nv.stroke({ev: e}).;

        $code = '';

//         $code = <<<EOD
//     chart.lines.dispatch.on(
//         'elementClick',
//         function(e) {
//             console.log('elementClick', e, {$this}, e.series);
//             alert('trigger click!');
//             Wsys({$this}).q('id_markupchart').forward('elementClick', e);
//         }
//     );
//     // console.dir(chart);
// EOD;

        ///
        ///  <<<EOD ends
        ///

        $data1 = '[]';

        $code .= /* @js */ <<<EOD
        ///{{{ <Javascript>

    // chart.lines.dispatch.on(
    //     'elementClick',
    //     function(e) {
    //         console.log('another event', e);
    //         // chart.dispatch.call('elementClick', e);
    //         // chart.dispatch.call('elementClick', e);
    //     }
    // );

    // chart.dispatch.on(
    //     'click',
    //     function(e) {
    //         console.log('event 1', e);
    //     }
    // );

    // chart.lines.dispatch.on('elementMouseover', function(ev) {
    //     console.debug('event elementMouseover', ev);
    //     console.dir('event elementMouseover this', this);
    //     chartd().highlight(ev, {'stroke-width': 4, fill: 'orange'});
    // });

    // chart.lines.dispatch.on('elementMouseout', function(ev) {
    //     console.debug('event elementMouseout', ev);
    //     chartd().unhighlight(ev, {fill: 'none'});
    // });


    // chart.interactiveLayer.dispatch.on(
    //     'elementClick',
    //     function(e) {
    //         console.log('event 2', e);
    //     }
    // );

    // Wsys({$this}).q('id_markupchart').reg('chart', chart);
    // Wsys({$this}).q('id_markupchart').reg('data', {$data1});
    // console.log('reg:', Wsys({$this}).q('id_markupchart').reg('data'), Wsys({$this}).q('id_markupchart').reg('chart'));

    /// 'highlight', ['seriesIndex' => 1], 'svg', 'wsys.chart.nvd3'
    // Wsys({$this}).q('id_markupchart').chart();
    // Wsys({$this}).q('id_markupchart').chart().highlight({seriesIndex: 1, seriesId: '1000042610', 'stroke-width': 4});
    // Wsys({$this}).q('id_markupchart').chart().unhighlight({seriesIndex: 1, seriesId: '1000042610', fill: 'none'});

    // Wsys({$this}).q('id_markupchart').chart().blink({seriesIndex: 1});
    // _.delay(() => Wsys({$this}).q('id_markupchart').chart().unblink({seriesIndex: 1}), 3000);

    Wsys({$this}).q('id_markupchart').chart().labelPoints(
        {
            seriesIndex: 1,
            filter: {rem: 5},
            label: {
                dx: '-6em',
                dy: '-4em'
            },
        }
    );

    /*
    _.delay(() =>
        Wsys({$this}).q('id_markupchart').chart().unlabelPoints(
            {
                seriesIndex: 1
            }
        ), 7000);
    */

    // _.delay(() =>
    //     Wsys({$this}).q('id_markupchart').chart().unlabelPoints(), 7000);

    // var nIntervId;

    // function* generateCount() {
    //     var index = 0;
    //     while(true)
    //         yield index++;
    // }

    // let genCount = generateCount();


    // function changeColour() {
    //   nIntervId = setInterval(flashingText, 1000);
    // }

    // function flashingText() {
    //   // var oElem = document.getElementById('my_box');
    //   var oElem = Wsys({$this}).q('id_l_chart').elem();
    //   oElem.style.color = oElem.style.color == 'red' ? 'blue' : 'red';
    //   // oElem.style.color == 'red' ? 'blue' : 'red' is a ternary operator.

    //   /// interaction with chart by seriesIndex or seriesId
    //   Wsys({$this}).q('id_markupchart').chart().highlight({seriesIndex: 1, 'stroke': oElem.style.color == 'red' ? 'blue' : 'red'});
    //   const stroke = Wsys({$this}).q('id_markupchart').chart().select({seriesIndex: 1}).style('stroke');
    //   console.debug('stroke', stroke);

    //   /// stop flashing
    //   let cnt = genCount.next().value;
    //   console.debug('next', cnt);
    //   if (cnt > 7) {
    //       stopTextColour();
    //   }
    // }

    // function stopTextColour() {
    //   clearInterval(nIntervId);
    // }

    // changeColour();

    // console.dir(chart);

    ///}}}

EOD;


        ///
        /// <<<EOD ends
        ///

        return str_replace(
            'return chart;',
            "\n" . $code . "\n" . 'return chart;',
            $acode
        );
    }

    private function forwardElem($meth, array $args = [])
    {
        return Lang::forward($this->getElement(), $meth, $args);
    }

    private function _getUsedContext()
    {
        if ($this->_isResuming()) {
            return $this->_getElementPersistentSpec();
        } else {
            return $this->_params->props['use_context'];
        }
    }

    private function _getElementPersistentSpec()
    {
        $pbi = $this->makePostbackInfo();
        return $pbi->context;
    }

    /**
     * @warning same as in AbstractForm
     *
     *
     * @return
     */
    private function _isResuming()
    {
        return ($this->getCreatingParams() instanceof ParamsResuming);
    }

    /**
     * @brief Create postback info for widget
     *
     * @internal same as makePostbackInfo but override delegate field
     *
     * @see WSPLRTL\Widget\AbstractWidget::makePostbackInfo
     * @see WSPLRTL\Form\Helper\Reference\Facade
     *
     * @param mixed $method method name of handling events:
     *        string, defaults: null or ommit
     *
     * @return object com\danscode\wf\postback\Info
     */
    protected function makeWidgetPostbackInfo(
        $method = Delegate::DEFAULT_EVENT_METHOD
    ) {
        //$pbi = $this->makePostbackInfo();
        $pbi = new PostbackInfo(
            get_class($this), $this->_getUsedContext()
        );
        $pbi->method = ($method) ? $method : Delegate::DEFAULT_EVENT_METHOD;
        return $pbi;
    }

    /**
     * @todo proposal: use setForm method
     *
     *
     * @return
     */
    protected function createElement()
    {
        //$class_name = static::ELEMENT_CLASS;
        $f = $this->_params->props['form'];
        $this->_form = $f;
        return $f->attachWidget($this);
    }

    protected function initialize()
    {
        $p = $this->_params;
        $p->props['__component'] = $this;
        if ($this->isInitialize()) {
            $this->initParameters();
            $p->flush();
            // self::console(
            //     __METHOD__,
            //     array_keys($p->props),
            //     $p->attrs
            // );
        }
        $this->_element = $this->createElement();
    }

    protected function isInitialize()
    {
        $p = $this->_params;
        return !($p instanceof ParamsResuming) && ($p instanceof Params);
    }

    protected function initParameters()
    {
        //
    }

    /**
     * Gets element
     *
     * @return WSPLRTL\Element\AbstractElement
     */
    protected function getElement()
    {
        return $this->_element;
    }

    protected function callElement($method, $args = [])
    {
        return call_user_func_array([$this->getElement(), $method], $args);
    }
}
