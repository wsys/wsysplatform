<?php
/**
 * @file
 *
 * @brief Password.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Password
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\Edit;

/**
 * The Password class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Password
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Password extends Edit
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Password';
}
