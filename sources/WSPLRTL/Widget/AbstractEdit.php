<?php
/**
 * AbstractEdit.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Access\ValueHost as ValueHostAccess;
use WSPLRTL\Widget\Addition\Feature\ValueHost as ValueHostFeature;
use WSPLRTL\Widget\AbstractGeneric;
use WSPLRTL\Widget\AbstractControl;
use com\danscode\lib\Lists;

/**
 * AbstractEdit.php
 *
 * @class WSPLRTL\Widget\AbstractEdit
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractEdit extends AbstractControl implements ValueHostAccess
{
    use ValueHostFeature;

    protected function initialize()
    {
        parent::initialize();
        $this->_initDefvalue();
    }

    public function getStoreProps()
    {
        return Lists::add(parent::getStoreProps(), []);
    }

    private function _initDefvalue()
    {
        //
    }
}
