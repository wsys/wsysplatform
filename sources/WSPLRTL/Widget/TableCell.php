<?php
/**
 * TableCell.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractWidget;

/**
 * TableCell.php
 *
 * description
 *
 * @class WSPLRTL\Widget\TableCell
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class TableCell extends AbstractWidget
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\TableCell';

    protected function initialize()
    {
        parent::initialize();
    }
}
