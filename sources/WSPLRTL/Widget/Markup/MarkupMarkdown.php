<?php
/**
 * @file
 *
 * @brief MarkupMarkdown.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Markup\MarkupMarkdown
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Markup;

use WSPLRTL\Widget\Markup\AbstractMarkup;

/**
 * The MarkupMarkdown class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Markup\MarkupMarkdown
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
class MarkupMarkdown extends AbstractMarkup
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Markup\MarkupMarkdown';
}
