<?php
/**
 * @file
 *
 * @brief MarkupHTML.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Markup\MarkupHTML
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Markup;

use WSPLRTL\Widget\Markup\AbstractMarkup;

/**
 * The MarkupHTML class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Markup\MarkupHTML
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
class MarkupHTML extends AbstractMarkup
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Markup\MarkupHTML';

    public static function create($props = [], $attrs = [])
    {
        $f = $props['form'];
        $o = parent::create($props, $attrs);
        // $f->addEventListener(self::EV_FORWARD, $o, 'debugEvent');
        return $o;
    }

    public function emptyNode($selector = '> *')
    {
        $this->getElement()->emptyNode($selector);
    }
}
