<?php
/**
 * @file
 *
 * @brief AbstractMarkup.php
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Markup\AbstractMarkup
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Markup;

use WSPLRTL\Widget\AbstractControl;

/**
 * The AbstractMarkup class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Markup\AbstractMarkup
 *
 * @copyright Copyright (C) 2018  The Wsysplatform Development Team
 */
abstract class AbstractMarkup extends AbstractControl
{
    public function setMarkup($markup)
    {
        // self::console(__METHOD__, $markup);
        $this->getElement()->setMarkup($markup);
    }

    public function refreshMarkup($markup)
    {
        $this->setMarkup($markup);
        $this->refresh();
    }

    public function refresh()
    {
        $this->getElement()->refresh();
    }

    public function renderMarkup()
    {
        return $this->getElement()->renderMarkup();
    }

    public function getRenderMarkup()
    {
        return $this->renderMarkup();
    }
}
