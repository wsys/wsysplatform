<?php
/**
 * DateEdit.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractEdit;

/**
 * DateEdit.php
 *
 * description
 *
 * @class WSPLRTL\Widget\DateEdit
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class DateEdit extends AbstractEdit
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\DateEdit';

    protected function initialize()
    {
        parent::initialize();
    }
}
