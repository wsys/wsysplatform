<?php
/**
 * AbstractRadio.php
 *
 * @page WSPLRTL\Widget\AbstractRadio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\AbstractRadio
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget package
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\Action\Radio\Check as ActionCheck;
use WSPLRTL\Widget\Action\Radio\Update as ActionUpdate;

use com\danscode\lib\Lists;

/**
 * AbstractRadio.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\AbstractRadio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\AbstractRadio package
 */
abstract class AbstractRadio extends AbstractButton
{
    /**
     * Determinate checked state of the radio button
     *
     * Checking state of radio button whether checked
     *
     * @ingroup WSPLRTL-API-User
     * @ingroup WSPLRTL-API-Widget
     * 
     * @return boolean checked or not state
     */
    public function isChecked()
    {
        return ActionCheck::bootstrap($this);
    }

    /** 
     * @defgroup WSPLRTL-widget-feature-AbstractRadio-setChecked
     *
     * A group of related actions with the call "setChecked". The entire
     * implementation, except for some calls, in this group, is in
     * ActionUpdate
     *
     * @see WSPLRTL\Widget\Action\Radio\Update
     */

    /**
     * Set checked state
     *
     * @param bool $checked
     *
     * @ingroup WSPLRTL-API-User
     * @ingroup WSPLRTL-API-Widget
     * @ingroup WSPLRTL-widget-feature-AbstractRadio-setChecked
     *
     * @return void
     */
    public function setChecked($checked = true)
    {
        ActionUpdate::bootstrap($this, $checked);
    }

    /**
     * @implements @todo StoreAttrsAccess
     * @override parent::getStoreAttrs()
     *
     * @return
     */
    public function getStoreAttrs()
    {
        return Lists::add(parent::getStoreAttrs(), ['name']);
    }

    /**
     * Change checked state
     *
     * Switching checked state
     *
     * @param bool $checked new checked state
     *
     * @ingroup WSPLRTL-API-Internal
     * @ingroup WSPLRTL-API-SDK
     * @ingroup WSPLRTL-API-Widget
     * @ingroup WSPLRTL-widget-feature-AbstractRadio-setChecked
     * 
     * @return void
     */
    public function changeChecked($checked = true)
    {
        // self::logTrace(__METHOD__, $checked);
        $this->getElement()->changeChecked($checked);
    }

    /** 
     * Clears the status "checked"
     *
     * Performing resetting of radio button state from "checked"
     * onto "not checked" by sending action on js-code to client-side
     *
     * @ingroup WSPLRTL-API-Internal
     * @ingroup WSPLRTL-API-SDK
     * @ingroup WSPLRTL-API-Widget
     * @ingroup WSPLRTL-widget-feature-AbstractRadio-setChecked
     * 
     * @return void
     */
    public function clearChecked()
    {
        $this->changeChecked(false);
    }
}
