<?php
/**
 * AbstractForm.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractWidget;
use com\danscode\wf as WF;
use WSPLRTL\Widget\ParamsResuming;
use Nodelimit\WF\Systype\Postback\Delegate;
use Nodelimit\WF\Systype\Postback\Event\Context as EventContext;
use com\danscode\wf\postback\Info as PBI;
use com\danscode\wf\postback\Event as PostbackEvent;
use Nodelimit\WF\Systype\RenewInfo;
use com\danscode\systype\LazyInit;
use WSPLRTL\Systype\SentVars;
use WSPLRTL\Widget\Label as WidgetLabel;
use WSPLRTL\Widget\Edit as WidgetEdit;
use WSPLRTL\Widget\DateEdit as WidgetDateEdit;
use WSPLRTL\Widget\TableCell as TableCell;
use WSPLRTL\Widget\Memo as WidgetMemo;
use WSPLRTL\Widget\TableRow as TableRow;
use WSPLRTL\Widget\Table as Table;
use WSPLRTL\Widget\Button as WidgetButton;
use WSPLRTL\Widget\ButtonOk as WidgetButtonOk;
use WSPLRTL\Widget\ButtonCancel as WidgetButtonCancel;
use WSPLRTL\Widget\Panel as WidgetPanel;
use WSPLRTL\Widget\Radio as WidgetRadio;
use WSPLRTL\Widget\Checkbox as WidgetCheckbox;
use WSPLRTL\Widget\AbstractRadio as WidgetAbstractRadio;
use com\danscode\lib\Lists;
use Nodelimit\Exception\UnexpectedValue;
use WSPLRTL\Form\Model\Facade as Model;
use WSPLRTL\Access\ValueHost as ValueHostAccess;
use Nodelimit\Access\Widget\Feature\Caption as CaptionFeatureAccess;
use WSPLRTL\Event\AbstractEvent;
use WSPLRTL\Event\Accept as EventAccept;
use WSPLRTL\Event\Click as EventClick;
use com\danscode\struct\state\Mediator as StateMediator;
use com\danscode\systype\exception\Generic as SysException;
use WSAPL\ClassesNode\Form\Launch\Controller\Facade as LaunchController;
use Nodelimit\Access\Widget\Feature\Visualize\Visibling as VisiblingAccess;
use Nodelimit\Access\Widget\Feature\Visualize\Exposing as ExposingAccess;
use WSPLRTL\Form\Helper\Reference\Facade as HelperReference;
use Nodelimit\WF\Systype\Postback\Event\Spec as EventSpec;
use Nodelimit\Access\UI\Defs as UIDefsAccess;
use WSPLRTL\Form\Access\Defs\Key as DefsKeyAccess;
use com\danscode\lib\Props;
use WSPLRTL\Form\Action\Preprocess\Radio\Click as ActionPreprocessRadioClick;
use WSPLRTL\Element\Combobox\Access\Defs as ComboboxDefsAccess;
use WSPLRTL\Form\Action\Combobox\Dispatch as ActionComboboxDispatch;
// use WSPLRTL\Form\Action\Dispatch\Upload as DispatchUpload;
use WSPLRTL\Event\Socket\Combobox\RPC as EventSocketComboboxRPC;
use com\danscode\systype\Arguments;
use WSPLRTL\Widget\Form\Action\Sys\Raise\Raise as ActionRaise;
use WSPLRTL\Widget\Form\Action\Sys\Raise\Halt as ActionHalt;
use Nodelimit\App\Action\Sandbox;
use WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\Exception as RaiseException;
use WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\HaltException;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use WSPLRTL\Widget\Form\Backend;
use WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade as FilterFacade;
use WSPLRTL\Widget\Form\API\Filter\Custom\Custom as FilterCustom;
use WSPLRTL\Widget\Form\Action\Msg\Msg as ActionMsg;
use Nodelimit\WF\ElementAccess;
use WSPLRTL\Event\Custom\Mediator as CustomEventMediator;
use WSPLRTL\Element\Upload\DefsInterface as UploadDefsInterface;
use WSPLRTL\Event\Socket\Upload\Trap as EventSocketUploadTrap;
use WSPLRTL\Event\DefsInterface as EventDefsInterface;
use WSPLRTL\Internals\UtilsTrait;
use WSPLRTL\Event\Grid\Row\Click as RowClick;
use WSPLRTL\Event\Grid\Row\Select as RowSelect;
use WSPLRTL\Event\Grid\Row\Loaded as RowsLoaded;
use com\wsplatform\syslib\exception\procinit\client\ServerError as ProcinitException;
use com\danscode\systype\exception\db\oci8\ErrorInfo as OCI8ErrorInfo;
use Namedio\Core\Edge\Lang\Value\Value as LangValue;
use WSPLRTL\Widget\Form\DevUtilsTrait;
use WSPLRTL\Widget\Form\API\Filter\Custom\CustomFilterExtractorTrait;
use WSPLRTL\Event\Custom\Timer as CustomEventTimer;
use WSPLShared\Exception\FormSessionExpiredException;
use Namedio\Core\Exception\DB\NoDataFoundException;
use Namedio\Core\Edge\Lang\Strings\Strings;
use WSPLRTL\Event\Forward as ForwardEvent;
use WSPLRTL\Widget\AbstractFormTrait;
use Nodelimit\App\App;
use Namedio\Core\Edge\Util\Registry;
use Nodelimit\App\Prove\Prove;
use Namedio\Ext\SimpleResult\ExceptionResult;
use Namedio\Ext\System\System;

/**
 * AbstractForm.php
 *
 * @class WSPLRTL\Widget\AbstractForm
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractForm extends AbstractWidget implements
    VisiblingAccess,
    ExposingAccess,
    UIDefsAccess,
    EventDefsInterface
{
    use UtilsTrait;
    use LibClientServiceLocal;
    use DevUtilsTrait;
    use CustomFilterExtractorTrait;
    use AbstractFormTrait;

    const ELEMENT_CLASS = 'WSPLRTL\Element\Form';

    protected $wsys_form_module;

    private $_lazy_model;
    private $_state_mediator;
    private $_launch_ctl;

    private $firstActivate;
    private $activate;

    /// @internal in most cases these props passed from calling params (props)
    private static $_store_props = [
        '__procd_conn',
        '__run_info',
        'shared_context',
        '___refpool',
        DefsKeyAccess::MODAL_RESULT,
        'custom.filter',
        /// custom events handlers registry: ArrayObject
        'backend.pspec',
        'activates'
    ];

    protected function initialize()
    {
        parent::initialize();
        $this->_init();
        $this->hookFormInit();
    }

    protected function initParameters()
    {
        parent::initParameters();
    }

    /**
     * @brief Create reference to backend form representation
     * @overload WSPLRTL\Widget\AbstractWidget::createElement()
     *
     * @note property __component required for access interface method renderForm
     * @see WSPLRTL\Element\Form
     *
     * @return object instance of WSPLRTL\Element\Form class
     */
    protected function createElement()
    {
        $class_name = static::ELEMENT_CLASS;
        $p = $this->getCreatingParams();
        if ($p instanceof ParamsResuming) {
            //resume
        } else {
            $p->props['use_context'] = WF::storageSpec();
        }
        return new $class_name($p->props, $p->attrs, $p->context);
    }

    protected function wireAlert($msg)
    {
        $s = <<<EOT
alert(
`
{$msg}
`
);
EOT;
        // $code = "alert(`{$msg}`);";
        $code = $s;
        WF::wire($code);
    }

    protected function alertDump($t)
    {
        $string = print_r($t, true);
        $patterns = array('/\\\\/', '/\n/', '/\r/', '/\t/', '/\v/', '/\f/');
        $replacements = array('\\\\\\', '\n', '\r', '\t', '\v', '\f');
        $string = preg_replace($patterns, $replacements, $string);
        $code = sprintf('alert("%s");', $string);
        WF::wire($code);
    }

    /**
     * Create event context
     *
     * Wrap event related data to container
     *
     * @param widget
     * @param mixed $type click change ...
     * @param meth_name
     *
     * @return Nodelimit\WF\Systype\Postback\Event\Context
     */
    protected function makeEventContext(
        $widget = null, $type = null, $meth_name = null
    ) {
        $pbi_sender = null;
        if ($widget) {
            $pbi_sender = $widget->makeWidgetPostbackInfo($meth_name);
        }
        return EventContext::create(
            $this->makeWidgetPostbackInfo(), $pbi_sender, $type
        );
    }

    protected function startup()
    {
        /// noop
    }

    /**
     * @warning this method currently unused
     *
     * @param cmd
     *
     * @return
     */
    protected function closeForm($cmd = self::MR_CANCEL)
    {
        $state = false;
        if ($cmd === self::MR_CANCEL) {
            $state = self::BTCLOSE_CANCEL;
        } else {
            throw UnexpectedValue::create($cmd, 'unknown command');
        }
        $this->_putStateMediator($state);
    }

    protected function raiseAlert($msg = '')
    {
        self::console($msg);
        return ActionRaise::bootstrap(
            [
                'args' => Arguments::create(func_get_args(), 0, 1)
            ]
        );
    }

    protected function raise($msg = '')
    {
        return ActionRaise::bootstrap(
            [
                'args' => Arguments::create(func_get_args(), 0, 1)
            ]
        );
    }

    protected function halt()
    {
        return ActionHalt::bootstrap();
    }

    protected function msg($msg = '')
    {
        return ActionMsg::bootstrap(
            [
                'msg' => $msg,
                'form' => $this
            ]
        );
    }

    /**
     * Returns helper info about RS objects
     *
     * @return array
     */
    abstract public function getRSMap();

    public function __invoke()
    {
        $nargs = func_num_args();
        if ($nargs) {
            return $this->q(func_get_arg(0));
        } elseif ($nargs === 0) {
            return $this;
        } else {
            // self::console(func_num_args());
            // self::logTraceDump(func_get_args());
            throw new \BadMethodCallException();
        }
    }

    public function __toString()
    {
        // return (string) wf::target($this->getProp('target'))->toGuide()->toSelector();
        // return __METHOD__;
        // return (string) wf::target($this->getProp('target'))->toGuide()->toSelector();
        // $s = __METHOD__;
        // $s = $this->getContext()->toSelector();
        // $s = (string) wf::target((string) wf::target($this->getProp('target'))->toGuide()->toSelector())->toGuide()->toSelector();
        // $s = (string) $this->getTarget()->toSelector();
        $s = $this->getSelector();
        // self::console(__METHOD__, (string) $s);
        // $s = __METHOD__;
        return Strings::quote((string) $s);
    }

    public function getTargetEx($quoted = true)
    {
        $s = $this->getSelector();
        if ($quoted) {
            return Strings::quote((string) $s);
        } else {
            return (string) $s;
        }
    }

    public function getOriginSpec()
    {
        return $this->getSpec();
    }

    public function getOriginReg()
    {
        return $this->reg(':sys');
    }

    public function getSelector()
    {
        return (string) $this->getTarget()->toSelector();
    }

    public function wireModule($code)
    {
        $inject = '';

//         $inject = <<<EOT
// import {Printout} from '/sys/w/unpkg/vnt.choise.smk.form_30/printstub.js';

// let m = new Printout({form: $this});
// m.run();

// console.debug('wireModule: loaded');

// EOT;

//         $inject = <<<EOT
// import {Stub} from '/sys/w/unpkg/vnt.choise.smk.form_30/printstub.js';

// let m = new Stub({form: $this});
// // let m = new Stub();
// console.debug('wireModule printstub override', m.run());
// EOT;

        $inject = '';

//         $inject = <<<EOT
// import {Client} from '/sys/dev/stub/dial.js';
// let m = new Client({form: $this});
// console.debug('wireModule: Client loaded');
// EOT;

        self::console(
            __METHOD__,
            $code,
            $inject,
            $this->getFormId(),
            $this->getProp('control_id')
        );
        // $code = $inject;

        $p = [
            'code' => $this->patch($code)
        ];
        $this->sys()->injectES6($p);
    }

    public function getTarget()
    {
        return $this->readProp('target');
    }

    public function getCreatingParams()
    {
        $params = parent::getCreatingParams();
        // self::console(
        //     __METHOD__,
        //     array_keys($params->props),
        //     $params->attrs
        // );
        return $params;
    }

    public function getFormId()
    {
        return $this->_getFormId();
    }

    /**
     * Associate event handler with widget and event type
     *
     * @param string $type type of event
     * @param object $widget AbstractWidget
     * @param mixed $data any portable data associated with event
     *
     * @return mixed void or bool
     */
    public function addEventListener(
        $type,
        AbstractWidget $widget,
        $meth_name = null,
        $data = null
    ) {
        if (method_exists($widget, 'prepareEventListener')) {
            $r = $widget->prepareEventListener($type, $widget, $meth_name, $data);
            if (is_null($r)) {
                return null;
            } elseif ($r) {
                $this->_registerCustomEventListener(
                    array_combine(
                        ['type', 'widget', 'meth_name', 'data'],
                        [$type, $widget, $meth_name, $data]
                    )
                );
                return true;
            }
        }
        $this->attachEventListener(
            $type,
            $widget,
            $meth_name,
            $data
        );
    }

    public function attachEventListener(
        $type,
        AbstractWidget $widget,
        $meth_name = null,
        $data = null
    ) {
        $spec = $this->createEventSpec(
            self::_undots($type),
            $widget,
            $meth_name,
            $data
        );
        $selector = $widget->getContext()->toSelector();
        // self::logger(
        //     __FILE__, __LINE__, get_class($widget), $type, (string) $selector
        // );
        WF::wire($selector, $spec->toArray());
    }

    public function getSystemGID($id)
    {
        return $this->_getBackend()->getSystemGID($id);
    }

    public function makeSenderInfo(
        $method = Delegate::DEFAULT_EVENT_METHOD
    ) {
        throw new \BadMethodCallException();
    }

    public function getSysProp($name)
    {
        switch ($name) {
            case 'md.cache.dsn':
                return $this->_getProcdConn()->getCacheDSN();
            case 'procd_conn':
                return $this->_getProcdConn();
            case 'run_info':
                return $this->readProp('__run_info');
            case 'run_info.new_id':
                return $this->readProp('__run_info')->new_id;
            case 'run_info.meth_rec.id':
                // self::console($this->readProp('__run_info'));
                return $this->readProp('__run_info')->meth_rec['ID'];
            case 'run_info.criteria_id':
                return $this->readProp('__run_info')->criteria_id;
            case 'run_info.mnode_shspec':
                return $this->readProp('__run_info')->mnode_shspec;
            default:
                throw UnexpectedValue::create($name);
        }
    }

    public function getMasterNodeSharedSpec()
    {
        return $this->getSysProp('run_info.mnode_shspec');
    }

    public function getProcdConn()
    {
        return $this->_getProcdConn();
    }

    /**
     * @implements @todo ControlsAccess
     *
     * @param id
     *
     * @return
     */
    public function q($id)
    {
        return $this->_getWidgetById($id);
    }

    public function qsafe($id)
    {
        return $this->getWidgetByIdSafe($id);
    }

    public function findByName($name)
    {
        $pool = $this->_getWidgetPool();
        $find = $pool->findByName($name);
        $out =[];
        foreach ($find as $k => $elem) {
            $out[] = $pool->getProp($k);
        }
        return $out;
    }

    /**
     * @implements AttachWidgetAccess
     * @todo delegate create element to widget back (only register and modify params)
     *
     * @param object $widget
     *
     * @return
     */
    public function attachWidget($widget)
    {
        $p = $widget->getCreatingParams();
        if ($p instanceof ParamsResuming) {
            //$initial = false;
        } else {
            $props = & $p->props;
            $id = (isset($props['id'])) ? $props['id'] : null;
            $props['target'] = WF::idtarget($id);
            $props['use_context'] = $this->_createWidgetStorageSpec($widget);
        }
        $element = $widget->makeElement();
        $this->_syncWidgetPool($widget, $element);
        return $element;
    }

    /**
     * @override parent::getStoreProps()
     * @todo use WSPLRTL\Form\Model\Node\RefPool\Facade::KEY_POOL instead ___refpool
     *
     * @return
     */
    public function getStoreProps()
    {
        return Lists::add(parent::getStoreProps(), self::$_store_props);
    }

    /**
     * @brief The Event handler of a form
     *
     * @~russian Обработчик событий на форме @~
     *
     * @details Performs instance recovery form translates incoming events,
     * directing them to a particular recipient if necessary
     *
     * @~russian Выполняет восстановление экземпляра формы, транслирует
     * входящие события, направляя их при необходимости конкретному
     * получателю @~
     *
     * @param PostbackEvent $ev an event with structure like below:
     *
     *        com\danscode\wf\postback\Event Object
     *          (
     *              [what] => click
     *              [data] =>
     *              [context] => Nodelimit\WF\Systype\Postback\Event\Context Object
     *                  (
     *                      [mediator] => com\danscode\wf\postback\Info Object
     *                          (
     *                              [delegate] => a\settings\edit_yy_auto\form_10\Main
     *                              [method] => handleEvent
     *                              [context] => 7b4630e146497a20c2589ccb626001e9:c219f3e457b19eca1f88554dd6425e91
     *                          )
     *
     *             [sender] => com\danscode\wf\postback\Info Object
     *                 (
     *                     [delegate] => WSPLRTL\Widget\Label
     *                     [method] => handleEventClick_id_p_key_lab
     *                     [context] => 7b4630e146497a20c2589ccb626001e9:909fc99f65b0b9f19fd53e7f376b3b44
     *                 )
     *
     *             [type] => click
     *         )
     *
     *     [tag] =>
     *     [sender] =>
     * )
     *
     * @note all incoming events are translated to internal form events in dispatcher
     *
     * @return void
     */
    public static function handleEvent(PostbackEvent $ev)
    {
        // self::setBreakpoint();
        // self::console(__METHOD__);
        // self::logger(__FILE__, __METHOD__, __LINE__, microtime(true), getmypid(), $ev->what);
        $form = self::_resumeForm($ev);
        $form->dispatchSandboxed($ev);
    }

    /**
     * Gets model
     *
     * @return WSPLRTL\Form\Model\Facade
     */
    public function getModel()
    {
        return $this->_lazy_model->read();
    }

    /**
     * Creating model
     *
     * @return WSPLRTL\Form\Model\Facade
     */
    public function createModel()
    {
        return Model::create($this);
    }

    public function RS($key = false)
    {
        if (func_num_args() > 0) {
            return $this->getModel()->RS($key);
        } else {
            return $this->getModel()->getRSFacade();
        }
    }

    public function gc()
    {
        static $n = 0;
        if (++$n % 25 == 0) {
            $r = \gc_collect_cycles();
            usleep(250000);
            return $r;
        }
    }

    public function refresh()
    {
        $this->dispatchFromRS([$this, 'updateFromRS']);
    }

    public function refreshRelated()
    {
        // noop
    }

    public function refreshWidget(AbstractWidget $elem)
    {
        $id = $elem->readProp('meth_param_id');
        $this->dispatchFromRS(
            function ($elem, $key) use ($id) {
                if ($elem->readProp('meth_param_id') === $id) {
                    call_user_func([$this, 'updateFromRS'], $elem, $key);
                }
            }
        );
    }

    public function updateFromRS($elem, $rs_key)
    {
        $v = $this->RS($rs_key)->getValue();
        if (($elem instanceof WidgetMemo)
            || ($elem instanceof ValueHostAccess)
        ) {
            $elem->setValue($v);
        } elseif ($elem instanceof CaptionFeatureAccess) {
            // self::console(__METHOD__, get_class($elem), $v);
            $elem->setCaption($v);
        } else {
            //noop
        }
    }

    public function validate()
    {
        // gc_collect_cycles();
        return $this->invokeValidate($this->getControlID());
    }

    public function invokeValidate($control_id)
    {
        return $this->getModel()->callValidate(
            $this->getSysProp('run_info.new_id'),
            $this->getControlID(),
            $control_id
        );
    }

    /**
     * Get modal result of form after closing
     *
     * @see WSPLRTL\Form\Access\Defs\Key
     * @see Nodelimit\Access\UI\Defs
     *
     * @return int current modal result when form closed
     */
    public function getModalResult()
    {
        return $this->readProp(DefsKeyAccess::MODAL_RESULT);
    }

    /**
     * Perform rollback
     *
     * @return void
     */
    public function rollback()
    {
        $this->getModel()->rollback();
    }

    /**
     * Perform commit
     *
     * @return void
     */
    public function commit()
    {
        $this->getModel()->commit();
    }

    /**
     * Create EventSpec
     *
     * Create EventSpec object
     *
     * @param string $type can be: click, change, ...
     * @param object $widget
     * @param mixed $meth_name string or null
     * @param mixed $data  any associated data
     *
     * @ingroup WSPLRTL-API-Internal
     *
     * @return EventSpec object
     */
    public function createEventSpec(
        $type,
        AbstractWidget $widget,
        $meth_name = null,
        $data = null
    ) {
        $delegate = $this->_makeDelegateForm();
        $eventContext = $this->makeEventContext($widget, $type, $meth_name);
        $event = $this->_createEvent($type, $data, $eventContext);
        // $sender = $widget;
        $sender = null;
        return EventSpec::create($event, $delegate, $sender);
    }

    public function refreshSystemGrid(FilterCustom $invoker)
    {
        $this->_getLaunchController()->refreshSystemGrid($invoker);
    }

    public function api($name)
    {
        return $this->_getBackend()->api($name);
    }

    public function lib($name)
    {
        return $this->_getBackend()->lib($name);
    }

    /**
     * Gets Sys backend
     *
     * @return WSPLRTL\Sys\Sys
     */
    public function sys()
    {
        return $this->_getBackend()->sys();
    }

    /**
     * Gets or define cursor
     *
     * @return object
     */
    public function cursor($name)
    {
        return $this->api('cursors')->cursor($name);
    }

    /**
     * function_title
     *
     * description
     *
     * @author author <email>
     */
    public function im()
    {
        return $this->api('im');
    }

    public function refreshConnection()
    {
        return $this->sys()->connection()->refresh();
    }

    public function live()
    {
        return $this->refreshConnection();
    }

    public function isFirstActivate()
    {
        return $this->firstActivate;
    }

    public function isActivate()
    {
        return $this->activate;
    }

    public function triggerActivate()
    {
        if (method_exists($this, 'onActivate')) {
            $this->incActivates();
            $this->dispatchSandboxedCaller([$this, 'delegateActivate']);
        }
    }

    public function delegateActivate()
    {
        $this->onActivate();
    }

    public function runActivate()
    {
        $f = $this;
        // self::logger(__FILE__, __LINE__, __METHOD__, 'check', getmypid());
        if ($f->isActivate()) {
            // self::logger(__FILE__, __LINE__, __METHOD__, 'run', getmypid());
            $f->triggerActivate();
        } else {
            // self::logger(__FILE__, __LINE__, __METHOD__, 'not run', getmypid());
        }
        $out = [];
        return $out;
    }

    // public function render()
    // {
    //     $elem = $this->getElement();
    //     return $elem;
    // }

    // public function render()
    // {
    //     $elem = $this->getElement();
    //     $out = $elem->render();
    //     if ($this->isFirstActivate()) {
    //         $this->triggerActivate();
    //     }
    //     return $out;
    // }


    public function renderFormCall()
    {
        return $this->dispatchSandboxedCaller([$this, 'renderForm']);
    }

    protected function dumpHook()
    {
        $src = get_class_methods($this);
        $db = new \ArrayIterator($src);
        $kw = 'raise';
        $it = new \RegexIterator($db, '/.*'.$kw.'.*/i');
        self::console(
            iterator_to_array($it),
            method_exists($this, 'raiseAlert'),
            method_exists($this, 'raise')
        );
    }

    private function patch($acode)
    {
       $ret = $acode;
        if ($this->isPatch()) {
            $lines = $this->getPatch();
            $ret = $acode . "\n" . $lines;
        }
        return $ret;
    }

    private function isPatch()
    {
        ///
        $o[] = '1000074699';
        $debug = true;
        if ($debug && in_array($this->getFormId(), $o)) {
            return true;
        }
        return false;
    }

    private function _registerCustomEventListener(array $p)
    {
        $this->_getBackend()->aux('ev.cust.handlers')->register($p);
    }

    /**
     * @see Nodelimit\Access\UI\Defs
     *
     * @param int $value
     *
     * @return void
     */
    private function _setModalResult($value)
    {
        $this->getSpec()->setProp(DefsKeyAccess::MODAL_RESULT, $value);
    }

    private function _getStateMediator()
    {
        $v = & $this->_state_mediator;
        if (!$v) {
            $v = new StateMediator();
        }
        return $v;
    }

    /**
     * Dispatching postback event
     *
     * Perform dispatching form event derived from postback and
     * translate it onto widget event
     *
     * @param PostbackEvent $ev
     *
     * @note at this point (_dispatchSys) sender reference in $form_ev
     * has resolved to real sender
     *
     * @note stateful components like combobox push-state (radio)
     * buttons may transmit state by input hidden or similar.
     *
     * No no need to update state on server side:
     *
     * - all required data in $_POST/$_GET
     *
     * - only need update state holders at CS
     *
     * @see internal annotations for stateful components implementation
     *
     * @return void
     */
    private function _dispatch(PostbackEvent $ev)
    {
        $evc = $ev->context;
        $sender_method = $evc->sender->method;
        $form_ev = $this->_createFormEvent($ev);
        $form_ev->sender->eventing = true;

        self::logger(
            // __FILE__,
            // __LINE__,
            getmypid(),
            '*** EVENT:',
            System::getNow(),
            $form_ev->sender->getWID(),
            $ev->what,
            $sender_method,
            get_class($form_ev),
            var_export($form_ev->sender->isTriggered(), true)
        );

        $ignore = $this->_dispatchPreprocess($form_ev);
        if ($ignore) {
            return false;
        }
        // self::logger(
        //     __FILE__,
        //     __LINE__,
        //     $this->wsys_form_module,
        //     $ev->what,
        //     $sender_method,
        //     get_class($form_ev)
        // );
        if ($form_ev instanceof CustomEventMediator) {
            $this->_dispatchCustomEvent($form_ev);
        } else {
            call_user_func([$this, $sender_method], $form_ev);
            if ($sender_method === 'handleEventClick_id_l_clear') {
                // $this->q('id_cb_MY')->refresh();
                // $this->q('id_cb_Make')->refresh();
                self::logger(__METHOD__, $sender_method, 'can be modified');
            }
        }
        $this->_dispatchSys($form_ev);
        $this->_dispatchPostprocess($form_ev);
        // self::logger(
        //     __FILE__,
        //     __LINE__,
        //     $ev->what,
        //     $sender_method,
        //     'dispatch finished'
        // );
    }

    /**
     * Dispatch preprocess event
     *
     * @param AbstractEvent $ev
     *
     * @return void
     */
    private function _dispatchPreprocess(AbstractEvent $ev)
    {
        // $this->triggerClose();

        // $this->testRowEvent($ev);

        // $this->testForwardEvent($ev);

        $filtered = (
            ($ev instanceof EventSocketComboboxRPC)
            || ($ev instanceof CustomEventMediator)
        );

        // self::console(
        //     __METHOD__,
        //     $filtered,
        //     get_class($ev),
        //     $ev instanceof EventSocketComboboxRPC,
        //     $ev instanceof CustomEventMediator
        // );

        if ($this->isDeadlockProtect() && !$filtered) {
            $prove = Prove::check($ev->sender);
            // $ok = $this->checkAttend($ev->sender, $ev);
            $ok = $prove;

            // $dump = [
            //     'ev.what' => get_class($ev),
            //     // 'sender.guide' => $ev->sender(),
            //     'sender.control_id' => (string) $ev->sender->getControlID(),
            //     'wid' => $ev->sender->getWID(),
            //     'prove' => $ok
            // ];

            // self::console(__METHOD__, $dump);

            if (!$ok) {
                return true;
            }
        }

        if ($ev instanceof EventAccept) {
            $sender = $ev->sender;
            $id = $sender->readProp('id');
            /// @internal after.ref.chosen
            $m = 'handleEventAfterRefChosen_' . $id;
            if (method_exists($this, $m)) {
                call_user_func([$this, $m], $ev);
            }
        }
    }

    private function testForwardEvent($ev)
    {
        if ($ev instanceof ForwardEvent) {
            $dump = [
                'whoami' => __METHOD__,
                'dig' => $ev->dig(),
                'JSON' => $ev->JSON(),
                'json.lo' => $ev->json()
            ];
            self::console($dump);
        }
    }

    private function _dispatchCustomEvent(CustomEventMediator $ev)
    {
        $t = WF::target(
            (string) $ev->sender->getProp('evinfo_target')->toClassName()
        );
        $k = $t->toGuide()->toBasenameAnchor();
        if (isset($_POST[$k])) {
            $info = json_decode($_POST[$k], true);
            if (isset($info['cmd'])) {
                $h = $this->_getBackend()->aux(
                    'ev.cust.handlers'
                )->getHandlers($ev->sender, $info['cmd']);
                // $dump = [
                //     // 'ev.what' => $ev->what,
                //     // 'sender_method' => $sender_method,
                //     'post' => $_POST,
                //     'handlers' => $h,
                // ];
                // self::console($dump);
                // // self::logTraceDump($evc, $evc->sender, $form_ev);
                foreach ($h as $elem) {
                    call_user_func([$this, $elem['name']], $ev);
                }
            } else {
                throw new \Exception('could not handle event');
            }
        }
    }

    private function dispatchSandboxed(PostbackEvent $ev)
    {
        $cb = function () use ($ev) {
            $this->_dispatch($ev);
        };
        return $this->dispatchSandboxedCaller($cb);
    }

    private function dispatchSandboxedCaller(callable $cb)
    {
        // $bt = App::getInstance()->getBacktrack();
        // self::vdump(__METHOD__, $bt->hasCurrent());
        // self::vdump(__METHOD__, $bt->getCurrent());

        // self::console(__METHOD__, 'before');
        list($ok, $ret) = Sandbox::bootstrap($cb);
        // self::console(__METHOD__, 'after');

        // self::logger(
        //     __METHOD__,
        //     __LINE__,
        //     $ok,
        //     LangValue::dump($ret, true)
        // );

        /// @todo Throwable
        if ($ret instanceof \Exception) {
            $e = $ret;
            $v = new ExceptionResult($ret);
            $msg = $v->extractMessage();
            self::console(
                get_class($this),
                getmypid(),
                '*** EXCEPTION ***',
                get_class($this),
                get_class($e),
                $msg,
                $e->getFile(),
                $e->getLine(),
                $e->getCode(),
                self::getBacktraceString()
            );
        }

        if ($ok) {
            // noop
            /// @internals this code must run when success but before render prefer
            // if (Registry::getInstance()->has('$backtrackAcquire')) {
            //     self::console(__METHOD__, 'reg ready');
            //     $acq = Registry::getInstance()->get('$backtrackAcquire');
            //     $acq();
            // } else {
            //     self::console(__METHOD__, 'reg unready');
            // }
            return $ret;
        } elseif ($ret instanceof ProcinitException) {
            $pc = $ret->getRecycledPortableContext();
            ///
            /// @todo warning  !!! in some cases $pc->code or $pc may inaccessible
            ///
            self::logTraceDump($pc);
            // self::logTraceDump($pc->code, $pc);
            // if ($pc instanceof OCI8ErrorInfo
            //     && ($pc->code >= 20980 && $pc->code <= 20999)
            // ) {
            if ($pc instanceof OCI8ErrorInfo) {
                $this->dispatchOCI8ErrorInfo($ret);
            }
            // System::raise($ret);
            throw $ret;
            // $this->_alert(new RaiseException('application server error', 0, $ret));
        } elseif ($ret instanceof RaiseException) {
            $this->_alert($ret->getMessage());
            return true;
        } elseif ($ret instanceof HaltException) {
            $this->_halt($ret);
            return true;
        } elseif ($ret instanceof \Exception) {
              $fs = "\n";
              $this->_alert(
                  '*** UNHANDLED EXCEPTION ***' .
                  $fs .
                  $fs .
                  $msg
              );
        } else {
            throw new \LogicException();
        }
    }

    private function _halt(HaltException $e)
    {
        // noop
    }

    private function _alert($msg)
    {
        $this->wireAlert($msg);
    }

    private function _dispatchEventSocket(AbstractEvent $ev)
    {
        // self::logTraceDump($ev);
        if ($ev instanceof EventSocketComboboxRPC) {
            ActionComboboxDispatch::createWithArrayContext(
                $this,
                [
                    'event' => $ev
                ]
            )->run();
        } elseif ($ev instanceof EventSocketUploadTrap) {
            self::logTraceDump($ev->sender->getSpec());
            self::console(__METHOD__, $ev->sender->readProp('id'));
            // DispatchUpload::createWithArrayContext(
            //     $this, ['event' => $ev]
            // )->run();
            $sender = $ev->sender;
            $meth = 'handleEventUpload_' . $sender->readProp('id');
            call_user_func([$this, $meth], $ev);
        } else {
            $this->_unknownEvent($ev);
        }
    }

    private function _unknownEvent(AbstractEvent $ev)
    {
        throw new \UnexpectedValueException('unknown event');
    }

    /**
     * @warning this method not actual now and not called
     *
     * @param ev
     *
     * @return
     */
    private function _dispatchPreprocessRadio(AbstractEvent $ev)
    {
        ActionPreprocessRadioClick::createCustom($this, ['event' => $ev])->run();
    }

    private function _dispatchSys(AbstractEvent $ev)
    {
        $sender = $ev->sender;
        if ($sender instanceof WidgetButtonOk) {
            // $this->triggerClose();
            $this->_dispatchButtonOK($ev);
        } else {
            $sm = $this->_getStateMediator();
            if ($sm->has(self::BTCLOSE_CANCEL)) {
                //$sm->set('req.exit');
            } elseif ($sender instanceof WidgetButtonCancel) {
                $this->_putStateMediator(self::BTCLOSE_CANCEL);
            } elseif ($sender->isReferencer()) {
                if ($ev instanceof EventAccept) {
                    //noop
                } elseif ($ev instanceof EventClick) {
                    $this->_handleReferencer($ev);
                }
            }

            // if ($sm->has(self::BTCLOSE_CANCEL)) {
            //     $this->triggerClose();
            // }
        }
        return true;
    }

    /**
     * @todo proposal: instead Helper use Action?
     *
     * @param ev
     *
     * @return
     */
    private function _handleReferencer(AbstractEvent $ev)
    {
        $c = [
            'event' => $ev,
            'launcher-hinting' => $this->_getLauncherHinting(),
            'launcher-opener-pbi' => $this->_getLauncherOpenerPBI()
        ];
        // self::logTraceDump($ev->sender, $ev);
        HelperReference::create($this, $c)->run();
    }

    private function _getLauncherHinting()
    {
        return $this->_getLaunchController()->getLauncherHinting();
    }

    private function _getLauncherOpenerPBI()
    {
        return $this->_getLaunchController()->getOpenerPBI();
    }

    /**
     * @todo check type of event and filter to click only
     * @todo same as above in other deps?
     *
     * @param ev
     *
     * @return
     */
    private function _dispatchButtonOK(AbstractEvent $ev)
    {
        $m = $this->getModel();
        $m->submit($ev);
        $this->_putStateMediator(self::BTOK_SUCCESS);
        return true;
    }

    private function _putStateMediator($state)
    {
        $this->_getStateMediator()->set($state);
    }

    private function _dispatchPostprocess(AbstractEvent $ev)
    {
        $state = $this->_getStateMediator();
        if ($state->has(self::BTOK_SUCCESS)) {
            $this->_dispatchNext($ev);
        } elseif ($state->has(self::BTCLOSE_CANCEL)) {
            $this->_dispatchCloseCancel($ev);
        }
    }

    private function _dispatchCloseCancel($ev)
    {
        list($lc, $r_next) = $this->_dispatchBase(false);
        //self::logTrace($r_next);
        //self::logTrace();
        //$next = $r_next['NEXT_FORM'];
        $next = $r_next['PREV_FORM'];
        if ($next) {
            $this->_dispatchPrevForm($lc, $r_next);
        } else {
            $this->_setModalResult(self::MR_CANCEL);
            $this->_dispatchClose($ev);
        }
    }

    private function _dispatchClose($ev)
    {
        //self::logTrace();
        $this->_getLaunchController()->actionClose($this);
    }

    private function _dispatchPrevForm($lc, $next_info)
    {
        $lc->prevForm($this, $next_info);
    }

    private function _dispatchBase($next = true)
    {
        $lc = $this->_getLaunchController();
        $run_info = $this->getSysProp('run_info');
        if ($next) {
            $r_next = $lc->handleNext($run_info);
        } else {
            $r_next = $lc->handlePrev($run_info);
        }
        return [$lc, $r_next];
    }

    private function _dispatchNext(AbstractEvent $ev)
    {
        list($lc, $r_next) = $this->_dispatchBase();
        //self::logTrace($r_next);
        $next = $r_next['NEXT_FORM'];
        if ($next) {
            $this->_dispatchNextForm($lc, $r_next);
        } else {
            $this->_dispatchSubmitFinally($ev);
        }
    }

    private function _dispatchNextForm($lc, $next_info)
    {
        $lc->nextForm($this, $next_info);
    }

    private function _getLaunchController()
    {
        $lc = & $this->_launch_ctl;
        if (!$lc) {
            $lc = LaunchController::createFromContext(
                $this->_getLaunchControllerContext()
            );
        }
        return $lc;
    }

    private function _getLaunchControllerContext()
    {
        return $this->readProp('shared_context');
    }

    private function _dispatchSubmitFinally(AbstractEvent $ev)
    {
        $m = $this->getModel();
        $m->submitFinally($ev);
        $m->commit();
        $this->_setModalResult(self::MR_OK);
        $this->_dispatchClose($ev);
    }

    private function _init()
    {
        $this->_lazy_model = LazyInit::create([$this, 'createModel']);

        ///
        /// @note override default initialization: special case
        ///

        /// @note default is deadlock  protecting on
        $this->enableDeadlockProtectOnce();
        // $this->disableDeadlockProtectOnce();

        $s = $this->getSpec();
        $s->setPropOnce('activates', 0);

        if ($this->_isInitiation()) {
            $this->_setModalResult(self::MR_NONE);
            $this->_initGreeting();
        }
        $this->_activate();
    }

    /**
     * Form being active.
     *
     * Perform handling of form activation. Called whenever form has
     * been activated. Its also call launch controller to set active
     * form_id, used later when need open form from Backtrack for
     * example
     *
     *
     * @return void
     */
    private function _activate()
    {
        $lc = $this->_getLaunchController();
        $old = $lc->getCurrentFormId();
        $lc->setCurrentFormId($this->_getFormId());
        // self::logger(__FILE__, __LINE__, __METHOD__, getmypid(), 'form activate', $this->getFormId(), $old);
        // $keys = ['file', 'line', 'method', 'pid', 'msg', 'getFormId', 'old'];
        // $values = [__FILE__, __LINE__, __METHOD__, getmypid(), 'form activate', $this->getFormId(), $old];
        // $dump = array_combine($keys, $values);
        if ($old !== $this->getFormId()) {
            // self::logger(__FILE__, __LINE__, __METHOD__, getmypid(), 'trigger form activate', $this->getFormId(), $old);
            // self::vdump(__FILE__, __LINE__, __METHOD__, getmypid(), 'trigger form activate $old:', $old, (bool) $old, !((bool) $old));
            // $dump['msg'] = 'trigger form activate';
            // $dump['setFirstActivate'] = !((bool) $old);
            $activates = $this->getActivates();
            $this->setFirstActivate($activates < 1);
            $this->setActivate(true);
            if ($this->isResuming()) {
                $this->runActivate();
            }
        } else {
            $this->setFirstActivate(false);
            $this->setActivate(false);
            // $dump['msg'] = 'skip trigger form activate';
        }
        // self::console($dump);
    }

    private function setFirstActivate($flag)
    {
        $this->firstActivate = $flag;
    }

    private function setActivate($flag)
    {
        $this->activate = $flag;
    }

    private function incActivates()
    {
        $s = $this->getSpec();
        $inc = $s->getProp('activates');
        $s->setProp('activates', ++$inc);
    }

    private function getActivates()
    {
        $s = $this->getSpec();
        return $s->getProp('activates');
    }


    private function _getFormId()
    {
        //self::logTrace($this->getSysProp('run_info'));
        return $this->getSysProp('run_info')->form_id;
    }

    private function _initGreeting()
    {
        $s = $this->getSpec();
        // $s->setProp('activates', 0);
        // $s->setProp('custom.filter', FilterFacade::create());
        $s->setProp('backend.pspec', $this->makeSlotSpec());
        //$this->_launch_ctl = $this->getProp('__launch_ctl', false);
        $this->_launch_ctl = $this->readProp('__launch_ctl');
        //call package init if flag init

        // self::logger(__METHOD__, $s->getProp('activates'));

        $this->getModel()->greeting();
        $this->startup();
    }

    private function _getProcdConn()
    {
        $ret = $this->getProp('__procd_conn');
        return $ret;
    }

    /**
     * Create Form Eevent from PostbackEevent
     *
     * @param PostbackEvent $ev
     *
     * @return WSPLRTL\Event\AbstractEvent
     */
    private function _createFormEvent(PostbackEvent $ev)
    {
        $type = strtolower($ev->what);
        $base = '';
        // self::console($type);
        switch ($type) {
            case 'click':
                $base = 'Click';
                break;
            case 'accept':
                $base = 'Accept';
                break;
            case 'change':
                $base = 'Change';
                break;
            case 'focus':
                $base = 'Focus';
                break;
            case 'blur':
                $base = 'Blur';
                break;
            case 'focusin':
                $base = 'Focusin';
                break;
            case 'focusout':
                $base = 'Focusout';
                break;
            case 'input':
                $base = 'Input';
                break;
            case 'dblclick':
                $base = 'Dblclick';
                break;
            case 'forward':
                self::console(
                    __METHOD__,
                    $_POST,
                    $this->getProp('exchangeTarget')
                );
                $base = 'Forward';
                break;
            case '___sender___':
                $base = 'System';
                break;
            case ComboboxDefsAccess::EVSOCK_RPC:
                $base = 'Socket\\Combobox\\RPC';
                break;
            case UploadDefsInterface::EVSOCK_TRAP:
                $base = 'Socket\\Upload\\Trap';
                break;
            case ComboboxDefsAccess::EVCUST_MEDIATOR:
                $base = 'Custom\\Mediator';
                break;
            case 'timer':
                $base = 'Custom\\Timer';
                break;
            case self::EVMAP_CLICK:
                $base = 'Map\\Click';
                break;
            case self::EVMAP_OBJ_CLICK:
                $base = 'Map\\Obj\\Click';
                break;
            case self::EVMAP_SEARCH_RESULTS_CLICK:
                $base = 'Map\\Search\\Results\\Click';
                break;
            case self::EVGRID_ROW_CLICK:
                $base = 'Grid\\Row\\Click';
                break;
            case self::EVGRID_ROW_SELECT:
                $base = 'Grid\\Row\\Select';
                break;
            case self::EVGRID_ROWS_LOADED:
                $base = 'Grid\\Row\\RowsLoaded';
                break;
            case self::EVGRID_ROWS_ALL:
                $base = 'Grid\\Row\\RowsAll';
                break;
            default:
                throw new \UnexpectedValueException('unknown event');
        }
        $class_name = 'WSPLRTL\\Event\\' . $base;
        $sender = $this->_createLazySender($ev);
        $sentvars = new SentVars($this);
        $a = [
            'sender' => $sender,
            'data' => $ev->data,
            'sentvars' => $sentvars,
            'type' => $type
        ];
        // self::logTrace($_POST, $ev);
        // self::console(__FILE__, __LINE__, $_POST, $type, $class_name);
        $evOut = $class_name::create($a);
        return $evOut;
    }

    private function _createLazySender(PostbackEvent $ev)
    {
        $sender_context = $ev->context->sender->context;
        $f = function () use ($sender_context) {
            return $this->_getWidgetByContext($sender_context);
        };
        $lazy = LazyInit::create($f);
        return $lazy;
    }

    private function _getWidgetByContext($c)
    {
        $ret = $this->_getWidgetPool()->findKeyByContext($c);
        if ($ret) {
            return $this->q($ret);
        } else {
            throw \UnexpectedValueException('cant locate element by context');
        }
    }

    private function _getWidgetById($id)
    {
        // self::console(
        //     $this->isRecycle(),
        //     $this->getSpec()->hasProp('wpool'),
        //     $this->getSpec()->isPropSet('wpool'),
        //     $id
        // );
        return $this->_getWidgetPool()->getProp($id);
    }

    private function getWidgetByIdSafe($id)
    {
        $pool = $this->_getWidgetPool();
        if ($pool->hasBinding($id)) {
            return $this->q($id);
        }
        return null;
    }

    private function _createEvent($what, $data, $context)
    {
        return new PostbackEvent(
            $what,
            $data,
            $context,
            null,               /**< tag */
            null,               /**< sender */
            null,               /**< info */
            null,               /**< aux */
            $what               /**< type is equals to $what here */
        );
    }

    /**
     * Create module-level delegation info
     *
     * @param mixed $method method name or null
     *
     * @return object instance of Nodelimit\WF\Systype\Postback\Delegate
     */
    private function _makeDelegateForm($method = Delegate::DEFAULT_EVENT_METHOD)
    {
        return Delegate::create(
            get_called_class(), $this->wsys_form_module, 'phar-module', $method
        );
    }

    private function _createWidgetStorageSpec($widget)
    {
        /// @note only creating stage
        $p = $this->getCreatingParams(); /**< here form params and use_context */
        $c = null;
        if ($p instanceof ParamsResuming) {
            $c = $p->context;   /**< @warning this not used and incorrect */
            throw new \Exception('internal error of creating widget');
        } else {
            if ($widget instanceof AbstractGrid) {
                return WF::storageSpec();
            } else {
                $wp = $widget->getCreatingParams();
                // self::console(
                //     [
                //         'use_context.isset' => isset($wp->props['use_context']),
                //         'use_context' => isset($wp->props['use_context'])
                //         ? $wp->props['use_context'] : 'N/A',
                //         'widget' => get_class($widget)
                //     ]
                // );
                return WF::storageSpec();
            }
        }
        // return $c->makeSlot();
    }

    /**
     * Perform synchronization with pool and creating widget
     *
     * @todo see annotation for line withh access to $p->attrs
     *
     * @param widget
     * @param elem
     *
     * @return void
     */
    private function _syncWidgetPool($widget, $elem)
    {
        $pool = $this->_getWidgetPool();
        //has binding, no cached; no binding if creating
        //id_wcontrol -> not exists when creat
        //id_wcontrol -> known when resuming
        //$p = $widget->getCreatingParams();
        if ($this->_isResuming()) {
            //
        } else {
            $p = $widget->getCreatingParams();
            $props = $p->props;
            $id = $props['id'];
            $def = new RenewInfo(
                get_class($widget),
                $props['use_context'],
                Props::readKey($p->attrs, 'name') /**< @warning attrs */
            );
            $pool->ensureUndefined($id);
            $pool->addCached($id, $def, $widget);
        }
    }

    private function _isResuming()
    {
        return ($this->getCreatingParams() instanceof ParamsResuming);
    }

    private function _isInitiation()
    {
        return !$this->_isResuming();
    }

    private function _getWidgetPool()
    {
        // self::console(
        //     $this->isRecycle(),
        //     $this->getSpec()->hasProp('wpool'),
        //     $this->getSpec()->isPropSet('wpool')
        // );
        return $this->readProp('wpool');
    }

    /**
     * Create form from event
     *
     * @param PostbackEvent $ev
     *
     * @throws \UnexpectedValueException when undefined event context
     *
     * @return AbstractForm
     */
    private static function _resumeForm(PostbackEvent $ev)
    {
        // self::logTraceDump($ev, $ev->context);
        $evc = $ev->context;
        if (isset($evc)) {
            $form_class = $evc->mediator->delegate;
            $form = $form_class::resume($evc->mediator->context);
            return $form;
        } else {
            throw new \UnexpectedValueException(
                'encountered event context value with an undefined type'
            );
        }
    }

    /**
     * Opens Backend
     *
     * @return Backend
     */
    private function _getBackend()
    {
        return $this->_getLocalServiceItem('backend');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'backend' => function () {
                return Backend::create(
                    [
                        'originator' => $this,
                        'pspec' => $this->getSpec()->getProp(
                            'backend.pspec' /**< persist spec */
                        )
                    ]
                );
            }
        ];
    }
}
