<?php
/**
 * @file
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Access\API\User\Combobox
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Access\API\User;

use WSPLRTL\Widget\Access\API\User\Combobox\ReadInterface;

/**
 * The Combobox user API interface
 *
 * This interface describes the user API for combobox
 *
 * @interface WSPLRTL\Widget\Access\API\User\Combobox
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 */
interface Combobox extends ReadInterface
{

    /**
     * Removes all items from the item list
     *
     * @note WSPLRTL-API-User
     *
     * @return void
     */
    public function clear();

    /**
     * @~english Performs refreshing operation on the widget
     * @~russian Выполняет регенерацию виджета @~
     *
     * @details @~russian Запускает механизм регенерации виджета на
     * клиенте. @~
     *
     * @note WSPLRTL-API-User
     * @note experimental
     *
     * @return void
     */
    public function refresh();

    /**
     * Adds an item to the item list
     *
     * @param string $item
     *
     * @note WSPLRTL-API-User
     *
     * @return void
     */
    public function addItem($item);

    /**
     * @~english Removes an item from the item list by index
     * @~russian Удаляет элемент из списка по индексу @~
     *
     * @param int $index
     *
     * @note WSPLRTL-API-User
     *
     * @return void
     */
    public function removeItemAt($index);

    /**
     * Removes item from comboboxes list
     *
     * @param string $item
     *
     * @todo Combobox::removeItemAll()
     *
     * @note will remove first found item - not all
     *
     * @note WSPLRTL-API-User
     *
     * @return bool TRUE if item was found
     */
    public function removeItem($item);

    /**
     * Make selected item by $item
     *
     * @param string $item
     *
     * @note it make selected if value of $item exactly present in list
     * of elements of combobox
     *
     * @note if $item not exists in set of elements, current state of
     * selected item will not changed - just return FALSE
     *
     * @note WSPLRTL-API-User
     *
     * @return bool TRUE if $item was found
     */
    public function setSelectedItem($item);

    public function putSelectedItem($item);

    /**
     * Selects the item at $index
     *
     * @param int $index of selected index
     *
     * @note WSPLRTL-API-User
     *
     * @throws \OutOfBoundsException
     * @return void
     */
    public function setSelectedItemIndex($index);

    /**
     * This method is short alias of WSPLRTL\Widget\Combobox::setSelectedItemIndex()
     * @see WSPLRTL\Widget\Combobox::setSelectedItemIndex()
     * @note WSPLRTL-API-User
     */
    public function setSelectedIndex($index);

    public function unselect($refresh = true);

    public function discard($refresh = false);
}
