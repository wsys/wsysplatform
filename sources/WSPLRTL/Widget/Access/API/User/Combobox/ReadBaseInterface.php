<?php
/**
 * @file
 *
 * @brief ReadBaseInterface.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Access\API\User\Combobox\ReadBaseInterface
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Access\API\User\Combobox;

/**
 * The ReadBaseInterface interface
 *
 * Description
 *
 * @interface WSPLRTL\Widget\Access\API\User\Combobox\ReadBaseInterface
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
interface ReadBaseInterface
{
    /**
     * Returns the current selected item
     *
     * @note WSPLRTL-API-User
     *
     * @return mixed can be string or NULL if selection is not performed
     * in control.
     */
    public function getSelectedIndex();

    /**
     * @~english Returns whether the item was selected
     * @~russian Возвращает признак что элемент был выбран @~
     *
     * @note WSPLRTL-API-User
     *
     * @return bool
     */
    public function hasIndex();

    /**
     * Returns text from selected item
     *
     * @note WSPLRTL-API-User
     * @note this method is exception safe if combobox has no selected
     * item - just return null
     *
     * @return mixed string|NULL whether performed selection on items in combobox
     */
    public function getText();

    /**
     * Returns text from selected item or throw exception if selection
     * not available
     *
     * This method is similar as getText but can throw exception if no selection
     *
     * @note WSPLRTL-API-User
     * @note to prevent exception use Combobox::getText() method or
     * validate with hasIndex()
     *
     * @throws Produces exception on an E_NOTICE error message when the
     * specified index does not exist.
     *
     * @return string the text of the selected item
     */
    public function getSelectedItem();
}
