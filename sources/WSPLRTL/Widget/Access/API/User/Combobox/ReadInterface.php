<?php
/**
 * @file
 *
 * @brief ReadInterface.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Access\API\User\Combobox\ReadInterface
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Access\API\User\Combobox;

use WSPLRTL\Widget\Access\API\User\Combobox\ReadBaseInterface;

/**
 * The ReadInterface interface
 *
 * Description
 *
 * @interface WSPLRTL\Widget\Access\API\User\Combobox\ReadInterface
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
interface ReadInterface extends ReadBaseInterface
{
    /**
     * Get count of items
     *
     * @note WSPLRTL-API-User
     *
     * @return int
     */
    public function getCount();

    /**
     * Returns the list item by the specified index
     *
     * @param int $index
     *
     * @note WSPLRTL-API-User
     *
     * @return string
     */
    public function getItem($index);

    /**
     * Returns whether the requested index exists
     *
     * @param int $index The index being checked
     *
     * @note WSPLRTL-API-User
     *
     * @return bool TRUE if the requested index exists, otherwise FALSE
     */
    public function hasItemIndex($index);

    /**
     * This method is alias of hasItemIndex()
     * @see Combobox::hasItemIndex()
     */
    public function hasItemAt($index);
}
