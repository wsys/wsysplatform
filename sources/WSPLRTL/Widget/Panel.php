<?php
/**
 * Panel.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractGeneric;
use Nodelimit\Access\Widget\Feature\Caption;
use WSPLRTL\Widget\Addition\Feature\Caption as CaptionFeatureAddition;

/**
 * Panel.php
 *
 * @class WSPLRTL\Widget\Panel
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Panel extends AbstractGeneric implements Caption
{
    use CaptionFeatureAddition;

    const ELEMENT_CLASS = 'WSPLRTL\Element\Panel';

    protected function initialize()
    {
        parent::initialize();
    }

    public function setBody($body, $refresh = true)
    {
        $this->getElement()->setBody($body, $refresh);
    }
}
