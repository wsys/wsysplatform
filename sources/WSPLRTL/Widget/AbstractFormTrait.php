<?php
/**
 * @file
 *
 * @brief AbstractFormTrait.php
 *
 * @copyright Copyright (C) 2021  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\AbstractFormTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use com\wsplatform\syslib\exception\procinit\client\ServerError as ProcinitException;
use WSPLShared\Exception\FormSessionExpiredException;
use Namedio\Core\Exception\DB\NoDataFoundException;
use DataURI\Parser as DataURIParser;
use DataURI\Data as DataURIData;
use DataURI\Dumper as DataURIDumper;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use Imagick;
use ImagickPixel;

/**
 * The AbstractFormTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\AbstractFormTrait
 *
 * @copyright Copyright (C) 2021  The Wsysplatform Development Team
 */
trait AbstractFormTrait
{

    private function debugEvent2($ev)
    {
        /// get payload from forward event
        $json = $ev->JSON();
        $uri = $json['uri'];

        /// parse uri and create data object
        $dataObject = DataURIParser::parse($uri);

        /// check mime type of payload: expected image/png
        $mime = $dataObject->getMimeType();
        if ($mime === 'image/png') {

            /// extract uri data: (decode base64 to binary image)
            $data = $dataObject->getData();

            /// build path base, to mntcat directory
            $mntcat = $this->lib('env')->get('wsys.mntcat.path');
            $mntpath = $mntcat . '/vz/proj/upload';

            /// build filename (without path)
            $prefix = 'svg';
            $ext = '.png';
            $name = $prefix . $ext;

            /// make FS layer to local filesystem at $mntpath
            $adapter = new Local($mntpath);
            $fs = new Filesystem($adapter);

            ///
            /// put file with contents at $relpath that relative to $mntcat
            ///

            /// make $filename and $fullname
            $relpath = '/';
            $filename = $relpath . $name;
            $fullname = $mntpath . $filename;

            ///
            /// optional: remove file if same exists
            ///
            if ($fs->has($filename)) {
                $fs->delete($filename);
            }

            /// write content to file
            $fs->write($filename, $data);

            /// change image with imagick
            $imagick = new Imagick($fullname);
            $imagick->flopImage();
            // $imagick->setImageAlphaChannel(Imagick::ALPHACHANNEL_EXTRACT);
            $imagick->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
            // $imagick->setImageBackgroundColor('white');
            // $imagick->setImageAlphaChannel(Imagick::ALPHACHANNEL_SHAPE);

            /// overwrite image with imagick changes
            $fs->delete($filename);
            $fs->write($filename, $imagick->getImageBlob());

            $ev->sender->wired()->insertAfter('<p>project file saved</p>');

            $dataObject = DataURIData::buildFromFile($fullname);
            $dataUri = DataURIDumper::dump($dataObject);
            $this->q('id_btn_print2')->wired()->insertAfter("<img src='{$dataUri}'>");

        } else {
            $this->raiseAlert('incorrect uri data format');
        }
    }

    private function testRowEvent($ev)
    {
        if ($ev instanceof RowClick
            || $ev instanceof RowSelect
        ) {
            $dump = [
                'whoami' => __METHOD__,
                'dumpInternals' => $ev->own()->dumpInternals(),
                'getCheckedScope' => $ev->own()->getCheckedScope(),
                'isCheckedScopeAll' => $ev->own()->isCheckedScopeAll(),
                'isCheckedScopeNone' => $ev->own()->isCheckedScopeNone(),
                'isCheckedScopeSome' => $ev->own()->isCheckedScopeSome(),
                'isCheckedScopeRow' => $ev->own()->isCheckedScopeRow()
            ];
            self::console($dump);
        }
    }

    private function testRowEvent0($ev)
    {
        if ($ev instanceof RowClick
            || $ev instanceof RowSelect
        ) {
            // self::console(
            //     __METHOD__,
            //     'RowClick',
            //     $ev->sender->isSelected(),
            //     $ev->own()->isSelected(),
            //     $ev->own()->isSelectedChecked(),
            //     $ev->own()->isSelectedRadio(),
            //     $ev->own()->getSelectedRowRadio(),
            //     $ev->own()->getSelectedRowIndex(),
            //     $ev->own()->getSelectedRowChecked(),
            //     $ev->own()->getSelectedRow()->fieldByName('ID')->getValue(),
            //     $ev->own()->getSelectedRow()->field(0)->getValue(),
            //     $ev->own()->getSelectedRow()->field(1)->getValue(),
            //     $ev->own()->getSelectedRow()->field(2)->getValue()
            // );
            // // $ev->debugDraft();
            // $rs = $ev->own()->getSelectedRowset();
            // self::logTraceDump($rs);
            // foreach ($rs as $r) {
            //     self::console($r->fieldByName('ID')->getValue());
            // }
            // $s = $ev->own()->selected();
            $o = $ev->own();
            self::logTraceDump($o);
            // $n = $s->count();
            // $s->first();
            // $out = [$n];
            // while ($s->valid()) {
            //     $out[] = $s->field(2)->getValue();
            //     $out[] = $s->fieldByName('ID')->getValue();
            //     $s->next();
            // }
            // self::console($out);
        }
    }

    private function debugEvent($ev)
    {
        /// get payload from forward event
        $json = $ev->JSON();
        $uri = $json['uri'];

        /// parse uri and create data object
        $dataObject = DataURIParser::parse($uri);

        /// check mime type of payload: expected image/png
        $mime = $dataObject->getMimeType();
        if ($mime === 'image/png') {

            /// extract uri data: (decode base64 to binary image)
            $data = $dataObject->getData();

            /// build path base, to mntcat directory
            $mntcat = $this->lib('env')->get('wsys.mntcat.path');
            $mntpath = $mntcat . '/vz/proj/upload';

            /// build filename (without path)
            $prefix = 'svg';
            $ext = '.png';
            $name = $prefix . $ext;

            /// make FS layer to local filesystem at $mntpath
            $adapter = new Local($mntpath);
            $fs = new Filesystem($adapter);

            ///
            /// put file with contents at $relpath that relative to $mntcat
            ///

            /// make $filename and $fullname
            $relpath = '/';
            $filename = $relpath . $name;
            $fullname = $mntpath . $filename;

            ///
            /// optional: remove file if same exists
            ///
            if ($fs->has($filename)) {
                $fs->delete($filename);
            }

            /// write content to file
            $fs->write($filename, $data);


            ///
            /// optional: modify image, output to browser
            ///

            /// change image with imagick
            /// @see https://www.php.net/manual/en/book.imagick.php
            $imagick = new Imagick($fullname);
            // $imagick->flopImage();
            $imagick->autoLevelImage();
            $imagick->contrastImage(true);
            // $imagick->minifyImage();
            $imagick->resampleImage(80, 80, Imagick::FILTER_LANCZOS, 1);

            /// overwrite current image with changes
            $fs->delete($filename);
            $fs->write($filename, $imagick->getImageBlob());

            $ev->sender->wired()->insertAfter('<p>project file saved</p>');

            $dataObject = DataURIData::buildFromFile($fullname);
            $dataUri = DataURIDumper::dump($dataObject);
            $this->q('id_btn_print2')->wired()->insertAfter("<img src='{$dataUri}'>");

        } else {
            $this->raiseAlert('incorrect uri data format');
        }
    }

    private function debugEvent1($ev)
    {
        self::logTraceDump(__METHOD__);
        $json = $ev->JSON();
        $uri = $json['uri'];
        $dataObject = DataURIParser::parse($uri);
        $mime = $dataObject->getMimeType();
        if ($mime === 'image/png') {
            $data = $dataObject->getData();
            $mntcat = $this->lib('env')->get('wsys.mntcat.path');
            $path = $mntcat . '/vz/proj/upload';
            $prefix = 'svg';
            $ext = '.png';
            $name = $prefix . $ext;
            $filename = $path . '/' . $name;
            $bytes = file_put_contents($filename, $data);
            if ($bytes === false) {
                $this->raiseAlert('не удается сохранить файл проекта');
            }
        } else {
            $this->raiseAlert('incorrect uri data format');
        }
        // $dump = [
        //     'mime' => $dataObject->getMimeType()
        // ];
        // self::console($dump);
        // $data = $dataObject->getData();
        // $filename = '/tmp/svg.png';
        // file_put_contents($filename, $data);
    }

    private function debugEvent0($ev)
    {
        self::logTraceDump(__METHOD__);
        $json = $ev->JSON();
        $uri = $json['uri'];
        $parts = explode(',', $uri);
        $decode = base64_decode($parts[1]);
        $filename = '/tmp/svg.png';
        file_put_contents($filename, $decode);
    }

    private function getPatch()
    {
        $out = <<<EOT
console.debug('im patch');
 _.delay(() =>
        Wsys({$this}).q('id_btn_print').forward(
            'forward',
            {
                'foo': 'bar'
            }
        ), 7000);
EOT;

        return $out;
    }

    private function dispatchOCI8ErrorInfo(ProcinitException $e)
    {
        $pc = $e->getRecycledPortableContext();
        $code = $pc->code;
        if ($code === 20982) {
            throw new FormSessionExpiredException(
                /// <<<EOT
                /// <<<EOT
                <<<'EOT'
Primary: form session expired or was reset
Detail: Failed to resume form session.
The database connection was closed
due to inactivity or other technical reasons in most cases.
Hint: To restart and reload page press F5 key.
EOT
            );
        } elseif ($code === 1403) {
            throw new NoDataFoundException(
                /// <<<EOT
                /// <<<EOT
                <<<EOT
Primary: unexpectedly empty data buffer
or expected data not found
Detail: {$e->getMessage()}
Hint: To restart and reload page press F5 key.
EOT
            );
        }
    }

    private function triggerClose()
    {
        $target = $this->getProp('target');
        $args = [];
        $args[] = (string) $target;
        $args[] = (string) $target;
        $args[] = (string) $target;
        $code =<<<'EOT'
console.debug('formClose elem:', '%s', $('%s'));
$('%s').trigger('formClose');
console.debug('formClose elem triggered');
EOT;
/// <<<EOT ends

        $this->send(
            vsprintf($code, $args)
        );
    }

    protected function startupDial()
    {
        //noop
        // $this->wireAlert(
        //     json_encode($_SERVER, JSON_PRETTY_PRINT)
        // );

        $rnd = rand();
        $who = $this->im()->who();
        $dial = '';
        $autoDial = false;
        $to = null;
        $first = null;
        if ($who->count() > 0) {
            $to = $this->im()->who()[0]->toRecip();
            $first = $to['jid']['jid.real'];
        }
        if ($who->count() > 0 && $autoDial) {
            $p = json_encode(['to' => $to]);
            $dial = <<<EOT
_.delay(
    () => {
        Wsys($this).o('dial').dial($p);
    },
    12000
);
EOT;

        } else {
            $dial = <<<EOT
console.warn('dial: no on-line users or disabled');
EOT;
        }

        $conf = json_encode(
            [
                'form' => $this->getSelector(),
                'who' => [
                    'first' => $first
                ]
            ]
        );

        $inject = <<<EOT
import {Client} from '/sys/dev/stub/client.js?{$rnd}';
let m = new Client(${conf});
m.connect();
console.debug('wireModule: Client loaded', $this);
${dial}
EOT;

        $this->wireModule(
            $inject
        );
    }
}
