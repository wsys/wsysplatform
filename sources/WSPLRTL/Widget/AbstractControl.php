<?php
/**
 * AbstractControl.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractGeneric;

/**
 * AbstractControl.php
 *
 * description
 *
 * @class WSPLRTL\Widget\AbstractControl
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
abstract class AbstractControl extends AbstractGeneric
{
    /**
     * Sets enabling for control
     *
     * @param bool $enable
     *
     * @internal
     * + disabled = "disabled" HTML attribute for disabled state
     * + disabled = ""         HTML attribute for normal state
     *
     * @todo handle also properties for input-based controls
     * @todo check for non-input based (like Memo) controls related properties
     *
     * @see com\danscode\wf\html\element\ControlBase
     * @see com\danscode\wf\component\rover\applet\farm\builder\strategy\kit\Flow
     *
     * @return void
     */
    public function setEnabled($enable = true)
    {
        $w = $this->getSpec()->wired();
        if ($enable) {
            $w->removeClass('disabled');
            // if ($w->hasAttr('disabled')) {
            //     $w->removeAttr('disabled');
            // }
            if ($w->hasAttr('disabled')) {
                 $w->unsetClientAttr('disabled');
            }
            $w->removeAttr('disabled');
        } else {
            $w->addClass('disabled');
            $w->setAttr('disabled', 'disabled');
        }
    }
}
