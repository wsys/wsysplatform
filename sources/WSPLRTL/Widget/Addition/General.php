<?php
/**
 * General.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Addition;

use Nodelimit\WF\Element\Lib\Capable\Driven\Client\Simple
    as LibCapableDriveClientSimple;

/**
 * General.php
 *
 * @note to use this trait private $_element required
 *
 * @implements WSPLRTL\Access\General
 *
 * @trait WSPLRTL\Widget\Addition\General
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
trait General
{
    use LibCapableDriveClientSimple;

    public function setColor($value)
    {
        $this->_element->setColor($value);
    }

    public function getColor()
    {
        return $this->_element->getColor();
    }

    public function setBgColor($value)
    {
        $this->_element->setBgColor($value);
    }

    public function getBgColor()
    {
        return $this->_element->getBgColor();
    }

    public function setWidth($value)
    {
        $this->_element->setWidth($value);
    }

    public function getWidth()
    {
        $this->_element->getWidth();
    }

    public function setHeight($value)
    {
        $this->_element->setHeight($value);
    }

    public function getHeight()
    {
        $this->_element->getHeight();
    }

    public function refresh()
    {
        //noop
        //self::logTrace(__METHOD__);
        $this->getForm()->refresh();
    }

    public function refreshRelated()
    {
        $this->getForm()->refreshWidget($this);
    }
}
