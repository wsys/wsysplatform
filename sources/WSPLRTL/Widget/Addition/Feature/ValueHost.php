<?php
/**
 * ValueHost.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Addition\Feature;

use com\danscode\lib\Lists;

/**
 * ValueHost.php
 *
 * @trait WSPLRTL\Widget\Addition\Feature\ValueHost
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
trait ValueHost
{
    /**
     * @implements @todo StoreAttrsAccess
     * @override parent::getStoreAttrs()
     * @todo refactor this call to registerWidgetStoreAttrs
     *
     * @return
     */
    public function getStoreAttrs()
    {
        return Lists::add(parent::getStoreAttrs(), ['value']);
    }

    /**
     *
     * @implements @todo IdentifyAccess
     *
     * @return
     */
    // public function getValueKey()
    // {
    //     $elem = $this->getElement();
    //     $c = $elem->getContext();
    //     list($elem_props, $elem_attrs) = $elem->reflect();
    //     $anchor = $c->toBasenameAnchor();
    //     $id = $c->toBasenameID();
    //     return $anchor . '.' . $id;
    // }

    /**
     * @implements ValueHostAccess
     *
     *
     * @return
     */
    public function getValue()
    {
        self::logTraceDump(
            $this->getAttr('value')
        );
        return $this->getAttr('value');
    }

    /**
     * @implements ValueHostAccess
     *
     * @return
     */
    public function setValue($value)
    {
        $elem = $this->getElement();
        $elem->setValue((string) $value);
    }

    public function __toString()
    {
        return (string) $this->getValue();
    }
}
