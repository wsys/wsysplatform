<?php
/**
 * Label.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use Nodelimit\Access\Widget\Feature\Caption;
use WSPLRTL\Widget\Addition\Feature\Caption as CaptionFeatureAddition;
use WSPLRTL\Widget\AbstractGeneric;
use com\danscode\lib\Lists;

/**
 * Label.php
 *
 * @class WSPLRTL\Widget\Label
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Label extends AbstractGeneric implements Caption
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Label';

    use CaptionFeatureAddition;

    private static $_store_props = [
        // 'caption'
    ];

    protected function initialize()
    {
        //self::logger(__METHOD__);
        parent::initialize();
    }

    public function getStoreProps()
    {
        return Lists::add(parent::getStoreProps(), self::$_store_props);
    }
}
