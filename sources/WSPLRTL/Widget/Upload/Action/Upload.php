<?php
/**
 * @file
 *
 * @brief Upload.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Upload\Action\Upload
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Upload\Action;

use Nodelimit\Stdlib\Command\Managed\AbstractManaged;
use Nodelimit\Upload\File\Accept as File;

/**
 * The Upload class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Upload\Action\Upload
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Upload extends AbstractManaged
{
    private static $_keys = [
        'client',
        'temp',
        'path',
        'name'
    ];

    protected function init()
    {
        // parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    public function run()
    {
        $this->_accept();
    }

    private function _accept()
    {
        $file = $this->_createFile();
        $file->move(
            $this->getServiceItem('path'),
            $this->getServiceItem('name')
        );
    }

    private function _createFile()
    {
        return File::createFromTemp(
            $this->getServiceItem('temp')
        );
    }
}
