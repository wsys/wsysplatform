<?php
/**
 * DbGrid.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractGrid;
use Nodelimit\Db\Query\Query;

/**
 * DbGrid.php
 *
 * @class WSPLRTL\Widget\DbGrid
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class DbGrid extends AbstractGrid
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\DbGrid';

    public function render()
    {
        // $this->_testAPI();
        // $this->_testCols();
        // $this->_testRS();
        return parent::render();
    }

    public function openQuery()
    {
        return $this->_callModel('openQuery', func_get_args());
    }

    public function query()
    {
        return $this->_callModel('query', func_get_args());
    }

    private function _callModel($meth, array $args = [])
    {
        return call_user_func_array(
            [$this->getModel(), $meth],
            $args
        );
    }

    private function _testRS()
    {
        //
    }

    private function _testCols()
    {
        // self::logTraceDump(
        //     $this->getModel()->getHead()->getCols(),
        //     $this->getModel()->getLayout()->getCols(),
        //     $this->cols()->count()
        // );
        self::logTraceDump(
            $this->cols()->count()
        );
        if ($this->cols()->count()) {
            self::console($this->cols(0));
            $c = $this->cols(2);
            // $c->getProps()->offsetSet('title', 'Привет чудесный мир колонок!');
            // $c->getProps()->offsetSet('align', 'left');
            // $c->getProps()->offsetGet('width')->setValue(88);
            $c->setTitle('123');
            $c->setCaption('321');
            $c->setSize(300);
            $c->setAlign('right');
        } else {
            self::console('empty cols');
        }
        $this->setBodyScrollVertical(false);
        $this->setHeadVisible(false);
        $this->setHeadCellBorderVertical(true);
        $this->setHeadCellBorderHorizontal(true);
        $this->setBodyCellBorderVertical(true);
        $this->setBodyCellBorderHorizontal(true);
    }

    private function _testCursorBase()
    {
        $c = $this->getForm()->cursor('OBJ_TREE');
        $c->param('MIN_LEVEL')->setValue(2);
        $c->open();
        self::console($c->count());
        $c->first();
        while ($c->notEof()) {
            self::logger(
                'ID  : ',
                $c->fieldByName('ID')->getValue(),
                'NAME: ',
                $c->fieldByName('NAME')->getValue(),
                'LEV : ',
                $c->fieldByName('LEV')->getValue()
            );
            $c->next();
        }
    }

    private function _testAPI()
    {
        // $this->_testSQL();
        // $this->_testQuery();
        $this->_testCursor();
    }

    private function _testQuery()
    {
        $q = Query::create();
        $sql = 'SELECT * FROM ::FROM WHERE rownum < :rnum';
        $q->sql($sql)->p(':rnum', 7)->p('::FROM', 'CAT');
        self::logTraceDump((string) $q);
        // $this->openQuery($q)->perform()->refresh();
        $r = $this->openQuery($q)->refresh();
        self::logTraceDump($r);
        // $q = $this->openQuery($q);
        // $rs = $q->execute()->fetchAll();
        // $rs->refresh();
    }

    private function _testCursor()
    {
        // $this->_testCursorBase();
        $c = $this->getForm()->cursor('OBJ_TREE');
        // $c->first();
        // $this->openQuery($c)->refresh();
        self::console('cursor...');
        $q = $this->openQuery($c);
        self::console('cursor opened query');
        $q->p('MIN_LEVEL', 2);
        self::console('cursor p filter');
        $q->refresh();
        self::console('cursor refreshed');
        // $rs = $q->execute()->fetchAll();
        // self::logTraceDump($rs); /**< Nodelimit\WF\Component\Grid\Forge\Rowset */
        // $rs->refresh();
        $dump = ['whoami' => [__METHOD__, __LINE__]];
        // $dump['rs.it.count'] = $rs->getIterator()->count();
        //$dump['rs.it.array'] = iterator_to_array($rs->getIterator());
        self::logger($dump);
    }

    private function _testSQL()
    {
        // $sql = 'SELECT * FROM W.V_#MD#CLASSES WHERE rownum <= :rnum';
        $sql = 'SELECT * FROM ::FROM WHERE rownum <= :rnum';
        // $sql = 'SELECT * FROM CAT';
        // $sql = 'SELECT * FROM W.V_W#AGES';
        $q = $this->openQuery($sql)->p(':rnum', 5)->p('::FROM', 'W.V_#MD#CLASSES');
        self::console($sql);
        $q->refresh();
        // $rs = $q->execute()->fetchAll();
        // self::logTraceDump($rs); /**< Nodelimit\WF\Component\Grid\Forge\Rowset */
        // $rs->refresh();
        // $dump = ['whoami' => [__METHOD__, __LINE__]];
        // // $dump['rs.it.count'] = $rs->getIterator()->count();
        // //$dump['rs.it.array'] = iterator_to_array($rs->getIterator());
        // self::logger($dump);
    }
}
