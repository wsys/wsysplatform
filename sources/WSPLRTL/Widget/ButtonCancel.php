<?php
/**
 * ButtonOK.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\Button;

/**
 * ButtonOK.php
 *
 * description
 *
 * @class WSPLRTL\Widget\ButtonCancel
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class ButtonCancel extends Button
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\ButtonCancel';

    protected function initialize()
    {
        parent::initialize();
    }
}
