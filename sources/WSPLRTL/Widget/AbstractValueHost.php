<?php
/**
 * AbstractValueHost.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractEdit;
use WSPLRTL\Access\ValueHost as ValueHostAccess;
use WSPLRTL\Widget\Addition\Feature\ValueHost as ValueHostFeature;

/**
 * AbstractValueHost.php
 *
 * description
 *
 * @class WSPLRTL\Widget\AbstractValueHost
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class AbstractValueHost extends AbstractEdit implements ValueHostAccess
{
    use ValueHostFeature;

    protected function initialize()
    {
        parent::initialize();
    }
}
