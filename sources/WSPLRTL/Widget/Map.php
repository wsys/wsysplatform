<?php
/**
 * @file
 *
 * @brief Map.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Map
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractGeneric;
use com\danscode\wf as WF;
use WSPLRTL\Widget\ParamsResuming;
use WSPLRTL\Widget\AbstractWidget;
use Nodelimit\Code\Code;
use com\danscode\wf\selector\Utils as SelectorUtils;
use WSPLRTL\Internals\UtilsTrait;

/**
 * The Map class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Map
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Map extends AbstractGeneric
{
    use UtilsTrait;

    const ELEMENT_CLASS = 'WSPLRTL\Element\Map\Yandex';

    public static function create($props = [], $attrs = [])
    {
        // $f = $props['form'];
        $o = parent::create($props, $attrs);
        // $f->addEventListener(self::EVMAP_CLICK, $o, '_debugEvent');
        return $o;
    }

    private function _prepareLegacy($name)
    {
        $elem = $this->getElement();
        $base = $elem->makeBase('api.js.params');
        $body = Code::vsprintf(
            '$dc.ymaps.%s(%s);',
            [
                $name,
                json_encode($base)
            ]
        );
        WF::wire($this->_whenLoad($base['sel'], $body));
    }

    /**
     * Render prepared code to bindding event when load
     *
     * @param string $event
     *
     * @return void
     */
    private function _prepare($event)
    {
        $elem = $this->getElement();
        $base = $elem->makeBase('api.js.params');
        $base['event'] = $event;
        $body = Code::vsprintf(
            '$dc.ymaps.addHandler(%s);',
            [
                json_encode($base)
            ]
        );
        WF::wire($this->_whenLoad($base['sel'], $body));
    }

    private function _whenLoad($sel, $body)
    {
        return Code::vsprintf(
            '$(\'%s\').on(\'%s\', function (e) { %s });',
            [
                $sel,
                self::EVMAP_INIT,
                $body
            ]
        );
    }

    public function prepareEventListener(
        $type,
        AbstractWidget $widget,
        $meth_name = null,
        $data = null
    ) {
        $t = self::_undots($type);
        switch ($t) {
            case self::EVMAP_CLICK:
                $this->_prepareLegacy('addClickHandler');
                return false;
            case self::EVMAP_OBJ_CLICK:
                $this->_prepareLegacy('addObjClickHandler');
                return false;
            case self::EVMAP_SEARCH_RESULTS_CLICK:
                $this->_prepare($t);
                return false;
            default:
                return false;
        }
    }

    public function makeElement()
    {
        $element_class = static::ELEMENT_CLASS;
        $p = $this->getCreatingParams();
        return new $element_class($p->props, $p->attrs, $p->context);
    }

    /**
     * Extract value key
     *
     * @warning supports only simple targets without ids and ns
     *
     * @return string as value key
     */
    public function getValueKey()
    {
        $elem = $this->getElement();
        $c = $elem->getEventInfoTarget()->toGuide();
        return SelectorUtils::splitSys($c->toClassName());
    }

    public function addPlacemark($geometry, $props = [], $opts = [], $index = null)
    {
        $this->getElement()->addPlacemark($geometry, $props, $opts, $index);
    }

    public function addMapObject(array $p)
    {
        $this->getElement()->addMapObject($p);
    }

    public function setBounds(array $bounds, array $options = [])
    {
        return $this->mapAPI('setBounds', func_get_args());
    }

    public function setCenter(array $center, $zoom = null, array $options = [])
    {
        return $this->mapAPI('setCenter', func_get_args());
    }

    public function removeMapObjectAll()
    {
        return $this->mapAPI('removeAll', [], 'om');
    }

    public function removeMapObject(array $objects)
    {
        return $this->mapAPI('remove', func_get_args(), 'om');
    }

    public function removeGeoObjectsAll()
    {
        return $this->mapAPI('removeAll', [], 'map.geoObjects');
    }

    public function fitToViewport($preservePixelPosition = null)
    {
        // $dc.ymaps.api({id: 'a5d7e5c061c5c6', api:2, domain:'map.container', method: 'fitToViewport', args:[]});
        return $this->mapAPI('fitToViewport', func_get_args(), 'map.container');
    }

    public function spliceGeoObjects($index, $number = 1)
    {
        return $this->mapAPI('splice', func_get_args(), 'map.geoObjects');
    }

    public function setMapObjectManagerOption()
    {
        $this->callElement('setMapObjectManagerOption', func_get_args());
    }

    public function setMapOption()
    {
        $this->callElement('setMapOption', func_get_args());
    }

    public function mapAPI($method, array $args = [], $domain = 'map')
    {
        return call_user_func_array([$this->getElement(), 'mapAPI'], func_get_args());
    }
}
