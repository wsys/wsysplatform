<?php
/**
 * @file
 *
 * @brief Img.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Img
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractGeneric;

/**
 * The Img class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Img
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Img extends AbstractGeneric
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Img';

    public function setSrc($src)
    {
        $this->src()->setPath($src);
    }

    public function setFileName($name)
    {
        $this->src()->setFileName($name);
    }

    public function src()
    {
        return $this->getElement()->src();
    }

    public function refresh()
    {
        $this->src()->save();
        $this->getElement()->reload();
    }
}
