<?php
/**
 * Checkbox.php
 *
 * @page WSPLRTL\Widget\Checkbox
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Checkbox
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget package
 */
namespace WSPLRTL\Widget;

/**
 * Checkbox.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Checkbox
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget package
 */
class Checkbox extends AbstractRadio
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Checkbox';
}
