<?php
/**
 * AbstractGrid.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractWidget;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Nodelimit\UI\Grid\Column\Set\API\APITrait as ColumnSetAPITrait;
use Namedio\UI\Grid\Customize\Customize;
use Namedio\UI\Grid\Event\EventTrait;
use Namedio\UI\Grid\Event\GridEventInterface;

/**
 * AbstractGrid.php
 *
 * @class WSPLRTL\Widget\AbstractGrid
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractGrid extends AbstractWidget implements GridEventInterface
{
    use LibClientServiceLocal;
    use ColumnSetAPITrait;
    use Customize;
    use EventTrait;

    /**
     * Notify observer
     *
     * @return mixed
     */
    public function notify($info = null)
    {
        return $this->getElement()->notify($info);
    }

    public function debugDraft()
    {
        self::console(
            __METHOD__,
            $this->getProp('control_id')
        );
        // self::logTraceDump($this, $this->getGridElement());
        // $this->getGridElement()->debugDraft();
    }

    public function getGridElement()
    {
        return $this->_getLocalServiceItem('grid.element');
    }

    public function selectRow($id = null)
    {
        return $this->callElement('selectRow', [$id]);
    }

    public function selectRowByIndex($index = null)
    {
        return $this->callElement('selectRowByIndex', [$index]);
    }

    protected function getModel()
    {
        return $this->_getLocalServiceItem('model');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'model' => function () {
                $m = $this->getElement()->getModel();
                $m->attach($this);
                return $m;
            },
            'grid.element' => function () {
                return $this->getElement()->getGridElement();
            }
        ];
    }
}
