<?php
/**
 * @file
 *
 * @brief Upload.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Upload
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\Edit;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use WSPLRTL\Widget\Upload\Action\Upload as Accept;
use com\danscode\wf\ExceptionRequestPathNotFound;

/**
 * The Upload class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Upload
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Upload extends Edit
{
    use LibClientServiceLocal;

    const ELEMENT_CLASS = 'WSPLRTL\Element\Upload\Upload';

    public function accept($temp, $path, $name = false)
    {
        // throw new \Exception('5xx test');
        // throw new ExceptionRequestPathNotFound();
        Accept::bootstrap(
            [
                'client' => $this,
                'temp' => $temp,
                'path' => $path,
                'name' => $name
            ]
        );
    }

    public function getFiles()
    {
        return $this->_getLocalServiceItem('files');
    }

    private function _getLocalServiceBindings()
    {
        return
            [
                'files' => function () {
                    return new \ArrayObject();
                }
            ];
    }
}
