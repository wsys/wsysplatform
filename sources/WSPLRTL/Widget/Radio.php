<?php
/**
 * Radio.php
 *
 * @page WSPLRTL\Widget\Radio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Radio
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget package
 */
namespace WSPLRTL\Widget;

/**
 * Radio.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Radio
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget package
 */
class Radio extends AbstractRadio
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Radio';
}
