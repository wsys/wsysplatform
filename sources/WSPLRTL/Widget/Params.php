<?php
/**
 * Params.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use com\danscode\systype\Arguments;
use Namedio\Core\Edge\Lang\Arrays\Arrays;

/**
 * Params.php
 *
 * @class WSPLRTL\Widget\Params
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Params
{
    public $props;
    public $attrs;
    public $context;

    private $spool;

    protected function __construct()
    {
        //
    }

    public static function create()
    {
        $args = new Arguments(func_get_args(), 0, 2);
        $props = null;
        $attrs = null;
        switch ($args->check()) {
            case 0:
                $props = [];
                $attrs = [];
                break;
            case 1:
                $props = $args[0];
                $attrs = [];
                break;
            case 2:
                $props = $args[0];
                $attrs = $args[1];
                break;
            default:
                throw new \LogicException();
        }
        $o = new static();
        $o->props = $props;
        $o->attrs = $attrs;
        return $o;
    }

    /**
     * Flush all initial parameters
     *
     * @return void
     */
    public function flush()
    {
        $spool = $this->getSpool();
        $this->props = Arrays::vmerge($this->props, $spool['props']);
        $this->attrs = Arrays::vmerge($this->attrs, $spool['attrs']);
    }

    public function attachProps(array $props)
    {
        $this->spoolTarget('props', $props);
    }

    public function attachAttrs(array $props)
    {
        $this->spoolTarget('attrs', $props);
    }

    private function spoolTarget($target, array $p)
    {
        $this->spool[$target][] = $p;
    }

    private function getSpool()
    {
        return isset($this->spool)
            ? $this->spool
            : ($this->spool = new \ArrayObject(['props' => [], 'attrs' => []]));
    }
}
