<?php
/**
 * ParamsResuming.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\Params;
use com\danscode\systype\Arguments;

/**
 * ParamsResuming.php
 *
 * @class WSPLRTL\Widget\ParamsResuming
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class ParamsResuming extends Params
{
    public $context;

    public static function create()
    {
        $args = new Arguments(func_get_args(), 1);
        $context = null;
        switch ($args->check()) {
            case 1:
                $context = $args[0];
                break;
            default:
                throw new \LogicException();
        }
        $o = new static();
        $o->context = $context;
        $o->props = [];
        $o->attrs = [];
        return $o;
    }

    public static function createFromForm($form, $context)
    {
        $o = static::create($context);
        $o->props['form'] = $form;
        return $o;
    }
}
