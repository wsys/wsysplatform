<?php
/**
 * Table.php
 *
 * @package   package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractWidget;

/**
 * Table.php
 *
 * description
 *
 * @class WSPLRTL\Widget\Table
 * @package package
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 */
class Table extends AbstractWidget
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Table';

    protected function initialize()
    {
        parent::initialize();
    }
}
