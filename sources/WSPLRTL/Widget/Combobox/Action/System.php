<?php
/**
 * System.php
 *
 * @page WSPLRTL\Widget\Combobox\Action\System
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Combobox\Action\System
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget\Combobox\Action package
 */
namespace WSPLRTL\Widget\Combobox\Action;

use WSPLRTL\Widget\Action\Event\AbstractEvent as AbstractActionEvent;
use WSPLRTL\Event\AbstractEvent;

use WSPLRTL\Widget\Combobox;

use com\danscode\wf as WF;

use Nodelimit\Exception\Raise;
use Namedio\Core\Edge\Lang\Strings\Strings;


/**
 * System.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Combobox\Action\System
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\Combobox\Action package
 */
class System extends AbstractActionEvent
{
    const TERM = 'term';

    public static function createFromDispatch(Combobox $combobox, AbstractEvent $ev)
    {
        return self::createWithArrayContext($combobox, ['event' => $ev]);
    }

    /**
     * Run system action
     *
     * @warning when running EventSocket event response can by only in
     * json format for data-related events.
     *
     * @return void
     */
    public function run()
    {
        $w = $this->_getWidget();
        $type = $this->_which();
        $key = $w->getValueKey();
        $m = $this->_getModel();
        // $dump = [
        //     'key' => $key,
        //     'count' => $w->getCount(),
        //     'which' => $type,
        //     'first' => $m->getFirstText(),
        //     'post' => $_POST
        // ];
        // self::console(
        //     __METHOD__,
        //     $dump
        // );
        // self::logger(__METHOD__, __LINE__, $type, $key);
        // $this->_updateIndex();
        // $this->_debugRun($type);
        switch ($type) {
        case 'ls':
            $this->_ls();
            break;
        case 'find':
            $this->_find();
            break;
        case 'select':
            $this->_select();
            break;
        default:
            Raise::logic($type);
        }
        // $this->_getWidget()->refresh();
    }

    private function _updateIndex()
    {
        // ActionState::create($this->_getWidget(), $query)->updateIndex();
        $this->_getWidget()->iupdateIndex();
    }

    private function _debugRun()
    {
        $w = $this->_getWidget();
        $before = $w->getCount();
        $w->addItem('value added at microtime: ' . microtime());
        // $w->removeItem(1);
        $info = [
            'which' => $this->_which(),
            'count.before' => $before,
            'count.widget' => $w->getCount(),
            'count.model' => $w->getCount(),
            'getItem' => $w->getItem(0),
            'post.data' => $_POST,
            'getSelectedIndex' => $w->getSelectedIndex()
        ];
        self::logTrace($info);
    }

    private function _getWidget()
    {
        return $this->getSubject();
    }

    private function _which()
    {
        // self::console(__METHOD__, $_POST);
        if (isset($_POST['eventContextValue'])) {
            return 'select';
        } elseif (isset($_POST[self::TERM])) {
            $t = $_POST[self::TERM];
            return (Strings::isBlank($t)) ? 'ls' : 'find';
        } else {
            return 'error';
        }
    }

    private function _blank()
    {
        $row = ['id' => '', 'text' => utf8_encode('Start Typing....')];
        $ret = [];
        $ret = $row;
        $this->_out($ret);
    }

    private function _select()
    {
        /// @todo validate integer input
        /// @todo wrap to sandbox
        $index = (int) $_POST['eventContextValue'];
        $ls = call_user_func_array(
            [$this->_getModel(), 'ls'],
            []
        );
        // self::console(
        //     call_user_func_array(
        //         [$this->_getModel(), 'query'],
        //         [$_POST['eventContextValue']]
        //     ),
        //     call_user_func_array(
        //         [$this->_getModel(), 'ls'],
        //         []
        //     )
        // );
        $s = ['id' => '', 'text' => ''];
        if ($ls && isset($ls[$index])) {
            $found = $ls[$index];
            $s['id'] = $found['id'];
            $s['text'] = $found['text'];
        }
        $row = [
            'id' => $s['id'],
            'text' => $s['text']
        ];
        $ret = [];
        $ret = $row;
        $this->_out($ret);
    }

    private function _find()
    {
        $this->_perform('query', [$_POST[self::TERM]]);
    }

    private function _ls()
    {
        $this->_perform('ls');
    }

    private function _perform($meth, array $args = [])
    {
        $ret = [];
        $ret['results'] = call_user_func_array([$this->_getModel(), $meth], $args);
        // self::console(
        //     __METHOD__,
        //     $meth,
        //     $args,
        //     $ret
        // );
        $this->_out($ret);
    }

    private function _out(array $a)
    {
        // $a1 = [];
        // foreach ($a['results'] as $elem) {
        //     $a1[] = ['id' => (string) $elem['id'], 'text' => $elem['text']];
        // }
        // $out = json_encode(['results' => $a1]);
        // self::console(__METHOD__, $a);
        $out = json_encode($a);
        header('Content-Type: text/plain');
        WF::wire($out);
    }

    private function _getData()
    {
        $m = $this->_getModel();
        // return self::$_data;
        return $m->getData()->getArrayCopy();
    }

    private function _getModel()
    {
        return $this->_getWidget()->getModel();
    }
}
