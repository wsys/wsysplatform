<?php
/**
 * State.php
 *
 * @page WSPLRTL\Widget\Combobox\Action\State
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Combobox\Action\State
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Widget\Combobox\Action package
 */
namespace WSPLRTL\Widget\Combobox\Action;

use WSPLRTL\Widget\Action\AbstractAction as AbstractWidgetAction;

use Nodelimit\Exception\Raise;

/**
 * State.php
 *
 * Description
 *
 * @class WSPLRTL\Widget\Combobox\Action\State
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Widget\Combobox\Action package
 */
class State extends AbstractWidgetAction
{
    /**
     * The default run method of the action
     *
     * Performing default action as getSelected index
     *
     * @return mixed integer or null as selected index
     */
    public function run()
    {
        // self::console(
        //     [
        //         'key' => $this->getKey(),
        //         'isPostKey' => $this->isPostKey(),
        //         'post' => $_POST,
        //         // 'model.dump' => [
        //         //     $this->getSubject()->getModel()->getData(),
        //         //     $this->getSubject()->getModel()->ls()
        //         // ]
        //     ]
        // );
        if ($this->isReactive()) {
            return $this->_runReactive();
        } else {
            return $this->_runCreate();
        }
    }

    /**
     * The iupdateIndex method
     *
     * @todo move this method and related method to separated action
     *
     * @return void
     */
    public function iupdateIndex()
    {
        if ($this->isReactive()) {
            $this->_updateQuery(
                $this->getKey(),
                $this->_readQuery($this->_getValKey())
            );
        } else {
            Raise::logic('Incorrect method call');
        }
    }

    private function _updateQuery($k, $v)
    {
        $pv = $this->getPostVars();
        if (!isset($pv[$k])) {
            $_POST[$k] = $v;
        }
    }

    private function _getValKey()
    {
        return 'val';
    }

    private function _readQuery($k)
    {
        return $this->getPostVars()[$k];
    }

    /**
     * Perform run in reactive stage of component
     *
     * @warning assume it is running in POST-query
     *
     * @todo proposal adapt to non-POST allowed queries such as: GET,
     * websocket
     *
     * @todo proposal use query dict abstraction for access to variables
     * in various queries.
     *
     * @return void
     */
    private function _runReactive()
    {
        return $this->_retValue($this->_readPost());
    }

    private function _runCreate()
    {
        return $this->_retValue($this->_readInitial());
    }

    private function _retValue($value)
    {
        if (is_numeric($value)) {
            return intval($value);
        } elseif (empty($value)) {
            return null;
        } else {
            Raise::unexpectedValue($value);
        }
    }

    private function _readPost()
    {
        return $this->getPostVars()[$this->getKey()];
    }

    private function _readInitial()
    {
        return null;
    }
}
