<?php
/**
 * @file
 *
 * @brief Exception.php
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\Exception
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Action\Sys\Raise\Exception;

use \Exception as BaseException;

/**
 * The Exception class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\Exception
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 */
class Exception extends BaseException
{
}
