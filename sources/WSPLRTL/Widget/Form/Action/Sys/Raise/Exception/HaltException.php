<?php
/**
 * @file
 *
 * @brief HaltException.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\HaltException
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Action\Sys\Raise\Exception;

use \Exception as BaseException;

/**
 * The HaltException class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\HaltException
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class HaltException extends BaseException
{
}
