<?php
/**
 * @file
 *
 * @brief Raise.php
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Action\Sys\Raise\Raise
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Action\Sys\Raise;

use Nodelimit\Stdlib\Command\Managed\AbstractManaged;
use WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\Exception;

/**
 * The Raise class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Action\Sys\Raise\Raise
 *
 * @copyright Copyright (C) 2015 The Wsysplatform Development Team
 */
class Raise extends AbstractManaged
{
    private static $_keys = ['args'];

    public function run()
    {
        return $this->_run();
    }

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    private function _run()
    {
        $this->_validate();
        $this->_raise($this->_getMessage());
        return true;
    }

    private function _raise($msg)
    {
        throw new Exception($msg);
    }

    private function _getMessage()
    {
        return $this->_getArgs()[0];
    }

    private function _validate()
    {
        return $this->_getArgs()->check();
    }

    private function _getArgs()
    {
        return $this->getServiceItem('args');
    }
}
