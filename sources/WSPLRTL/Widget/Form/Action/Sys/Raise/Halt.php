<?php
/**
 * @file
 *
 * @brief Halt.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Action\Sys\Raise\Halt
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Action\Sys\Raise;

use Nodelimit\Stdlib\Command\Managed\AbstractManaged;
use WSPLRTL\Widget\Form\Action\Sys\Raise\Exception\HaltException;

/**
 * The Halt class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Action\Sys\Raise\Halt
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Halt extends AbstractManaged
{
    private static $_keys = [];

    public function run()
    {
        return $this->_run();
    }

    private function _run()
    {
        $this->_raise();
        return true;
    }

    private function _raise()
    {
        throw new HaltException();
    }
}
