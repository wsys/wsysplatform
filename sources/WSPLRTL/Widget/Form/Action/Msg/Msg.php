<?php
/**
 * @file
 *
 * @brief Msg.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Action\Msg\Msg
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Action\Msg;

use Nodelimit\Stdlib\Command\Managed\AbstractManaged;
use Nodelimit\WF\Element\Lib\Composer\Base as ComposerBase;
use Nodelimit\WF\Element\Lib\Composer\TWBS3;
use Nodelimit\WF\Element\Lib\Composer\Button as ComposerButton;

/**
 * The Msg class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Action\Msg\Msg
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Msg extends AbstractManaged
{
    use ComposerBase;
    use TWBS3;
    use ComposerButton;

    private static $_keys = ['msg', 'form'];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    protected function getElement()
    {
        return $this->getServiceItem('form');
    }

    public function run()
    {
        return $this->_run();
    }

    private function _run()
    {
        $this->_msg($this->_getMsg());
        return true;
    }

    private function _getMsg()
    {
        return $this->getServiceItem('msg');
    }

    private function _msg($msg)
    {
        return $this->panel(
            [
                $this->makeAlert(
                    sprintf(
                        '%s',
                        $msg
                    ),
                    [
                        'info', 'dismissible'
                    ]
                )
            ],
            ['class' => 'container-fluid']
        );
    }
}
