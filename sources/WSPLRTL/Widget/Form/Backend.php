<?php
/**
 * @file
 *
 * @brief Backend.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Backend
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form;

use Nodelimit\DI\Service\AbstractManaged;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Namedio\Core\Edge\DI\Service\Entity\Pool as Named;
use com\danscode\WF;
use WSPLRTL\Widget\Form\API\Filter\Custom\Custom as CustomFilter;
use WSPLRTL\Widget\Form\Env\Server as ServerEnv;
use WSPLRTL\Widget\Form\Env\Env;
use WSPLRTL\Widget\Form\API\File\Storage\Client as FileStorageClient;
use WSPLRTL\Widget\Form\Event\Custom as Handlers;
use WSPLRTL\Widget\Form\API\Cursor\Cursors;
use WSPLRTL\Sys\Sys;
use WSPLRTL\Ext\IM\IM;
use Namedio\Core\Util\Cached;

/**
 * The Backend class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Backend
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Backend extends AbstractManaged
{
    use LibClientServiceLocal;

    private static $_keys = [
        'pspec',
        'originator'
    ];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    protected function getOriginator()
    {
        return $this->getServiceItem('originator');
    }

    public function getSystemGID($id)
    {
        return $this->_getNamed()->get($id);
    }

    public function createNamedEntity($pool, $key)
    {
        return WF::tempID();
    }

    public function aux($name)
    {
        switch ($name) {
            case 'ev.cust.handlers':
                return $this->_getLocalServiceItem($name);
            default:
                throw new \UnexpectedValueException();
        }
    }

    /**
     * Gets Sys backend
     *
     * @return WSPLRTL\Sys\Sys
     */
    public function sys()
    {
        return $this->_getLocalServiceItem('sys');
    }

    /**
     * API
     *
     * @param string $name
     *
     * @return mixed
     * + WSPLRTL\Widget\Form\API\Filter\Custom\Custom
     */
    public function api($name)
    {
        // self::console(__METHOD__, $name);
        switch ($name) {
            case 'custom.filter':
                return $this->_getCustomFilter();
            case 'file.storage':
                return $this->_getLocalServiceItem($name);
            case 'cursors':
                return $this->_getLocalServiceItem($name);
            case 'im':
                return Cached::getInstance()->get(
                    'im',
                    function () {
                        return new IM();
                    }
                );

            default:
                throw new \UnexpectedValueException('unknown API');
        }
    }

    public function lib($name)
    {
        switch ($name) {
            case 'env.server':
                return $this->_getLocalServiceItem($name);
            case 'env':
                return $this->_getLocalServiceItem($name);
            default:
                throw new \UnexpectedValueException('unknown lib');
        }
    }

    public function getProcdConn()
    {
        return $this->_getForm()->getSysProp(
            'procd_conn'
        );
    }

    /**
     * Gets custom filter
     *
     * @return WSPLRTL\Widget\Form\API\Filter\Custom\Custom
     */
    private function _getCustomFilter()
    {
        // self::console(__METHOD__);
        return $this->_getLocalServiceItem('custom.filter');
    }

    private function _getNamed()
    {
        return $this->_getLocalServiceItem('named');
    }

    private function _getForm()
    {
        return $this->getOriginator();
    }

    private function getMntcatPath()
    {
        return $this->_getLocalServiceItem('wsys.mntcat.path');
    }

    /**
     * Gets local service bindings
     *
     * @return array with keys:
     *   + custom.filter CustomFilter
     */
    private function _getLocalServiceBindings()
    {
        return [
            'named' => function () {
                return new Named([$this,  'createNamedEntity']);
            },
            'env.server' => function () {
                return ServerEnv::create([]);
            },
            'env' => function () {
                return Env::create(
                    [
                        'wsys.mntcat.path' => function () {
                            return $this->getMntcatPath();
                        }
                    ]
                );
            },
            'custom.filter' => function () {
                // self::console(__METHOD__, 'custom.filter');
                return CustomFilter::create(['client' => $this->getOriginator()]);
            },
            'file.storage' => function () {
                return FileStorageClient::create(
                    [
                        /// wsys.document.root
                        /// wsys.docroot
                        /// wsys.doc.root
                        /// wsys.mnt
                        /// wsys.mntcat
                        /// wsys.mntcat.root
                        /// wsys.mntcat.mp
                        /// wsys.mntcat.path
                        /// wsys.mnt.cat
                        /// wsys.fs.cat
                        /// wsys.fs.mnt.cat
                        'document.root' => $this->getMntcatPath()
                    ]
                );
            },
            'wsys.mntcat.path' => function () {
                return realpath($_SERVER['DOCUMENT_ROOT'] . '/../mnt');
            },
            'cursors' => function () {
                return Cursors::create(
                    [
                        'method_id' => function () {
                            return $this->_getForm()->getSysProp(
                                'run_info.meth_rec.id'
                            );
                        },
                        'procd_conn' => [$this, 'getProcdConn']
                        // 'procd_conn' => $this->_getForm()->getSysProp('procd_conn')
                    ]
                );
            },
            'ev.cust.handlers' => function () {
                $c = $this->getOriginator();
                return Handlers::create(
                    [
                        'client' => $c,
                        'pspec' => $this->getServiceItem('pspec')
                    ]
                );
            },
            'sys' => function () {
                $sys = Sys::create(
                    [
                        'client' => $this->getOriginator(),
                        'md.node' => $this->_getForm()->getModel()->getMetaDataNode(),
                        'procd_conn' => [$this, 'getProcdConn']
                    ]
                );
                $sys->setContextProp(
                    'current.form.id',
                    $this->_getForm()->getSysProp('run_info')->form_id
                );
                return $sys;
            }
        ];
    }
}
