<?php
/**
 * @file
 *
 * @brief TestInternalsTrait.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\TestInternalsTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form;

/**
 * The TestInternalsTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\Form\TestInternalsTrait
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
trait TestInternalsTrait
{
    public function mainTestInternalsTrait()
    {
        ///
    }
}
