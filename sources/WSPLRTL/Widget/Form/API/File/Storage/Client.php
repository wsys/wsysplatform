<?php
/**
 * @file
 *
 * @brief Client.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\File\Storage\Client
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\File\Storage;

use Nodelimit\File\Storage\Client as ClientBase;

/**
 * The Client class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\API\File\Storage\Client
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Client extends ClientBase
{
}
