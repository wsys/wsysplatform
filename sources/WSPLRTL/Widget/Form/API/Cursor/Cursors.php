<?php
/**
 * @file
 *
 * @brief Cursors.php
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Cursor\Cursors
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Cursor;

use Nodelimit\DI\Service\AbstractManaged;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use WSPLL\DB\Cursor\Pool;


/**
 * The Cursors class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\API\Cursor\Cursors
 *
 * @copyright Copyright (C) 2017 The Wsysplatform Development Team
 */
class Cursors extends AbstractManaged
{
    use LibClientServiceLocal;

    private static $_keys = [
        'method_id', 'procd_conn'
    ];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    /**
     * Gets cursor
     *
     * Getting cursor by name
     *
     * @return object
     */
    public function cursor($name)
    {
        return $this->_getPool()->get($name);
    }

    private function _getPool()
    {
        return $this->_getLocalServiceItem('pool');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'pool' => function () {
                return Pool::create(
                    [
                        'method_id' => $this->getServiceItem('method_id'),
                        'procd_conn' => function () {
                            return $this->getServiceItem('procd_conn');
                        }
                    ]
                );
            }
        ];
    }
}
