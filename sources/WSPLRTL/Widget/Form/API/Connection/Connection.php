<?php
/**
 * @file
 *
 * @brief Connection.php
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Connection\Connection
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Connection;

use Nodelimit\DI\Service\AbstractManaged;
use WSAPL\Procd\Access\Peer as ProcdPeerAccess;
use WSAPL\Procd\Request\Lib\Client as LibProcdRequestClient;
use Nodelimit\App\Lib\Client\Inner as LibAppClientInner;

/**
 * The Connection class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\API\Connection\Connection
 *
 * @copyright Copyright (C) 2020  The Wsysplatform Development Team
 */
class Connection extends AbstractManaged implements ProcdPeerAccess
{
    use LibProcdRequestClient;
    use LibAppClientInner;

    private static $_keys = [
        'procd_conn'
    ];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
        $this->_initializeLibProcdRequestClient($this);
    }

    public function refresh()
    {
        $sql = 'SELECT 1 FROM DUAL';
        $payload = [
            'sql' => $sql,
            'bind_vars' => [],
            'context.props' => $this->makeQueryContextProps()
        ];
        $r = $this->_requestCmd('db.request', $payload);
        return $r;
    }

    public function getProcdConn()
    {
        return $this->getServiceItem('procd_conn');
    }

    private function makeQueryContextProps()
    {
        return $this->_createProcdContext();
    }
}
