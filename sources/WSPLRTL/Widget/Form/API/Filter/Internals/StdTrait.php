<?php
/**
 * @file
 *
 * @brief StdTrait.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Filter\Internals\StdTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Filter\Internals;

/**
 * The StdTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\Form\API\Filter\Internals\StdTrait
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
trait StdTrait
{
    abstract protected function addFilterByArguments(array $args);
    abstract protected function addWhereSQLByArguments(array $args);
    abstract protected function bindWhereSQLFormatByArguments(array $args);
    abstract protected function bindWhereSQLByArguments(array $args);

    public function addFilter()
    {
        $this->addFilterByArguments(func_get_args());
    }

    public function addWhereSQL()
    {
        $this->addWhereSQLByArguments(func_get_args());
    }

    public function bindWhereSQLFormat()
    {
        $this->bindWhereSQLFormatByArguments(func_get_args());
    }

    public function bindWhereSQL()
    {
        $this->bindWhereSQLByArguments(func_get_args());
    }
}
