<?php
/**
 * @file
 *
 * @brief AliasTrait.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Filter\Internals\AliasTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Filter\Internals;

use com\danscode\systype\Arguments;

/**
 * The AliasTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\Form\API\Filter\Internals\AliasTrait
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
trait AliasTrait
{

    abstract public function getMetaDataNode();

    abstract protected function hasCriteria();
    abstract protected function getCriteriaId();

    /**
     * Transform first argument from column alias to column id
     *
     * @param Arguments $args
     *
     * @return void
     */
    protected function mapColumnAliasArgument(Arguments $args)
    {
        $args[0] = $this->_mapColumnAlias($args[0]);
    }

    /**
     *
     *
     * @param mixed $crit
     *
     * @return object with structure: (object) ['id' => string, 'alias' => string]
     */
    private function _mapColumnAlias($alias)
    {
        $criteria_id = $this->_ensureCriteria();
        $mdn = $this->getMetaDataNode();
        $v = $mdn->getById('views');
        $c = $v->open($criteria_id);
        $r = $c->getByAlias($alias);
        return (object) [
            'criteria_id' => $criteria_id,
            'column_id' => $r['ID'],
            'alias' => $alias
        ];
    }

    private function _ensureCriteria()
    {
        if ($this->hasCriteria()) {
            return $this->getCriteriaId();
        } else {
            throw new \UnexpectedValueException('criteria is not defined');
        }
    }
}
