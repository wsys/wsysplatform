<?php
/**
 * @file
 *
 * @brief InternalsTrait.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Filter\Internals\InternalsTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Filter\Internals;

use com\danscode\systype\Arguments;

/**
 * The InternalsTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\Form\API\Filter\Internals\InternalsTrait
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
trait InternalsTrait
{
    abstract protected function getFilterHost();
    abstract protected function mapColumnAliasArgument(Arguments $args);

    protected function addFilterByArguments(array $args)
    {
        $a = new Arguments($args, 2, 5);
        $a->check();
        $this->mapColumnAliasArgument($a);
        $this->getFilterHost()->add($a);
    }

    protected function addWhereSQLByArguments(array $args)
    {
        $a = new Arguments($args, 1, 1);
        $a->check();
        $this->getFilterHost()->addWhereSQL($a);
    }

    protected function bindWhereSQLFormatByArguments(array $args)
    {
        $a = new Arguments($args, 1, -1);
        $a->check();
        $this->getFilterHost()->bindWhereSQLFormat($a);
    }

    protected function bindWhereSQLByArguments(array $args)
    {
        $a = new Arguments($args, 1, -1);
        $a->check();
        $this->getFilterHost()->bindWhereSQL($a);
    }
}
