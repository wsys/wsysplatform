<?php
/**
 * @file
 *
 * @brief CustomFilterStoreTrait.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Filter\Custom\CustomFilterStoreTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Filter\Custom;

use com\danscode\WF;

/**
 * The CustomFilterStoreTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\Form\API\Filter\Custom\CustomFilterStoreTrait
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
trait CustomFilterStoreTrait
{
    public function getCustomFilter()
    {
        return $this->getCustomFilterStore()->get('makeup');
    }

    abstract protected function getCustomFilterStoreSpec();

    private function getCustomFilterStore()
    {
        $spec = $this->getCustomFilterStoreSpec();
        return WF::storageSlot($spec);
    }
}
