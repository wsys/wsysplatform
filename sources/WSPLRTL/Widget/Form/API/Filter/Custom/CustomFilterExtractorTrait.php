<?php
/**
 * @file
 *
 * @brief CustomFilterExtractorTrait.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Filter\Custom\CustomFilterExtractorTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Filter\Custom;

use WSPLRTL\Widget\Form\API\Filter\Custom\CustomFilterStoreTrait;
use com\danscode\WF;

/**
 * The CustomFilterExtractorTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\Form\API\Filter\Custom\CustomFilterExtractorTrait
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
trait CustomFilterExtractorTrait
{
    use CustomFilterStoreTrait;

    abstract protected function getMasterNodeSharedSpec();

    protected function getCustomFilterStoreSpec()
    {
        return WF::storageSlot($this->getMasterNodeSharedSpec())->get('filter')['custom.filter.spec'];
    }
}
