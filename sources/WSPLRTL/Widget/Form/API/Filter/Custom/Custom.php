<?php
/**
 * @file
 *
 * @brief Custom.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\API\Filter\Custom\Custom
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\API\Filter\Custom;

use Nodelimit\DI\Service\AbstractManaged;
use com\danscode\systype\Arguments;
use WSPLRTL\Widget\Form\API\Filter\Internals\AliasTrait;
use WSAPL\Procd\Access\Peer as ProcdPeerAccess;
use WSPLL\MD\Access\Peer as MDPeerAccess;
use WSPLL\MD\Lib\Peer as LibMDPeer;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use WSPLRTL\Widget\Form\API\Filter\Internals\InternalsTrait;
use WSPLRTL\Widget\Form\API\Filter\Internals\StdTrait;
use WSPLRTL\Widget\Form\API\Filter\Custom\CustomFilterExtractorTrait;

/**
 * The Custom class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\API\Filter\Custom\Custom
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Custom extends AbstractManaged implements
    MDPeerAccess,                /**< @see WSPLL\MD\Lib\Peer */
    ProcdPeerAccess             /**< @see self::getProcdConn() */
{
    use AliasTrait;
    use InternalsTrait;
    use LibMDPeer;
    use LibClientServiceLocal;
    use StdTrait;
    use CustomFilterExtractorTrait;

    private static $_keys = [
        'client'
    ];

    /**
     * Get client
     *
     * @return WSPLRTL\Widget\AbstractForm an instance
     */
    private function _getClient()
    {
        return $this->getServiceItem('client');
    }

    /**
     * Get filter
     *
     * @see Nodelimit\WF\Systype\ElementSpec
     * @see WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade
     * @see WSPLRTL\Widget\AbstractForm
     *
     * @return WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade
     */
    private function _getFilter()
    {
        // return $this->_getClient()->getSpec()->getProp('custom.filter');
        return $this->getCustomFilter();
    }

    /**
     * Get form
     *
     * @return WSPLRTL\Widget\AbstractForm an instance
     */
    private function _getForm()
    {
        return $this->_getClient();
    }

    private function _getLocalServiceBindings()
    {
        return [
            'criteria_id' => function () {
                return $this->_getForm()->getSysProp('run_info.criteria_id');
            }
        ];
    }

    private function _getCriteriaId()
    {
        return $this->_getLocalServiceItem('criteria_id');
    }

    private function _hasCriteria()
    {
        return !($this->_getCriteriaId() === false);
    }

    public function reset()
    {
        $this->_getFilter()->reset();
    }

    public function getFilter()
    {
        return $this->getFilterHost();
    }

    public function refresh()
    {
         $this->_getForm()->refreshSystemGrid($this);
    }

    public function getProcdConn()
    {
        return $this->_getForm()->getSysProp('procd_conn');
    }

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
        $this->_initLibMDPeer();
    }

    /**
     * Get filter host
     *
     * @return WSPLRTL\Form\Model\Node\RS\Value\Ref\Filter\Facade
     */
    protected function getFilterHost()
    {
        return $this->_getFilter();
    }

    protected function hasCriteria()
    {
        return $this->_hasCriteria();
    }

    protected function getCriteriaId()
    {
        return $this->_getCriteriaId();
    }

    protected function getMasterNodeSharedSpec()
    {
        return $this->_getForm()->getMasterNodeSharedSpec();
    }
}
