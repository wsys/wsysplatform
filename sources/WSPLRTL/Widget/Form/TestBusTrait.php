<?php
/**
 * @file
 *
 * @brief TestBusTrait.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\TestBusTrait
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form;

use Namedio\Ext\RabbitBus\ChannelWriter;
use Namedio\Ext\RabbitBus\ChannelReader;
use Namedio\Ext\Sched\Sched;

/**
 * The TestBusTrait trait
 *
 * Description
 *
 * @trait WSPLRTL\Widget\Form\TestBusTrait
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
trait TestBusTrait
{

    public static function sched()
    {
        self::console(__METHOD__);
    }

    private function testBus()
    {
        // $this->testChannelWriter();
        // $this->testChannelReader();
        $this->testSched();
    }

    private function testSched()
    {
        Sched::job(
            '@once',
            // [__CLASS__, 'sched']
            [get_class($this), 'sched']
        );
    }

    private function testChannelWriter()
    {
        $channel = new ChannelWriter(
            [
                'queue.name' => 'worker',
                'exchange.name' => 'w'
            ]
        );
        $channel->send(
            [
                'data' => microtime()
            ]
        );
        $channel->sendCommand(
            'test.bus',
            [
                'data' => microtime()
            ]
        );
    }

    private function testChannelReader()
    {
        self::console(
            __METHOD__,
            'start'
        );
        $channel = new ChannelReader(
            /// @see PhpAmqpLib\Message\AMQPMessage
            function ($msg) {
                self::logTraceDump($msg);
                self::console(
                    __METHOD__,
                    $msg->body
                );
            }
        );
        $channel->dispatch();
        self::console(
            __METHOD__,
            'finish'
        );
    }
}
