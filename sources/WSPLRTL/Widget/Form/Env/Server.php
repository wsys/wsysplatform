<?php
/**
 * @file
 *
 * @brief Server.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Env\Server
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Env;

use Nodelimit\DI\Service\AbstractManaged;
use com\danscode\lib\Props;
use Namedio\Core\Edge\Lang\Value\Value;

/**
 * The Server class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Env\Server
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Server extends AbstractManaged
{
    public function fetch($name)
    {
        return $_SERVER[$name];
    }

    public function get($name)
    {
        return $this->_ensure($name);
    }

    public function getSafe($name, $default = null)
    {
        return Props::getSafe($_SERVER, $name, $default);
    }

    public function hasKey($name)
    {
        return isset($_SERVER[$name]);
    }

    private function _ensure($name)
    {
        if ($this->hasKey($name)) {
            return $this->fetch($name);
        } else {
            throw new \DomainException(
                Value::sprintf(
                    'undefined or unassigned key: %s',
                    $name
                )
            );
        }
    }
}
