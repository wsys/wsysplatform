<?php
/**
 * @file
 *
 * @brief Env.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Env\Env
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Env;

use Nodelimit\DI\Service\AbstractManaged;
use com\danscode\lib\Props;
use Namedio\Core\Edge\Lang\Value\Value;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use Nodelimit\DI\Service\Service;
use Nodelimit\App\Lib\Client\Inner as LibAppClientInner;
use Namedio\Ext\Arrayed\Arrayed;

/**
 * The Env class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Env\Env
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Env extends AbstractManaged
{
    use LibClientServiceLocal;
    use LibAppClientInner;

    public function fetch($name)
    {
        return $this->_getPool()->get($name);
    }

    public function get($name)
    {
        return $this->_ensure($name);
    }

    public function rget($name)
    {
        return $this->_ensure('#' . $name);
    }

    public function hasKey($name)
    {
        return $this->_getPool()->hasKey($name);
    }

    public function getSafe($name, $default = null)
    {
        return $this->_getPool()->getSafe($name, $default);
    }

    private function _ensure($name)
    {
        if ($this->hasKey($name)) {
            return $this->fetch($name);
        } else {
            throw new \DomainException(
                Value::sprintf(
                    'undefined key: %s',
                    $name
                )
            );
        }
    }

    private function _getPool()
    {
        return $this->_getLocalServiceItem('pool');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'pool' => function () {
                $defs = new \ArrayObject(
                    [
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'https' => (isset($_SERVER['HTTPS'])
                                    && !($_SERVER['HTTPS'] === 'off'))
                    ]
                );
                $this->_setup($defs);
                // $s = Service::create($defs);
                // self::console($s->getArrayCopy());
                // return $s;
                // self::console($defs);
                return Service::create($defs);
            }
        ];
    }

    private function _setup(\ArrayAccess $a)
    {
        self::_fill($a, $_SERVER, 'HTTP_REFERER', 'referer');
        self::_fill($a, $_SERVER, 'HTTP_USER_AGENT', 'user-agent');
        // self::console($a, $_COOKIE);
        $this->_setupSession($a);
        $a['wsys.mntcat.path'] = function () {
            return $this->getServiceItem('wsys.mntcat.path');
        };
        $this->setupDoc($a);
    }

    private function setupDoc(\ArrayAccess $a)
    {
        /// ~/work/site-wsplatform/htdocs/sys/doc/master/doc/translations/ru/handbook
        /// wsys.doc.root
        /// wsys.doc.handbook.root
        /// wsys.doc.handbook
        /// wsys.doc.handbook.wclient
        /// wsys.doc.handbook.wclient.root

        $root = $_SERVER['DOCUMENT_ROOT'] . '/sys/doc';
        $branch = 'master';
        $lang = 'ru';

        $doc = [];
        $doc['wsys.doc.root'] = "$root/$branch/doc";
        $home = $doc['wsys.doc.home'] = "$root/$branch/doc/translations/$lang";
        $handbook = $doc['wsys.doc.handbook.root'] = "$home/handbook";
        $doc['wsys.doc.handbook'] = "$handbook/README.md";
        $doc['wsys.doc.handbook.wclient'] = "$handbook/wclient/guide.md";
        $doc['wsys.doc.handbook.wclient.root'] = "$handbook/wclient";

        $rel = [
            // '#wsys.doc.root' => '/'
        ];
        $exclude = ['wsys.doc.root', 'wsys.doc.home'];
        $prefix = $home;
        $f = function ($k, $v) use ($prefix) {
            return substr($v, strlen($prefix) + 1);
        };

        foreach ($doc as $key => $val) {
            if (!in_array($key, $exclude)) {
                $rel['#' . $key] = $f($key, $val);
            }
        }

        Arrayed::append($a, $doc);
        Arrayed::append($a, $rel);

        $dump = [
            'doc' => $doc,
            'rel' => $rel,
            'a.keys' => array_keys($a->getArrayCopy()),
        ];
        self::console(
            $dump
        );
    }

    private function _setupSession(\ArrayAccess $a)
    {
        $s = $this->_getSession();
        $b = $s->getBackend();
        $a['sess.authorized'] = $this->_isAuthorized();
        $a['sess.uuid'] = $b->getCID();
        self::_fill($a, $_COOKIE, 'UUID', 'sess.uuid');
        $state = 'init';
        if (isset($_COOKIE['UUID'])) { /**< @todo validate UUID */
            $state = 'accept';
        }
        $a['sess.uuid.state'] = $state;
    }

    private static function _fill(
        \ArrayAccess $dest, array $src, $inkey, $outkey
    ) {
        if (isset($src[$inkey])) {
            $dest[$outkey] = $src[$inkey];
        }
    }
}
