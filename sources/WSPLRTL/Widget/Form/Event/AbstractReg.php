<?php
/**
 * @file
 *
 * @brief AbstractReg.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Event\AbstractReg
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Event;

use Nodelimit\DI\Service\AbstractManaged;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;


/**
 * The AbstractReg class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Event\AbstractReg
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
abstract class AbstractReg extends AbstractManaged
{
    use LibClientServiceLocal;

    private static $_keys = [
        'pspec'
    ];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    private function _getLocalServiceBindings()
    {
        return [
            'foo' => function () {
                return false;
            }
        ];
    }
}
