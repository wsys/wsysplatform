<?php
/**
 * @file
 *
 * @brief Custom.php
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Form\Event\Custom
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget\Form\Event;

use WSPLRTL\Widget\Form\Event\AbstractReg;
use WSPLRTL\Widget\AbstractWidget;
use Nodelimit\DI\Service\Lib\Client\Local as LibClientServiceLocal;
use com\danscode\WF;

/**
 * The Custom class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Form\Event\Custom
 *
 * @copyright Copyright (C) 2016 The Wsysplatform Development Team
 */
class Custom extends AbstractReg
{
    use LibClientServiceLocal;

    private static $_keys = [
    ];

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
    }

    public function getHandlers(AbstractWidget $w, $type)
    {
        $entry = $this->_getEntry($w);
        if (isset($entry[$type])) {
            return $entry[$type];
        } else {
            return [];
        }
    }

    /**
     * Register custom event
     *
     * @param array $p with keys:
     * + string type
     * + AbstractWidget widget
     * + string meth_name
     * + mixed data
     *
     * @return void
     */
    public function register(array $p)
    {
        // self::logTraceDump(
        //     $this->_getLocalServiceItem('storage'),
        //     $this->_getLocalServiceItem('storage')->get('ev.cust.reg'),
        //     (string) $this->getServiceItem('pspec')
        // );
        // self::console(
        //     $this->_getLocalServiceItem('ev.cust.reg'),
        //     $p['widget']->getProp('control_id'),
        //     $p['meth_name'],
        //     $p['type']
        // );
        // $r = $this->_getLocalServiceItem('ev.cust.reg');
        $w = $p['widget'];
        $entry = $this->_getEntry($w);
        $type = $p['type'];
        if (!$entry->offsetExists($type)) {
            $entry[$type] = [];
        }
        $entry[$type][] = $this->_makeInfo($p);
        if (!$this->_has($w)) {
            $this->_appendEntry($w, $entry);
        }
        // self::console(
        //     $entry,
        //     $this->_getLocalServiceItem('ev.cust.reg'),
        //     $r
        // );
    }

    private function _has(AbstractWidget $w)
    {
        $r = $this->_getLocalServiceItem('ev.cust.reg');
        $k = $this->_pullWidgetKey($w);
        return $r->offsetExists($k);
    }

    private function _appendEntry(AbstractWidget $w, \ArrayObject $entry)
    {
        $r = $this->_getLocalServiceItem('ev.cust.reg');
        $k = $this->_pullWidgetKey($w);
        $r->offsetSet($k, $entry);
    }

    private function _makeInfo(array $p)
    {
        return [
            'name' => $p['meth_name']
        ];
    }

    private function _getEntry(AbstractWidget $w)
    {
        $k = $this->_pullWidgetKey($w);
        if ($this->_has($w)) {
            return $this->_getLocalServiceItem('ev.cust.reg')->offsetGet($k);
        } else {
            // entry is not exists or cannot be found
            // @todo proposal: validate that $k is valid
            // @todo proposal: validate control_id or id_wname
            // @todo proposal: check by wpool for checking id
            return $this->_createEntry($k, $w);
        }
    }

    private function _createEntry($k, AbstractWidget $w)
    {
        return new \ArrayObject();
    }

    private function _pullWidgetKey(AbstractWidget $w)
    {
        return $w->getProp('control_id');
    }

    private function _getLocalServiceBindings()
    {
        return [
            'storage' => function () {
                return $this->_openStorage();
            },
            'ev.cust.reg' => function () {
                return $this->_getStorageItem(
                    'ev.cust.reg',
                    function () {
                        return new \ArrayObject();
                    }
                );
            }
        ];
    }

    private function _openStorage()
    {
        return WF::storageSlot($this->getServiceItem('pspec'));
    }

    private function _getStorageItem($k, callable $create)
    {
        $s = $this->_getLocalServiceItem('storage');
        if ($s->has($k)) {
            return $s->get($k);
        } else {
            $v = call_user_func($create);
            $s->put($k, $v);
            return $v;
        }
        return $s;
    }

}
