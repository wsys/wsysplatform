<?php
/**
 * AbstractGeneric.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractWidget;

use com\danscode\lib\Lists;

/**
 * AbstractGeneric.php
 *
 * Base for AbstractButton, AbstractEdit, Label, Memo, Panel
 *
 * @class WSPLRTL\Widget\AbstractGeneric
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
abstract class AbstractGeneric extends AbstractWidget
{
    ///**
    // * @override parent::getStoreProps()
    // *
    // * @return
    // */
    //public function getStoreProps()
    //{
    //    return Lists::add(
    //        parent::getStoreProps(),
    //        [
    //            '__ref_mode'
    //        ]
    //    );
    //}
    //
    //final protected function setRefMode($mode = null)
    //{
    //    if ($this->isReferenced()) {
    //        $this->getSpec()->setProp('__ref_mode', (string) $mode);
    //    } else {
    //        throw new \BadMethodCallException();
    //    }
    //}
    //
    //final public function getRefMode()
    //{
    //    if ($this->isReferenced()) {
    //        $this->getSpec()->getProp('__ref_mode');
    //    } else {
    //        return null;
    //    }
    //}
}
