<?php
/**
 * @file
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Combobox
 * @see WSPLRTL\Widget\Access\API\User\Combobox
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractControl;

use WSPLRTL\Event\AbstractEvent;

use WSPLRTL\Widget\Combobox\Action\System as ActionSystem;
use WSPLRTL\Widget\Combobox\Action\State as ActionState;

use WSPLRTL\Widget\Access\API\User\Combobox as APIUserComboboxAccess;
use WSPLRTL\Widget\AbstractWidget;
use Nodelimit\Code\Code;
// use com\danscode\wf;
use com\danscode\WF;
use WSPLRTL\Element\Combobox\Access\Defs;
use com\danscode\lib\Lists;
use WSPLRTL\Widget\ParamsResuming;
use Nodelimit\App\Prove\Prove;

/**
 * The Combobox class
 *
 * @class WSPLRTL\Widget\Combobox
 *
 * @todo proposal: ArrayAccess
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 */
class Combobox extends AbstractControl implements APIUserComboboxAccess, Defs
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Combobox\Combobox';
    const EVHMAP = '__evhmap';

    private static $_store_props = [self::EVHMAP];

    protected function initialize()
    {
        parent::initialize();
        $this->getSpec()->setProp(self::EVHMAP, new \ArrayObject());
    }

    public function getEventHandlerName($evname)
    {
        list($ok, $name) = $this->_checkRegistry($evname);
        if ($ok) {
            return $name;
        } else {
            return false;
        }
    }

    public function getStoreProps()
    {
        return Lists::add(parent::getStoreProps(), self::$_store_props);
    }

    public function render()
    {
        // self::logger(__METHOD__, __LINE__, $this->getContext()->toSelector());
        $this->_attachCustomEventMediator();
        return parent::render();
    }

    public function prepareEventListener(
        $type,
        AbstractWidget $widget,
        $meth_name = null,
        $data = null
    ) {
        /// return false: default behaviour
        switch ($type) {
            case self::EVCUST_MEDIATOR:
                return false;
            case 'clearing':
            case 'removing':
            case 'removed':
            case 'blur':
                // return false;
            case 'focus':
                $this->_prepareFocus($type);
                return true;
            // case 'change':
            //     $this->bindChange(
            //         $type,
            //         $widget,
            //         $meth_name,
            //         $data
            //     );
            //     return null;
            default:
                return false;
        }
    }

    /**
     * Perform system dispatching
     *
     * Generally performing such system-wide operations as like RPC
     * calls for data portions or search in dataset
     *
     * @param WSPLRTL\Event\AbstractEvent $ev
     *
     * @note WSPLRTL-API-System
     *
     * @callgraph
     *
     * @callergraph
     *
     * @return void
     */
    public function dispatch(AbstractEvent $ev)
    {
        ActionSystem::createFromDispatch($this, $ev)->run();
    }

    /**
     * Returns selected index if available
     *
     * @note WSPLRTL-API
     * @note This method is shortcut to Combobox::getSelectedIndex()
     * method
     *
     * @return mixed int|NULL
     */
    public function getVal()
    {
        return $this->getSelectedIndex();
    }

    /**
     * Selects the item at index $value by Combobox::setSelectedIndex method
     *
     * @param int $value of selected index
     *
     * @note This method is similar as Combobox::setSelectedIndex method
     * @note WSPLRTL-API
     *
     * @see Combobox::setSelectedIndex()
     *
     * @throws \OutOfBoundsException
     * @return void
     */
    public function setVal($value)
    {
        $this->setSelectedIndex($value);
    }

    /**
     * Get the data Model object of a combobox
     *
     * @note WSPLRTL-API-User-Advanced
     *
     * @return object WSPLRTL\Element\Combobox\Model\Model
     */
    public function getModel()
    {
        return $this->getElement()->getModel();
    }

    public function getCount()
    {
        return $this->getModel()->getCount();
    }

    public function addItem($item, $key = false)
    {
        $this->invalidate();
        $this->getModel()->addItem($item, $key);
    }

    public function addItemWithTag($item, $tag = null)
    {
        $this->getModel()->addItemWithTag($item, $tag);
    }

    public function removeItemAt($index)
    {
        $this->getModel()->removeItemAt($index);
    }

    public function removeItem($item)
    {
        list($found, $index) = $this->_findItemIndex($item);
        if ($found) {
            $this->removeItemAt($index);
        }
        return $found;
    }

    public function getItem($index)
    {
        return $this->getModel()->getItemAt($index);
    }

    public function hasIndex()
    {
        $i = $this->getSelectedIndex();
        // return is_int($this->getSelectedIndex());
        $valid = is_int($i) && $this->hasItemAt($i);
        // $valid = is_int($i);
        if ($valid) {
            return true;
        } else {
            return false;
        }
    }

    public function getSelectedIndex()
    {
        return ActionState::bootstrap($this);
    }

    public function getText()
    {
        return ($this->hasIndex()) ? $this->getSelectedItem() : null;
    }

    public function readSelectedItem()
    {
        return $this->getItem($this->getSelectedIndex());
    }

    public function getSelectedItem()
    {
        // self::logger(__METHOD__, getmypid(), $this->getValueKey(), $this->hasIndex(), $this->getSelectedIndex());
        return $this->_getSelected([$this, 'getItem']);
    }

    public function getSelectedTag()
    {
        return $this->_getSelectedBy('tag');
    }

    public function getSelectedKey()
    {
        return $this->_getSelectedBy('key');
    }

    public function hasItemIndex($index)
    {
        return $this->getModel()->hasItemIndex($index);
    }

    public function hasItemAt($index)
    {
        return $this->hasItemIndex($index);
    }

    /**
     * Perform updating internal state
     *
     * Update internal system relevant variables (POST|GET) and may other
     * to integrity state
     *
     * @warning use only at system preprocess actions of combobox
     *
     * @warning processing with inconsistent state such us system
     * preprocess actions
     *
     * @todo proposal: lazy create ActionState
     *
     * @note WSPLRTL-API-Internal
     * @note WSPLRTL-API-Internal-Experimental
     *
     * @note high experimental
     *
     * @return void
     */
    public function iupdateIndex()
    {
        ActionState::create($this)->iupdateIndex();
    }

    public function clear()
    {
        $this->invalidate();
        $this->getModel()->clear();
    }

    public function refresh()
    {
        Prove::spool($this);
        $this->invalidate();
        $this->getElement()->refresh();
    }

    public function invalidate()
    {
        Prove::invalidate($this);
    }

    public function unselect($refresh = true)
    {
        // self::logger(__METHOD__, getmypid(), $this->getValueKey(), $refresh);
        $attend = true;
        if ($refresh) {
            /// spoolOnce seems don't clear dataset because calls own refresh
            /// maybe need to use context as second parameter or use single point to refresh
            /// refresh for clearing sets null and call change - this another way
            // Prove::spoolOnce($this);
            // $refresh = $attend = $this->spoolAttend($this, 'unselect');
            // $attend = $this->spoolAttend($this, 'unselect');
            Prove::spool($this, 'unselect');
        }
        $this->invalidate();
        if ($attend) {
            $this->getElement()->unselect($refresh);
        }
        // $this->getElement()->unselect($refresh);
        /// @todo reset internal state!
        // $this->iupdateIndex();
    }

    public function discard($refresh = false)
    {
        // self::logger(__METHOD__, $this->getValueKey(), $refresh);
        // $refresh = true;
        // Prove::spoolOnce($this);
        // if (func_num_args() == 0) {
        //     $refresh = true;
        // }

        /// js: calls val with null
        /// js: if refresh then call trigger change
        $this->unselect($refresh);

        /// just clear data in model
        $this->clear();
    }

    public function setSelectedItem($item)
    {
        $out = $this->_select([$this, '_findItemIndex'], [$item]);
        // $ps = $this->getElement()->getPersistentSpec();
        // $s = WF::storage($ps);
        // // $s->flush(true);
        // self::console(
        //     __METHOD__,
        //     $s->getFilename(),
        //     $this->getModel()->ls(),
        //     $item
        // );
        return $out;
    }

    public function setSelectedItemIndex($index)
    {
        // self::logger(__METHOD__, getmypid(), $this->getValueKey(), $index, $this->_isResuming(), $this->options()->getSafe('sync', 'auto'));
        if ($this->hasItemIndex($index)) {
            $this->getElement()->setSelectedIndex($index);
            if ($this->_isResuming()) {
                $sync = $this->options()->getSafe('sync', 'auto');
                if ($sync == 'auto') {
                    $this->refresh();
                }
            }
        } else {
            throw new \OutOfBoundsException();
        }
    }

    public function putSelectedItem($item)
    {
        $opts = $this->options();
        $sav = $opts->getSafe('sync', 'auto');
        $opts->set('sync', 'none');
        $this->setSelectedItem($item);
        $opts->set('sync', $sav);
    }

    public function setSelectedTag($tag)
    {
        return $this->_setSelectedBy('tag', $tag);
    }

    public function setSelectedKey($key)
    {
        return $this->_setSelectedBy('key', $key);
    }

    public function setSelectedItemAt($index)
    {
        $this->setSelectedItemIndex($index);
    }

    public function setSelectedIndex($index)
    {
        $this->setSelectedItemIndex($index);
    }

    public function isTriggered()
    {
        return $this->getElement()->isTriggered();
    }

    private function bindChange(
        $type,
        AbstractWidget $widget,
        $meth_name = null,
        $data = null
    ) {
        $keys = ['type', 'widget', 'methName', 'data'];
        $c = new \ArrayObject(
            array_combine($keys, func_get_args())
        );
        $this->on(
            'widget.render.after',
            function () use ($c) {
                ///
                $this->getForm()->attachEventListener(
                    $c['type'],
                    $c['widget'],
                    $c['methName'],
                    $c['data']
                );
            }
        );
    }

    private function _select(callable $cb, $args = [])
    {
        list($found, $index) = call_user_func_array($cb, $args);
        if ($found) {
            $this->setSelectedItemIndex($index);
        }
        return $found;
    }

    private function _findItemIndex($item)
    {
        return $this->getModel()->findItemIndex($item);
    }

    private function _getSelectedBy($name)
    {
        return $this->_getSelected([$this->getModel(), 'getItemField'], [$name]);
    }

    private function _setSelectedBy($name, $v)
    {
        return $this->_select([$this->getModel(), 'findBy'], [$name, $v]);
    }

    private function _getSelected(callable $cb, $args = [])
    {
        if ($this->hasIndex()) {
            // self::console(__METHOD__, $args);
            // self::console(
            //     $this->getControlID(),
            //     $this->getSelectedIndex(),
            //     Lists::prepend($args, $this->getSelectedIndex()),
            //     $args,
            //     $this->getModel()->ls()
            // );
            return call_user_func_array(
                $cb, Lists::prepend($args, $this->getSelectedIndex())
            );
        } else {
            return null;
        }
    }

    private function _attachCustomEventMediator()
    {
        $this->getForm()->addEventListener(self::EVCUST_MEDIATOR, $this);
    }

    private function _prepareFocus($event)
    {
        $fbody = <<<'EOT'
$(%s).val(JSON.stringify({cmd:%s}));
$(%s).trigger(%s);
EOT;

        $s = $this->getContext()->toSelector();
        // self::console(array_keys($this->getElement()->getSpec()->getAllProps()));

        $f = Code::vsprintf(
            'function (ev) {%s}',
            [
                Code::vsprintfq(
                    $fbody,
                    [
                        $this->getProp('evinfo_target')->toSelector(),
                        $event,
                        $s,
                        self::EVCUST_MEDIATOR,
                        $event  /**< @warning excessive parameter */
                    ]
                )
            ]
        );
        $t = '$(\'%s\').on(\'%s\', %s);';
        $code = Code::vsprintf(
            $t,
            [
                $s,
                'select2-' . $event,
                $f
            ]
        );
        WF::wire($code);
    }

    private function _registerHandler($evname, $method)
    {
        $r = $this->_getHandlerRegistry();
        $r->offsetSet($evname, $method);
    }

    private function _checkRegistry($evname)
    {
        $r = $this->_getHandlerRegistry();
        if ($r->offsetExists($evname)) {
            return [true, $r->offsetGet($evname)];
        } else {
            return [false, false];
        }
    }

    private function _getHandlerRegistry()
    {
        return $this->getSpec()->getProp(self::EVHMAP);
    }

    private function _isResuming()
    {
        return ($this->getCreatingParams() instanceof ParamsResuming);
    }
}
