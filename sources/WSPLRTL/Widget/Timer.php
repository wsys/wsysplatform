<?php
/**
 * @file
 *
 * @brief Timer.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\Widget\Timer
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL\Widget;

use WSPLRTL\Widget\AbstractGeneric;

/**
 * The Timer class
 *
 * Description
 *
 * @class WSPLRTL\Widget\Timer
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
class Timer extends AbstractGeneric
{
    const ELEMENT_CLASS = 'WSPLRTL\Element\Timer\Timer';

    public function setEnabled($enabled = false)
    {
        $this->getElement()->setEnabled($enabled);
    }

    public function setInterval()
    {
        return $this->callElement('setInterval', func_get_args());
    }
}
