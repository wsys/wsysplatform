<?php
/**
 * @page WSPLRTL\Db\Filter\Access\Operator
 *
 * Operator.php
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @see WSPLRTL\Db\Filter\Access\Operator
 *
 * @section LICENSE
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package WSPLRTL\Db\Filter\Access package
 *
 */
namespace WSPLRTL\Db\Filter\Access;

use Nodelimit\Db\Filter\Access\Operator\Defs as OperatorDefsAccess;

/**
 * Operator.php
 *
 * description
 *
 * @interface WSPLRTL\Db\Filter\Access\Operator
 *
 * @copyright Copyright (C) 2014 The Wsysplatform Development Team
 *
 * @package WSPLRTL\Db\Filter\Access package
 *
 */
interface Operator extends OperatorDefsAccess
{
}
