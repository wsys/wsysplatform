<?php
/**
 * @file
 *
 * @brief SpoolActions.php
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 *
 * @see WSPLRTL\SpoolActions
 *
 * @license
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace WSPLRTL;

use Nodelimit\DI\Service\AbstractManaged;

/**
 * The SpoolActions class
 *
 * Description
 *
 * @class WSPLRTL\SpoolActions
 *
 * @copyright Copyright (C) 2019  The Wsysplatform Development Team
 */
class SpoolActions extends AbstractManaged
{
    private static $_keys = ['owner'];

    /// @var \ArrayObject
    private $pool;

    protected function init()
    {
        parent::init();
        $this->ensureServiceKeys(self::$_keys);
        $this->pool = new \SplQueue();
    }

    /**
     * Spool
     *
     * @return void
     */
    public function spool($action, $args = [], $refresh = true)
    {
        $item = $this->makeItem(func_get_args());
        if ($refresh) {
            $this->forward($item);
        } else {
            $this->getPool()->append($item);
        }
    }

    public function refresh()
    {
        $q = $this->pool;
        $q->setIteratorMode(SplQueue::IT_MODE_DELETE);
        $q->rewind();
        while ($q->valid()) {
            $this->forward($q->current());
            $q->next();
        }
    }

    private function makeItem(array $args)
    {
        return new \ArrayObject(
            array_combine(['action', 'args', 'refresh'], $args)
        );
    }

    private function forward(\ArrayObject $item)
    {
        return call_user_func($this->mapAction($item), $item);
    }

    private function mapAction(\ArrayObject $item)
    {
        switch ($item['action']) {
            case 'wired.body':
                return [$this, 'actionSetBody'];
            default:
                throw new \DomainException('unknown action');
        }
    }

    private function actionSetBody(\ArrayObject $item)
    {
        return $this->wired()->setBody($item['args'][0]);
    }

    private function wired()
    {
        return $this->getServiceItem('owner')->getSpec()->wired();
    }
}
