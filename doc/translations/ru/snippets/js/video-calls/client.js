
// import {Peer} from "https://unpkg.com/peerjs@1.3.1/dist/peerjs.min.js";
// import {Peer} from "https://codepen.io/pwdr/pen/VXxMoy.js";

const Base = Wsys().importModule('Dial');
const render = Wsys().importModule('render');
const html = Wsys().importModule('html');

export class Client extends Base {

    /**
     * The constructor
     *
     * @param args[0] object
     *   + args.form string CSS-selector of form
     *   + args.who object
     *     + args.who.first string real.jid
     *
     * @return void
     */
    constructor(...args) {
        /// вызов родительского конструктора:
        /// инициализация, устaновка основных системных обработчиков, вызов пользовательского метода render
        super(...args);

        ///
        /// установка обработчиков событий: debug, peer:connected, im:ready
        ///
        /// допустимо назначать любые прикладные обработчики, в том числе кастомные события
        /// вызов события, например: this.emit('debug', 1, 2, 3)
        ///
        /// кастомное событие:
        /// this.on('myapp.event', (info) => console.debug(info));
        /// this.on('myapp.event', this.handleEventMy); /* метод должен быть в классе */
        ///
        /// вызов кастомного обработчика:
        /// this.emmit('myapp.event', {baz: 'bar', message: 'my event triggered!'});

        /// пример установки обработчика: вызывается метод debug
        this.on('debug', this.debug);

        /// установка обработчика события peer:connected
        /// срабатывает когда происходит коннект WebRTC в пакете peerjs
        this.on(
            'peer:connected',
            /// Для примера значение peerID на момент коннекта отображается в DOM-элементе с классом .peerid
            /// setContent(selector, value)  меняет содержимое дочернего элемента по селектору '.peerid' внутри формы
            /// value представляет собой строку в виде текста или HTML-кода
            ///
            /// для удобства и краткости используется стрелочная функция (однострочник)
            /// @see https://learn.javascript.ru/arrow-functions-basics
            /// вместо стрелочных функций можно задавать:
            ///   + любое валидное выражение
            ///   + указатель на метод (this.method)
            ///   + анонимную функцию
            ///   + переменную с типом функции
            ///
            /// допустимо использовать любой доступный селектор внутри формы
            (peerID) => this.setContent('.peerid', peerID)
        );

        /// событие происходит когда происходит коннект, доступен системный объект xmpp и доступен peerID
        this.on(
            'im:ready',
            /// по событию разрешить элементы управления: ввод адреса вызываемого абонента, кнопка вызова звонка
            () => this.enable()
        );

        /// при необходимости обработчики могут быть полностью или частично перекрыты через объявление метода класса
        /// пример перекрытия: метод callDial
    }

    /**
     * simple debug method
     *
     * @param args
     *
     * @return void
     */
    debug(...args) {
        console.debug('client dial', args);
    }

    /**
     * Method stub
     *
     * @return void
     */
    click() {
        alert(1);
    }

    /**
     * Пример перекрытия системного метода callDial
     *
     * При необходимости может быть дополнительная бизнес-логика
     *
     * @param args
     *
     * @return void
     */
    callDial(...args) {
        super.callDial(...args);
    }

    /**
     * Формирования DOM-элементов панели дозвона: метод отвечает за отрисовку панели дозвона
     *
     * Метод вызывается автоматически при инициации клиента.
     *
     * @return void
     */
    render() {
        console.debug('render start');

        /// набор различных параметров, используемых в блоке верстки
        let p = {
            /// ширина элемента видео
            width:"250",

            /// высота элемента видео
            height:"auto",

            /// показ селфи приглушенным
            muted:"true",

            /// свой jid
            ami: this.getAMI(),

            /// объект с информацией о недавно появлявшемся в онлайн первом абоненте
            who: this.getWho()
        };

        /// вызов системного метода, ответственного за непосредственный рендер кода и размещение в предусмотренный слот
        /// по уиолчанию, содержимой размещается в контейнере доступном по селектору '.dial.slot.down'
        /// селектор слота кастомизируем
        this.put(
            ///
            /// формирование html-кода клиента видеосвязи
            ///
            /// верстка использует bootstrap
            /// @see https://getbootstrap.com/docs/3.3/css/
            ///
            /// "вставка" параметров осуществляется через "шаблонизацию строк" конструкцией вида ${expression},
            ///  например для параметров p.width, p.height:
            ///   <video class="rspaced" id=remVideo width="${p.width}" height="${p.height}" ></video>
            ///
            /// или вставка обработчика:
            ///   onclick='${() => this.callAnswer()}'
            ///
            /// @see https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Template_literals
            ///
            html`
                <!-- секция кастомных CSS-правил -->
                <style type="text/css">
                 .rspaced {
                     margin-right: 1rem;
                 }
                 .bspaced {
                     margin-bottom: 3rem;
                 }
                 .tspaced {
                     margin-top: 3rem;
                 }
                </style>

                <!--
                  панель приема звонка: появляется в момент получения звонка

                  используемые системные обработчики:
                  + callAnswer()
                  + callCancel()
                  + callClose()
                  + callDial()
                -->
                <div class="dial answer bspaced" style="display: none" >
                    <div>Входящий звонок</div>

                    <!--
                       Кнопка приема звонка с назначенным обработчиком
                       При клике вызывается системный обработчик callAnswer()

                       Можно назначить любой обработчик для поддерживаемых событий
                       @see https://developer.mozilla.org/en-US/docs/Web/Events#event_index
                    -->
                    <button class='btn btn-success rspaced btn-xs' onclick='${() => this.callAnswer()}' >Принять</button>

                    <!--
                       Кнопка отклонения звонка
                       При клике вызывается обработчик callCancel()
                    -->
                    <button class="btn btn-danger btn-xs" onclick='${() => this.callCancel()}' >Отклонить</button>
                </div>

                <!--
                  Отображение видео изображения звонящего абонента
                -->
                <video class="rspaced" id=remVideo width="${p.width}" height="${p.height}" ></video>

                <!--
                  Отображение собственного видео изображения: селфи
                -->
	            <video id=myVideo muted="${p.muted}" width="${p.width}" height="${p.height}" ></video>

                <!-- информационная панель для отображения сведений об звонящем абоненте -->
	            <div id=callinfo ></div>

                <!-- динамическая панель во время сеaнса связи, позволяет завершить звонок -->
                <div class="dial active bspaced" style="display: none" >
                    <div>Звонок начат...</div>
                    <button class="btn btn-danger" onclick='${() => this.callClose()}' >Завершить звонок</button>
                </div>

                <!-- отладочная панель -->
                <div class="dev debug rspaced bspaced" >
                    <h3>Dev panel:</h3>
                    <div>My jid: ${p.ami}</div>
                    <div>
                        PeerID:
                        <span class="debug peerid">
                            ${this.getPeerID()}
                        </span>
                    </div>
                    <div>who on-line (first): ${p.who.first}</div>

                    <input
                          value=${p.who.first}
                          class="callee input-sm form-control disabled aware"
                          type="text"
                          placeholder="enter full jid: user@domain/resource"
                    />

                    <!--
                       конпка вызова звонка
                    -->
                    <button class="btn btn-primary disabled aware" onclick='${() => this.callDial()}' >Test call</button>
                </div>

            `
        );

        /// динамическое выставление ряда свойств для видео-контейнеров
        /// статичные значение могут не работать
        this.setAttr('#remVideo', 'height','auto');
        this.setAttr('#myVideo', 'height','auto');
        this.setProp('#myVideo', 'muted',true);

        console.debug('render finish');
    }
}
