<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Методы управления защитой от взаимных блокировок событий](#методы-управления-защитой-от-взаимных-блокировок-событий)
    - [API](#api)
    - [`isDeadlockProtect`](#isdeadlockprotect)
    - [`enableDeadlockProtect`](#enabledeadlockprotect)
    - [`disableDeadlockProtect`](#disabledeadlockprotect)
    - [`isTriggered`](#istriggered)
    - [Примечания](#примечания)

<!-- markdown-toc end -->

# Методы управления защитой от взаимных блокировок событий
+ Доступны в контексте методов формы `$this` (в большинстве случаев)

## API

## `isDeadlockProtect`
```php
boolean $this->isDeadlockProtect()
```
Возвращает `boolean` признак включена защита или нет. По умолчанию
защита включена. Включенная защита означает, что серверные события
инициируемые через "postback" (такие как `change`) будут
игнорироваться, если это может привести зацикливанию обработки. По сути
блокируются попытки автоматически вызвать текущий обработчик снова.

## `enableDeadlockProtect`
```php
void $this->enableDeadlockProtect()
```
Включает защиту.

## `disableDeadlockProtect`
```php
void $this->disableDeadlockProtect()
```
Выключает защиту.

## `isTriggered`
```php
boolean|NULL $this->q('combobox1')->isTriggered()
```
Возращает `boolean` признак, что событие контрола было вызвано
программным способом. Если значение `FALSE`, то событие было инициировано
пользователем. Метод доступен для соответствующих контролов (`combobox`).

Внимание:
+ валидное значение признака `isTriggered` доступно только в контексте работающего обработчика события


## Примечания
+ это экспериментальные методы
  + возможны изменения, дополнения и доработки
