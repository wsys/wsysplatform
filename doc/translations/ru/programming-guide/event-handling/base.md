# Проверка отправителя события
Когда происходит событие, вызывается прикладной обработчик контрола, с
которым непосредственно связано событие. Например при клике на кнопку в
обработчик будет передан экземпляр объекта имеющего родительский класс
`WSPLRTL\Event\AbstractEvent`

Обычно, данный экземпляр объекта передается в обработчик события как
первый аргумент с именем $ev. В языковых терминах справедливо утверждение:
`($ev instanceof AbstractEvent === true)`

Поле `$ev->sender` содержит ссылку на объект (обычно это контрол), с
которым связано событие.

В исходном коде прикладного модуля формы доступны для прикладных методов
следующие имена классов:

```php
use WSPLRTL\Widget\Label;
use WSPLRTL\Widget\Button;
use WSPLRTL\Widget\ButtonOK;
use WSPLRTL\Widget\ButtonCancel;
use WSPLRTL\Widget\Panel;
use WSPLRTL\Widget\Edit;
use WSPLRTL\Widget\Memo;
use WSPLRTL\Widget\DateEdit;
use WSPLRTL\Widget\Table;
use WSPLRTL\Widget\TableRow;
use WSPLRTL\Widget\TableCell;
use WSPLRTL\Widget\AbstractForm;
use WSPLRTL\Widget\DbGrid;
use WSPLRTL\Widget\Radio;
use WSPLRTL\Widget\Checkbox;
use WSPLRTL\Widget\Combobox;
use WSPLRTL\Widget\Password;
use WSPLRTL\Widget\Upload;
use WSPLRTL\Widget\Img;
use WSPLRTL\Widget\Map;
use WSPLRTL\Db\Filter\Access\Operator as OperatorAccess;
use WSPLRTL\Widget\Markup\MarkupHTML;
use WSPLRTL\Widget\Markup\MarkupMarkdown;
use WSPLRTL\Widget\Timer;
```

В дальнейшем, возможно использование данных имен, например для проверки
экземпляра класса.

```php
if ($ev->sender instanceof Button) {
    $this->raiseAlert('Button!');
} elseif ($ev->sender instanceof Label) {
    $this->raiseAlert('Label!');
} else {
    $this->raiseAlert('Another sender');
}
```

# Определение имени контрола
## Способ №1 - по методу `getWID()`
Каждый элемент управления `W-формы` имеет встроенный API-метод `getWID()`
Данный метод возвращает строку, содержащую идентификатор `W-контрола`,
связанный в метаданных формы с атрибутом `ID_WCONTROL`

Например для обработчика кнопки с `ID_WCONTROL` равным `id_button1`:
```php

/// @note $wid === 'id_button1';
$wid = $ev->sender->getWID();

```
