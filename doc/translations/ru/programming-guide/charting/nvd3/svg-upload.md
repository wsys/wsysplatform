# Загрузка svg файла на сервер
## Шаг 0: подготовка
Предполагается, что прикладная форма уже содержит все необходимые
зависимости для корректного построения графики `nvd3`, обработчики
элементов управления и собственно сформированный целевой
svg-элемент.

### Зависимости
#### Пакеты composer
  + https://github.com/alchemy-fr/PHP-dataURI
    + data-uri/data-uri версия: 0.2.5
  + https://github.com/thephpleague/flysystem
    + league/flysystem версия: 1.0.70
#### Объявления Use
```php
use DataURI\Parser as DataURIParser;
use DataURI\Data as DataURIData;
use DataURI\Dumper as DataURIDumper;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use Imagick;
use ImagickPixel;
```

## Шаг 1: вызов загрузки

Добавить в тело обработчика, например для кнопки `id_btn_save_project`
```php
$fn = <<<EJS
            (uri) => {
  Wsys({$this}).q('id_markupchart_detail').forward('svg.upload', {'uri': uri, 'format': 'png'});
}
EJS;
$this->q('id_markupchart_detail')->js('svgAsPngUri', ['fn' => $fn], 'wsys.chart.nvd3');
```
## Шаг 2: отправка данных на сервер через событие forward
Собственно, после отработки шага 1, в браузере будет выполнен код из
переменной `$fn`
## Шаг 3: обработка события forward на сервере
Пример рабочего кода, выполняющего прием `uri` в формате base64,
распаковку, сохранение, модификацию, чтение и отправку обратно в браузер:
```php
    private function forwardSVG($ev)
    {
        /// get payload from forward event
        $json = $ev->JSON();
        $uri = $json['uri'];

        /// parse uri and create data object
        $dataObject = DataURIParser::parse($uri);

        /// check mime type of payload: expected image/png
        $mime = $dataObject->getMimeType();
        if ($mime === 'image/png') {

            /// extract uri data: (decode base64 to binary image)
            $data = $dataObject->getData();

            /// build path base, to mntcat directory
            $mntcat = $this->lib('env')->get('wsys.mntcat.path');
            $mntpath = $mntcat . '/vz/proj/upload';

            /// build filename (without path)
            $prefix = 'svg';
            $ext = '.png';
            $name = $prefix . $ext;

            /// make FS layer to local filesystem at $mntpath
            $adapter = new Local($mntpath);
            $fs = new Filesystem($adapter);

            ///
            /// put file with contents at $relpath that relative to $mntcat
            ///

            /// make $filename and $fullname
            $relpath = '/';
            $filename = $relpath . $name;
            $fullname = $mntpath . $filename;

            ///
            /// optional: remove file if same exists
            ///
            if ($fs->has($filename)) {
                $fs->delete($filename);
            }

            /// write content to file
            $fs->write($filename, $data);


            ///
            /// optional: modify image, output to browser
            ///

            /// change image with imagick
            /// @see https://www.php.net/manual/en/book.imagick.php
            $imagick = new Imagick($fullname);
            // $imagick->flopImage();
            $imagick->autoLevelImage();
            $imagick->contrastImage(true);
            // $imagick->minifyImage();
            $imagick->resampleImage(80, 80, Imagick::FILTER_LANCZOS, 1);

            /// overwrite current image with changes
            $fs->delete($filename);
            $fs->write($filename, $imagick->getImageBlob());

            ///
            /// optional: send response to browser
            ///
            $ev->sender->wired()->insertAfter('<p>project file saved</p>');

            $dataObject = DataURIData::buildFromFile($fullname);
            $dataUri = DataURIDumper::dump($dataObject);
            $this->q('id_btn_print2')->wired()->insertAfter("<img src='{$dataUri}'>");

        } else {
            $this->raiseAlert('incorrect uri data format');
        }
    }
```
## NEXT
+ Для загрузки нескольких svg-элементов (в рамках данного примера), допустимо выполнить серию
соответствущих последовательных вызовов как в разделе `Шаг 1`
