# Установка "встроенных" маркеров для nvd3 chartLine - пошаговая инструкция

* Краткое руководство

# css file
```css
.marker.label text {
    font:  normal 10px sans-serif;
    /* fill: black */
    stroke-width: 1px;
    /* stroke: #000406; */
    stroke: #002c95;
    text-anchor: middle;
}

.marker.label rect {
    /* background-color: white; */
    /* fill: white; */
    fill: white;
    fill-opacity: 0.8;
    opacity: 1;
    font: normal 10px sans-serif;
    stroke: #051986;
    stroke-width: 1px;
}

.label-key {
  font-weight: normal;
}
```

## js-обработчик событий resize chart

По умолчанию, в коде создания (инициации) lineChart присутствует
строка: `nv.utils.windowResize(chart.update);` - её необходимо
заменить на код приведенный ниже.

    * срабатывает всякий раз при изменении размеров видимой области
      диаграммы (svg)
    * необходим для перестройки позиций меток при ресайзе

```javascript
    nv.utils.windowResize(
        () => {
            Wsys(${this}).q('id_markupchart').chart().refreshMarkers();
            return chart.update();
        }
    );
```

## пример php-кода для задания меток в сериях

```php
$needmarker = $vnum % 5 == 0;
if ($needMarker) {
  $values[$vnum]['marker'] = [
    'label' => [
      'dy' => '-4em',
      'dx' => '-6em',
      'text' => $values[$vnum]['y']
    ]
  ];
}
```

## основные js-методы

  * для запуска вставок js-кода используется php-метод `$this->send()`
  * вместо параметра `seriesIndex` возможно использование `seriesId`

### установка меток

```javascript
    Wsys({$this}).q('id_markupchart').chart().labelPoints(
        {
            seriesIndex: 1
        }
    );
```

### снятие меток

```javascript
    Wsys({$this}).q('id_markupchart').chart().unlabelPoints(
        {
            seriesIndex: 1
        }
    );
```

### обновление (перерисовка) всех меток

```javascript
Wsys(${this}).q('id_markupchart').chart().refreshMarkers();

```
