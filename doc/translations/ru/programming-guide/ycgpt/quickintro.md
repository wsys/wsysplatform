# Основные шаги
## Предварительные шаги
+ В целевой модуль необходимо добавить объявления:
  + use Psr\Http\Message\ResponseInterface;
  + use Namedio\Ext\Out\Out;

## Добавить приватный метод для вызова HTTP-клиента для curl-запросов

```php
    private function makeHTTPClientYC($opts = [])
    {
        $xFolderId = $this->lib('env')->get('YC.GPT.FOLDER_ID');
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . ($this->lib('env')->get('YC.IAM_TOKEN')),
            'x-folder-id' => $xFolderId,
        ];

        $dflt = [
            'base_uri' => 'https://llm.api.cloud.yandex.net/foundationModels/v1/completion',
            'headers' => $headers
        ];

        $config = array_merge_recursive($dflt, $opts);
        $defs = [
            ':config' => $config
        ];
        return $this->sys()->makeClass(\GuzzleHttp\Client::class, $defs);
    }
```

### Реализация примера промт-режима

```php
    private function promptYCGPT()
    {
        $client = $this->makeHTTPClientYC();
        $p = [];

        $headers = [];

        // По умолчанию YandexGPT API сохраняет все данные запросов.
        // Если в запросах вы передаете персональные или конфиденциальные данные или другую чувствительную информацию, отключите логирование.
        // Для этого добавьте в заголовок запроса REST или метаинформацию вызова gRPC опцию x-data-logging-enabled: false.
        // Запросы, переданные с отключенной опцией логирования, не будут сохраняться на серверах Yandex Cloud.
        $headers['x-data-logging-enabled'] = false;

        $p['headers'] = $headers;

        $json = [
            "modelUri" => "gpt://{$this->lib('env')->get('YC.GPT.FOLDER_ID')}/yandexgpt-lite",
            "completionOptions" => [
                "stream" =>  false,
                "temperature" =>  0.1,
                "maxTokens" =>  "1000"
            ],
            "messages" =>  [
                [
                    "role" =>  "system",
                    "text" =>  "Переведи текст"
                ],
                [
                    "role" =>  "user",
                    "text" =>  "To be, or not to be: that is the question."
                ]
            ]
        ];

        $p['json'] = $json;

        $promise = $client->requestAsync('POST', '', $p);

        $promise
            ->then(fn(ResponseInterface $res) => Out::new((string) $res->getBody()))
            ->then(fn(Out $r) => $r->transform('json_decode', true))
            ->then(fn(Out $r) => $r->hasPath('result.alternatives.0.message.text')->raise(\DomainException::class, 'reply error'))
            ->otherwise(fn(\Throwable $e) => Out::new($e));
        $promise->wait();
        $r = Out::release();

        $reply = null;
        if ($r->isOK()) {
            /// чтение результатов успешного ответа сервера
            $reply = $r->getByPath('result.alternatives.0.message.text');

            /// проверка статуса ответа: "status": "ALTERNATIVE_STATUS_FINAL"
            $replyStatus = $r->getByPath('result.alternatives.0.status');
        } else {
            /// чтение результатов в случае ошибки обработки ответа сервера: в общем случае сообщение: 'reply error'
            $reply = $r->view();
        }
    }
```

# Примечания
  + Это концептуальный пример, реализация может поменяться
# Ссылки

+ Документация YandexGPT API
  + https://cloud.yandex.ru/ru/docs/yandexgpt/
+ Пример и документация по запросу в промт-режиме
  + https://cloud.yandex.ru/ru/docs/yandexgpt/operations/create-prompt
