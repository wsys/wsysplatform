<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [API внутреннего key-value хранилища](#api-внутреннего-key-value-хранилища)
    - [Условные обозначения переменных](#условные-обозначения-переменных)
    - [`set`](#set)
    - [`setOnce`](#setonce)
    - [`get`](#get)
    - [`has`](#has)
    - [`is`](#is)
    - [`remove`](#remove)
    - [`discard`](#discard)
    - [`cmp`](#cmp)
    - [`inc`](#inc)
    - [`dec`](#dec)
    - [`fromArray`](#fromarray)
    - [`toArray`](#toarray)
    - [`appendArray`](#appendarray)
    - [`getAsInt`](#getasint)
    - [`setAsInt`](#setasint)
    - [`getAsNumebr`](#getasnumebr)
    - [`setAsNumber`](#setasnumber)
    - [`print`](#print)
    - [`dump`](#dump)
- [Examples](#examples)
    - [Array access](#array-access)
    - [destructuring](#destructuring)

<!-- markdown-toc end -->

# API внутреннего key-value хранилища
## Условные обозначения переменных
  + `object $target` указатель на форму (`$this`) или на контрол
  + `mixed $scope` область действия группы key/value
    + параметр может быть опущен
      + по умолчанию используется частная область действия с именем `:priv`
    + следующие имена scope зарезервированы:
      + `:sys*` используется для системных нужд
      + `:priv` умолчательная область для прикладных целей
      + начинающиеся с не буквенно-цифровых символов
    + области с разными именами могут независимо иметь одинаковые имена
      ключей
    + примеры допустимых прикладных имен
      + `:priv`
      + `:my`
      + `:name`
      + `:a`
      + `:abc`
  + `string|int $key` в качестве имени ключа могут использоваться произвольные
    валидные алфавитно-цифровые индентификаторы

## `set`
```php
void $target->reg($scope)->set(string|int $key, mixed $value)
```
Установить значение


## `setOnce`
```php
boolean $target->reg($scope)->setOnce(string|int $key, mixed $value)
```
Установить значение однажды. `$value` может быть с типом `callable`
Пример:
```php
        $this->reg()->setOnce(
            'o',
            function () {
                return microtime(true);
            }
        );
```


## `get`
```php
mixed $target->reg($scope)->get(string|int $key)
```
Получить значение по ключу. Если ключ отсутствует - ошибка.

## `has`
```php
boolean $target->reg($scope)->has(string|int $key)
```
Проверить, существует ли такой ключ.

## `is`
```php
boolean $target->reg($scope)->is(string|int $key)
```
Проверить, определено ли значимое значение по заданному ключу. Значимым
считается: если строка, то не пустая строка, для остальных типов `!empty($value)`

## `remove`
```php
void $target->reg($scope)->remove(string|int $key)
```
Удаляет значение по ключу.

## `discard`
```php
void $target->reg($scope)->discard()
```
Очищает всю область

## `cmp`
```php
boolean $target->reg($scope)->cmp(string|int $key, mixed $value)
```
Выполняет сравнение оператром `===` со значением по заданному ключу

## `inc`
```php
int $target->reg($scope)->inc(string|int $key)
```
Выполняет инкремент значения с шагом `1`. Если поле не инициализировано то
результатом будет `1`.

## `dec`
```php
int $target->reg($scope)->dec(string|int $key)
```
Выполняет декремент значения с шагом `1`. Если поле не инициализировано то
результатом будет `-1`.

## `fromArray`
```php
array $target->reg($scope)->fromArray($input)
```

Выполняется заполнение хранилища из массива `$input`.
Ключи `$input` становятся ключами в хранилище `$target->reg($scope)`

Пример:
```php
$this->reg()->fromArray(['foo' => 'foofoo', 'bar' => 'barBar', 'baz' => 'Baz']);

// результат: $x == 'barBar'
$x = $this->reg()->get('bar');
```

ВНИМАНИЕ: `fromArray` очищает все текущие значения хранилища!

Для сохранения значений при добавлении из массива, рекомендуется использовать `appendArray`

## `toArray`
```php
array $target->reg($scope)->toArray()
```

Возвращает копию всех значений в виде ассоциативного массива


## `appendArray`
```php
array $target->reg($scope)->appendArray($input)
```

Добавляет значения в хранилище из массива $input.

Ключи `$input` становятся ключами в хранилище `$target->reg($scope)`

Одноименные ключи замещаются новыми значениями.

Пример:
```php
$this->reg()->appendArray(['foo2' => 'foofoo2', 'bar2' => 'barBar2', 'baz2' => 'Baz2']);

// результат: $x == 'Baz2'
$x = $this->reg()->get('baz2');
```


## `getAsInt`
```php
int $target->reg($scope)->getAsInt(string|int $key)
```

## `setAsInt`
```php
void $target->reg($scope)->setAsInt(string|int $key, mixed $value)
```

Пример:
```php
$r = $this->reg();

// asInt.0 == 123
$r->setAsInt('asInt.0', 123);

// asInt.1 == 123
$r->setAsInt('asInt.1', '123.321');
```

## `getAsNumebr`
```php
float $target->reg($scope)->getAsNumber(string|int $key)
```


## `setAsNumber`
```php
void $target->reg($scope)->setAsNumber(string|int $key, mixed $value)
```

Пример:
```php
$r = $this->reg();

// asNumber.0 == 321.537
$r->setAsNumber('asNumber.0', 321.537);

// asNumber.1 == -135
$r->setAsNumber('asNumber.1', '-135');
```

## `print`
```php
string $target->reg($scope)->print(string $filter = null);
```

Вывести содержимое хранилища в строковом виде PHP-массива. Строчное
представление аналогично результату встроенной PHP-функции `print_r`
("Выводит удобочитаемую информацию о переменной")

Если указан параметр `$filter`, то будут отфильтрованы значения,
соответствующие подстроке в ключах хранилища `reg`, иначе - все значения.

Подстрока ищется в любом месте ключа и независимо от регистра.

Пример:
```php
$r = $this->reg();
$r->fromArray(['foobar123' => 'foo123', 'baa321' => -953.1234579, 'BAZ321' => true, 'bra' => null]);
$s = $r->print();

/*

Array
(
    [foobar123] => foo123
    [baa321] => -953.1234579
    [BAZ321] => 1
    [bra] =>
)

*/
```

```php
$r = $this->reg();
$r->fromArray(['foobar123' => 'foo123', 'baa321' => -953.1234579, 'BAZ321' => true, 'bra' => null]);
$s = $r->print('ba');

/*

Array
(
    [foobar123] => foo123
    [baa321] => -953.1234579
    [BAZ321] => 1
)

*/
```


## `dump`
```php
string $target->reg($scope)->dump(string $filter = null);
```

Вывести содержимое хранилища в строковом виде PHP-массива. Строчное
представление аналогично результату встроенной PHP-функции `var_dump`
("Выводит информацию о переменной") примененной к каждому
значению ключа. Это позволяет вывести подробно информацию о типе значения.

Если указан параметр `$filter`, то будут отфильтрованы значения,
соответствующие подстроке в ключах хранилища `reg`, иначе - все значения.

Подстрока ищется в любом месте ключа и независимо от регистра.

Пример:
```php
$r = $this->reg();
$r->fromArray(['foobar123' => 'foo123', 'baa321' => -953.1234579, 'BAZ321' => true, 'bra' => null]);
$s = $r->dump();

/*

Array
(
    [foobar123] => string(6) "foo123"
    [baa321] => float(-953.1234579)
    [BAZ321] => bool(true)
    [bra] => NULL
)

*/
```

```php
$r = $this->reg();
$r->fromArray(['foobar123' => 'foo123', 'baa321' => -953.1234579, 'BAZ321' => true, 'bra' => null]);
$s = $r->dump('ba');

/*

Array
(
    [foobar123] => string(6) "foo123"
    [baa321] => float(-953.1234579)
    [BAZ321] => bool(true)
)

*/
```

# Examples
## Array access
```php

// можно сослаться просто на переменную, вместо `$this->reg()`
$r = $this->reg();

// доступ к стореджу как к массиву (альтернативы set/get)
$r['foo'] = 123;
$r[0] = 'bar';

// допустимо и так:
$this->reg()['baz'] = 'baz';

// результаты:
// $r['foo']        == 123
// $r[0]            == 'bar'
// $r['baz']        == 'baz'
// isset($r['foo']) == true
```

## destructuring
Деструктурирование доступа к массиву

```php
// для упрощения, используется переменная $r
$r = $this->reg();

$r->fromArray(['foo' => 'foofoo', 'bar' => 'barBar', 'baz' => 'Baz']);

// можно использовать (везде) вместо $r "длинный вызов" $this->reg()
$this->reg()->appendArray(['foo2' => 'foofoo2', 'bar2' => 'barBar2', 'baz2' => 'Baz2']);


/// собственно, деструктурирование или упрощенный способ "группового присваивания"

// выборочное извлечение ключей из стореджа, порядок не важен
['foo' => $foo, 'baz' => $baz, 'foo2' => $foo2] = $r;

// и можно так:
['foo' => $foo, 'baz' => $baz, 'bar2' => $bar2] = $this->req();

```
