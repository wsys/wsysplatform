## selectRow

`selectRow($id = null)` выбирает строку в гриде, осуществляя при этом операцию
имитирующую щелчок мыши на выбранной записи.

* `@param` `string` `$id` - идентификатор записи (значение первичного
  ключа , обычно находится в поле с именем `ID` заданной строки) Если
  параметр опущен или равен `NULL`, то выбирается первая запись
* `@return` `void`

### Пример
```php

/// select and click first row in rowset
$this->q('id-dbgrid1')->selectRow();

/// select and click a row with ID equals '1230000000'
$this->q('id-dbgrid1')->selectRow('1230000000');
```

## selectRowByIndex

`selectRowByIndex($index = null)` выбирает строку в гриде по порядковому
номеру (индексация элементов с нуля), осуществляя при этом операцию
имитирующую щелчок мыши на выбранной записи.

* `@param` `string` `$index` - индекс, как порядковый номер в наборе
  записей. Если параметр опущен или равен `NULL`, то выбирается первая запись
* `@return` `void`

### Пример
```php

/// select and click first row in rowset
$this->q('id-dbgrid1')->selectRowByIndex();

/// select and click a row by index equals 0 (also first row)
$this->q('id-dbgrid1')->selectRowByIndex(0);
```
