# Вводный пример

## Серверная часть

### Шаг 1. Процедура начальной загрузки.

Для примера,  используется идентификатор пакета `w.samples.webrtc.form_10`

Создать метод `openDial`:

```php

    private function openDial()
    {
        $who = $this->im()->who();
        $to = null;
        $first = null;
        if ($who->count() > 0) {
            $to = $this->im()->who()[0]->toRecip();
            $first = $to['jid']['jid.real'];
        }

        $conf = json_encode(
            [
                'form' => $this->getSelector(),
                'who' => [
                    'first' => $first
                ]
            ]
        );

        $p = [];
        $p['wirePath'] = $this->wirePath('w.samples.webrtc.form_10', 'client.js');

        $inject = <<<EOT
import {Client} from {$p['wirePath']};
let m = new Client(${conf});
m.connect();
console.debug('wireModule: openDial started', $this);
EOT;

        $this->wireModule(
            $inject
        );
    }

```

## Шаг 2. Привязка к событию onShow.

В обработчике onShow разместить вызов `openDial`:

```php
$this->openDial();
```

## Клиентская часть

Добавить в систему модуль ES2015 с именем client.js

Исходный текст модуля и дополнительная информация (см. комментарии):
[исходный код клиентской части для вводного примера](../../snippets/js/video-calls/client.js)

# Примечания
+ все тесты должны выполнятся для пользователей dial1, dial2, ... dialN
