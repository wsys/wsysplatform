# Пример кода для вставки формул в Word в виде графических ихзображений

```php
    $phpWord = new \PhpOffice\PhpWord\PhpWord();

    /* Note: any element you append to a document must reside inside of a Section. */

    // Adding an empty Section to the document...
    $section = $phpWord->addSection();
    // Adding Text element with font customized using named font style...
    $fontStyleName = 'oneUserDefinedStyle';
    $phpWord->addFontStyle(
        $fontStyleName,
        //array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
        array('name' => 'Tahoma', 'size' => 10)
    );
    $section->addText(
        //$this->q('id_markup_calk')->readProp('markup'),
        'Исходные данные:',
        $fontStyleName
    );


    // @see https://editor.codecogs.com
    $m =
    '\textrm{T}_{sm}=\textrm{T}_{r}+\frac{\textrm{C}_{psm}\cdot\textrm{r}\cdot\textrm{Q}_{f}}{\textrm{C}_{pk}\cdot\alpha\cdot\left(\textrm{h}_{sm}\cdot\textrm{l}_{sm}&plus;\textrm{A}_{sm}\right)}\cdot\left[1-\textrm{exp}\left(-\frac{\alpha\cdot\left(\textrm{h}_{sm}\cdot\textrm{l}_{sm}&plus;\textrm{A}_{sm}\right)}{\textrm{C}_{psm}\cdot\textrm{G}_{k}}\right)\right]=';

    //
    // set parameters: $f->set($key, $value) default value: 0
    //
    //   base syntax (example 1): C.pk ==> {C}_{psm}
    //   base syntax (example 2): Q.f ==> {C}_{f}
    //   base syntax (example 3): alpha ==> \alpha
    //
    $f = $this->ext()->newFormula($m)
        ->set('T.r')
        ->set('C.psm')
        ->set('r')
        ->set('Q.f')
        ->set('C.pk')
        ->set('alpha')
        ->set('h.sm')
        ->set('l.sm')
        ->set('A.sm')
        ->set('G.k');

    // optional: gif|png default value: gif
    $f->setImageType('gif');

    $section->addImage(
        $f->getUrl(),
        ['width' => 405]
    );

    $section->addTextBreak(3);

    $section->addImage(
        $f->getURLWithValues(),
        ['width' => 405]
    );

    // Saving the document as OOXML file...
    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

    $path = $this->lib('env')->get('wsys.mntcat.path') . '/doc/';
    $objWriter->save($path . 'calk' . '.docx');
```
# примечания
## редактор формул https://editor.codecogs.com/home
+ для вставки кода в редактор рекомендуется использовать Shift+Ins
# ссылки
+ [Редактор формул](https://editor.codecogs.com)
+ [LaTex markup](https://en.wikibooks.org/wiki/LaTeX/Mathematics)
+ [Набор математических формул в LaTex](https://grammarware.net/text/syutkin/MathInLaTeX.pdf)
+ [альтернативный редактор формул](https://www.bruot.org/tex2img/)
