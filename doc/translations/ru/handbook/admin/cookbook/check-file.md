[Индекс](./README.md)

# Проверка (существования) файла в каталогах портала
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Проверка (существования) файла в каталогах портала](#проверка-существования-файла-в-каталогах-портала)
    - [Введение](#введение)
    - [Команда `locate`](#команда-locate)
        - [Примечание](#примечание)
    - [Команда `find`](#команда-find)

<!-- markdown-toc end -->

## Введение
Проверку существования файла проще всего выполнять в консольном
доступе.

- Для начала, необходимо открыть `ssh-сеанс`
- Используйте [эту ссылку](https://gitlab.com/initstream/cookbook/-/blob/master/translations/ru/tools/putty/putty.md)
для помощи в организации настройки, если по каким-то причинам не предусмотрен `ssh-доступ`


Допустим, требуется выяснить есть ли файл с определенным именем.
Существует немалое число способов, технологий и подходов сделать
это. Рассмотрим простейшие способы.

Дано: `UPOR-KO 6,3-A-2.bmp`

## Команда `locate`
На серверах, часто уже установлена полезная команда `locate`

Данная команда, для ускорения поиска, использует регулярно индексируемый список всех
файлов в системе. Если эта команда не доступна - рекомендуется
установить пакет `locate` или `slocate` или `mlocate`

Итак, результат команды
```
locate "UPOR-KO 6"
```
с поиском по фрагменту будет:
```
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-A-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-B-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-B-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-D-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-D-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-E-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-G-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-G-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-I-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-J-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-V-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-V-4.bmp
```

Как видно из листинга, такого файла `UPOR-KO 6,3-A-2.bmp` в каталоге
нет.


### Примечание
- т.к. `locate` использует индексируемую базу, результаты могут
  отличаться, если например файл был удалён после индексирования:
  используйте ключ `-e` для вывода только существующих на данный момент
  файлов
- используйте `updatedb` для актуализации индекса текущих файлов
  - ВНИМАНИЕ: данная команда при выполнении требует время и нагружает
    процессор, требует привилегий
- данную команду полезно применять как оценочную, когда известны лишь
  некоторые фрагменты имени файла

## Команда `find`

Данную команду можно использовать как альтернативу `locate`, но с
уточнением пути и это будет гораздо медленней.

Пример с ключом `-iregex` ,где префикс `i` означает игнорировать
регистр:

```
find ~/work/site-wsplatform -type f -iregex '.*UPOR-KO\s6.*bmp'
```

```
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-G-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-A-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-B-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-D-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-D-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-I-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-B-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-E-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-V-4.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-V-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-J-2.bmp
/home/nodemaster/work/site-wsplatform/mnt/vz/UPOR-KO 6,3-G-4.bmp

```

[Индекс](./README.md)
