[Домой](../README.md#руководство-разработчика-документации)

# Руководство разработчика документации
# Содержание

[[_TOC_]]

# Общие указания
Используйте простые, понятные и ясные предложения, позволяющие быстро и
эффективно усвоить материал. Материал, требующий специфичные определения
следует предварительно дополнить, дав необходимые пояснения.

Если не знаете что писать или с этим сложности, ограничьтесь общими
фразами и выражениями. Оставьте структуру (в виде подзаголовков или
подразделов) для дальнейшей доработки. Если есть возможность,
используйте диаграммы, изображения, схемы, таблицы.

Для структурного оформления текста (с отступами) можно использовать
иерархические списки (структурным элементом считается, параграф, абзац, блочный
элемент без разрывов за исключением таблиц) Например как в разделе ниже:
["Полезные ссылки"](#полезные-ссылки)

Избегайте усложнений. Не пишите много воды - если в данный момент не
получается написать, оставьте на доработку. Помните, что важнее всего,
предоставить пользователю быстро, чёткий, понятный и ясный ответ в
достаточном объёме по основным разделам. Детали могут быть опущены и уже
рассмотрены в отдельных разделах или документах.

Стремитесь оставить приятное впечатление от прочтения документации
т.к. это один из ключевых факторов программного продукта. Документация
должна быть и охватывать наиболее востребованные вопросы в достаточном
объёме, но не более этого. Именно, благодаря охвату и востребованным
темам, документация постепенно становится исчерпывающей.

Используйте пропуски строк, параграфы, списки, заголовки для логического
выделения документа. Стремитесь по возможности, чтобы текст не
растягивался слишком по ширине. Идеально - не более 60% Изучайте примеры
и образцы.

Проверяйте орфографию и периодический проводите вычитку
документа. Опечатки и ошибки неизбежны. Избегайте повторов. Используйте специальные
словари. Включайте проверку орфографии или убедитесь что проверка активна - это существенно сократит время
разработки и улучшит документ.

- пишите как можно больше полезной конкретики
- сосредоточьтесь на самом важном, а не на отдельном разделе или параграфе. Фокусируйтесь на документе
  или комплекте документов в целом, на результатах
- проработку деталей можно сделать потом
- на каждой итерации пишите наиболее важные и значимые вещи, опуская
  второстепенное
- воздерживайтесь от специфики, выходящей за пределы темы - вполне достаточно
  несколько фраз и ссылок на специализированные источники

Полезный подход, при создании отдельного документа - предварительно
расписать его структурное содержание в виде иерархии заголовков. Затем,
уже можно сосредоточиться на отдельных подразделах. Для прояснения
структуры будущего документа, достаточно сформировать несколько
подзаголовков, что в свою очередь значительно упрощает процесс
подготовки документа.

Сообщения коммитов как правило пишется на английском языке. Используйте
рекомендации, приведённые ниже в разделе ["Полезные ссылки"](#полезные-ссылки)


## Примечание
В настоящий момент используется `markdown`, но в дальнейшем могут
использоваться другие (совместимые) форматы:
- https://ru.wikipedia.org/wiki/ReStructuredText

# Имена
Используйте практически везде по возможности низкий регистр, кроме
заглавных букв разумеется и специализированных аббревиатур (например `PHP`). В некоторых
случаях низкий регистр должен применяться полностью, например при
создании ссылок.

Следует по возможности избегать длинных слов, заменяя их на
синонимы или родственные понятия.

Используйте специальное выделение для аббревиатур в виде апострофов:
например `SQL`
```
`SQL`
```

Выбирайте имена, которые легко воспринимаются на
слух, особенно через такие каналы связи как телефон или на
расстоянии. Избегайте многозначности и похожих букв.

Пример, хорошо продуманной иерархии имён:
- https://github.com/torvalds/linux/tree/master/Documentation
- https://github.com/torvalds/linux

# Создание заголовков
```
# Header 1
## Header 2
### Header 3
#### Заголовок 4
```
В общем случае используется "плоская" модель оформления, когда в
документе только заголовок первого уровня. Использование большей
вложенности также допустимо, но не так распространено.

Все заголовки при рендеринге `Markdown` автоматически получают идентификаторы,
за исключением комментариев, однако при формировании например оглавления ссылок,
может потребоваться ручная коррекция.

При наведении мыши `Ссылка` на эти идентификаторы становится видимой,
чтобы облегчить копирование ссылки в заголовок и передачу её кому-то
другому.

Идентификаторы генерируются из содержимого заголовка в соответствии со следующими правилами:

Весь текст преобразуется в нижний регистр.
1. Весь текст без слов (например, пунктуация, HTML) удаляется.
2. Все пробелы преобразуются в дефисы.
3. Два или более дефиса в строке преобразуются в один.
4. Если заголовок с тем же идентификатором уже был сгенерирован, то
   добавляется уникальный инкрементный номер, начинающийся с 1.

Например:
```
# This header has spaces in it
## This header has a :thumbsup: in it
# This header has Unicode in it: ??
## This header has spaces in it
### This header has spaces in it
## This header has 3.5 in it (and parentheses)
```
Сгенерирует следующие идентификаторы:

1. this-header-has-spaces-in-it
2. this-header-has-a-in-it
3. this-header-has-unicode-in-it-??
4. this-header-has-spaces-in-it
5. this-header-has-spaces-in-it-1
6. this-header-has-3-5-in-it-and-parentheses

# Создание оглавления документа
Вы можете использовать общепринятый подход для автоматического внедрения
заголовка. Разместите в начале документа следующую инструкцию:
```
[[_TOC_]]
```

Это работает для большинства markdown-диалектов ([`Gitlab`](https://gitlab.com/help/user/markdown#table-of-contents), `Github`)

# Создание внешних ссылок
Распространённая форма ссылок состоит из двух частей: текстовая часть,
заключённая в квадратные скобки и `url` заключенный в круглые скобки

## Внимание
Довольно частая ошибка: сначала круглые скобки, затем квадратные. Как
правило, ссылка появляется сразу, после синтаксически правильной конструкции.

Примеры:
```
http://w3.org/
[Duckduckgo поиск](http://duckduckgo.com)
[внешняя ссылка на md-документ](https://gitlab.com/wsys/wsysplatform/-/tree/master/doc/translations/ru/handbook/README.md)
[внешняя ссылка на якорь внутри md-документа](https://gitlab.com/wsys/wsysplatform/-/tree/master/doc/translations/ru/handbook/README.md#руководство-разработчика-документации)
```

Вышеприведённые ссылки:
- http://w3.org/
- [Duckduckgo поиск](http://duckduckgo.com)
- [внешняя ссылка на md-документ](https://gitlab.com/wsys/wsysplatform/-/tree/master/doc/translations/ru/handbook/README.md)
- [внешняя ссылка на якорь внутри md-документа](https://gitlab.com/wsys/wsysplatform/-/tree/master/doc/translations/ru/handbook/README.md#руководство-разработчика-документации)


# Создание внутренних ссылок
```
[внутренняя ссылка на данный документ](./guide.md)
[внутренняя ссылка на данный документ + подзаголовок выше](./guide.md#cоздание-внутренних-ссылок)
```
Вышеприведённый пример:
- [внутренняя ссылка на данный документ](./guide.md)
- [внутренняя ссылка на данный документ + подзаголовок выше](./guide.md#cоздание-внутренних-ссылок)


## Примечание
При указании ссылки как во втором примере, общее правило: "якорь",
указывающий на заголовок  с текстом "Создание внутренних ссылок" пишется пишется
без пробелов, заменённых на дефис, все в нижнем регистре. Другие знаки
пунктуации, такие как `:` игнорируются без замены.

# Создание списка
```
- элемент 1
- элемент 2
  Параграф элемента 2.
  Вторая строка параграфа элемента 2.
  Третья строка параграфа элемента 2.
- элемент 3
  - элемент 3.1
  - элемент 3.2
- элемент 4
  - элемент 4.1
    - элемент 4.1.1
    - элемент 4.1.2
    - элемент 4.1.3
  - элемент 4.2
  - элемент 4.3
- элемент 5
  Строка 1
  Строка 2, перед разрывом, список также "разрывается"

- Список 2 (еще пример)
  - Элемент 1 списка 2
  - Элемент 2 списка 2
```
# Добавление внутренних картинок

Общая схема:
```
![alt text](image.jpg)
```

1. Разместить соответствующее изображение в область файловой системы
   целевого документа. Для онлайн систем, таких как `Gitlab` это может быть
   кнопка со знаком `+` и выбором команды `Upload file` (см. в верхней
   части экрана над каталогом файлов - доступна при листинге каталога)
   При необходимости, там же создайте подкаталог для хранения медийных
   файлов.
2. В целевом документе прописать с помощью языка разметки имя файла и
   соответствущий путь к изображению.
   Например:`![screenshot #1](./img/screenshot-1.png)` где одна точка в
   начале пути означает текущий каталог, две точки, соответственно
   родительский каталог относительно текущего документа.

Пример (файлы реально доступны):
```
![screenshot #1](./img/screenshot-1.png)
![screenshot #2](./img/screenshot-2.png)
```

Результат:

![screenshot #1](./img/screenshot-1.png)
![screenshot #2](./img/screenshot-2.png)

Разумеется, все эти манипуляции можно также выполнить и с помощью команды `git push`

# Добавление внешних картинок
Все тоже самое как и для внутренних ссылок, за исключением того, что
вместо имени файла используется `url`

Общая схема:
```
![alt text](http://example.com/path/to/image.jpg)
```

```

!["Coastline"](https://images.unsplash.com/photo-1503756234508-e32369269deb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=675&q=80)

```
Вышеприведённый пример:

!["Coastline"](https://images.unsplash.com/photo-1503756234508-e32369269deb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=675&q=80)


# Создание таблиц
```
| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |
```
Результат:

| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |

Примечание: для корректного отображения, перед таблицей и после добавьте пустую строку.
# Создание фрагментов с кодом
Примеры:

````
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```
````

Результат:

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```

# Создание подраздела в документе

Если, содержимое раздела нужно показать в оглавлении `README.md`
(опционально: можно пропустить до 6-го пункта)
1. [Перейдите в README.md](../README.md)
2. Перейдите в режим редактирования документа (например кнопка `Edit` в
   `Gitlab`)
3. Выберите соответствующий подраздел с названием документа, например
   `Wportal: краткое руководство прикладного программиста`
4. Найдите место для нового подраздела, например в конце данной секции
5. После строки `- [Модули](./programmer/guide.md#модули)` добавьте
новую строку, например:  `- [Новый раздел](./programmer/guide.md#новый-раздел)`
6. Сохраните документ
7. Откройте документ [Wportal: краткое руководство прикладного программиста](../programmer/guide.md)
8. Перейдите в режим редактирования документа
9. Аналогично, найдите окончание последнего подраздела `Модули`
10. Добавьте соответствущий раздел в виде заголовка `# Новый раздел` и
    сохраните его
# Создание документа
1. [Перейдите в README.md](../README.md)
2. Перейдите в режим редактирования документа (например кнопка `Edit` в
   `Gitlab`)
3. Перейдите в конец документа или в желаемое место
4. Добавьте строчку, например: `# [Новый документ](./newdocument/guide.md)`
5. Сохраните документ
6. Создайте в репозитарии файловой системы соответствующий примеру
   каталог: `newdocument`
7. Перейдите в соответствущий каталог и создайте файл с именем для
   примера: `guide.md`
8. Отредактируйте и сохраните документ
# Образцы хорошей документации

Просто загляните в исходник:

- https://github.com/d3/d3-shape
- https://github.com/d3/d3
- https://www.kernel.org/doc/html/latest/
  - https://www.kernel.org/doc/html/latest/process/4.Coding.html?highlight=style
- https://github.com/SedovSG/fias-dbf
- https://github.com/ardumont/markdown-toc/blob/master/README.md

# Другие примеры документации
- https://github.com/mailru/FileAPI
- https://gist.github.com/Guest007/e3a09aa97a827916b0b91b726a8c2c66
- https://gist.github.com/Guest007/2340489e2917f03a40cabd909f4c7911
- https://github.com/silverbulleters/OneScript
- https://github.com/silverbulleters/OneScript.Web/tree/master/docs
- https://github.com/silverbulleters/whereismythirty
- https://github.com/joaotavora/yasnippet/blob/master/doc/index.org
- https://github.com/freeCodeCamp/devdocs
- https://github.com/metagrover/ES6-for-humans
- https://readthedocs.org/
- https://python-evdev.readthedocs.io/en/latest/tutorial.html
- https://github.com/less/less-docs/
- https://github.com/WebAssembly/design
- https://github.com/zendframework/zend-view/blob/master/docs/book/quick-start.md
- https://github.com/zendframework/zend-view/blob/master/docs/book/cookbook/setting-module-specific-layouts.md
- https://github.com/appserver-io/docs/blob/master/pages/03.tutorials/01.my-first-webapp/docs.md
- https://github.com/appserver-io/docs/tree/master/pages
- https://github.com/php-fig/fig-standards
- https://github.com/ardumont/markdown-toc/blob/master/README.md
- https://github.com/Tinram/Linux-Utilities/blob/master/README.md
- https://github.com/alphapapa/org-ql/blob/master/examples/defpred.org

# Примеры хорошей структуры имен
- https://github.com/torvalds/linux
  - https://github.com/torvalds/linux/tree/master/Documentation
- https://docs.python.org/3/reference/index.html
# Полезные ссылки
- markdown
  - синтаксис
    - https://daringfireball.net/projects/markdown/syntax
  - валидация и проверка ошибок
    - https://dlaa.me/markdownlint/
    - https://www.tools-utils.com/markdown-validator
  - guides
    - https://guides.github.com/features/mastering-markdown/
  - on-line editors
    - https://markdown-editor.github.io/
    - https://jbt.github.io/markdown-editor/
    - https://markdown-editor.andona.click/
    - https://dillinger.io/
  - converters/generators
    - https://word2md.com/
    - https://www.tablesgenerator.com/markdown_tables
  - editors (crossplatform)
    - https://wereturtle.github.io/ghostwriter/
    - http://abricotine.brrd.fr/
    - https://marktext.app/
    - https://remarkableapp.github.io/
    - расширяемые и настраиваемые (customizable)
      - Emacs
        - https://www.gnu.org/software/emacs/
        - [почему емакс](https://habr.com/ru/post/190790/ "обзорная статья на habr.com")
        - [плагины](https://melpa.org/)
      - Sublime text 3
        - https://www.sublimetext.com/
        - плагины
          - https://github.com/jonschlinkert/sublime-markdown-extended
          - https://github.com/SublimeText-Markdown/MarkdownEditing
- git
  - clients
    - windows and crossplatform
      - https://git-scm.com/download/gui/windows
    - статьи, обзоры
      - https://qna.habr.com/q/422299
      - https://levashove.ru/luchshie-klienty-git-gui-dlya-windows/
      - https://git-scm.com/book/ru/v2/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-Git
      - https://git-scm.com/book/ru/v2/Appendix-A%3A-Git-%D0%B2-%D0%B4%D1%80%D1%83%D0%B3%D0%B8%D1%85-%D0%BE%D0%BA%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F%D1%85-%D0%93%D1%80%D0%B0%D1%84%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B5-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81%D1%8B
      - https://tech-geek.ru/best-git-gui-clients-windows/
  - commit messages (how to)
    - http://karma-runner.github.io/5.0/dev/git-commit-msg.html
    - [Semantic Commit Messages](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)
    - https://www.conventionalcommits.org/en/
    - https://gist.github.com/adeekshith/cd4c95a064977cdc6c50
    - https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53#file-commit-message-guidelines-md
    - https://github.com/erlang/otp/wiki/writing-good-commit-messages
    - https://gist.github.com/RichardBronosky/beacdea6106de0d0c7d36fbaf625139e
    - [A specification for adding human and machine readable meaning to commit messages](https://www.conventionalcommits.org/)
      - [russian](https://www.conventionalcommits.org/ru/v1.0.0-beta.4/)
- naming
  - https://tools.ietf.org/html/rfc1178

[Домой](../README.md#руководство-разработчика-документации)
