
[[_TOC_]]

# Введение
# Активация и вход
## Активация предустановленной учетной записи
В данном случае потребуется подтверждение смены привязки почтового
адреса (email) Необходимо перейти по ссылке
представленной в почтовом сообщении от сервиса `GitLab` с заголовком
`Confirmation instructions` как показано в примере:
![Активация учетной записи](./img/screenshot_20220825_192741.png)

Далее, перейдя по ссылке, необходимо авторизоваться:
![Подтверждение смены email](./img/screenshot_20220826_123757.png)

+ Примечания
  + Как правило переход по ссылке сразу подтверждает email
    + сверху появляется соответствующее сообщение
  + При необходимости, страницу авторизации можно обновить или загрузить в отдельной
    сессии
  + В качестве логина может использоваться email или имя пользователя
    + Используются логин/пароль зарегистрированные в системе GitLab
  + При необходимости можно использовать альтернативные способы
    авторизации, как показано в нижней части экрана потдверждения смены email

# Быстрый старт
## Просмотр отдельного обсуждения
Используя указатель мыши, выбрать значок с меткой `Обсуждения`:
![Список проектов](./img/screenshot_20220825_193032.png)
Далее, экран может иметь следующий вид:
![Просмотр отдельного обсуждения](./img/screenshot_20220825_193742.png)
## Список тем для обсуждений
![Темы для обсуждений (список)](./img/screenshot_20220825_193100.png)
# Дополнительно
## Терминология
## Настройки
![Экран настроек](./img/screenshot_20220825_162846.png)
## Быть в курсе событий
