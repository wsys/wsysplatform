GitLab: базовая настройка окружения клиентской среды
====================================================

Содержание
==========
[[__TOC__]]

## Git: подготовка
```
# установка пакета git если не установлено
apt-get install git
```

Возможный вывод команды:
```bash
vmuser@udesk:~$ sudo apt-get install git
[sudo] password for vmuser:
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  git-man liberror-perl
Suggested packages:
  git-daemon-run | git-daemon-sysvinit git-doc git-el git-email git-gui gitk gitweb git-arch git-cvs git-mediawiki git-svn
The following NEW packages will be installed:
  git git-man liberror-perl
0 upgraded, 3 newly installed, 0 to remove and 50 not upgraded.
Need to get 3932 kB of archives.
After this operation, 25.6 MB of additional disk space will be used.
Do you want to continue? [Y/n]
```
Для продолжения установки ввести `Y`

### Локальные настройки GIT'а

Единовременная настройка по умолчанию вашего локального гита. Настройки
можно сменить или задать индивидуально для проектов.

Настройки принадлежности ваших коммитов:
```bash
# При необходимости ключ `--global` можно опустить.
git config --global user.name "Your Name Surname"
git config --global user.email "your@e.mail"
```

Включить вывод в цвете:
```bash
git config --global color.ui true
```

Для просмотра полного списка настроек, поддерживаемых вашей версией Git,
используйте команду:
```bash
man git-config
```


## Настроить SSH-ключ

```bash
# задать права каталога для SSH-ключей
mkdir -p ~/.ssh
chmod 700 ~/.ssh
```

Далее:
```bash
# вместо "you@computer-name" можно указать в кавычках e-mail или иной идентификатор
ssh-keygen -t rsa -b 4096 -C "you@computer-name"
```

```bash
# Вам будет предложено ввести следующую информацию. Нажмите Enter, чтобы принять значения по умолчанию. Значения по умолчанию указаны в скобках.
# Не указывайте passphrase, чтобы не вводить каждый раз.
Generating public/private rsa key pair.
Enter file in which to save the key (/home/vmuser/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/vmuser/.ssh/id_rsa.
Your public key has been saved in /home/vmuser/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:3xXTAYW0zRyP4EmDZruRxL2Tv4v+ynURe3QRgAT4cbA you@computer-name
The key's randomart image is:
+---[RSA 4096]----+
|        .+++*+=*.|
|       . .B=.==o=|
|        .Eooo++==|
|         .+ +  ++|
|        S  o o.o.|
|         ... .. o|
|          . . ...|
|           . o.. |
|           .=oo. |
+----[SHA256]-----+
```

## Добавление ключа в профиль GitLab
Скопируйте свой открытый ключ и добавьте его в свой профиль GitLab.
```bash
cat ~/.ssh/id_rsa.pub
```

```bash
ssh-rsa AAAAB3NzaC1yc2EAAAADAQEL17Ufacg8cDhlQMS5NhV8z3GHZdhCrZbl4gz you@example.com
```
или воспользуйтесь утилитой копирования в буфер обмена `xclip`:
```
cat ~/.ssh/id_rsa.pub | xclip
```

## create project
## clone project
## make changes
## commit changes
## push changes
## additional information
