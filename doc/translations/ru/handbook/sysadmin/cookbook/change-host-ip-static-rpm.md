# Смена статического ip-адреса хоста для rpm-based linux-дистрибутивов
[[_TOC_]]

### Пошаговая инструкция
#### Шаг 0: Подготовка

Это не обязательный шаг, но рекомендуемый в случае хоста `olora`
```
sudo /etc/init.d/oracle stop
```
- это может занять некоторое время (более 5-и минут): дождитесь завершения
- после приведенных ниже шагов изменения сетевой конфигурации, запускаем вновь: `sudo /etc/init.d/oracle start`
  - если сервер перезагружался, то старт вышеуказанного сервиса
    выполняется автоматически
#### Шаг 1: Предварительные шаги

- [Общие инструкции](./net-common.md)
  - Сведения о конфигурации сети: хосты, адреса, порты
  - Установка ssh-соединения и/или авторизация
  - Получение сведений о сетевых интерфейсах системы

#### Шаг 2: Сведения о текущих сетевых настройках
```
cat /etc/sysconfig/network-scripts/ifcfg-ens33
```
Например:
```
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="none"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="ens33"
UUID="dd859d10-a5d1-4d2a-bb5c-c83bb796c665"
DEVICE="ens33"
ONBOOT="yes"

IPADDR=172.16.80.51
NETMASK=255.255.255.0
GATEWAY=172.16.80.254
DNS1=172.16.80.254
USERCTL=no

```
#### Шаг 3: Изменение ip-адреса и сопутствующей информации
```
sudo nano -w /etc/sysconfig/network-scripts/ifcfg-ens33
```

Далее, отредактируйте строчку, например `IPADDR=172.16.80.51` в
соответствии с требуемым ip-адресом

Для выхода с сохранением, вводим комбинацию `Ctrl` + `x`

Внизу, редактора, появится характерное сообщение:
```
Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?
 Y Yes
 N No           ^C Cancel

```
Вводим `y` и нажимаем клавишу `<Enter>` для сохранения

#### Щаг 4: Рестарт сети

Внимание: после рестарта сети существует риск недоступности управления хостом
по `ssh` - будьте внимательны в настройках.

Для систем, на базе `systemd` выполните:
```
sudo service networking reload
```

- не все сервисы поддерживают `reload`

Примечание: если требуется `stop + start` т.е. более полный рестарт
сети, то используйте `sudo service networking restart`


Для полной перезагрузки сервера:
```
sudo reboot
```

### Полезные ссылки

- Файлы сетевой конфигурации
  - https://oracle-base.com/articles/linux/linux-network-configuration#networking_files
