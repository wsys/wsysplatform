[Индекс](../guide.md)

# Ситуационный справочник системного администриатора
[[_TOC_]]

## Что такое `deb-based` система ?
К данным системам относятся как правило все системы использующие в
качестве файлов пакетной системы расширение `deb`
Очевидно, это пакеты широко распрострaнённого linux-дистрибутива
[Debian](debian.org)

## Что такое `rpm-based` система ?
К данным системам относятся как правило все системы использующие в
качестве файлов пакетной системы расширение `rpm`
Как правило, это пакеты хорошо известного linux-дистрибутива
[Red Hat](redhat.com)

- [Red Hat Linux](redhat.com)
- [Oracle Linux](https://www.oracle.com/ru/linux/)

## Как узнать версию операционной системы linux-хоста

### Для `deb-based` систем
С помощью команды:
```
lsb_release -a
```

Результат может быть, например таким:
```
nodemaster@w01:~$ lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 16.04.6 LTS
Release:        16.04
Codename:       xenial
```
В большинстве случаев данная команда подходит и для других систем.

## Смена статического ip-адреса хоста `Wsystem`
### Для `deb-based` систем
- [Смена статического ip-адреса хоста для deb-based linux-дистрибутивов](./change-host-ip-static-deb.md)
  - w01
  - udesk
### Для `rpm-based` систем
- [Смена статического ip-адреса хоста для rpm-based linux-дистрибутивов](./change-host-ip-static-rpm.md)
  - olora
## Полезные ссылки

- [DistroWatch](distrowatch.com)

[Индекс](../guide.md)
