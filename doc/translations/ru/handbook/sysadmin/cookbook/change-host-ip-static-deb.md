# Смена статического ip-адреса хоста для deb-based linux-дистрибутивов
[[_TOC_]]

### Пошаговая инструкция
#### Шаг 1: Предварительные шаги
- [Общие инструкции](./net-common.md)
  - Сведения о конфигурации сети: хосты, адреса, порты
  - Установка ssh-соединения и/или авторизация
  - Получение сведений о сетевых интерфейсах системы

#### Шаг 2: Сведения о текущих сетевых настройках
```
cat /etc/network/interfaces
```
Например:
```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

#
# The primary network interface
# initial config
#
#auto ens33
#iface ens33 inet dhcp
# This is an autoconfigured IPv6 interface
#iface ens33 inet6 auto

#
# alternative config
#
auto ens160
iface ens160 inet static
address 172.16.80.53
netmask 255.255.255.0
gateway 172.16.80.254
dns-nameservers 172.16.80.254
# This is an autoconfigured IPv6 interface
iface ens160 inet6 auto

```
#### Шаг 3: Изменение ip-адреса и сопутствующей информации
```
sudo nano -w /etc/network/interfaces
```

Далее, отредактируйте строчку, например `address 172.16.80.53` на необходимый ip-адрес

Для выхода с сохранением, вводим комбинацию `Ctrl` + `x`

Внизу, редактора, появится характерное сообщение:
```
Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?
 Y Yes
 N No           ^C Cancel

```
Вводим `y` и нажимаем клавишу `<Enter>` для сохранения

#### Щаг 4: Рестарт сети

Внимание: после рестарта сети существует риск недоступности управления хостом
по `ssh` - будьте внимательны в настройках.

Для систем, на базе `systemd` выполните:
```
sudo service networking reload
```

- не все сервисы поддерживают `reload`

Примечание: если требуется `stop + start` т.е. более полный рестарт
сети, то используйте `sudo service networking restart`


Для полной перезагрузки сервера:
```
sudo reboot
```

### Полезные ссылки

- Ubuntu related
  - https://askubuntu.com/
- Прочее
  - https://oracle-base.com/
