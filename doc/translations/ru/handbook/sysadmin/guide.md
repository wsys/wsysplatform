[Начало](../README.md)

# Руководство системного администратора
# Содержание
[[_TOC_]]
# Введение
Предполагается, что у Вас настроен SSH-доступ. Большинство специфичных
команд выполняется в терминале. В качестве эталонной (наиболее типичной и
распространённой) конфигурации рассматривается `Ubuntu Server 16.04.6
LTS`

Наиболее значимые файлы `Wsystem` при стандартной конфигурации:

| Маршрут  | Назначение  | Примечание  |
|---|---|---|
| `${WSPLAPP_HOME}` |домашний каталог сервера приложений   |обычно это `${HOME}/wsplapp_home` *|
| `${WSPLAPP_HOME}/priv` | конфигурационные файлы  |   |
| `${WSPLAPP_HOME}/var/proc/master/pub/workers/` | каталог файловых сокетов |   |
| `${WSPLAPP_HOME}/var/sess/db/` | каталог сессионных файлов |   |
| `${HOME}/work/site-wsplatform` | www-каталог Wsystem |   |

Примечание:
* переменная окружения `$WSPLAPP_HOME` должна быть определена и
  указывает на каталог по умолчанию

Большинство команд выполняется с помощью штатной системы инициализации,
поддерживающей сервис-ориентированный интерфейс. Wsystem использует
следующие сервисы:
- nginx
- php5.6-fpm
Например, для получения справки по конкретному сервису, можно выполнить:
```
service php5.6-fpm help
# или
service nginx help
```
Результат при выполнении:
```
nodemaster@w01:~$ service php5.6-fpm help
Usage: /etc/init.d/php-fpm5.6 {start|stop|status|restart|reload|force-reload}
nodemaster@w01:~$ service nginx help
Usage: nginx {start|stop|restart|reload|force-reload|status|configtest|rotate|upgrade}
```


# Системные конфигурационные файлы

На примере конфигурации Ubuntu Server 16.04.6 LTS :

| Файл  | Назначение  | Примечание  |
|---|---|---|
| /etc/php/5.6/fpm/pool.d/wspl.conf | конфигурация пула php-fpm |   |
| /etc/php/5.6/fpm/php-fpm.conf | конфигурация php-fpm |   |
| /usr/local/php/conf.d/99-local.ini | локальная конфигурация расширений для php |   |
| /usr/local/nginx/sites-enabled/site-wsplatform.conf | локальная конфигурация nginx |   |
| /etc/nginx/nginx.conf | конфигурация nginx |   |
| /etc/nginx/sites-available/default | конфигурация сайтов nginx |   |

Файлы `php.ini` :

```
locate php.ini
```

Например вывод может быть таким:

```
/etc/php/5.6/cli/php.ini
/etc/php/5.6/fpm/php.ini
/home/nodemaster/php.ini
/usr/lib/php/5.6/php.ini-development
/usr/lib/php/5.6/php.ini-production
/usr/lib/php/5.6/php.ini-production.cli
```

# Конфигурационные файлы
# Консоль управления `Wsystem`
Все команды управления `Wsystem` низкого уровня, выполняется в
терминальном режиме под пользователем nodemaster
## Старт системы
```
wadm_start
```
## Останов системы
```
wadm_stop
```
# Регламентные работы
## Очистка журналов
```
truncate -s 0 ~/*.out
sudo truncate -s 0 /tmp/*.{log,out}
```
## Очистка хранилища
Рекомендуемая периодичность:
- низкая загруженность: один раз в месяц
- небольшая загруженность: не менее 1 раза в 2 недели
- средняя загруженность: каждые две недели
- высокая загруженность: каждую неделю
```
sudo service nginx stop
wadm_stop
sudo rm -rf ${WSPLAPP_HOME}/var/sess/db/
wadm_start
sudo service nginx start
```
Примечание:
- после данной процедуры все текущие сессии будут сброшены.
- увеличение интервалов пропорционально времени очистки
## Сброс кэша
```
sudo rm /tmp/ndl.w.cache/*
```
## Сброс public-кэша метаданных
```
rm ${WSPLAPP_HOME}/var/proc/master/pub/cache/db.sqlite
```
# Мониторинг системы
Листинг активных воркеров в системе:
```
ls ${WSPLAPP_HOME}/var/proc/master/pub/workers/
```

Например:
```
nodemaster@w01:~$ ls -lah /home/nodemaster/wsplapp_home/var/proc/master/pub/workers/
total 8.0K
drwxrwxr-x 2 nodemaster nodemaster 4.0K Jun 17 01:22 .
drwxrwxr-x 4 nodemaster nodemaster 4.0K Jun 16  2018 ..
srw-rw---x 1 nodemaster wsplgrp       0 Jun 15 20:21 w.0.117.sock
srw-rw---x 1 nodemaster wsplgrp       0 Jun 15 17:03 w.1.3.sock
srw-rw---x 1 nodemaster wsplgrp       0 Jun 16 08:26 w.1.4.sock
srw-rw---x 1 nodemaster wsplgrp       0 Jun 16 14:42 w.1.5.sock
```

Примечание:
- префикс w.0 означает public воркер
- префикс w.1 означает пользовательский воркер
## Оценка количества воркеров
```
ls ${WSPLAPP_HOME}/var/proc/master/pub/workers|wc -l
```
# Устранение неисправностей
## Файловые сокеты
Как правило, супервизор Wsystem отслеживает подвисшие и неактивные
сокеты воркеров и при необходимости удаляет их. Однако, возможны нештатные
ситуации когда сокет становится невалидным и не принадлежит ни одному из
активных воркеров. Несмотря на то, что система сборки мусора регулярно
зачищает такие объекты, все же возможность их появления существует. В
этом случае, необходимо удалить такие сокеты вручную.

Самый простой способ определить зависший сокет - временно остановить
сервер приложений Wsystem:
```
sudo service nginx stop
wadm_stop
sudo rm ${WSPLAPP_HOME}/var/sys/init/gsev.sock
sudo rm ${WSPLAPP_HOME}/var/proc/master/pub/workers/*.sock
wadm_start
sudo service nginx start
```
## Аварийный рестарт
### Стандартный уровень
```
sudo service php5.6-fpm restart && sudo service nginx restart
```
Примечание:
- для облегченного рестарта допустимо использовать вместо `restart`
  `reload` или `force-reload`
### Полный рестарт
```
sudo service nginx stop ; sudo service php5.6-fpm stop ; wadm_stop
sudo rm ${WSPLAPP_HOME}/var/sys/init/gsev.sock
sudo rm ${WSPLAPP_HOME}/var/proc/master/pub/workers/*.sock
wadm_start && sudo service php5.6-fpm start && sudo service nginx start
```

## Холодный рестарт
Данный рестарт предполагает полный рестарт системы с перезагрузкой. Это
также подойдёт и просто для холодного старта системы. Хотя, в зависимости от
Вашей конфигурации старт Wsystem может выполняться автоматически, иногда
может потребоваться ручное управление.
```
sudo service nginx stop ; sudo service php5.6-fpm stop ; wadm_stop
sudo rm ${WSPLAPP_HOME}/var/sys/init/gsev.sock
sudo rm ${WSPLAPP_HOME}/var/proc/master/pub/workers/*.sock
sudo reboot
wadm_stop
wadm_start
sudo service php5.6-fpm restart && sudo service nginx restart
```


# Рекомендуемые номинальные характеристики оборудования

| Наименование |Значение  |  |
|-|-|-|
| RAM  | 8GB+ |  |
| CPU | 4+ |  |
| CPU freq| 2000MHz+ |  |
| HDD | 50GB+ |  |

# Тематический указатель документов
- [Ситуационный справочник системного администратора](./cookbook/README.md)


[Начало](../README.md)
